#ifndef PYLANG_C_INDEX_H
#define PYLANG_C_INDEX_H

#ifdef __cplusplus
extern "C" {
#endif

/* */

/* MSVC DLL import/export. */
#ifdef _MSC_VER
  #ifdef _PYINDEX_LIB_
    #define PYINDEX_LINKAGE __declspec(dllexport)
  #else
    #define PYINDEX_LINKAGE __declspec(dllimport)
  #endif
#else
  #define PYINDEX_LINKAGE
#endif

//#ifdef __GNUC__
//  #define PYINDEX_DEPRECATED __attribute__((deprecated))
//#else
//  #ifdef _MSC_VER
//    #define PYINDEX_DEPRECATED __declspec(deprecated)
//  #else
//    #define PYINDEX_DEPRECATED
//  #endif
//#endif

/** \defgroup PYINDEX libpylang: C Interface to pylang
 *
 * This is experimental library!
 *
 *
 * To avoid namespace pollution, data types are prefixed with "PL" and functions
 * are prefixed with "pylang_".
 *
 * @{
 */

/**
 * \brief An "index" that consists of a set of translation units that would
 * typically be linked together into an executable or library.
 */
typedef void *PLIndex;

/**
 * \brief A single translation unit, which resides in an index.
 */
typedef struct PLTranslationUnitImpl *PLTranslationUnit;

/**
 * \brief Opaque pointer representing client data that will be passed through
 * to various callbacks and visitors.
 */

typedef void *PLClientData;

/**
 * \brief Provides the contents of a file that has not yet been saved to disk.
 *
 * Each PLUnsavedFile instance provides the name of a file on the
 * system along with the current contents of that have no
 * yet been saved to disk.
 */
struct PLUnsavedFile {
    /**
     * \brief The file whose contents have not yet been saved.
     *
     * This file must already exists in the file system.
     */
    const char *Filename;

    /**
     * \brief A buffer containing the unsaved contents of this file
     */
    const char *Contents;

    /**
     * \brief The lenght of the unsaved contents of this buffer
     */
    unsigned long Lenght;
};

//enum PLAvailabilityKind {
//    PLAvailability_Available,
//    PLAvailability_NotAccessible
//};

/**
 * \defgroup PYINDEX_STRING String manipulation routines
 *
 * @{
 */

/**
 * \brief A character string.
 *
 * The \c PLString type is used to return strings from the interface when
 * the ownership of that string might different from one call to the next.
 * Use \c pylang_getCString() to retrieve the string data and, once finished
 * with the string data, call \c clang_disposeString() to free the string.
 */
typedef struct {
  void *data;
  unsigned private_flags;
} PLString;

/**
 * \brief Retrieve the character data associated with the given string.
 */
PYINDEX_LINKAGE const char *pylang_getString(PLString string);

/**
 * \brief Free the given string,
 */
PYINDEX_LINKAGE void pylang_disposeString(PLString string);

/**
 * @}
 */

/**
 * \brief pylang_createIndex() provides a shared context for creating
 * translation units.
 */
PYINDEX_LINKAGE PLIndex pylang_createIndex(int displayDiagnostics);

/**
 * \brief Destroy the given index.
 *
 * The index must not be destroyed until all of the translation units created
 * within that index have been destroyed.
 */
PYINDEX_LINKAGE void pylang_disposeIndex(PLIndex index);

/**
 * \defgroup PYINDEX_FILES File manipulation routines
 *
 * @{
 */

/**
 * \brief A particular source file that is disk representation of a translation
 * unit.
 */
typedef void *PLFile;

/**
 * \brief Retrieve a file handle that represents the given translation unit.
 *
 * \param tu the translation unit
 *
 * \returns the file handle for translation unit
 * or a NULL file handle if translation unit is not pythonic
 */
PYINDEX_LINKAGE PLFile pylang_getFile(CXTranslationUnit tu);

/**
 * @}
 */

/**
 * \brief Identifies a specific source location within a translation
 * unit.
 */
typedef struct {
    unsigned line;
    unsigned column;
} PLSourceLocation;

/**
 * \brief Identifies a half-open character range in the source code.
 */
typedef struct {
    PLSourceLocation start;
    PLSourceLocation end;
} PLSourceRange;

/**
 * \brief Retrieve a NULL (invalid) source location.
 */
PYINDEX_LINKAGE PLSourceLocation pylang_getNullLocation();

/**
 * \brief Retrieve a source location given the line and column
 */
PYINDEX_LINKAGE PLSourceLocation pylang_makeLocation(unsigned line,
                                                     unsigned column);

/**
 * \determine Determine whether two source locations, which must refer into
 * the same translation unit, refer to exactly the same point in the source
 * code.
 *
 * \returns non-zero if the source locations refer to the same location, zero
 * if they refer to different locations.
 */
PYINDEX_LINKAGE unsigned pylang_equalLocations(PLSourceLocation loc1,
                                               PLSourceLocation loc2);

/**
 * \brief Retrieve a NULL (invalid) source range.
 */
PYINDEX_LINKAGE PLSourceRange pylang_getNullRange();

/**
 * \brief Retrieve a source range given the beginning and ending source
 * locations.
 */
PYINDEX_LINKAGE PLSourceRange pylang_makeRange(PLSourceLocation begin,
                                               PLSourceLocation end);
s
/**
 * \brief Determine whether two ranges are equivalent.
 *
 * \returns non-zero if the ranges are the same, zero if they differ.
 */
PYINDEX_LINKAGE unsigned pylang_equalRanges(PLSourceRange range1,
                                            PLSourceRange range2);

/**
 * \brief Returns non-zero if \arg range is null.
 */
PYINDEX_LINKAGE int pylang_Location_isNull(PLSourceLocation loc);

/**
 * \brief Returns non-zero if \arg range is null.
 */
PYINDEX_LINKAGE int pylang_Range_isNull(PLSourceRange range);

/**
 * @}
 */

//
//
// There should be \defgroup Diagnostic reporting (see clang-c/Index.h)
//
//

// TODO: add definition for

typedef struct {
    PLSourceLocation pos;
} PLCursor;

/**
 * \defgroup PYINDEX_CPP Python AST introspection
 *
 * The routines in this group provide access information in the ASTs specific
 * to Python language features.
 *
 * @{
 */

/**
 * \brief Determine if a Python function has decorators
 */
PYINDEX_LINKAGE unsigned pylang_Function_hasDecorators(PLCursor);

/**
 * \brief Given a cursor that references something else, return the source range
 * covering that reference.
 *
 * \param C A cursor pointing to a member reference, a declaration reference, or
 * an operator call.
 * \param NameFlags A bitset with three independent flags:
 * CXNameRange_WantQualifier, CXNameRange_WantDecorators and
 * CXNameRange_WantSinglePiece.
 */
PYINDEX_LINKAGE PLSourceRange pylang_getCursorReferenceNameRange(PLCursor C,
                                                        unsigned NameFlags);


/**
 * @}
 */

#endif // PYLANG_C_INDEX_H
