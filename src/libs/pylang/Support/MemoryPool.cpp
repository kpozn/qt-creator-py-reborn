#include "MemoryPool.h"
#include <cstring>
#include <cassert>
#include <cstdlib>
#include <cstddef>

namespace pylang {

// TODO: does pool need reseting and blocks reusing?

MemoryPool::MemoryPool() :
    _ptr(0),
    _end(0)
{
}

MemoryPool::~MemoryPool()
{
    for (unsigned i = 0; i < _blocks.size(); ++i)
        std::free(_blocks[i]);
    std::set<void *>::const_iterator it;
    for (it = _customBlocks.begin(); it != _customBlocks.end(); ++it)
    {
        std::free(*it);
    }
}

void *MemoryPool::allocate_helper(size_t size)
{
    assert(size < BLOCK_SIZE);

    char *block =  reinterpret_cast<char *>(std::malloc(BLOCK_SIZE));
    _blocks.push_back(block);

    _ptr = block;
    _end = _ptr + BLOCK_SIZE;

    void *addr = _ptr;
    _ptr += size;
    return addr;
}

void *MemoryPool::allocate_custom_block(size_t blockSize)
{
    void *address = std::malloc(blockSize);
    _customBlocks.insert(address);

    return address;
}

void *MemoryPool::reallocate_custom_block(void *address, size_t blockSize)
{
    _customBlocks.erase(address);
    address = std::realloc(address, blockSize);
    _customBlocks.insert(address);

    return address;
}

void MemoryPool::free_custom_block(void *address)
{
    if (_customBlocks.count(address) > 0)
    {
        _customBlocks.erase(address);
        std::free(address);
    }
}

} // namespace pylang
