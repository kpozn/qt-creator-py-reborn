/**
 * Copyright (c) 2008 Roberto Raggi <roberto.raggi@gmail.com>
 * Modified by Sergey Shambir <sergey.shambir.auto@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PYTHON_MEMORYPOOL_H
#define PYTHON_MEMORYPOOL_H

#include <assert.h>
#include "../pylang_global.h"
#include <new>
#include <string>
#include <vector>
#include <set>

namespace pylang {

class PYLANG_EXPORT MemoryPool
{
    MemoryPool(const MemoryPool &other);
    void operator =(const MemoryPool &other);

public:
    MemoryPool();
    ~MemoryPool();

    inline void *allocate(size_t size);

    void *allocate_custom_block(size_t blockSize);
    void *reallocate_custom_block(void *address, size_t blockSize);
    void free_custom_block(void *address);

private:
    void *allocate_helper(size_t size);

    std::vector<void *> _blocks;
    std::set<void *> _customBlocks;
    char *_ptr;
    char *_end;

    static const unsigned BLOCK_SIZE = 8 * 1024;
    static const unsigned DEFAULT_BLOCK_COUNT = 8;
};

class PYLANG_EXPORT Managed
{
    Managed(const Managed &other);
    void operator = (const Managed &other);

public:
    Managed() {}
    virtual ~Managed() {}

    template <class TPoolProvider>
    void *operator new(size_t size, TPoolProvider &provider)
    {
        return provider.pool().allocate(size);
    }

    void *operator new(size_t size, MemoryPool &pool)
    {
        return pool.allocate(size);
    }

    void operator delete(void *) { }

    void operator delete(void *, MemoryPool &) { }
};

void *MemoryPool::allocate(size_t size)
{
    size = (size + 7) & ~7; // round up to the next multiple of 8
    if (_ptr && (_ptr + size < _end)) {
        void *addr = _ptr;
        _ptr += size;
        return addr;
    }
    return allocate_helper(size);
}

namespace Internal {

template <class TManaged>
class ManagedClassRef
{
public:
    ManagedClassRef() : _pointer(0) { }
    ManagedClassRef(TManaged &object) : _pointer(&object) {
        assert(0 == dynamic_cast<pylang::Managed*>(_pointer));

    }

    inline void set(TManaged &object) {
        _pointer = &object;
        assert(0 == dynamic_cast<pylang::Managed*>(_pointer));
    }

    inline bool isNull() const {
        return (0 == _pointer);
    }

    inline const TManaged &operator *() const {
        assert(_pointer);
        return *_pointer;
    }

    inline TManaged &operator *() {
        assert(_pointer);
        return *_pointer;
    }

    inline const TManaged *operator ->() const
    {
        assert(_pointer);
        return _pointer;
    }

    inline TManaged *operator ->()
    {
        assert(_pointer);
        return _pointer;
    }

private:
    TManaged *_pointer;
};

}

} // namespace pylang

#endif // PYTHON_MEMORYPOOL_H
