#ifndef PYTHON_SOURCELOCATION_H
#define PYTHON_SOURCELOCATION_H

#include <stdint.h>

namespace pylang {

class SourceLocation
{
public:
    explicit SourceLocation(uint32_t line = (uint32_t)-1,
                            uint32_t column = (uint32_t)-1)
        : m_line(line), m_column(column)
    { }

    inline uint32_t line() const { return m_line; }
    inline uint32_t column() const { return m_column; }

    inline void setLine(uint32_t value) { m_line = value; }
    inline void setColumn(uint32_t value) { m_column = value; }

    inline bool isInvalid() const
    { return (m_line == (uint32_t)-1) && (m_column == (uint32_t)-1); }

    inline bool isValid() const
    { return (m_line != (uint32_t)-1) || (m_column != (uint32_t)-1); }

    inline bool operator <(const SourceLocation &other) const {
        return (other.m_line > m_line)
                || (other.m_line == m_line && other.m_column > m_column);
    }

    inline bool operator >(const SourceLocation &other) const {
        return (other.m_line < m_line)
                || (other.m_line == m_line && other.m_column < m_column);
    }

    inline bool operator <=(const SourceLocation &other) const {
        return (other.m_line > m_line)
                || (other.m_line == m_line && other.m_column >= m_column);
    }

    inline bool operator >=(const SourceLocation &other) const {
        return (other.m_line < m_line)
                || (other.m_line == m_line && other.m_column <= m_column);
    }

    inline bool operator ==(const SourceLocation &other) const {
        return other.m_line == m_line && other.m_column == m_column;
    }

    inline bool operator !=(const SourceLocation &other) const {
        return other.m_line != m_line || other.m_column != m_column;
    }

    SourceLocation locationAtRight(unsigned colonAddenum) {
        SourceLocation temp(*this);
        temp.m_column += colonAddenum;
        return temp;
    }

private:
    uint32_t m_line;
    uint32_t m_column;
};

} // namespace pylang

#endif // PYTHON_SOURCELOCATION_H
