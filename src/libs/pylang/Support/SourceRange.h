#ifndef PYLANG_SOURCERANGE_H
#define PYLANG_SOURCERANGE_H

#include "SourceLocation.h"

namespace pylang {

class SourceRange
{
private:
    SourceLocation m_firstLoc;
    SourceLocation m_lastLoc;

public:
    explicit SourceRange() {}

    explicit SourceRange(SourceLocation const& first,
                         SourceLocation const& last)
        : m_firstLoc(first)
        , m_lastLoc(last)
    { }

    inline const SourceLocation &first() const { return m_firstLoc; }
    inline const SourceLocation &last() const { return m_lastLoc; }

    void setFirst(SourceLocation const& first) { m_firstLoc = first; }
    void setLast(SourceLocation const& last) { m_lastLoc = last; }
};

} // namespace pylang

#endif // PYLANG_SOURCERANGE_H
