#include "StringRef.h"

#include <cstring>

#ifdef NEED_RUNTIME_CHECKS
#include <stdexcept>
#endif

namespace {

const size_t UNSIGNED_SIZE = sizeof(unsigned) / sizeof(char);
const size_t POINTER_SIZE = sizeof(pylang::StringRef *) / sizeof(char);

const int NEXT_OFFSET = 2 * UNSIGNED_SIZE + POINTER_SIZE;
const int SIZE_OFFSET = 2 * UNSIGNED_SIZE;
const int HASHCODE_OFFSET = UNSIGNED_SIZE;

} // end of anonymous namespace

namespace pylang {

/**
 * \brief The Literal class - a wrapper around pointer to string in MemoryPool.
 * If isNull() returns true, than other methods may cause segfault when compiled
 * with QtCreator and std::runtime_error otherwise.
 * All methods have constant complexity.
 *
 * \internal keeps all data (size, hash and pointer *next) at one array
 * in memory, using positions with negative offset.
 *
 * Allocated memory looks like:
 *  [....][....][....][...........]
 *  *next  size  hash  characters
 *                   /\ _chars points to this position
 */

StringRef::StringRef()
    : _chars(0)
{
}

StringRef StringRef::create(const char *chars, unsigned size, MemoryPool &pool)
{
    register size_t memsize = (size + 1 + NEXT_OFFSET) * sizeof(char);
    char *data = reinterpret_cast<char*>(pool.allocate(memsize));

    StringRef **next = reinterpret_cast<StringRef **>(data);
    next[0] = 0;

    unsigned *uintPtr = reinterpret_cast<unsigned*>(data + POINTER_SIZE);
    uintPtr[0] = size;
    uintPtr[1] = hashCode(chars, size);

    data += NEXT_OFFSET;
    std::strncpy(data, chars, size);
    data[size] = '\0';

    StringRef ret;
    ret._chars = data;
    return ret;
}

StringRef StringRef::create(PODHandle *handle)
{
    StringRef ret;
    ret._chars = reinterpret_cast<char *>(handle);
    return ret;
}

StringRef::PODHandle *StringRef::getPODHandle()
{
    return reinterpret_cast<PODHandle *>(_chars);
}

/**
 * @brief Internal function to check if string ref somewhere is null,
 * in client code you can guess that it's always returns false
 * @return true if buggy parser produced null string reference,
 * or if you are using own uninitialized StringRef
 */
bool StringRef::isNull() const
{
    return (_chars == 0);
}

StringRef::iterator StringRef::begin() const
{
#ifdef NEED_RUNTIME_CHECKS
    if (_chars == nullptr)
        throw std::runtime_error("Literal not initialized");
#endif
    return _chars;
}

StringRef::iterator StringRef::end() const
{
#ifdef NEED_RUNTIME_CHECKS
    if (_chars == nullptr)
        throw std::runtime_error("Literal not initialized");
#endif
    return _chars + size();
}

char StringRef::at(unsigned index) const
{
#ifdef NEED_RUNTIME_CHECKS
    if (_chars == nullptr)
        throw std::runtime_error("Literal not initialized");
    if (index > size()) // >, not >=
        throw std::runtime_error("Literal::at method: out of bounds");
#endif
    return _chars[index];
}

const char *StringRef::chars() const
{
#ifdef NEED_RUNTIME_CHECKS
    if (_chars == nullptr)
        throw std::runtime_error("Literal not initialized");
#endif
    return _chars;
}

unsigned StringRef::size() const
{
#ifdef NEED_RUNTIME_CHECKS
    if (_chars == nullptr)
        throw std::runtime_error("Literal not initialized");
#endif
    register unsigned *ptr = reinterpret_cast<unsigned *>(_chars - SIZE_OFFSET);
    return *ptr;
}

unsigned StringRef::hashCode() const
{
#ifdef NEED_RUNTIME_CHECKS
    if (_chars == nullptr)
        throw std::runtime_error("Literal not initialized");
#endif
    register unsigned *ptr = reinterpret_cast<unsigned *>(_chars - HASHCODE_OFFSET);
    return *ptr;
}

unsigned StringRef::hashCode(const char *chars, unsigned size)
{
    // comment below taken from CPlusPlus::Literal written by Roberto Raggi
    /* Hash taken from QtCore's qHash for strings, which in turn has the note:

    These functions are based on Peter J. Weinberger's hash function
    (from the Dragon Book). The constant 24 in the original function
    was replaced with 23 to produce fewer collisions on input such as
    "a", "aa", "aaa", "aaaa", ...
    */

    unsigned h = 0;

    while (size--) {
        h = (h << 4) + *chars++;
        h ^= (h & 0xf0000000) >> 23;
        h &= 0x0fffffff;
    }
    return h;
}

bool StringRef::equalTo(const StringRef &other) const
{
#ifdef NEED_RUNTIME_CHECKS
    if (_chars == nullptr)
        throw std::runtime_error("Literal not initialized");
#endif
    if (_chars == other._chars)
        return true;
    if (hashCode() != other.hashCode())
        return false;
    if (size() != other.size())
        return false;
    return ! std::strcmp(_chars, other._chars);
}

StringRef *StringRef::next() const
{
#ifdef NEED_RUNTIME_CHECKS
    if (_chars == nullptr)
        throw std::runtime_error("Literal not initialized");
#endif
    register StringRef **ptr = reinterpret_cast<StringRef **>(_chars - NEXT_OFFSET);
    return *ptr;
}

void StringRef::setNext(StringRef *next)
{
#ifdef NEED_RUNTIME_CHECKS
    if (_chars == nullptr)
        throw std::runtime_error("Literal not initialized");
#endif
    register StringRef **ptr = reinterpret_cast<StringRef **>(_chars - NEXT_OFFSET);
    *ptr = next;
}

} // namespace pylang
