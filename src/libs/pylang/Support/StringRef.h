/**
 * Copyright (c) 2008 Roberto Raggi <roberto.raggi@gmail.com>
 * Modified by Sergey Shambir <sergey.shambir.auto@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PYTHON_LITERALS_H
#define PYTHON_LITERALS_H

#include "../pylang_global.h"
#include "MemoryPool.h"

namespace pylang {

class PYLANG_EXPORT StringRef
{
public:
    typedef const char *iterator;
    typedef iterator const_iterator;

    class PODHandle;

public:
    StringRef();

    static StringRef create(const char *chars, unsigned size, MemoryPool &pool);

    bool isNull() const;

    iterator begin() const;
    iterator end() const;

    char at(unsigned index) const;
    const char *chars() const;
    unsigned size() const;

    unsigned hashCode() const;
    static unsigned hashCode(const char *chars, unsigned size);

    bool equalTo(const StringRef &other) const;

    // TODO: remove this methods, use PODHandle instead
    StringRef *next() const;
    void setNext(StringRef *next);

    static StringRef create(PODHandle *handle);
    PODHandle *getPODHandle();

private:
    char *_chars;
};

} // namespace pylang

#endif // PYTHON_LITERALS_H
