#include "StringsHash.h"

namespace pylang {

StringsHash::StringsHash(MemoryPool &pool)
    : _pool(pool)
    , _literals(0)
    , _buckets(0)
    , _allocatedLiterals(0)
    , _literalsCount(-1)
    , _allocatedBuckets(0)
{
}

StringsHash::~StringsHash()
{
    // managed class, deletion not necessary
    if (_literals) {
        _pool.free_custom_block(_literals);
    }
    if (_buckets)
        _pool.free_custom_block(_buckets);

    _literals = 0;
    _buckets = 0;
    _allocatedLiterals = 0;
    _literalsCount = -1;
    _allocatedBuckets = 0;
}

bool StringsHash::empty() const
{
    return _literalsCount == -1;
}

unsigned StringsHash::size() const
{
    return _literalsCount + 1;
}

StringRef StringsHash::at(unsigned index) const
{
    return *_literals[index];
}

StringsHash::iterator StringsHash::begin() const
{
    return _literals;
}

StringsHash::iterator StringsHash::end() const
{
    return _literals + _literalsCount + 1;
}

const StringRef *StringsHash::findLiteral(const char *chars, unsigned size) const
{
    if (_buckets)
    {
        unsigned h = StringRef::hashCode(chars, size);
        StringRef *literal = _buckets[h % _allocatedBuckets];
        for (; literal; literal = static_cast<StringRef *>(literal->next()))
        {
            if (literal->size() == size && ! std::strncmp(literal->chars(), chars, size))
                return literal;
        }
    }
    return 0;
}

StringRef StringsHash::findOrInsertLiteral(const char *chars, unsigned size)
{
    if (_buckets) {
        unsigned h = StringRef::hashCode(chars, size);
        StringRef *literal = _buckets[h % _allocatedBuckets];
        for (; literal; literal = static_cast<StringRef *>(literal->next())) {
            if (literal->size() == size && ! std::strncmp(literal->chars(), chars, size))
                return *literal;
        }
    }

    void *buffer = _pool.allocate(sizeof(StringRef));
    StringRef *literal = new (buffer) StringRef(StringRef::create(chars, size, _pool));

    if (++_literalsCount == _allocatedLiterals) {
        if (! _allocatedLiterals)
            _allocatedLiterals = 4;
        else
            _allocatedLiterals <<= 1;

        size_t memsize = sizeof(StringRef *) * _allocatedLiterals;
        void *p = _pool.reallocate_custom_block(_literals, memsize);
        _literals = reinterpret_cast<StringRef **>(p);
        for (int li = _literalsCount; li < _allocatedLiterals; ++li)
            _literals[li] = 0;
    }

    _literals[_literalsCount] = literal;

    if (! _buckets || _literalsCount * 5 >= _allocatedBuckets * 3)
        rehash();
    else {
        unsigned h = literal->hashCode() % _allocatedBuckets;
        literal->setNext(_buckets[h]);
        _buckets[h] = literal;
    }

    return *literal;
}

void StringsHash::rehash()
{
    if (_buckets)
        _pool.free_custom_block(_buckets);

    if (! _allocatedBuckets)
        _allocatedBuckets = 12;
    else
        _allocatedBuckets <<= 1;

    size_t memsize = _allocatedBuckets * sizeof(StringRef *);
    _buckets = (StringRef **) _pool.allocate_custom_block(memsize);
    memset(_buckets, 0, memsize);

    StringRef **lastLiteral = _literals + (_literalsCount + 1);

    for (StringRef **it = _literals; it != lastLiteral; ++it) {
        StringRef *literal = *it;
        unsigned h = literal->hashCode() % _allocatedBuckets;

        literal->setNext(0);
        _buckets[h] = literal;
    }
}

}
