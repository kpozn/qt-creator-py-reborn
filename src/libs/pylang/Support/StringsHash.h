/**
 * Copyright (c) 2008 Roberto Raggi <roberto.raggi@gmail.com>
 * Modified by Sergey Shambir <sergey.shambir.auto@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PYTHON_LITERALS_TABLE_H
#define PYTHON_LITERALS_TABLE_H

#include "../Support/StringRef.h"
#include <cstring>

namespace pylang {

class StringsHash : public Managed
{
    StringsHash(const StringsHash &other);
    void operator =(const StringsHash &other);

public: // types
    typedef StringRef *const *iterator;

public:
    StringsHash(MemoryPool &pool);
    ~StringsHash();

    bool empty() const;
    unsigned size() const;
    StringRef at(unsigned index) const;

    iterator begin() const;
    iterator end() const;

    const StringRef *findLiteral(const char *chars, unsigned size) const;
    StringRef findOrInsertLiteral(const char *chars, unsigned size);

protected:
    void rehash();

protected:
    MemoryPool &_pool;

    StringRef **_literals;
    StringRef **_buckets;
    int _allocatedLiterals;
    int _literalsCount;
    int _allocatedBuckets;
};

} // namespace pylang

#endif // PYTHON_LITERALS_TABLE_H
