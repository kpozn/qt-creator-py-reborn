#include "TokenizedUnit.h"

namespace pylang {

TokenizedUnit::TokenizedUnit()
    : m_lastTokenLineNo(0)
{
}

TokenizedUnit::~TokenizedUnit()
{
}

bool TokenizedUnit::tokenAt(const SourceLocation &cursor, Token &result) const
{
    if (cursor.line() >= m_lineTokenIndicies.size())
        return false;

    uint32_t tokenIdx = m_lineTokenIndicies[cursor.line()];
    for (; tokenIdx < m_tokens.size(); ++tokenIdx)
    {
        SourceRange const& range = m_tokens[tokenIdx].range();

        if (range.first().line() > cursor.line())
            return false;

        if ((range.last().column() >= cursor.column())
                || (range.last().line() > cursor.line()))
        {
            if (range.first().column() <= cursor.column())
            {
                result = m_tokens[tokenIdx];
                return true;
            }
            else
                return false;
        }
    }
    return false;
}

bool TokenizedUnit::tokenNearest(const SourceLocation &cursor, Token &result) const
{
    if (cursor.line() < m_lineTokenIndicies.size())
        return false;

    uint32_t tokenIdx = m_lineTokenIndicies[cursor.line()];
    for (; tokenIdx < m_tokens.size(); ++tokenIdx)
    {
        SourceRange const& range = m_tokens[tokenIdx].range();

        if (range.first().line() > cursor.line())
        {
            result = m_tokens[tokenIdx];
            return true;
        }

        if ((range.last().column() >= cursor.column())
                || (range.last().line() > cursor.line()))
        {
            if (range.first().column() <= cursor.column())
            {
                result = m_tokens[tokenIdx];
                return true;
            }
            else
                return false;
        }
    }
    return false;
}

void TokenizedUnit::pushToken(const Token &token)
{
    m_tokens.push_back(token);

    const unsigned tokenLineNo(token.range().first().line());
    if (m_lastTokenLineNo != tokenLineNo
            || m_lineTokenIndicies.empty())
    {
        const unsigned tokenIndex(m_tokens.size() - 1u);
        for (unsigned i = m_lastTokenLineNo + 1; i <= tokenLineNo; ++i)
        {
            m_lineTokenIndicies.push_back(tokenIndex);
        }
        m_lastTokenLineNo = tokenLineNo;
    }
}

std::vector<uint32_t> &TokenizedUnit::lineOffsets()
{
    return m_lineOffsets;
}

const Token &TokenizedUnit::tokenAt(uint32_t index) const
{
    return m_tokens[index];
}

uint32_t TokenizedUnit::tokensCount() const
{
    return m_tokens.size();
}

} // namespace pylang
