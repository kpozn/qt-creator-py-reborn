#ifndef PYLANG_TOKENIZEDUNIT_H
#define PYLANG_TOKENIZEDUNIT_H

#include "../lexer/Token.h"

namespace pylang {

/**
   @brief The pylang::TokenizedUnit class incapsulates tokenized translation unit.
   NOTE: making methods inline may improve performance - I'm not sure
   (anyway, LTO and profile guided optimization are the future)
 */

class TokenizedUnit
{
    TokenizedUnit(const TokenizedUnit&);
    void operator =(const TokenizedUnit&);

public:
    TokenizedUnit();
    ~TokenizedUnit();

    /**
       @brief Finds token under cursor
       @return true if any token placed under cursor
     */
    bool tokenAt(const SourceLocation &cursor, Token &result) const;

    /**
       @brief Finds token under cursor or nearest token after it
       @return true if any token placed under/after cursor
     */
    bool tokenNearest(const SourceLocation &cursor, Token &result) const;

    void pushToken(const Token &token);

    /**
       @brief Returns reference to change offsets directly
     */
    std::vector<uint32_t> &lineOffsets();

    const Token &tokenAt(uint32_t index) const;
    uint32_t tokensCount() const;

private:
    /**
       @brief Keeps absolute offset for each line of code.
       This information used to get token by it's location in source code.
     */
    std::vector<uint32_t> m_lineOffsets;

    /**
       @brief Keeps first token index for each line of code
       This information used to get token by it's location in source code.
     */
    std::vector<uint32_t> m_lineTokenIndicies;
    unsigned m_lastTokenLineNo;

    std::vector<Token> m_tokens;
};

} // namespace pylang

#endif // PYLANG_TOKENIZEDUNIT_H
