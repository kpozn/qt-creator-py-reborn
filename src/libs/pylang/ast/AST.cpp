#include "AST.h"
#include "../lexer/Lexem.h"
#include <stdexcept>

#include "assert.h"

#define assert_not_null(astRef) assert(false == astRef.isNull() && "Enexpected null ast pointer")

namespace pylang {

/*
//////////////////////////////////////////////////////////////////////////////////////////

                  Implementation of AST.h - accept method

//////////////////////////////////////////////////////////////////////////////////////////
 */

void TranslationUnitAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        m_body.accept(visitor);
        visitor.endVisit(*this);
    }
}

void ClassDefinitionAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        if (false == m_docstring.isNull())
            m_docstring->accept(visitor);
        m_decorators.accept(visitor);
        m_baseClasses.accept(visitor);
        m_body.accept(visitor);
        visitor.endVisit(*this);
    }
}

void FunctionDefinitionAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        if (false == m_docstring.isNull())
            m_docstring->accept(visitor);
        m_decorators.accept(visitor);
        m_arguments.accept(visitor);
        m_body.accept(visitor);
        visitor.endVisit(*this);
    }
}

void IfStmtAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_condition);
        m_condition->accept(visitor);
        m_body.accept(visitor);
        m_elif.accept(visitor);
        m_bodyElse.accept(visitor);
        visitor.endVisit(*this);
    }
}

void WhileStmtAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_condition);
        m_condition->accept(visitor);
        m_body.accept(visitor);
        m_bodyElse.accept(visitor);
        visitor.endVisit(*this);
    }
}

void ForStmtAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_collection);
        assert_not_null(m_iterator);
        m_collection->accept(visitor);
        m_iterator->accept(visitor);
        m_body.accept(visitor);
        m_bodyElse.accept(visitor);
        visitor.endVisit(*this);
    }
}

void WithStmtAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        m_expressions.accept(visitor);
        m_aliases.accept(visitor);
        m_body.accept(visitor);
        visitor.endVisit(*this);
    }
}

void TryStmtAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        m_excepts.accept(visitor);
        m_finally.accept(visitor);
        visitor.endVisit(*this);
    }
}

void ExceptAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        if (false == m_exceptionType.isNull())
            m_exceptionType->accept(visitor);
        if (false == m_exceptionName.isNull())
            m_exceptionName->accept(visitor);
        visitor.endVisit(*this);
    }
}

void ReturnStmtAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        if (false == m_value.isNull())
            m_value->accept(visitor);
        visitor.endVisit(*this);
    }
}

void ImportAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        if (false == m_host.isNull())
            m_host->accept(visitor);
        m_names.accept(visitor);
        visitor.endVisit(*this);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

ControlFlowAst::ControlFlowAst(Kind kind)
    : _kind(kind)
{
}

void ControlFlowAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        visitor.endVisit(*this);
    }
}

ControlFlowAst::Kind ControlFlowAst::kind() const
{
    return _kind;
}

void RaiseAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_exception);
        m_exception->accept(visitor);
        if (false == m_traceback.isNull())
            m_traceback->accept(visitor);
        visitor.endVisit(*this);
    }
}

void PrintAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        m_values.accept(visitor);
        visitor.endVisit(*this);
    }
}

void DelAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        m_expressions.accept(visitor);
        visitor.endVisit(*this);
    }
}

void GlobalAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        m_names.accept(visitor);
        visitor.endVisit(*this);
    }
}

void ExecAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_codeObject);
        m_codeGlobals->accept(visitor);
        if (false == m_codeGlobals.isNull())
            m_codeGlobals->accept(visitor);
        if (false == m_codeLocals.isNull())
            m_codeLocals->accept(visitor);
        visitor.endVisit(*this);
    }
}

void AssertAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_condition);
        m_condition->accept(visitor);
        if (false == m_errorMessage.isNull())
            m_errorMessage->accept(visitor);
        visitor.endVisit(*this);
    }
}

void IdentifierAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        visitor.endVisit(*this);
    }
}

LiteralExpressionAst::LiteralExpressionAst()
    : m_tokenKind(T_None)
{
}

void LiteralExpressionAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        visitor.endVisit(*this);
    }
}

/**
 * \brief converts token to Kind, does not convert colon into DictionaryPair
 */
BinaryExpressionAst::Kind BinaryExpressionAst::convertToken(int tokenKind)
{
    // performance note: gcc reorganizes any switch to get logarithmic comparison time
    // probably MSVC and CLang both use the same optimization
    switch (tokenKind)
    {
    case T_OpAdd:
        return Plus;
    case T_OpSub:
        return Minus;
    case T_OpMul:
        return Multiply;
    case T_OpDiv:
        return Divide;
    case T_OpFloorDiv:
        return FloorDivide;
    case T_OpMod:
        return Modulus;
    case T_OpLeftShift:
        return ShiftLeft;
    case T_OpRightShift:
        return ShiftRight;
    case T_OpEQ:
        return Equal;
    case T_OpNE:
    case T_OpNE2:
        return NotEqual;
    case T_OpLT:
        return LesserThan;
    case T_OpLE:
        return LesserEqual;
    case T_OpGE:
        return GreaterEqual;
    case T_OpGT:
        return GreaterThan;
    case T_KwAnd:
        return LogicalAnd;
    case T_KwOr:
        return LogicalOr;
    case T_OpBitwiseAnd:
        return BitwiseAnd;
    case T_OpBitwiseOr:
        return BitwiseOr;
    case T_OpBitwiseXor:
        return BitwiseXor;

    // augment assign
    case T_AugAdd:
        return AugPlus;
    case T_AugBitand:
        return AugBitwiseAnd;
    case T_AugBitor:
        return AugBitwiseOr;
    case T_AugBitxor:
        return AugBitwiseXor;
    case T_AugDiv:
        return AugDivide;
    case T_AugFloorDiv:
        return AugFloorDivide;
    case T_AugLShift:
        return AugShiftLeft;
    case T_AugMod:
        return AugModulus;
    case T_AugMul:
        return AugMultiply;
    case T_AugPower:
        return AugPower;
    case T_AugRShift:
        return AugShiftRight;
    case T_AugSub:
        return AugMinus;

    case T_KwIn:
        return InCollection;
    case T_KwIs:
        return IsThis;

    default:
#ifdef NEED_RUNTIME_CHECKS
        throw std::logic_error("Internal error: BinaryExpressionAst"
                               " got invalid token as type of expression");
#endif
        return Plus;
    }
}

void BinaryExpressionAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        visitor.endVisit(*this);
    }
}

void UnaryExpressionAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_child);
        m_child->accept(visitor);
        visitor.endVisit(*this);
    }
}

void AssignmentAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        m_expressions.accept(visitor);
        visitor.endVisit(*this);
    }
}

void LambdaAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        visitor.endVisit(*this);
    }
}

void ConditionExpressionAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_condition);
        m_condition->accept(visitor);
        visitor.endVisit(*this);
    }
}

void IfExpressionAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_condition);
        assert_not_null(m_then);
        assert_not_null(m_otherwise);
        m_condition->accept(visitor);
        m_then->accept(visitor);
        m_otherwise->accept(visitor);
        visitor.endVisit(*this);
    }
}

void ForExpressionAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_iterator);
        assert_not_null(m_collection);
        m_iterator->accept(visitor);
        m_collection->accept(visitor);
        visitor.endVisit(*this);
    }
}

void ListExpressionAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        m_items.accept(visitor);
        visitor.endVisit(*this);
    }
}

void FunctionCallAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_function);
        m_function->accept(visitor);
        m_arguments.accept(visitor);
        visitor.endVisit(*this);
    }
}

void SliceAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_array);
        m_array->accept(visitor);
        m_subscripts.accept(visitor);
        visitor.endVisit(*this);
    }
}

void SubscriptAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        if (false == m_begin.isNull())
            m_begin->accept(visitor);
        if (false == m_end.isNull())
            m_end->accept(visitor);
        if (false == m_step.isNull())
            m_step->accept(visitor);
        visitor.endVisit(*this);
    }
}

void DictPairAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_key);
        assert_not_null(m_value);
        m_key->accept(visitor);
        m_value->accept(visitor);
        visitor.endVisit(*this);
    }
}

void AttributeAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        assert_not_null(m_object);
        assert_not_null(m_attribute);
        m_object->accept(visitor);
        m_attribute->accept(visitor);
        visitor.endVisit(*this);
    }
}

void AliasAst::accept(IAstVisitor &visitor)
{
    if(visitor.visit(*this)) {
        m_name->accept(visitor);
        m_alias->accept(visitor);
        visitor.endVisit(*this);
    }
}

void NoExpressionAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        visitor.endVisit(*this);
    }
}

void FormalParameterAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        visitor.endVisit(*this);
    }
}

void DocStringAst::accept(IAstVisitor &visitor)
{
    if (visitor.visit(*this)) {
        visitor.endVisit(*this);
    }
}

} // namespace pylang
