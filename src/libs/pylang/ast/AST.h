#ifndef AST_H_41e99835_083d_4102_b379_0d65343e218d
#define AST_H_41e99835_083d_4102_b379_0d65343e218d

#include "detail/pyastcommons.h"
#include "detail/pyastvisitor.h"
#include "PySequence.h"

namespace pylang {

class PYLANG_EXPORT ExpressionAst
        : public Internal::StmtAst
{
public:
};

class PYLANG_EXPORT TranslationUnitAst : public Internal::ScopedAst
{
public:
    void accept(IAstVisitor &visitor);

    PySequence m_body;
};

class PYLANG_EXPORT ClassDefinitionAst
        : public Internal::DecorableAst
{
public:
    void accept(IAstVisitor &visitor);

    SourceLocation m_identifier;
    PySequence m_baseClasses;
};

class PYLANG_EXPORT FunctionDefinitionAst
        : public Internal::DecorableAst
{
public:
    void accept(IAstVisitor &visitor);

    SourceLocation m_identifier;
    PySequence m_arguments;
};

class PYLANG_EXPORT IfStmtAst
        : public Internal::ConditionTraits
        , public Internal::OtherwisedAst
{
public:
    void accept(IAstVisitor &visitor);

    PySequence m_elif; // NOTE: maybe refactor elif support?
};

class PYLANG_EXPORT WhileStmtAst
        : public Internal::ConditionTraits
        , public Internal::OtherwisedAst
{
public:
    void accept(IAstVisitor &visitor);
};

class PYLANG_EXPORT ForStmtAst
        : public Internal::OtherwisedAst
        , public Internal::IterationTraits
{
public:
    void accept(IAstVisitor &visitor);
};

/**
 * \class pylang::WithStmtAst
 * Example:
 * keyword   |  expression   |  alias
 *     \/          \/           \/
 * \code
 *    with open("input.txt") as f:
 *        content = f.readAll()
 * \endcode
 */
class PYLANG_EXPORT WithStmtAst : public Internal::CompoundStmtAst
{
public:
    void accept(IAstVisitor &visitor);

    PySequence m_expressions;
    PySequence m_aliases;
};

class PYLANG_EXPORT TryStmtAst : public Internal::OtherwisedAst
{
public:
    void accept(IAstVisitor &visitor);

    PySequence m_excepts;
    PySequence m_finally;
};

class PYLANG_EXPORT ExceptAst
        : public Internal::CompoundStmtAst
        //, public Internal::IAstWithType
{
public:
    void accept(IAstVisitor &visitor);

    AstRef<IdentifierAst> m_exceptionType;
    AstRef<IdentifierAst> m_exceptionName;
};

class PYLANG_EXPORT ReturnStmtAst : public Internal::StmtAst
{
public:
    void accept(IAstVisitor &visitor);

    AstRef<ExpressionAst> m_value;
};

class PYLANG_EXPORT ImportAst
        : public Internal::StmtAst
{
public:
    void accept(IAstVisitor &visitor);

    SourceLocation token_from;
    SourceLocation token_import;
    SourceLocation token_asterix;

    /**
       @brief m_host - basic module, e.g. PyQt4 in statement
       @code
       from PyQt4 import QtCore
       @endcode
       @return nullptr if there are no basic module (kind == FromGlobal)
     */
    AstRef<ExpressionAst> m_host;
    PySequence m_names;
};

/**
 * @brief The ControlFlowAst class - represents single-word statement.
 * Appears on lexems T_KwPass, T_KwBreak, T_KwContinue.
 */
class PYLANG_EXPORT ControlFlowAst
        : public Internal::StmtAst
{
public:
    enum Kind {
        Pass,
        Break,
        Continue
    };

    ControlFlowAst(Kind kind);
    void accept(IAstVisitor &visitor);
    Kind kind() const;

    SourceLocation m_keyword;

private:
    Kind _kind;
};

/**
 * \brief The RaiseAst class - represents raise statement, with exception and
 * (optionally) attached traceback. For python3 RaiseAst has no traceback attribute.
 * \note see http://www.python.org/dev/peps/pep-3109/ for more information
 */
class PYLANG_EXPORT RaiseAst
        : public Internal::StmtAst
{
public:
    void accept(IAstVisitor &visitor);

public: // attributes
    SourceLocation m_keywordRaise;

    /**
     * 'from' appears in python3 and shows a real reason of exception
     * @code
     *  try:
     *      print(1 / 0)
     *  except Exception as exc:
     *      raise RuntimeError("Something bad happened") from exc
     * @endcode
     */
    SourceLocation m_keywordFrom;

    AstRef<ExpressionAst> m_exception; /**< Thrown exception */
    AstRef<ExpressionAst> m_traceback; /**< @brief Additional traceback, can be null. */
    AstRef<ExpressionAst> m_reason;    /**< Reason is an identifier of previously
                                            caught exception, which was created earlier
                                         */
};

class PYLANG_EXPORT PrintAst : public Internal::StmtAst
{
public:
    void accept(IAstVisitor &visitor);

    SourceLocation m_keywordPrint;
    PySequence m_values;
};

class PYLANG_EXPORT DelAst : public Internal::StmtAst
{
public:
    void accept(IAstVisitor &visitor);
    PySequence m_expressions;
};

class PYLANG_EXPORT GlobalAst : public Internal::StmtAst
{
public:
    void accept(IAstVisitor &visitor);
    PySequence m_names;
};

// BANANA:
class PYLANG_EXPORT ExecAst : public Internal::StmtAst
{
public:
    void accept(IAstVisitor &visitor);

    SourceLocation m_keywordExec;
    AstRef<ExpressionAst> m_codeObject;
    AstRef<ExpressionAst> m_codeGlobals;
    AstRef<ExpressionAst> m_codeLocals;
};

/**
 * \class pylang::AssertAst
 * \brief Python assertion consists of two expressions.
 * The first expression is condition that should always be true,
 * the second (optional) expression should return string
 * that will be printed on assertion fail.
 */
class PYLANG_EXPORT AssertAst : public Internal::StmtAst
{
public:
    void accept(IAstVisitor &visitor);

    SourceLocation m_keywordAssert;

    AstRef<ExpressionAst> m_condition;
    AstRef<ExpressionAst> m_errorMessage;
};

/*****************************************************************************************
 *
 *      Expressions
 *
 ****************************************************************************************/

class PYLANG_EXPORT IdentifierAst : public ExpressionAst
{
public:
    void accept(IAstVisitor &visitor);

    SourceLocation m_identifier;
};

class PYLANG_EXPORT LiteralExpressionAst : public ExpressionAst
{
public:
    /**
     * @brief Initialises "Null" literal with null source location
     */
    LiteralExpressionAst();

    void accept(IAstVisitor &visitor);

    SourceLocation m_literal;
    int m_tokenKind;
};

class PYLANG_EXPORT BinaryExpressionAst : public ExpressionAst
{
public:
    void accept(IAstVisitor &visitor);

    SourceLocation m_operator;
    AstRef<ExpressionAst> m_left;
    AstRef<ExpressionAst> m_right;

    enum Kind {
        Plus,
        Minus,
        Multiply,
        Power,
        Divide,
        FloorDivide,
        Modulus,
        ShiftLeft,
        ShiftRight,
        Equal,
        NotEqual,
        LesserThan,
        LesserEqual,
        GreaterThan,
        GreaterEqual,
        LogicalAnd,
        LogicalOr,
        BitwiseAnd,
        BitwiseOr,
        BitwiseXor,

        AugPlus,
        AugMinus,
        AugMultiply,
        AugPower,
        AugDivide,
        AugFloorDivide,
        AugModulus,
        AugShiftLeft,
        AugShiftRight,
        AugBitwiseAnd,
        AugBitwiseOr,
        AugBitwiseXor,
        InCollection,    // keyword "in"
        IsThis           // keyword "is"
    };
    Kind m_kind;
    static Kind convertToken(int tokenKind);
};

class PYLANG_EXPORT UnaryExpressionAst
        : public ExpressionAst
        , public Internal::UnaryExpressionTraits
{
public:
    void accept(IAstVisitor &visitor);

    enum Kind {
        UnaryPlus,
        UnaryMinus,
        LogicalNot,
        BitwiseNot
    };
    Kind m_kind;
};

/**
   \class AssignmentAst

   This class represents single or group of exressions that delimited with '='
   and together are 'atomic' expressions, that cannot be divided in context of
   parent node, examples:

    \code

    li = [Base(), Base()]   # is an AssignmentAst, expressions().size == 2
    li = Derived(), Base()  # is an AssignmentAst, expressions().size == 2
    # now 'li' contains [Derived(), Base()]

    a + b       # is an AssigmentAst, expressions().size == 1
    a = b = c   # is an AssigmentAst, expressions().size == 3

    \endcode

    As you see, python supports multiple assignment:
    \code
    li = Derived(), Base()
    \endcode
    In this case, the first item in expressions() is IdentifierAst,
    and second item is generated tuple (ListExpressionAst) with two items:
    'Derived()' and 'Base()'
 */
class PYLANG_EXPORT AssignmentAst : public ExpressionAst
{
public:
    void accept(IAstVisitor &visitor);

    PySequence m_expressions;
};

class PYLANG_EXPORT LambdaAst : public ExpressionAst
{
public:
    void accept(IAstVisitor &visitor);

    AstRef<ExpressionAst> m_expression;
    PySequence m_arguments;
};

class PYLANG_EXPORT ConditionExpressionAst
        : public ExpressionAst
        , public Internal::ConditionTraits
{
public:
    void accept(IAstVisitor &visitor);
};

class PYLANG_EXPORT IfExpressionAst
        : public ExpressionAst
        , public Internal::ConditionTraits
{
public:
    void accept(IAstVisitor &visitor);

    AstRef<ExpressionAst> m_then;
    AstRef<ExpressionAst> m_otherwise;
};

class PYLANG_EXPORT ForExpressionAst
        : public ExpressionAst
        , public Internal::IterationTraits
{
public:
    void accept(IAstVisitor &visitor);
};

/**
 * \class pylang::ListExpressionAst
 * \brief represents expression with list of values:  dict, set or
 * list constructor. It unites both grammar forms, with "for" and without it.
 * \code
 * list1 = ['ann', 'kate', 'evelin']
 * dict2 = {'mike': 'cat', 'alex': 'parrot'}
 * tuple3 = (1, 5, 'six')
 * \endcode
 */
class PYLANG_EXPORT ListExpressionAst
        : public ExpressionAst
{
public:
    void accept(IAstVisitor &visitor);

    SourceLocation m_openingToken;
    SourceLocation m_closingToken;
    PySequence m_items;

    enum Kind {
        List,
        Dict,
        Set,
        Tuple,
        BackquotedExpr
    };

    Kind m_kind;
};

/**
 * @brief The FunctionCallAst class
 * NOTE: interface of this class still lacks some basic functionality
 */
class PYLANG_EXPORT FunctionCallAst
        : public ExpressionAst
{
public:
    void accept(IAstVisitor &visitor);

    SourceLocation m_openingBrace;
    SourceLocation m_closingBrace;
    AstRef<ExpressionAst> m_function;
    PySequence m_arguments;
};

/// BANANA:
class PYLANG_EXPORT SliceAst
        : public ExpressionAst
{
public:
    void accept(IAstVisitor &visitor);

    SourceLocation m_openingBracket;
    SourceLocation m_closingBracket;
    AstRef<ExpressionAst> m_array;
    PySequence m_subscripts;
};

class PYLANG_EXPORT SubscriptAst
        : public ExpressionAst
{
public:
    void accept(IAstVisitor &visitor);

    AstRef<ExpressionAst> m_begin;
    AstRef<ExpressionAst> m_end;
    AstRef<ExpressionAst> m_step;
};

/// BANANA:
class PYLANG_EXPORT DictPairAst
        : public ExpressionAst
{
public:
    void accept(IAstVisitor &visitor);

    AstRef<ExpressionAst> m_key;
    AstRef<ExpressionAst> m_value;
};

/// BANANA:
class PYLANG_EXPORT AttributeAst
        : public ExpressionAst
{
public:
    void accept(IAstVisitor &visitor);

    AstRef<ExpressionAst> m_object;
    AstRef<ExpressionAst> m_attribute;
};

/**
 * @brief The AliasAst class - two expressions, separated by "as",
 * @code
 *   from PySide import QtCore as Core
 *   except TypeError as e:
 *       pass
 * @end
 */
class PYLANG_EXPORT AliasAst
        : public ExpressionAst
{
public:
    void accept(IAstVisitor &visitor);

    AstRef<ExpressionAst> m_name;
    AstRef<ExpressionAst> m_alias;
};

/**
 * @brief Represents case when part of complex expression missed
 *
 * In parser logic, this ast also means that parsing error already reported
 * Since IDE parses incomplete code, there can be incomplete expression like
 * @code
 *      # num1 missed!
 *      prod = **num2
 * @endcode
 * Such code will be parsed to
 *              AssignmentAst
 *             /            \
 * IdentifierAst            BinaryExpressionAst
 *                         /                   \
 *                NoExpressionAst              IdentifierAst
 */
class PYLANG_EXPORT NoExpressionAst
        : public ExpressionAst
{
public:
    void accept(IAstVisitor &visitor);
};

/**
 * \brief The FormalParameterAst class - represents formal function
 * or lambda expression parameter, e.g.
 * \code
 *  def foo(self, parent = None, *param)
 * \endcode
 */
class PYLANG_EXPORT FormalParameterAst
        : public Ast
{
public:
    void accept(IAstVisitor &visitor);

    AstRef<ExpressionAst> m_parameter;
    AstRef<ExpressionAst> m_defaultValue;

    enum Kind {
        Normal,         // func(foo)
        VAList,         // func(*foo)
        KerwordVAList   // func(**foo)
    };

    Kind m_kind;
};

/**
 * \brief The DocStringAst class - represents docstring for function or class
 * \code
 *  def foo(self, parent = None, *param):
 *      """
 *      This function is only example and takes any parameters
 *      """
 * \endcode
 * DocString itself not parsed on document parsing pass
 */
class PYLANG_EXPORT DocStringAst
        : public Ast
{
public:
    void accept(IAstVisitor &visitor);
    SourceLocation m_stringToken;
};

} // namespace pylang

#endif // AST_H_41e99835_083d_4102_b379_0d65343e218d
