#ifndef PYAST_P_H_edeca81c_1010_4221_9afa_21f1892f9cd0
#define PYAST_P_H_edeca81c_1010_4221_9afa_21f1892f9cd0

#include "../pylang_global.h"
#include "../Support/MemoryPool.h"
#include "../Support/SourceRange.h"

namespace pylang {

//class ArgumentsAst;
//class ExceptionHandlerAst;
//class AliasAst;
//class ComprehensionAst;

class Ast;
class ExpressionAst;
class TranslationUnitAst;

class ClassDefinitionAst;
class FunctionDefinitionAst;
class IfStmtAst;
class WhileStmtAst;
class ForStmtAst;
class WithStmtAst;
class TryStmtAst;

class ReturnStmtAst;
class ImportAst;
class ControlFlowAst;
class RaiseAst;
class PrintAst; // not exists in python 3
class DelAst;
class GlobalAst;
class ExecAst;
class AssertAst;

//** Expressions **//
class IdentifierAst;
class LiteralExpressionAst;
class BinaryExpressionAst;
class UnaryExpressionAst;
class AssignmentAst;
class LambdaAst;
class ConditionExpressionAst;
class IfExpressionAst;
class ForExpressionAst;
class ListExpressionAst;
class FunctionCallAst;
class SliceAst;
class SubscriptAst;
class DictPairAst;
class AttributeAst;
class AliasAst;
class NoExpressionAst;

//** Utility nodes **//
class FormalParameterAst;
class ExceptAst;
class DocStringAst;

//class MemberAccessExpressionAst;

class IAstVisitor;

class PySequence;

/**
 * @class AstRef - reference to Ast node, initialized with nullptr
 */
template<class TAst>
class AstRef
{
public:
    inline TAst &operator *() {
        return *p_ast;
    }

    inline const TAst &operator *() const {
        return *p_ast;
    }

    inline TAst *operator ->() const {
        return p_ast;
    }

    inline AstRef(TAst *ast = nullptr) : p_ast(ast) { }

    inline bool isNull() const { return (nullptr == p_ast); }
    inline TAst *get() const { return p_ast; }
    inline void reset(TAst *ast) { p_ast = ast; }

    template<class TAnotherAst>
    inline AstRef<TAnotherAst> down_cast() {
        return AstRef<TAnotherAst>(p_ast);
    }

    template<class TAnotherAst>
    inline AstRef<TAnotherAst> up_cast() {
        return AstRef<TAnotherAst>(dynamic_cast<TAnotherAst*>(p_ast));
    }

private:
    TAst *p_ast;
};

class PYLANG_EXPORT Ast : public Managed
{
public:
    SourceRange m_range;

    /**
     * @brief accept - accepts this node without traversing
     * @param visitor - visitor accepted to this node
     */
    virtual void accept(IAstVisitor &visitor) = 0;

    const Ast *sibling() const;
    Ast *sibling();

protected:
    Ast() {}
    virtual ~Ast() {}

private:
    friend class PySequence;
    Ast *p_next;
};

} // namespace pylang

#endif // PYAST_P_H_edeca81c_1010_4221_9afa_21f1892f9cd0
