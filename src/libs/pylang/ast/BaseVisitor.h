#ifndef PYTHON_BASEVISITOR_H
#define PYTHON_BASEVISITOR_H

#include "AST.h"

namespace pylang {

/**
 * \class BaseVisitor - an template base class
 * for any AST visitor, use BaseVisitor<false>
 * if you want visit tree only through selected nodes,
 * and use BaseVisitor<true> if you want visit all tree even
 * if somebody added new node classes
 */

template<bool TRet>
class BaseVisitor : public IAstVisitor
{
protected:
    BaseVisitor() {}

public:
    bool visit(TranslationUnitAst &) { return TRet; }
    void endVisit(TranslationUnitAst &) { }

    bool visit(ClassDefinitionAst &) { return TRet; }
    void endVisit(ClassDefinitionAst &) { }

    bool visit(FunctionDefinitionAst &) { return TRet; }
    void endVisit(FunctionDefinitionAst &) { }

    bool visit(IfStmtAst &) { return TRet; }
    void endVisit(IfStmtAst &) { }

    bool visit(WhileStmtAst &) { return TRet; }
    void endVisit(WhileStmtAst &) { }

    bool visit(ForStmtAst &) { return TRet; }
    void endVisit(ForStmtAst &) { }

    bool visit(WithStmtAst &) { return TRet; }
    void endVisit(WithStmtAst &) { }

    bool visit(TryStmtAst &) { return TRet; }
    void endVisit(TryStmtAst &) { }

    bool visit(ReturnStmtAst &) { return TRet; }
    void endVisit(ReturnStmtAst &) { }

    bool visit(ImportAst &) { return TRet; }
    void endVisit(ImportAst &) { }

    bool visit(ControlFlowAst &) { return TRet; }
    void endVisit(ControlFlowAst &) { }

    bool visit(RaiseAst &) { return TRet; }
    void endVisit(RaiseAst &) { }

    bool visit(PrintAst &) { return TRet; }
    void endVisit(PrintAst &) { }

    bool visit(DelAst &) { return TRet; }
    void endVisit(DelAst &) { }

    bool visit(ExecAst &) { return TRet; }
    void endVisit(ExecAst &) { }

    bool visit(GlobalAst &) { return TRet; }
    void endVisit(GlobalAst &) { }

    bool visit(AssertAst &) { return TRet; }
    void endVisit(AssertAst &) { }

    bool visit(IdentifierAst &) { return TRet; }
    void endVisit(IdentifierAst &) { }

    bool visit(LiteralExpressionAst &) { return TRet; }
    void endVisit(LiteralExpressionAst &) { }

    bool visit(BinaryExpressionAst &) { return TRet; }
    void endVisit(BinaryExpressionAst &) { }

    bool visit(UnaryExpressionAst &) { return TRet; }
    void endVisit(UnaryExpressionAst &) { }

    bool visit(AssignmentAst &) { return TRet; }
    void endVisit(AssignmentAst &) { }

    bool visit(LambdaAst &) { return TRet; }
    void endVisit(LambdaAst &) { }

    bool visit(ConditionExpressionAst &) { return TRet; }
    void endVisit(ConditionExpressionAst &) { }

    bool visit(IfExpressionAst &) { return TRet; }
    void endVisit(IfExpressionAst &) { }

    bool visit(ForExpressionAst &) { return TRet; }
    void endVisit(ForExpressionAst &) { }

    bool visit(ListExpressionAst &) { return TRet; }
    void endVisit(ListExpressionAst &) { }

    bool visit(FunctionCallAst &) { return TRet; }
    void endVisit(FunctionCallAst &) { }

    bool visit(SliceAst &) { return TRet; }
    void endVisit(SliceAst &) { }

    bool visit(SubscriptAst &) { return TRet; }
    void endVisit(SubscriptAst &) { }

    bool visit(DictPairAst &) { return TRet; }
    void endVisit(DictPairAst &) { }

    bool visit(AttributeAst &) { return TRet; }
    void endVisit(AttributeAst &) { }

    bool visit(AliasAst &) { return TRet; }
    void endVisit(AliasAst &) { }

    bool visit(NoExpressionAst &) { return TRet; }
    void endVisit(NoExpressionAst &) { }

    bool visit(FormalParameterAst &) { return TRet; }
    void endVisit(FormalParameterAst &) { }

    bool visit(ExceptAst &) { return TRet; }
    void endVisit(ExceptAst &) { }

    bool visit(DocStringAst &) { return TRet; }
    void endVisit(DocStringAst &) { }
};

} // namespace pylang

#endif // PYTHON_BASEVISITOR_H
