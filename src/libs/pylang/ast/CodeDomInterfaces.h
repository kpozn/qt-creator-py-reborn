#ifndef ILITERALPROVIDER_H
#define ILITERALPROVIDER_H

#include <stdint.h>
#include "../Support/StringRef.h"
#include "AST.h"

namespace pylang {

class Ast;
class Token;
class SourceLocation;

class LanguageTraits
{
public:
    bool isPython3() const
    { return m_isPython3; }

    bool areTypesRestricted() const
    { return m_typesAreRestricted; }

protected:
    LanguageTraits()
        : m_isPython3(false) // Python3 still not mainstream
        , m_typesAreRestricted(false) // static typing still not mainstream
    {
    }

    ~LanguageTraits() {}

    void setPython3(bool isPython3)
    { m_isPython3 = isPython3; }

    void setTypesRestriction(bool typesAreRestricted)
    { m_typesAreRestricted = typesAreRestricted; }

private:
    bool m_isPython3;
    bool m_typesAreRestricted;
};

class ILiteralsProvider
{
public:
    virtual ~ILiteralsProvider() { }

    virtual StringRef literalAt(const SourceLocation &index) const = 0;
    virtual StringRef identifierAt(const SourceLocation &index) const = 0;
};

class ITokensProvider
{
public:
    virtual ~ITokensProvider() { }

    virtual bool tokenAt(const SourceLocation &tokenLoc, Token &result) const = 0;
    virtual bool tokenNearest(const SourceLocation &tokenLoc, Token &result) const = 0;
};

class IAstRoot
{
public:
    virtual ~IAstRoot() { }

    virtual void accept(IAstVisitor &visitor) const = 0;
    virtual TranslationUnitAst *astRoot() const = 0;
};

}

#endif // ILITERALPROVIDER_H
