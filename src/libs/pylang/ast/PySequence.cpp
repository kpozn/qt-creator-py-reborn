#include "PySequence.h"
#include "exception/exceptionwithmessage.h"

#ifdef NEED_RUNTIME_CHECKS
#include <QDebug>
#endif


namespace pylang {

namespace {

static Ast *theEnd = 0;

inline void checkNullAst(const Ast *ast)
{
#ifdef NEED_RUNTIME_CHECKS
    if (ast == nullptr)
    {
        throw ExceptionWithMessage("PySequence got null ast pointer");
    }
#else
    (void) ast;
#endif
}
}

PySequence::PySequence()
    : _first(0)
    , _last(0)
    , _size(0)
{
}

PySequence::PySequence(Ast *frontValue)
    : _first(0)
    , _last(0)
    , _size(0)
{
    append(frontValue);
}

void PySequence::append(Ast *ast)
{
    checkNullAst(ast);
    ++_size;

    ast->p_next = 0;
    if (!_first)
        _first = ast;
    if (_last)
        _last->p_next = ast;
    _last = ast;
}

void PySequence::append(const PySequence &seq)
{
    _size += seq._size;
    if (!_first)
    {
        _first = seq._first;
        _last = seq._last;
    }
    else if (seq._first)
    {
        _last->p_next = seq._first;
        _last = seq._last;
    }
}

unsigned PySequence::size() const
{
    return _size;
}

bool PySequence::isEmpty() const
{
    return (_size == 0);
}

PySequence::const_iterator PySequence::begin() const
{
    return const_iterator(&_first);
}

PySequence::iterator PySequence::begin()
{
    return iterator(&_first);
}

PySequence::const_iterator PySequence::end() const
{
    return const_iterator(&theEnd);
}

PySequence::iterator PySequence::end()
{
    return iterator(&theEnd);
}

Ast *PySequence::front() const
{
    checkNullAst(_first);
    return _first;
}

Ast *PySequence::back() const
{
    checkNullAst(_last);
    return _last;
}


PySequence::iterator::iterator(Ast **ast)
    :_ast(ast)
{
}

Ast *&PySequence::iterator::operator *() const
{
    checkNullAst(*_ast);
    return *_ast;
}

PySequence::iterator PySequence::iterator::operator ++()
{
    checkNullAst(*_ast);
    _ast = PySequence::getNext(_ast);
    return *this;
}

PySequence::iterator PySequence::iterator::operator ++(int)
{
    checkNullAst(*_ast);
    iterator ret = *this;
    _ast = PySequence::getNext(_ast);
    return ret;
}

PySequence::const_iterator::const_iterator(Ast * const *ast)
    :_ast(ast)
{
}

PySequence::const_iterator::const_iterator(const iterator &it)
    :_ast(&(*it))
{
}

Ast * const&PySequence::const_iterator::operator *() const
{
    checkNullAst(*_ast);
    return *_ast;
}

PySequence::const_iterator PySequence::const_iterator::operator ++()
{
    checkNullAst(*_ast);
    _ast = PySequence::getNextConst(_ast);
    return *this;
}

PySequence::const_iterator PySequence::const_iterator::operator ++(int)
{
    checkNullAst(*_ast);
    const_iterator ret = *this;
    _ast = PySequence::getNextConst(_ast);
    return ret;
}

Ast **PySequence::getNext(Ast **ast)
{
    Ast *&next = (*ast)->p_next;
    if (next == 0)
        return &theEnd;
    return &next;
}

Ast *const *PySequence::getNextConst(Ast *const *ast)
{
    Ast * const&next = (*ast)->p_next;
    if (next == 0)
        return &theEnd;
    return &next;
}

void PySequence::accept(IAstVisitor &visitor) const
{
    for (const_iterator it = begin(); it != end(); ++it)
    {
        Ast *ast = *it;
        ast->accept(visitor);
    }
}

#ifdef NEED_RUNTIME_CHECKS
bool PySequence::validate() const
{
    for (const_iterator it = begin(); it != end(); ++it)
    {
        if (*it == nullptr)
        {
            qDebug() << "PySequence contains null pointer."
                     << "This problem detected inside PySequence::validate()";
            QString dump;
            unsigned nexti = 1;
            for (const_iterator it = begin(); it != end(); ++it, ++nexti)
            {
                qulonglong p = reinterpret_cast<ptrdiff_t>(*it);
                dump.append(QString::number(p));
                if (nexti != size())
                    dump.append(", ");
            }
            qDebug() << "Sequence dump:" << dump.toAscii();

            // no need to check longer
            return false;
        }
    }
    return true;
}
#endif

} // namespace pylang
