#ifndef PYTHON_PYSEQUENCE_H
#define PYTHON_PYSEQUENCE_H

#include "AST_pwd.h"

namespace pylang {

class PySequence
{
public: // iterators
    class iterator
    {
    public:
        iterator(Ast **ast);
        Ast *&operator*() const;
        iterator operator++();
        iterator operator++(int);
        inline bool operator ==(const iterator &other)
        {
            return *_ast == *other._ast;
        }
        inline bool operator !=(const iterator &other)
        {
            return *_ast != *other._ast;
        }
    private:
        Ast **_ast;
    };

    class const_iterator
    {
    public:
        const_iterator(Ast * const*_ast);
        const_iterator(const iterator &it);
        Ast * const&operator*() const;
        const_iterator operator++();
        const_iterator operator++(int);
        inline bool operator ==(const const_iterator &other)
        {
            return *_ast == *other._ast;
        }
        inline bool operator !=(const const_iterator &other)
        {
            return *_ast != *other._ast;
        }

    private:
        Ast * const *_ast;
    };

public: // methods
    PySequence();
    PySequence(Ast *frontValue);
    void append(Ast *ast);
    void append(const PySequence &seq);
    unsigned size() const;
    bool isEmpty() const;

    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;

    Ast *front() const;
    Ast *back() const;

    /**
     * @brief enterAst - enters to all nodes in this sequence
     * @param visitor - visitor accepted to all nodes
     */
    void accept(IAstVisitor &visitor) const;
#ifdef NEED_RUNTIME_CHECKS
    bool validate() const;
#endif

    static Ast **getNext(Ast **ast);
    static Ast *const *getNextConst(Ast *const *ast);

private:
    Ast *_first;
    Ast *_last;
    unsigned _size;
};

} // namespace pylang

#endif // PYTHON_PYSEQUENCE_H
