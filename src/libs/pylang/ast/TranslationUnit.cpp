#include "TranslationUnit.h"

namespace pylang {

TranslationUnit::TranslationUnit()
    : m_stringsHash(m_pool)
{
}

TranslationUnit::~TranslationUnit()
{
}

StringRef TranslationUnit::literalAt(const SourceLocation &cursor) const
{
    Token token;
    if (m_tokenizedUnit.tokenAt(cursor, token))
        return token.getLiteral();
    return StringRef();
}

StringRef TranslationUnit::identifierAt(const SourceLocation &cursor) const
{
    Token token;
    if ((m_tokenizedUnit.tokenAt(cursor, token)) && (token.is(T_Identifier)))
        return token.getLiteral();
    return StringRef();
}

bool TranslationUnit::tokenAt(const SourceLocation &tokenLoc, Token &result) const
{
    return m_tokenizedUnit.tokenAt(tokenLoc, result);
}

bool TranslationUnit::tokenNearest(const SourceLocation &tokenLoc, Token &result) const
{
    return m_tokenizedUnit.tokenNearest(tokenLoc, result);
}

void TranslationUnit::accept(pylang::IAstVisitor &visitor) const
{
    if (false == m_astRoot.isNull())
        m_astRoot->accept(visitor);
}

TranslationUnitAst *TranslationUnit::astRoot() const
{
    return m_astRoot.get();
}

} // namespace pylang
