#ifndef TRANSLATIONUNIT_H
#define TRANSLATIONUNIT_H

#include "CodeDomInterfaces.h"
#include "../Support/StringsHash.h"
#include "../Support/TokenizedUnit.h"

/**
   @brief pylang::TranslationUnit - keeps whole information about
   single translation unit - a file parsed by pylang engine

   UnitBuilder is a friend of this class
 */

namespace pylang {
class UnitBuilder;

class PYLANG_EXPORT TranslationUnit :
        public LanguageTraits,
        public ILiteralsProvider,
        public ITokensProvider,
        public IAstRoot
{
    TranslationUnit(const TranslationUnit&);
    void operator =(const TranslationUnit&);

public:
    TranslationUnit();
    virtual ~TranslationUnit();

    /** ILiteralsProvider */
    StringRef literalAt(const SourceLocation &cursor) const;
    StringRef identifierAt(const SourceLocation &cursor) const;

    /** ITokensProvider */
    virtual bool tokenAt(const SourceLocation &tokenLoc, Token &result) const;
    virtual bool tokenNearest(const SourceLocation &tokenLoc, Token &result) const;

    /** IAstRoot */
    void accept(IAstVisitor &visitor) const;
    TranslationUnitAst *astRoot() const;

private:
    friend class UnitBuilder;

    TokenizedUnit m_tokenizedUnit;

    /**
       @brief Main storage for all objects, except Diagnostic
       Should be initialized first - so it placed first class declaration
      */
    MemoryPool m_pool;

    /**
       @brief Keeps strings for all literals
     */
    StringsHash m_stringsHash;

    /**
     * @brief Root node of Abstract Syntax Tree for this translation unit
     */
    AstRef<TranslationUnitAst> m_astRoot;
};

} // namespace pylang

#endif // TRANSLATIONUNIT_H
