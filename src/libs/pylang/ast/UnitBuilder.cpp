#include "UnitBuilder.h"

namespace pylang {

UnitBuilder::UnitBuilder(TranslationUnit &unit, IDiagnosticClient &client)
    : m_unit(&unit)
    , m_client(&client)
{
}

UnitBuilder::UnitBuilder(TranslationUnit &unit)
    : m_unit(&unit)
    , m_client(0)
{
}

UnitBuilder::UnitBuilder()
    : m_unit(0)
    , m_client(0)
{
}

UnitBuilder &UnitBuilder::operator =(const UnitBuilder &other)
{
    m_unit = other.m_unit;
    m_client = other.m_client;
    return *this;
}

UnitBuilder::~UnitBuilder()
{
}

bool UnitBuilder::isPython3() const
{
    return m_unit->isPython3();
}

bool UnitBuilder::areTypesRestricted() const
{
    return m_unit->areTypesRestricted();
}

const TokenizedUnit &UnitBuilder::tokenizedUnit() const
{
    return m_unit->m_tokenizedUnit;
}

TokenizedUnit &UnitBuilder::tokenizedUnit()
{
    return m_unit->m_tokenizedUnit;
}

const StringRef *UnitBuilder::findLiteral(const char *chars, unsigned size) const
{
    return m_unit->m_stringsHash.findLiteral(chars, size);
}

StringRef UnitBuilder::findOrInsertLiteral(const char *chars, unsigned size)
{
    return m_unit->m_stringsHash.findOrInsertLiteral(chars, size);
}

bool UnitBuilder::isDiagnosticEnabled()
{
    return (m_client != 0);
}

void UnitBuilder::pushDiagnostic(const Diagnostic &diagnostic)
{
    assert(m_client && "Attempt to push diagnostic to nullptr client");
    m_client->diagnose(diagnostic);
}

MemoryPool &UnitBuilder::pool()
{
    return m_unit->m_pool;
}

bool UnitBuilder::tokenAt(const SourceLocation &tokenLoc, Token &result) const
{
    return m_unit->m_tokenizedUnit.tokenAt(tokenLoc, result);
}

bool UnitBuilder::tokenNearest(const SourceLocation &tokenLoc, Token &result) const
{
    return m_unit->m_tokenizedUnit.tokenNearest(tokenLoc, result);
}

void UnitBuilder::setRootAst(TranslationUnitAst *ast)
{
    if (nullptr != ast)
        m_unit->m_astRoot.reset(ast);
}

} // namespace pylang
