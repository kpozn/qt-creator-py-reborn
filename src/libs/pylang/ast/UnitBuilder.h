#ifndef PYLANG_UNITBUILDER_H
#define PYLANG_UNITBUILDER_H

#include "TranslationUnit.h"
#include "../Support/TokenizedUnit.h"
#include "../lexer/Token.h"
#include "../diagnostic/IDiagnosticClient.h"

namespace pylang {

class PYLANG_EXPORT UnitBuilder
        : public ITokensProvider
       // , public ILiteralsProvider
{
public:
    enum class Language {
        Python2,
        Python3,
        RestrictedPython
    };

    explicit UnitBuilder(TranslationUnit &unit, IDiagnosticClient &client);
    explicit UnitBuilder(TranslationUnit &unit);
    explicit UnitBuilder();
    UnitBuilder& operator =(const UnitBuilder &other);
    ~UnitBuilder();

    bool isPython3() const;
    bool areTypesRestricted() const;

    const TokenizedUnit &tokenizedUnit() const;
    TokenizedUnit &tokenizedUnit();

    const StringRef *findLiteral(const char *chars, unsigned size) const;
    StringRef findOrInsertLiteral(const char *chars, unsigned size);

    /**
       @brief Determines if diagnostic messages pushing enabled
       @return true if m_client is not nullptr
     */
    bool isDiagnosticEnabled();
    void pushDiagnostic(const Diagnostic &diagnostic);

    MemoryPool &pool();

    /** ITokensProvider */
    bool tokenAt(const SourceLocation &tokenLoc, Token &result) const;
    bool tokenNearest(const SourceLocation &tokenLoc, Token &result) const;

    /**
     * @brief Sets root of AST for current TU, does nothing if nullptr passed
     */
    void setRootAst(TranslationUnitAst *ast);

private:
    TranslationUnit *m_unit;
    IDiagnosticClient *m_client;
};

} // namespace pylang

#endif // PYLANG_UNITBUILDER_H
