///* Copyright (c) 2008 Roberto Raggi <roberto.raggi@gmail.com>
// * Modified by Sergey Shambir <sergey.shambir.auto@gmail.com>
// *
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// *
// * The above copyright notice and this permission notice shall be included in
// * all copies or substantial portions of the Software.
// *
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// * THE SOFTWARE.
// */

//#ifndef PYLANG_TYPE_H
//#define PYLANG_TYPE_H

//#include "../pylang_global.h"
//#include "../Support/MemoryPool.h"

//namespace Pylang {

//class NullType;
//class BooleanType;
//class IntegerType;
//class FloatType;
//class ComplexType;
//class StringType;
//class ListType;
//class SetType;
//class DictType;
//class TupleType;
//class Class;
//class Function;
//class DuckType;
//class CompositeType;

//class Type;
//typedef Internal::ManagedClassRef<Type> TypeRef;

///**
//   \class Type - the base class in the type hierarchy. A central concept
//   with types is that each type, excluding CompositeType, always is
//   a canonical type. Special CompositeType unites set of possible types
//   into single object; it responds to any method that any united type has.

//   Composite type appears in functions that can return different types
//   in diffirent execution paths, e.g:
//   \code
//    def composite(wantString):
//        if wantString:
//            return ""
//        return 0

//    def baseAndDerived(wantDerived):
//        if wantDerived:
//            return Derived()
//        return Base()
//   \endcode

//    DuckType type appears in functions that accept any type if this type has some
//    methods or attributes, e.g:
//   \code
//    def duckTyping(container):
//        return container['my-key']
//   \endcode
// */

//class PYLANG_EXPORT Type : public Managed
//{
//public:
//    Type();
//    virtual ~Type();

//    bool isNull() const;
//    bool isBoolean() const;
//    bool isInteger() const;
//    bool isFloat() const;
//    bool isComplex() const;
//    bool isString() const;
//    bool isList() const;
//    bool isSet() const;
//    bool isDict() const;
//    bool isTuple() const;
//    bool isClass() const;
//    bool isFunction() const;
//    bool isDuckType() const;
//    bool isComposite() const;

//    virtual const NullType *asNull() const { return 0; }
//    virtual const BooleanType *asBoolean() const  { return 0; }
//    virtual const IntegerType *asInteger() const  { return 0; }
//    virtual const FloatType *asFloat() const  { return 0; }
//    virtual const ComplexType *asComplex() const  { return 0; }
//    virtual const StringType *asString() const  { return 0; }
//    virtual const ListType *asList() const  { return 0; }
//    virtual const SetType *asSet() const  { return 0; }
//    virtual const DictType *asDict() const  { return 0; }
//    virtual const TupleType *asTuple() const  { return 0; }
//    virtual const Class *asClass() const  { return 0; }
//    virtual const Function *asFunction() const  { return 0; }
//    virtual const DuckType *asDuckType() const  { return 0; }
//    virtual const CompositeType *asComposite() const  { return 0; }

//    virtual NullType *asNull()  { return 0; }
//    virtual BooleanType *asBoolean()  { return 0; }
//    virtual IntegerType *asInteger()  { return 0; }
//    virtual FloatType *asFloat()  { return 0; }
//    virtual ComplexType *asComplex()  { return 0; }
//    virtual StringType *asString()  { return 0; }
//    virtual ListType *asList()  { return 0; }
//    virtual SetType *asSet()  { return 0; }
//    virtual DictType *asDict()  { return 0; }
//    virtual TupleType *asTuple()  { return 0; }
//    virtual Class *asClass()  { return 0; }
//    virtual Function *asFunction()  { return 0; }
//    virtual DuckType *asDuckType()  { return 0; }
//    virtual CompositeType *asComposite()  { return 0; }
//};

//} // namespace pylang

//#endif // PYLANG_TYPE_H
