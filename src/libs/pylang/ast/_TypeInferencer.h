//#ifndef PYLANG_TYPEINFERENCER_H
//#define PYLANG_TYPEINFERENCER_H

//#include "BaseVisitor.h"
//#include "../ast/UnitBuilder.h"

//namespace Pylang {

///**
//    \class TypeInferencer
//    \brief Finds types in whole translation unit

//    Does not detect undefined variables, but detects TypeErrors
// */
//class TypeInferencer : private BaseVisitor<true>
//{
//public:
//    TypeInferencer(UnitBuilder &helper);
//    ~TypeInferencer();

//    void run(TranslationUnitAst &ast);

//private:
//    //bool visit(ClassDefinitionAst &) { return TRet; }
//    //bool visit(FunctionDefinitionAst &) { return TRet; }
//    //bool visit(ImportAst &) { return TRet; }
////    bool visit(ControlFlowAst &) { return TRet; }
////    bool visit(PrintAst &) { return TRet; }
////    bool visit(DelAst &) { return TRet; }
////    bool visit(ExecAst &) { return TRet; }
////    bool visit(GlobalAst &) { return TRet; }
////    bool visit(AssertAst &) { return TRet; }
////    bool visit(IdentifierAst &) { return TRet; }
//    bool visit(LiteralExpressionAst &ast);
////    bool visit(BinaryExpressionAst &) { return TRet; }
//    void endVisit(UnaryExpressionAst &ast);
//    void endVisit(AssignmentAst &ast);
////    bool visit(LambdaAst &) { return TRet; }
////    bool visit(IfExpressionAst &) { return TRet; }
////    bool visit(ForExpressionAst &) { return TRet; }
////    bool visit(ListExpressionAst &) { return TRet; }
////    bool visit(BackquotedExpressionAst &) { return TRet; }
////    bool visit(FunctionCallAst &) { return TRet; }
////    bool visit(FormalParameterAst &) { return TRet; }
////    bool visit(ExceptAst &) { return TRet; }

//    void assignmentHelper(ExpressionAst *expressions[], int size);

//private: //! attributes
//    UnitBuilder &helper;
//};

//}

//#endif // PYLANG_TYPEINFERENCER_H
