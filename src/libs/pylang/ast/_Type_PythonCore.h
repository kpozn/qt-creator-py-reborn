//#ifndef PYLANG_TYPE_PYTHONCORE_H
//#define PYLANG_TYPE_PYTHONCORE_H

//#include "Type.h"
//#include "../symbols/SymbolTable.h"
//#include "../ast/UnitBuilder.h"

//namespace Pylang {

//class PYLANG_EXPORT NullType : public Type
//{
//public:
//    const NullType *asNull() const { return this; }
//    NullType *asNull() { return this; }
//};

//class PYLANG_EXPORT BooleanType : public Type
//{
//public:
//    const BooleanType *asBoolean() const { return this; }
//    BooleanType *asBoolean() { return this; }
//};

//class PYLANG_EXPORT IntegerType : public Type
//{
//public:
//    const IntegerType *asInteger() const { return this; }
//    IntegerType *asInteger() { return this; }
//};

//class PYLANG_EXPORT FloatType : public Type
//{
//public:
//    const FloatType *asFloat() const { return this; }
//    FloatType *asFloat() { return this; }
//};

//class PYLANG_EXPORT ComplexType : public Type
//{
//public:
//    const ComplexType *asComplex() const { return this; }
//    ComplexType *asComplex() { return this; }
//};

//class PYLANG_EXPORT StringType : public Type
//{
//public:
//    const StringType *asString() const { return this; }
//    StringType *asString() { return this; }
//};

//class PYLANG_EXPORT Function : public Type
//{
//public:
//    const Function *asFunction() const { return this; }
//    Function *asFunction() { return this; }

//    //! Can be nullptr
//    Type *returnType();

//private:
//};

//} // namespace pylang

//#endif // PYLANG_TYPE_PYTHONCORE_H
