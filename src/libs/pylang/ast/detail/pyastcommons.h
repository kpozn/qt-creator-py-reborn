#ifndef PYTHON_PYASTCOMMONS_H
#define PYTHON_PYASTCOMMONS_H

#include "../AST_pwd.h"
#include "../PySequence.h"
#include "pytypes.h"

namespace pylang {

namespace Internal {

class StmtAst : public Ast
{
};

//! Ast node that increases indent level,
//! can be CompoundStmtAst or translation unit
class ScopedAst : public StmtAst
{
};

class CompoundStmtAst : public ScopedAst
{
public:
    PySequence m_body;
};

class OtherwisedAst : public CompoundStmtAst
{
public:
    PySequence m_bodyElse;
};

class DecorableAst : public CompoundStmtAst
{
public:
    PySequence m_decorators;
    AstRef<DocStringAst> m_docstring;
};

class ConditionTraits
{
public:
    AstRef<ExpressionAst> m_condition;
};

class IterationTraits
{
public:
    AstRef<ExpressionAst> m_collection;
    AstRef<ExpressionAst> m_iterator;
};

class UnaryExpressionTraits
{
public:
    AstRef<ExpressionAst> m_child;
};

} // namespace Internal
} // namespace pylang

#endif // PYTHON_PYASTCOMMONS_H
