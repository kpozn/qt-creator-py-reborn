#ifndef PYTHON_PYASTVISITOR_H
#define PYTHON_PYASTVISITOR_H

#include "../AST_pwd.h"

namespace pylang {

/**
 * @brief The AstVisitor class
 *
 * This class designed for auto traversing an abstract syntax tree.
 * Overriden visit() method should return true whenever you want use standard
 * tree traversal. Note that default traversal will visit only statements,
 * including nested statements like class or function body.
 */

class PYLANG_EXPORT IAstVisitor
{
public:
    virtual ~IAstVisitor() {}

    /**
     * @brief visit - visits given node
     * @return true if node descendants should be visited too after node itself
     */
    virtual bool visit(TranslationUnitAst &) = 0;

    /**
     * @brief endVisit - ends node visiting, when descendants already visited;
     * will be not called if visit() returned false
     *
     * @note this method is useful in type inference and any other process that
     * can be applied to node <A> after applying to descendants <B> and <C>
     */
    virtual void endVisit(TranslationUnitAst &) = 0;

    virtual bool visit(ClassDefinitionAst &) = 0;
    virtual void endVisit(ClassDefinitionAst &) = 0;

    virtual bool visit(FunctionDefinitionAst &) = 0;
    virtual void endVisit(FunctionDefinitionAst &) = 0;

    virtual bool visit(IfStmtAst &) = 0;
    virtual void endVisit(IfStmtAst &) = 0;

    virtual bool visit(WhileStmtAst &) = 0;
    virtual void endVisit(WhileStmtAst &) = 0;

    virtual bool visit(ForStmtAst &) = 0;
    virtual void endVisit(ForStmtAst &) = 0;

    virtual bool visit(WithStmtAst &) = 0;
    virtual void endVisit(WithStmtAst &) = 0;

    virtual bool visit(TryStmtAst &) = 0;
    virtual void endVisit(TryStmtAst &) = 0;

    virtual bool visit(ReturnStmtAst &) = 0;
    virtual void endVisit(ReturnStmtAst &) = 0;

    virtual bool visit(ImportAst &) = 0;
    virtual void endVisit(ImportAst &) = 0;

    virtual bool visit(ControlFlowAst &) = 0;
    virtual void endVisit(ControlFlowAst &) = 0;

    virtual bool visit(RaiseAst &) = 0;
    virtual void endVisit(RaiseAst &) = 0;

    virtual bool visit(PrintAst &) = 0;
    virtual void endVisit(PrintAst &) = 0;

    virtual bool visit(DelAst &) = 0;
    virtual void endVisit(DelAst &) = 0;

    virtual bool visit(ExecAst &) = 0;
    virtual void endVisit(ExecAst &) = 0;

    virtual bool visit(GlobalAst &) = 0;
    virtual void endVisit(GlobalAst &) = 0;

    virtual bool visit(AssertAst &) = 0;
    virtual void endVisit(AssertAst &) = 0;

    virtual bool visit(IdentifierAst &) = 0;
    virtual void endVisit(IdentifierAst &) = 0;

    virtual bool visit(LiteralExpressionAst &) = 0;
    virtual void endVisit(LiteralExpressionAst &) = 0;

    virtual bool visit(BinaryExpressionAst &) = 0;
    virtual void endVisit(BinaryExpressionAst &) = 0;

    virtual bool visit(UnaryExpressionAst &) = 0;
    virtual void endVisit(UnaryExpressionAst &) = 0;

    virtual bool visit(AssignmentAst &) = 0;
    virtual void endVisit(AssignmentAst &) = 0;

    virtual bool visit(LambdaAst &) = 0;
    virtual void endVisit(LambdaAst &) = 0;

    virtual bool visit(ConditionExpressionAst &) = 0;
    virtual void endVisit(ConditionExpressionAst &) = 0;

    virtual bool visit(IfExpressionAst &) = 0;
    virtual void endVisit(IfExpressionAst &) = 0;

    virtual bool visit(ForExpressionAst &) = 0;
    virtual void endVisit(ForExpressionAst &) = 0;

    virtual bool visit(ListExpressionAst &) = 0;
    virtual void endVisit(ListExpressionAst &) = 0;

    virtual bool visit(FunctionCallAst &) = 0;
    virtual void endVisit(FunctionCallAst &) = 0;

    virtual bool visit(SliceAst &) = 0;
    virtual void endVisit(SliceAst &) = 0;

    virtual bool visit(SubscriptAst &) = 0;
    virtual void endVisit(SubscriptAst &) = 0;

    virtual bool visit(DictPairAst &) = 0;
    virtual void endVisit(DictPairAst &) = 0;

    virtual bool visit(AttributeAst &) = 0;
    virtual void endVisit(AttributeAst &) = 0;

    virtual bool visit(AliasAst &) = 0;
    virtual void endVisit(AliasAst &) = 0;

    virtual bool visit(NoExpressionAst &) = 0;
    virtual void endVisit(NoExpressionAst &) = 0;

    virtual bool visit(FormalParameterAst &) = 0;
    virtual void endVisit(FormalParameterAst &) = 0;

    virtual bool visit(ExceptAst &) = 0;
    virtual void endVisit(ExceptAst &) = 0;

    virtual bool visit(DocStringAst &) = 0;
    virtual void endVisit(DocStringAst &) = 0;
};

} // namespace pylang

#endif // PYTHON_PYASTVISITOR_H
