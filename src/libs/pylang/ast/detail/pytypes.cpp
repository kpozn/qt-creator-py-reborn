#include "../AST.h"

#ifdef NEED_RUNTIME_CHECKS
#include <QtCore/QDebug>
#endif

namespace pylang {

void PyArguments::accept(IAstVisitor &visitor) const
{
    for (const_iterator it = constBegin(); it != constEnd(); ++it)
    {
        Ast *ast = *it;
        ast->accept(visitor);
    }
}

//unsigned PyExprType::getTypes() const
//{
//    return (_flags & ~Unreachable);
//}

//bool PyExprType::isNumeric() const
//{
//    unsigned numeric = Integer | Long | Float | Complex;
//    return (_flags & numeric);
//}

//bool PyExprType::isCertainType() const
//{
//    unsigned types = getTypes();
//    // is power of 2 and isn't zero
//    return (types != 0) && !(types & (types - 1));
//}

//bool PyExprType::canThrowException() const
//{
//    return (_flags & Unreachable);
//}

} // namespace pylang
