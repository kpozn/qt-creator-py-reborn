#ifndef PYTHON_PYTYPES_H
#define PYTHON_PYTYPES_H

#include <QtCore/QString>
#include <QtCore/QList>
#include "../AST_pwd.h"
#include "../../Support/StringRef.h"

namespace pylang {

typedef StringRef PyString;

class PyArguments : public QList<FormalParameterAst *>
{
public:
    /**
     * @brief enterAst - enters to all nodes in this sequence
     * @param visitor - visitor accepted to all nodes
     */
    void accept(IAstVisitor &visitor) const;
};

} // namespace pylang

#endif // PYTHON_PYTYPES_H
