#include "exceptionwithbacktrace.h"

// for stacktrace
#include <execinfo.h>
#include <cxxabi.h>

static const int BACKTRACE_LIMIT = 15;
static const char SELF_CLASS_NAME[] = "ExceptionWithBacktrace";

enum {
    AddSelfToTrace = 0
};

namespace pylang {

ExceptionWithBacktrace::ExceptionWithBacktrace()
{
    collectStackTrace();
}

ExceptionWithBacktrace::~ExceptionWithBacktrace()
{
}

/**
 * @brief Prints collected information to stdout
 */
void ExceptionWithBacktrace::prettyPrint() const
{
    std::string pretty = prettyPrintedString();
    puts(pretty.c_str());
}

std::string ExceptionWithBacktrace::prettyPrintedString() const
{
    return "Functions backtrace (top is last called):\n"
            + m_stackTrace;
}

void ExceptionWithBacktrace::collectStackTrace()
{
    void *traceBuffer[BACKTRACE_LIMIT];
    int traceSize = backtrace(traceBuffer, BACKTRACE_LIMIT);
    char** strings = backtrace_symbols(traceBuffer, traceSize);
    if (!strings)
    {
        m_stackTrace = "  Cannot retrieve stack trace";
        return;
    }

    for (int i = 0; i < traceSize; ++i) {
        std::string name = getDemangledName(strings[i]);
        if (!name.empty())
        {
            m_stackTrace.append("  ");
            m_stackTrace.append(name);
            m_stackTrace.append("\n");
        }
    }
    if (!m_stackTrace.empty()) {
        m_stackTrace.erase(m_stackTrace.size() - 1);
    }

    if (m_stackTrace.empty())
    {
        m_stackTrace = "  Cannot retrieve stack trace, did you add -rdynamic compiler option?";
        return;
    }
}

std::string ExceptionWithBacktrace::getDemangledName(const char *backtraceLine)
{
    std::string line(backtraceLine);
    size_t leftBraceIdx = line.rfind('(');
    size_t rightBraceIdx = line.rfind('+');

    if (leftBraceIdx == std::string::npos
            || rightBraceIdx == std::string::npos
            || (leftBraceIdx + 2) >= rightBraceIdx)
    {
        // cannot retrieve function name
        return std::string();
    }

    size_t mangledLength = rightBraceIdx - leftBraceIdx - 1;
    std::string mangled = line.substr(leftBraceIdx + 1, mangledLength);

    // convertion with cxxabi.h
    int status = 0;
    char *realname = __cxxabiv1::__cxa_demangle(mangled.c_str(), 0, 0, &status);
    if (0 != status)
        return mangled; // failed to demangle

    std::string ret = realname;
    free(realname);

    if (0 == AddSelfToTrace && std::string::npos != ret.find(SELF_CLASS_NAME))
    {
        // should not add self methods
        return std::string();
    }

    return ret;
}

} // namespace pylang
