#ifndef PYLANG_EXCEPTIONWITHBACKTRACE_H
#define PYLANG_EXCEPTIONWITHBACKTRACE_H

#include <string>

namespace pylang {

class ExceptionWithBacktrace
{
public:
    virtual ~ExceptionWithBacktrace();
    virtual void prettyPrint() const;
    virtual std::string prettyPrintedString() const;

protected:
    ExceptionWithBacktrace();

private:
    std::string m_stackTrace;

    void collectStackTrace();
    std::string getDemangledName(const char* backtraceLine);
};

} // namespace pylang

#endif // PYLANG_EXCEPTIONWITHBACKTRACE_H
