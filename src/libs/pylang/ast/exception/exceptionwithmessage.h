#ifndef PYLANG_EXCEPTIONWITHMESSAGE_H
#define PYLANG_EXCEPTIONWITHMESSAGE_H

#include "exceptionwithbacktrace.h"

namespace pylang {

class ExceptionWithMessage : public ExceptionWithBacktrace
{
public:
    ExceptionWithMessage(const std::string &message);

private:
};

} // namespace pylang

#endif // PYLANG_EXCEPTIONWITHMESSAGE_H
