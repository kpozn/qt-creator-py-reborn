#include "parserexception.h"
#include "../../Support/TokenizedUnit.h"
#include <stdint.h>
#include <stdio.h>

// for stacktrace
#include <execinfo.h>
#include <cxxabi.h>

#include <boost/lexical_cast.hpp>

using boost::lexical_cast;

static const int TOKENS_LINES_LIMIT = 3;

namespace pylang {

/**
 * @class pylang::ParserException
 * On constructing, collects stack trace of demangled functions and
 * pretty-printed tokens from last 3 lines of code
 *
 * @note requires -rdynamic to collect proper stack trace
 */

ParserException::ParserException(const TokenizedUnit &unit,
                                 const SourceLocation &cursor,
                                 const std::string &name)
{
    _header = "Exception " + name + " thrown\n"
            "File parsing stopped at line "
            + lexical_cast<std::string>(cursor.line())
            + ", col "
            + lexical_cast<std::string>(cursor.column());

    collectTokensInfo(unit);
}

ParserException::~ParserException()
{
}

std::string ParserException::prettyPrintedString() const
{
    return  _header + "\n"
            + "Last tokens:\n"
            + _tokens + "\n"
            + ExceptionWithBacktrace::prettyPrintedString();
}

/**
 * @brief Collects pretty-printed tokens from last 3 lines of code
 * @param unit - tokensized unit of parser that caused exception
 */
void ParserException::collectTokensInfo(const TokenizedUnit &unit)
{
    int tokensCount = unit.tokensCount();
    int startIndex = 0;
    int linesCount = 0;
    for (int i = tokensCount; i > 0; --i) {
        const Token &tok = unit.tokenAt(i - 1);
        if (tok.is(T_Newline)) {
            startIndex = i;
            ++linesCount;
            if (linesCount >= TOKENS_LINES_LIMIT)
                break;
        }
    }

    for (int i = startIndex; i < tokensCount; ++i) {
        const Token &tok = unit.tokenAt(i);
        if (tok.hasLiteral()) {
            std::string literal = tok.getLiteral().chars();
            if (tok.is(T_Identifier))
                _tokens += '\'' + literal + '\'';
            else
                _tokens += literal;
        }
        else if (tok.is(T_Newline)) {
            _tokens.append("\\n\n");
            continue;
        }
        else
            _tokens.append(LexemSpelling[tok.lexem()]);
        _tokens += " ";
    }
    if (!_tokens.empty()) {
        _tokens.erase(_tokens.size() - 1);
    }
}

} // namespace pylang
