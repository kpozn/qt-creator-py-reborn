#ifndef PYLANG_PARSEREXCEPTION_H
#define PYLANG_PARSEREXCEPTION_H

#include "exceptionwithbacktrace.h"

namespace pylang {

class TokenizedUnit;
class SourceLocation;

class ParserException : public ExceptionWithBacktrace
{
public:
    ParserException(const TokenizedUnit &unit,
                    const SourceLocation &cursor,
                    const std::string &name);
    ~ParserException();

    std::string prettyPrintedString() const /*override*/;

private:
    std::string _tokens;
    std::string _header;

    void collectTokensInfo(const TokenizedUnit &unit);
};

} // namespace pylang

#endif // PYLANG_PARSEREXCEPTION_H
