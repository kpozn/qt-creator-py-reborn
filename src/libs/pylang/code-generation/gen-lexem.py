#!/usr/bin/env python
# Generates Lexem.h file

import sys

HEADER = """#ifndef PYTHON_LEXEM_H
#define PYTHON_LEXEM_H

//
// This file is generated, changes will be lost
// Generated with gen-lexem.py
//

enum Lexem {
"""

FOOTER = """};

#endif // PYTHON_LEXEM_H
"""

def printHeader(out):
    """
    out should be already opened
    """
    out.write(HEADER)

def generate(out, line):
    """
    out should be already opened
    """
    parts = line.split(' ')
    if len(parts) == 0 or parts[0] != "%token":
        return
    out.write("   T_")
    out.write(parts[1])
    out.write(",")
    out.write("\n")

def printFooter(out):
    """
    out should be already opened
    """
    out.write(FOOTER)

if __name__ == "__main__" :
    out = open("Lexem.h", "w")
    with open("python.tkgen", "r") as source:
        try:
            printHeader(out)
            for line in source:
                generate(out, line)
            printFooter(out)
        finally:
            source.close()
            out.close()
