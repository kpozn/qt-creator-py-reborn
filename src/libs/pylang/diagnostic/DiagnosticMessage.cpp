#include "DiagnosticMessage.h"

namespace pylang {

Diagnostic::Diagnostic()
    : _kind(Warning)
{
}

Diagnostic::~Diagnostic()
{
}

Diagnostic::Kind Diagnostic::kind() const
{
    return _kind;
}

const QString &Diagnostic::message() const
{
    return _message;
}

void Diagnostic::setKind(Kind kind)
{
    _kind = kind;
}

void Diagnostic::setMessage(const QString &message)
{
    _message = message;
}

const SourceLocation &Diagnostic::location() const
{
    return m_location;
}

void Diagnostic::setLocation(const SourceLocation &location)
{
    m_location = location;
}

}
