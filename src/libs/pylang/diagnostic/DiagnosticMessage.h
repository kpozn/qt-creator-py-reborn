#ifndef PYTHON_PYDIAGNOSTICMESSAGE_H
#define PYTHON_PYDIAGNOSTICMESSAGE_H

#include "../pylang_global.h"
#include <QtCore/QString>

#include "../pythonmodel/Document.h"
#include "../Support/SourceRange.h"

namespace pylang {

/**
 * @brief The DiagnosticMessage class - incapsulates diagnostic message
 *
 * Diagnostic message consists of message kind, affected line no and
 * problem description
 */
class PYLANG_EXPORT Diagnostic
{
public:
    enum Kind {
        Error,
        Warning,
        Deprecation,
        TypesMismatch // For RPython
    };

    Diagnostic();
    ~Diagnostic();

    Kind kind() const;
    const QString &message() const;
    void setKind(Kind kind);

    /**
     * @brief setMessage
     * @param message - pure english message without info about line/column
     */
    void setMessage(const QString &message);

    const SourceLocation &location() const;
    void setLocation(const SourceLocation &location);

private:
    QString _message;
    Kind _kind;
    SourceLocation m_location;
};

} // namespace pylang

#endif // PYTHON_PYDIAGNOSTICMESSAGE_H
