#ifndef PYTHON_PYIDIAGNOSTICCLIENT_H
#define PYTHON_PYIDIAGNOSTICCLIENT_H

#include "../pylang_global.h"

namespace pylang {

class Diagnostic;

class PYLANG_EXPORT IDiagnosticClient
{
public:
    virtual void diagnose(const Diagnostic &message) = 0;

    virtual ~IDiagnosticClient() {}
};

} // namespace pylang

#endif // PYTHON_PYIDIAGNOSTICCLIENT_H
