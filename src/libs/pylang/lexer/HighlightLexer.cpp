// Self headers
#include "HighlightLexer.h"
#include "utility.h"

// Qt Library
#include <QtCore/QString>
#include <QtCore/QSet>

using namespace pylang::Internal;

namespace pylang {

/**
 * @brief HighlightLexer
 * @param source - line of source code
 * @param keywords - set of Python keywords
 * @param types - set of builtin or user-defined types
 * All parameters should not be destroyed before read ends
 */
HighlightLexer::HighlightLexer(const QString &source,
                               const QSet<QString> &keywords,
                               const QSet<QString> &types)
    :_state(0)
    ,_src(source.data(), source.length())
    ,_keywords(keywords)
    ,_types(types)
{
}

HighlightLexer::~HighlightLexer()
{
}

/**
 * @brief setState set state stored from previous instance of HighlightLexer
 * @param state - integer representation of internal lexer state
 */
void HighlightLexer::setState(int state)
{
    _state = state;
}

/**
 * @brief getState
 * @return integer representation of internal lexer state
 */
int HighlightLexer::getState() const
{
    return _state;
}

HighlightToken HighlightLexer::read()
{
    _src.setAnchor();
    if (_src.isEnd())
    {
        return HighlightToken(EndOfHighlight, _src.anchor(), 0);
    }

    State state;
    QChar saved;
    parseState(state, saved);

    switch (state)
    {
    case State_String:
        return readStringLiteral(saved);
    case State_MultilineString:
        return readMultiLineStringLiteral(saved);
    case State_DocString:
        return readDocString(saved);
    case State_AfterColon:
        return onAfterColonState();
    case State_Default:
    default:
        return onDefaultState();
    }
}

HighlightToken HighlightLexer::onAfterColonState()
{
    HighlightToken tk = onDefaultState();
    if (tk.format() == HighlightAs_String)
    {
        State state;
        QChar saved;
        parseState(state, saved);
        if (state == State_MultilineString)
        {
            saveState(State_DocString, saved);
            return HighlightToken(HighlightAs_DoxygenComment, tk.begin(), tk.length());
        }
    }
    else if (tk.format() != HighlightAs_Whitespace)
        clearState();
    return tk;
}

QString HighlightLexer::value(const HighlightToken &tk) const
{
    return _src.value(tk.begin(), tk.length());
}

HighlightToken HighlightLexer::onDefaultState()
{
    QChar first = _src.peek();
    _src.move();

    if ((first == '\\')
            && ((_src.peek() == '\n') || _src.peek().isNull()))
    {
        _src.move();
        return HighlightToken(HighlightAs_Whitespace, _src.anchor(), 2);
    }

    if ((first == QChar('.')) && _src.peek().isDigit())
        return readFloatNumber();

    if ((first == '\'') || (first == '\"'))
        return readStringLiteral(first);

    if (first.isLetter() || (first == '_'))
        return readIdentifier();

    if (first.isDigit())
        return readNumber();

    if (first == '#')
    {
        if (_src.peek() == '#')
            return readDoxygenComment();
        return readComment();
    }

    if (first.isSpace())
        return readWhiteSpace();

    return readOperator();
}

/**
 * @brief Lexer::passEscapeCharacter
 * @return returns true if escape sequence doesn't end with newline
 */
void HighlightLexer::checkEscapeSequence(QChar quoteChar)
{
    if (_src.peek() == '\\')
    {
        _src.move();
        QChar ch = _src.peek();
        if ((ch == '\n') || (ch.isNull()))
            saveState(State_String, quoteChar);
    }
}

/**
  reads single-line string literal, surrounded by ' or " quotes
  */
HighlightToken HighlightLexer::readStringLiteral(QChar quoteChar)
{
    QChar ch = _src.peek();
    if ((ch == quoteChar) && (_src.peek(1) == quoteChar))
    {
        saveState(State_MultilineString, quoteChar);
        return readMultiLineStringLiteral(quoteChar);
    }

    while ((ch != quoteChar) && !ch.isNull())
    {
        checkEscapeSequence(quoteChar);
        _src.move();
        ch = _src.peek();
    }
    if (ch == quoteChar)
        clearState();

    _src.move();
    return HighlightToken(HighlightAs_String, _src.anchor(), _src.length());
}

/**
  reads multi-line string literal, surrounded by ''' or """ sequencies
  */
HighlightToken HighlightLexer::readMultiLineStringLiteral(QChar quoteChar)
{
    forever
    {
        QChar ch = _src.peek();
        if (ch.isNull())
            break;

        if ((ch == quoteChar)
                && (_src.peek(1) == quoteChar)
                && (_src.peek(2) == quoteChar))
        {
            clearState();
            _src.move();
            _src.move();
            _src.move();
            break;
        }
        _src.move();
    }

    return HighlightToken(HighlightAs_String, _src.anchor(), _src.length());
}

/**
  reads multi-line string literal, surrounded by ''' or """ sequencies,
  and converts it to docstring
  */
HighlightToken HighlightLexer::readDocString(QChar quoteChar)
{
    HighlightToken tk = readMultiLineStringLiteral(quoteChar);
    return HighlightToken(HighlightAs_DoxygenComment, tk.begin(), tk.length());
}

/**
  reads identifier and classifies it
  */
HighlightToken HighlightLexer::readIdentifier()
{
    QChar ch = _src.peek();
    while (ch.isLetterOrNumber() || (ch == '_'))
    {
        _src.move();
        ch = _src.peek();
    }
    QString value = _src.value();

    // TODO: add python3 support (now false passed for all documents)
    if (isQuote(_src.peek()) && isStringLiteralPrefix(value, false))
    {
        QChar quote = _src.peek();
        _src.move();
        return readStringLiteral(quote);
    }

    HighlightAs format = HighlightAs_Text;
    if (_keywords.contains(value))
        format = HighlightAs_Keyword;
    else if (_types.contains(value))
        format = HighlightAs_Type;
    return HighlightToken(format, _src.anchor(), _src.length());
}

HighlightToken HighlightLexer::readNumber()
{
    if (!_src.isEnd())
    {
        QChar ch = _src.peek();
        if (ch.toLower() == 'b')
        {
            _src.move();
            while (isBinaryDigit(_src.peek()))
                _src.move();
        }
        else if (ch.toLower() == 'o')
        {
            _src.move();
            while (isOctalDigit(_src.peek()))
                _src.move();
        }
        else if (ch.toLower() == 'x')
        {
            _src.move();
            while (isHexDigit(_src.peek()))
                _src.move();
        }
        else
        {
            // either integer or float number
            return readFloatNumber();
        }

        // TODO: add python3 support (now false passed for all documents)
        if (isLongSuffix(_src.peek(), false))
            _src.move();
    }
    return HighlightToken(HighlightAs_Number, _src.anchor(), _src.length());
}

HighlightToken HighlightLexer::readFloatNumber()
{
    enum
    {
        State_INTEGER,
        State_FRACTION,
        State_EXPONENT
    } state;
    state = (_src.peek(-1) == '.') ? State_FRACTION : State_INTEGER;

    for (;;)
    {
        QChar ch = _src.peek();
        if (ch.isNull())
            break;

        if (state == State_INTEGER)
        {
            if (ch == '.')
            {
                state = State_FRACTION;
            }
            else if (!ch.isDigit())
                break;
        }
        else if (state == State_FRACTION)
        {
            if ((ch == 'e') || (ch == 'E'))
            {
                QChar next = _src.peek(1);
                QChar next2 = _src.peek(2);
                bool isExp = next.isDigit()
                        || (((next == '-') || (next == '+')) && next2.isDigit());
                if (isExp)
                {
                    _src.move();
                    state = State_EXPONENT;
                }
                else
                    break;
            } else if (!ch.isDigit())
                break;
        }
        else if (!ch.isDigit())
                break;
        _src.move();
    }
    {
        QChar ch = _src.peek();
        // TODO: add python3 support (now false passed for all documents)
        if (((state == State_INTEGER) && isLongSuffix(ch, false))
                || isComplexSuffix(ch))
            _src.move();
    }
    return HighlightToken(HighlightAs_Number, _src.anchor(), _src.length());
}

/**
  reads single-line python comment, started with "#"
  */
HighlightToken HighlightLexer::readComment()
{
    QChar ch = _src.peek();
    while ((ch != '\n') && !ch.isNull())
    {
        _src.move();
        ch = _src.peek();
    }
    return HighlightToken(HighlightAs_Comment, _src.anchor(), _src.length());
}

/**
  reads single-line python doxygen comment, started with "##"
  */
HighlightToken HighlightLexer::readDoxygenComment()
{
    QChar ch = _src.peek();
    while ((ch != '\n') && !ch.isNull())
    {
        _src.move();
        ch = _src.peek();
    }
    return HighlightToken(HighlightAs_DoxygenComment, _src.anchor(), _src.length());
}

/**
  reads whitespace
  */
HighlightToken HighlightLexer::readWhiteSpace()
{
    while (_src.peek().isSpace())
    {
        _src.move();
    }
    return HighlightToken(HighlightAs_Whitespace, _src.anchor(), _src.length());
}

/**
  reads punctuation symbols, excluding some special
  */
HighlightToken HighlightLexer::readOperator()
{
    static const QString excluded = "\\\'\"_#";
    QChar ch = _src.peek();
    while (ch.isPunct() && !excluded.contains(ch))
    {
        _src.move();
        ch = _src.peek();
    }
    //! check if operator is colon, so next line can be doxygen comment
    if (_src.peek(-1) == ':')
    {
        saveState(State_AfterColon, '\0');
    }
    return HighlightToken(HighlightAs_Operator, _src.anchor(), _src.length());
}

void HighlightLexer::clearState()
{
    _state = 0;
}

void HighlightLexer::saveState(State state, QChar savedData)
{
    _state = (state << 16) | static_cast<int>(savedData.unicode());
}

void HighlightLexer::parseState(State &state, QChar &savedData) const
{
    state = static_cast<State>(_state >> 16);
    savedData = static_cast<ushort>(_state);
}

} // namespace pylang
