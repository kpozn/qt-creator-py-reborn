#ifndef PYTHON_HIGHLIGHTLEXER_H
#define PYTHON_HIGHLIGHTLEXER_H

#include "HighlightToken.h"
#include "SourceString.h"
#include <QtCore/QSet>

namespace pylang {

/**
 * @brief The HighlightLexer class scans source code for fast pre-highlighting
 *
 * Usualy this lexer works with single line of source code, using the internal
 * state associated with given string to handle Python multiline strings
 */
class PYLANG_EXPORT HighlightLexer
{
    HighlightLexer(const HighlightLexer &other);
    void operator =(const HighlightLexer &other);

public:
    enum State {
        State_Default,
        State_String,
        State_MultilineString,
        State_DocString,
        State_AfterColon
    };

    HighlightLexer(const QString &source,
                   const QSet<QString> &keywords,
                   const QSet<QString> &types);
    ~HighlightLexer();

    /**
     * @brief setState - retrieve internal state to save it as user data
     * in parsed text block. Returned value isn't equal to enum State
     * @param state internal state of lexer
     */
    void setState(int state);

    int getState() const;

    HighlightToken read();
    QString value(const HighlightToken& tk) const;

private:
    HighlightToken onAfterColonState();
    HighlightToken onDefaultState();

    void checkEscapeSequence(QChar quoteChar);
    HighlightToken readStringLiteral(QChar quoteChar);
    HighlightToken readMultiLineStringLiteral(QChar quoteChar);
    HighlightToken readDocString(QChar quoteChar);
    HighlightToken readIdentifier();
    HighlightToken readNumber();
    HighlightToken readFloatNumber();
    HighlightToken readComment();
    HighlightToken readDoxygenComment();
    HighlightToken readWhiteSpace();
    HighlightToken readOperator();

    void clearState();
    void saveState(State state, QChar savedData);
    void parseState(State &state, QChar &savedData) const;

    int _state;
    Internal::QSourceString _src;
    const QSet<QString> &_keywords;
    const QSet<QString> &_types;
};

} // namespace pylang

#endif // PYTHON_HIGHLIGHTLEXER_H
