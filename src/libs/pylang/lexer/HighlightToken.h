#ifndef PYTHON_TEXTFORMAT_H_c2dcd31f_7334_4e6a_a37d_07b43ae3a41c
#define PYTHON_TEXTFORMAT_H_c2dcd31f_7334_4e6a_a37d_07b43ae3a41c

#include "../pylang_global.h"

namespace pylang {

enum HighlightAs {
    HighlightAs_Number,
    HighlightAs_String,
    HighlightAs_Keyword,
    HighlightAs_Type,
    HighlightAs_Member,
    HighlightAs_Operator,
    HighlightAs_Comment,
    HighlightAs_DoxygenComment,
    HighlightAs_Whitespace,
    HighlightAs_Text,
    HighlightAs_Import,
        // For future use:
    // HighlightAs_DisabledCode
    // HighlightAs_LocalVar

    EndOfHighlight,
    HighlightAsCount = EndOfHighlight
};

class PYLANG_EXPORT HighlightToken
{
public:
    HighlightToken(): _format(HighlightAs_Text), _begin(0), _length(0)
    { }

    HighlightToken(HighlightAs format, unsigned begin, unsigned length)
        :_format(format)
        ,_begin(begin)
        ,_length(length)
    { }

    inline HighlightAs format() const { return _format; }
    inline unsigned begin() const { return _begin; }
    inline unsigned end() const { return _begin + _length; }
    inline unsigned length() const { return _length; }

private:
    HighlightAs _format;
    unsigned _begin;
    unsigned _length;
};

} // namespace pylang

#endif // PYTHON_TEXTFORMAT_H_c2dcd31f_7334_4e6a_a37d_07b43ae3a41c
