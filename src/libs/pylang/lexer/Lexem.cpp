#include "Lexem.h"

#define LEXEM(Enum, Description) Description,

namespace pylang {

const char *LexemSpelling[LexemsCount] = {
    #include "tokens.def"
};

} // namespace pylang

#undef LEXEM
