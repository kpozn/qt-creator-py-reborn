#ifndef PYLANG_LEXEM_H
#define PYLANG_LEXEM_H

namespace pylang {

#define LEXEM(Enum, Description) T_##Enum,
enum Lexem {
    // Retrieve only names via LEXEM() macro
    #include "tokens.def"
    LexemsCount
};
#undef LEXEM

extern const char *LexemSpelling[LexemsCount];

} // namespace pylang

#endif // PYLANG_LEXEM_H
