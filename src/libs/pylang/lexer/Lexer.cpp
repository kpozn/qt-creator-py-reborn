#include "Lexer.h"
#include "../parser/Parser.h"
#include "../diagnostic/IDiagnosticClient.h"
#include "../diagnostic/DiagnosticMessage.h"

#include "utility.h"

#include <QtCore/QString>
#include <vector>
#include <cctype>
#include <cstdlib>
#include <cstring>
#include <QDebug>

#include <assert.h>
// TODO: remove or replace all commented strings
using namespace pylang::Internal;

namespace pylang {
namespace {

/**
 * @brief compWithKeyword - compares two std::string pointers
 * @param f - first string
 * @param s - second string
 * @return -1, 0 and 1 respectively
 */
int compWithKeyword(const void *f, const void *s)
{
    const std::string *first = reinterpret_cast<const std::string *>(f);
    const std::string *second = reinterpret_cast<const std::string *>(s);
    return first->compare(*second);
}

const std::string KEYWORDS[] = {
    "and",
    "as",
    "assert",
    "break",
    "class",
    "continue",
    "def",
    "del",
    "elif",
    "else",
    "except",
    "exec",
    "finally",
    "for",
    "from",
    "global",
    "if",
    "import",
    "in",
    "is",
    "lambda",
    "not",
    "or",
    "pass",
    "print", // excluded from Python 3
    "raise",
    "return",
    "try",
    "while",
    "with",
    "yield"
};

const Lexem KW_TOKENS[] = {
    T_KwAnd,
    T_KwAs,
    T_KwAssert,
    T_KwBreak,
    T_KwClass,
    T_KwContinue,
    T_KwDef,
    T_KwDel,
    T_KwElif,
    T_KwElse,
    T_KwExcept,
    T_KwExec,
    T_KwFinally,
    T_KwFor,
    T_KwFrom,
    T_KwGlobal,
    T_KwIf,
    T_KwImport,
    T_KwIn,
    T_KwIs,
    T_KwLambda,
    T_KwNot,
    T_KwOr,
    T_KwPass,
    T_KwPrint,
    T_KwRaise,
    T_KwReturn,
    T_KwTry,
    T_KwWhile,
    T_KwWith,
    T_KwYield
};

enum {
    State_Normal,
    State_LookForIndent,
    State_EndOfFileReached
};

} // anonymous namespace

void Lexer::onReadStart()
{
    m_startLocation = m_endLocation;
   // savePosition();
}

/**
 * \brief performs post-reading procedures, saves
 * token location and Literal (for identifiers only)
 */
void Lexer::onReadEnd()
{
    switch (m_ret.lexem())
    {
    case T_Colon:
        m_canAcceptDocString = true;
        break;
    case T_Newline:
    case T_Indent:
        break;
    default:
        m_canAcceptDocString = false;
        break;
    }

    // TODO: save state to make possible dropping tokens queue
    resetLocation(m_endLocation);

    m_ret.setRange(SourceRange(m_startLocation, m_endLocation));
    if (m_ret.is(T_Identifier))
    {
        uint32_t start = getOffset(m_ret.range().first());
        uint32_t len = getOffset(m_ret.range().last()) - start;
        StringRef id = m_helper.findOrInsertLiteral(m_source + start, len);
        m_ret.setLiteral(id);
    }
}

Lexem Lexer::readHelper()
{
    again:
    if (m_state == State_EndOfFileReached)
    {
        if (!m_indentStack.empty())
        {
            m_indentStack.pop_back();
            return T_Dedent;
        }
        return T_EndOfFile;
    }

    if (m_state == State_LookForIndent)
    {
        Lexem dlKind = passWhitespace();
        if (dlKind == T_EndOfFile)
            dlKind = indentAction();
        if (dlKind != T_EndOfFile)
            return dlKind;
    }
    else
    {
        Lexem kind = passWhitespace();
        if (kind != T_EndOfFile)
            return kind;
    }

   // savePosition();
  //  m_startLocation = m_savedLocation;
    resetLocation(m_startLocation);

    if (peekChar() == 0) {
        m_state = State_EndOfFileReached;
        return T_Newline;
    }

    Lexem kind = lookForString();
    if (kind == T_EndOfFile)
        kind = lookForIdentifier();
    if (kind == T_EndOfFile)
        kind = lookForNumber();
    if (kind == T_EndOfFile)
        kind = lookForOperator();
    if (kind == T_EndOfFile)
        kind = lookForDelimiter();

    // unexpected character
    if (kind == T_EndOfFile)
    {
        reportUnexpectedChar(peekChar());
        getChar();
        goto again;
    }

    return kind;
}

const SourceLocation &Lexer::location() const
{
    return m_startLocation;
}

bool Lexer::implicitLineJoiningOn() const
{
    return (m_implicitLineJoiningLevel > 0);
}

unsigned Lexer::intendation() const
{
    return m_indentCurrent;
}

void Lexer::insertToken(Token tk)
{
    switch (tk.lexem())
    {
    case T_LeftBrace:
    case T_LeftBracket:
    case T_LeftParen:
        ++m_implicitLineJoiningLevel;
        break;
    case T_RightBrace:
    case T_RightBracket:
    case T_RightParen:
        if (m_implicitLineJoiningLevel)
            --m_implicitLineJoiningLevel;
        break;
    default:
        break;
    }
}

/**
 * @brief getChar - gets char from source, renews _location info,
 * doesn't renew _location.begin()
 * @return
 */
char Lexer::getChar()
{
    char ret = peekChar();
    ++m_it;
    return ret;
}

bool Lexer::tryGetChar(char test)
{
    if (peekChar() == test) {
        getChar();
        return true;
    }
    return false;
}

/**
 * @brief tryGetChar try to get char if it's in given set
 * @param test set of characters those can be read
 * @return true if char got successfuly
 */
bool Lexer::tryGetChar(const char *test)
{
    if ((peekChar() != '\0')
            && (strchr(test, *m_it) != NULL))
    {
        getChar();
        return true;
    }
    return false;
}

char Lexer::peekChar(unsigned step)
{
    const char *ptr = m_it + step;

    if (ptr - m_source >= static_cast<ptrdiff_t>(m_sourceSize))
        return '\0';
    return (*ptr);
}

inline char Lexer::peekChar()
{
    return (*m_it);
}

inline void Lexer::moveForward(unsigned step)
{
    for (uint i = 0; i < step; ++i)
        getChar();
}

//inline void Lexer::savePosition()
//{
//    m_savedLocation = m_endLocation;
//    m_savedLocation.setOffset(m_it - m_source);
//}

//inline void Lexer::restorePosition()
//{
//    m_endLocation = m_savedLocation;
//    m_it = m_source + m_savedLocation.begin();
//}

inline Diagnostic Lexer::prepareErrorMessage() const
{
    Diagnostic ret;
    ret.setKind(Diagnostic::Error);

    SourceLocation current;
    resetLocation(current);
    ret.setLocation(current);

    return ret;
}

/**
 * @brief passWhitespace passes whitespace collecting info about indent level,
 * but not generating indent/dedent tokens
 * @return T_Newline or T_EndOfFile
 */
inline Lexem Lexer::passWhitespace()
{
  //  savePosition();
    resetLocation(m_startLocation);
    m_indentCurrent = 0;
    forever {
        char ch = peekChar();
        switch (ch) {
        case '\0': {
            resetLocation(m_startLocation);
            //savePosition();
            m_state = State_EndOfFileReached;
            return T_Newline;
        }
            break;
        case '\r':
            getChar();
            if (!m_implicitLineJoiningLevel) {
             //   savePosition();
                resetLocation(m_startLocation);
                tryGetChar('\n');
                m_state = State_LookForIndent;
                return T_Newline;
            }
            break;
        case '\n':
            getChar();
            if (!m_implicitLineJoiningLevel) {
                resetLocation(m_startLocation);
               // savePosition();
                tryGetChar('\r');
                m_state = State_LookForIndent;
                return T_Newline;
            }
            break;
        case '#':
            getChar();
            while ((peekChar() != '\n') && (peekChar() != '\0'))
                getChar();
            m_indentCurrent = 0;
            break;
        case ' ':
            getChar();
            m_indentCurrent += 1;
            break;
        case '\t':
            getChar();
            m_indentCurrent += 4;
            break;
        case '\\':
            getChar();
            if (tryGetChar('\n'))
                break;
        default:
            return T_EndOfFile;
        }
    }
}

/**
 * @brief indentAction generates indent/dedent tokens using info collected
 * by passWhitespace()
 * @return T_Indent, T_DL_DEDENT or T_EndOfFile
 */
inline Lexem Lexer::indentAction()
{
    unsigned level = m_indentCurrent;
    if (m_indentStack.empty())
    {
        if (level > 0)
        {
            m_indentStack.push_back(level);
            m_state = State_Normal;
            return T_Indent;
        }
    } else
    {
        if (level > m_indentStack.back())
        {
            m_indentStack.push_back(level);
            m_state = State_Normal;
            return T_Indent;
        }
        if (level < m_indentStack.back())
            return dedentAction();
    }
    m_state = State_Normal;
    return T_EndOfFile;
}

inline Lexem Lexer::dedentAction()
{
    unsigned level(m_indentCurrent);
    unsigned previousIndent = (m_indentStack.size() <= 1)
            ? 0
            : (m_indentStack.at(m_indentStack.size() - 2));

    if ((previousIndent < level) && (m_helper.isDiagnosticEnabled()))
    {
        Diagnostic mess = prepareErrorMessage();
        mess.setMessage(
                    QObject::tr("should be indented up to %1 spaces")
                    .arg(QString::number(previousIndent))
                    );
        m_helper.pushDiagnostic(mess);
    }
    m_indentStack.pop_back();

    if (previousIndent <= level)
    {
        m_state = State_Normal;
    }
    else
    {
        /* There can be at least one extra T_DL_DEDENT,
          we should return to the line start */
        m_it = 1 + m_source + m_lineOffsets[m_latestLine];
    }
    return T_Dedent;
}

/**
 * @brief lookForOperator - looks for Python operator token
 * @param it - pointer to current text position
 * @return T_EndOfFile if token is not operator, appropriate token kind otherwise
 */
inline Lexem Lexer::lookForOperator()
{
    const char pattern[3] = {peekChar(), peekChar(1), peekChar(2)};
    switch (pattern[0])
    {
        case '!':
            {
                getChar();
                if (pattern[1] == '=') {
                    getChar();
                    return T_OpNE;
                }
                return T_OpNot;
            }
        case '+':
            {
                getChar();
                if (pattern[1] == '=') {
                    getChar();
                    return T_AugAdd;
                }
                return T_OpAdd;
            }
        case '-':
            {
                getChar();
                if (pattern[1] == '=') {
                    getChar();
                    return T_AugSub;
                }
                return T_OpSub;
            }
        case '*':
            {
                getChar();
                if (pattern[1] == '*') {
                    getChar();
                    if (pattern[2] == '=') {
                        getChar();
                        return T_AugPower;
                    }
                    return T_OpPower;
                }
                if (pattern[2] == '=') {
                    getChar();
                    return T_AugMul;
                }
                return T_OpMul;
            }
        case '/':
            {
                getChar();
                if (pattern[1] == '/') {
                    getChar();
                    if (pattern[2] == '=') {
                        getChar();
                        return T_AugFloorDiv;
                    }
                    return T_OpFloorDiv;
                }
                if (pattern[2] == '=') {
                    getChar();
                    return T_AugDiv;
                }
                return T_OpDiv;
            }
        case '%':
            {
                getChar();
                if (pattern[1] == '=') {
                    getChar();
                    return T_AugMod;
                }
                return T_OpMod;
            }
        case '&':
            {
                getChar();
                if (pattern[1] == '=') {
                    getChar();
                    return T_AugBitand;
                }
                return T_OpBitwiseAnd;
            }
        case '^':
            {
                getChar();
                if (pattern[1] == '=') {
                    getChar();
                    return T_AugBitxor;
                }
                return T_OpBitwiseXor;
            }
        case '|':
            {
                getChar();
                if (pattern[1] == '=') {
                    getChar();
                    return T_AugBitor;
                }
                return T_OpBitwiseOr;
            }
        case '~':
            {
                getChar();
                return T_OpComplement;
            }
        case '.':
            {
                getChar();
                return T_OpDot;
            }
        case '=': // ==, =
            {
                getChar();
                if (pattern[1] == '=') {
                    getChar();
                    return T_OpEQ;
                }
                return T_Assign;
            }
        case '<': // <, <<, <<=, <=
            {
                getChar();
                if (pattern[1] == '>') {
                    getChar();
                    return T_OpNE2;
                }
                if (pattern[1] == '<') {
                    getChar();
                    if (pattern[2] == '=') {
                        getChar();
                        return T_AugLShift;
                    }
                    return T_OpLeftShift;
                }
                if (pattern[1] == '=') {
                    getChar();
                    return T_OpLE;
                }
                return T_OpLT;
            }
        case '>': // >, >>, >>=, >=
            {
                getChar();
                if (pattern[1] == '>') {
                    getChar();
                    if (pattern[2] == '=') {
                        getChar();
                        return T_AugRShift;
                    }
                    return T_OpRightShift;
                }
                if (pattern[1] == '=') {
                    getChar();
                    return T_OpGE;
                }
                return T_OpGT;
            }
        case '@':
            getChar();
            return T_OpAt;
        default:
            return T_EndOfFile;
    }
}

/**
 * @brief lookForDelimiter - looks for Python delimiter character
 * @param it - pointer to current text position
 */
inline Lexem Lexer::lookForDelimiter()
{
    const char test = peekChar();
    switch (test) {
        case '(':
            getChar();
            ++m_implicitLineJoiningLevel;
            return T_LeftParen;
        case ')':
            getChar();
            --m_implicitLineJoiningLevel;
            return T_RightParen;
        case '[':
            getChar();
            ++m_implicitLineJoiningLevel;
            return T_LeftBracket;
        case ']':
            getChar();
            --m_implicitLineJoiningLevel;
            return T_RightBracket;
        case '{':
            getChar();
            ++m_implicitLineJoiningLevel;
            return T_LeftBrace;
        case '}':
            getChar();
            --m_implicitLineJoiningLevel;
            return T_RightBrace;
        case ';':
            getChar();
            return T_Semicolon;
        case ':':
            getChar();
            return T_Colon;
        case ',':
            getChar();
            return T_Comma;
        case '`':
            getChar();
            return T_Backquote;
        default:
            return T_EndOfFile;
    }
}

/**
 * @brief lookAtIdentifier - read tail of identifier
 * @param it - pointer to current text position
 * @return T_IDENTIFIER if identifier has isn't reserved, appropriate token otherwise
 */
inline Lexem Lexer::lookForIdentifier()
{
    if (!isIdentifierHead(peekChar()))
        return T_EndOfFile;

    const char *const start = m_it;
    do {
        getChar();
    } while (isIdentifierMiddle(peekChar()));
    std::string word(start, m_it - start);

    if (word == "True") {
        return T_True;
    }
    if (word == "False") {
        return T_False;
    }
    if (word == "None") {
        return T_None;
    }

    if (isStringLiteralPrefix(word, m_helper.isPython3()) && (isQuote(peekChar())))
    {
        return lookForString();
    }

    return determineKeyword(word);
}

void Lexer::reportMissingQuoteError()
{
    if (m_helper.isDiagnosticEnabled())
    {
        Diagnostic mess = prepareErrorMessage();
        mess.setMessage(
                    QObject::tr("no matching quote at the end of file")
                    );
        m_helper.pushDiagnostic(mess);
    }
}

/**
 * @brief lookForString determines string literal
 * @return T_EndOfFile or T_LT_STRING
 */

Lexem Lexer::lookForString()
{
    char appropQuote;
    if (tryGetChar('\''))
        appropQuote = '\'';
    else if (tryGetChar('\"'))
        appropQuote = '\"';
    else
        return T_EndOfFile;

    bool isMultiline = (peekChar() == appropQuote)
            && (peekChar(1) == appropQuote);
    forever {
        if (peekChar() == '\\')
        {
            moveForward(2);
            continue;
        }
        if (peekChar() == 0)
        {
            reportMissingQuoteError();
            return T_String;
        }
        if (!isMultiline && (peekChar() == '\n'))
        {
            reportMissingQuoteError();
            return T_String;
        }
        if (!isMultiline && tryGetChar(appropQuote))
        {
            break;
        }
        if (isMultiline && (peekChar() == appropQuote)
                && (peekChar(1) == appropQuote)
                && (peekChar(2) == appropQuote))
        {
            moveForward(3);
            break;
        }
        getChar();
    }

    if (m_canAcceptDocString && isMultiline)
        return T_DocString;
    return T_String;
}

/**
 * @brief lookForNumber determines number literal
 * @return T_EndOfFile if token is not number
 */
Lexem Lexer::lookForNumber()
{
    if ((peekChar() == '.') && (isDecimalDigit(peekChar(1))))
    {
        moveForward(2);
        return lookForDecimal(true);
    }
    if (!isDecimalDigit(peekChar()))
        return T_EndOfFile;

    if (getChar() != '0')
        return lookForDecimal(false);

    if (tryGetChar("bB"))
        while (isBinaryDigit(peekChar()))
            getChar();
    else if (tryGetChar("oO"))
        while (isOctalDigit(peekChar()))
            getChar();
    else if (tryGetChar("xX"))
        while (isHexDigit(peekChar()))
            getChar();
    else
        return lookForDecimal(false);

    if (tryGetChar("lL") && m_helper.isPython3() && m_helper.isDiagnosticEnabled())
    {
        Diagnostic diagnostic;
        diagnostic.setKind(Diagnostic::Error);
        diagnostic.setLocation(m_startLocation);
        diagnostic.setMessage("Long integer deprecated in python3");
    }
    return T_IntegerValue;
}

/**
 * @brief lookForDecimal
 * @param insideFract true if lexer have passed '.' already
 * @return T_LT_INTEGER, T_LT_FLOAT or T_LT_COMPLEX
 */
Lexem Lexer::lookForDecimal(bool insideFract)
{
    NumberPart part = insideFract ? NumberPart_FRACTION : NumberPart_INTEGER ;

    forever
    {
        if (isValidFloatChar(part))
            getChar();
        else
            break;
    }
    if (tryGetChar("jJ"))
        return T_ComplexValue;
    if (part == NumberPart_INTEGER)
    {
        if (tryGetChar("lL") && m_helper.isPython3() && m_helper.isDiagnosticEnabled())
        {
            Diagnostic diagnostic;
            diagnostic.setKind(Diagnostic::Error);
            diagnostic.setLocation(m_startLocation);
            diagnostic.setMessage("Long integer deprecated in python3");
        }
        return T_IntegerValue;
    }
    return T_FloatValue;
}

inline bool Lexer::isValidFloatChar(NumberPart &part)
{
    switch (part)
    {
        case NumberPart_INTEGER:
            if (peekChar() == '.')
            {
                part = NumberPart_FRACTION;
                break;
            }
            // don't break
        case NumberPart_FRACTION:
            if ((peekChar() == 'e') || (peekChar() == 'E')) {
                char next = peekChar(1);
                char next2 = peekChar(2);
                bool isExp = isDecimalDigit(next)
                        || (((next == '-') || (next == '+'))
                            && isDecimalDigit(next2));
                if (isExp) {
                    getChar();
                    part = NumberPart_EXPONENT;
                    break;
                } else
                    return false;
            } else if (!isDecimalDigit(peekChar()))
                return false;
        case NumberPart_EXPONENT:
        default:
            if (!isDecimalDigit(peekChar()))
                return false;
    }
    return true;
}

void Lexer::reportUnexpectedChar(char unexpected)
{
    if (m_helper.isDiagnosticEnabled())
    {
        Diagnostic mess = prepareErrorMessage();
        mess.setMessage(
                    QObject::tr("Unexpected character: %1")
                    .arg(QString(QChar(unexpected)))
                    );
        m_helper.pushDiagnostic(mess);
    }
}

/**
 * @brief determineKeyword - determines if word is Python keyword
 * @param word - source code substring (ending zero isn't required)
 * @return T_IDENTIFIER if token is not keyword, appropriate token otherwise
 *
 * NOTE: can be optimized using big switch instead of binary search
 */
Lexem Lexer::determineKeyword(const std::string &word)
{
    const void *ptr = bsearch(&word, KEYWORDS,
                              sizeof(KEYWORDS) / sizeof(word), sizeof(word),
                              compWithKeyword);
    if (ptr == (const void *)0) {
        return T_Identifier;
    }
    const std::string *keyword = reinterpret_cast<const std::string *>(ptr);
    return KW_TOKENS[keyword - KEYWORDS];
}

Lexer::Lexer(UnitBuilder unitBuilder, const char *source, unsigned sourceSize)
    : m_helper(unitBuilder)
    , m_lineOffsets(m_helper.tokenizedUnit().lineOffsets())
    , m_latestLine(0)
    , m_source(source)
    , m_sourceSize(sourceSize)
    , m_it(source)
    , m_state(State_LookForIndent)
    , m_canAcceptDocString(false)
    , m_implicitLineJoiningLevel(0)
    , m_indentCurrent(0)
{
}

Lexer::~Lexer()
{
}

void Lexer::init()
{
    // TODO: add '\r' handling?
    m_lineOffsets.push_back(0); // The first line has zero offset
    const char *end(m_source + m_sourceSize);
    for (const char *it = m_source; it < end; ++it)
    {
        if (*it == '\n')
            m_lineOffsets.push_back(it - m_source);
    }
    m_latestLine = 0;
}

Token Lexer::read()
{
    if (0 < m_lookAhead.size())
    {
        m_ret = m_lookAhead.front();
        m_lookAhead.pop();
    }
    else
    {
        onReadStart();
        m_ret = Token(readHelper());
        onReadEnd();
    }

    return m_ret;
}

Lexem Lexer::lookAhead(unsigned step)
{
    assert((0 != step) && "step=0 cannot be passed to Lexer::lookAhead()");

    while (m_lookAhead.size() < step)
    {
        onReadStart();
        m_ret = Token(readHelper());
        onReadEnd();
        m_lookAhead.push_back(m_ret);
    }

    return m_lookAhead[step - 1].lexem();
}

const std::vector<Lexem> &Lexer::bracesStack() const
{
    return m_braces;
}

void Lexer::insertBrace(Lexem brace)
{
    switch (brace)
    {
        case T_RightBrace:
            if (!m_braces.empty() && m_braces.back() == T_LeftBrace)
                m_braces.pop_back();
            break;
        case T_RightBracket:
            if (!m_braces.empty() && m_braces.back() == T_LeftBracket)
                m_braces.pop_back();
            break;
        case T_RightParen:
            if (!m_braces.empty() && m_braces.back() == T_LeftParen)
                m_braces.pop_back();
            break;
        case T_LeftBrace:
        case T_LeftBracket:
        case T_LeftParen:
            m_braces.push_back(brace);
            break;
        default:
            break;
    }
}

uint32_t Lexer::getOffset(const SourceLocation &loc) const
{
    return m_lineOffsets[loc.line()] + loc.column();
}

/**
   @internal Can impicitely increase m_latestLine
 */
uint32_t Lexer::getLineNo() const
{
    uint32_t offset = m_it - m_source;
    uint32_t next = m_latestLine + 1;
    for (; next < m_lineOffsets.size(); ++next)
        if (m_lineOffsets[next] >= offset)
            break;
    m_latestLine = next - 1;

    return m_latestLine;
}

void Lexer::resetLocation(SourceLocation &loc) const
{
    uint32_t offset = m_it - m_source;
    uint32_t line = getLineNo();

    loc.setLine(line);
    loc.setColumn(offset - m_lineOffsets[line]);
}

} // namespace pylang
