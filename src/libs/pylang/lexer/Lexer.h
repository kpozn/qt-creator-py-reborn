#ifndef PYTHON_LEXER_H
#define PYTHON_LEXER_H

#include "Token.h"
#include "../Support/SourceLocation.h"
#include "../diagnostic/DiagnosticMessage.h"
#include "../ast/UnitBuilder.h"
#include "TokensQueue.h"
#include <vector>

namespace pylang {

class IDiagnosticClient;
class TranslationUnit;

class PYLANG_EXPORT Lexer
{
public:
    /**
     * @brief Lexer
     * @param client - if null, lexer will not report any error
     * @param source
     * @param sourceSize
     */
    Lexer(UnitBuilder unitBuilder, const char *source, unsigned sourceSize);
    ~Lexer();

    void init();

    /**
     * @brief read
     * @return token kind
     */
    Token read();

    /**
       @brief lookAhead - looks for the n-th token in the stream
       @param step - step for lookup, cannot be 0
     */
    Lexem lookAhead(unsigned step);

    /**
     * @brief location - returns location in source for latest token read
     * @return
     */
    const SourceLocation &location() const;

    /**
     * @brief bracesStack - stack of unclosed braces ({[
     * @return const ref to stack
     */
    const std::vector<Lexem> &bracesStack() const;

    /**
     * @brief insertBrace - hook for parser's error recovery, checks if token is
     * one of ({[]}) and fixes internal lexer state
     * @param brace - any token kind, function does nothing if it's not brace
     */
    void insertBrace(Lexem brace);

private: // methods
    enum NumberPart {
        NumberPart_INTEGER,
        NumberPart_FRACTION,
        NumberPart_EXPONENT
    };

    void onReadStart();
    void onReadEnd();
    Lexem readHelper();
    bool implicitLineJoiningOn() const;
    unsigned intendation() const;
    void insertToken(Token tk);
    inline char getChar();
    bool tryGetChar(char test);
    bool tryGetChar(const char *test);
    char peekChar(unsigned step);
    inline char peekChar();
    inline void moveForward(unsigned step);
    inline Diagnostic prepareErrorMessage() const;
    inline Lexem passWhitespace();
    inline Lexem indentAction();
    inline Lexem dedentAction();
    inline Lexem lookForOperator();
    inline Lexem lookForDelimiter();
    inline Lexem lookForIdentifier();
    inline void reportMissingQuoteError();
    inline Lexem lookForString();
    inline Lexem lookForNumber();
    inline Lexem lookForDecimal(bool insideFract);
    inline bool isValidFloatChar(NumberPart &part);
    inline void reportUnexpectedChar(char unexpected);
    inline Lexem determineKeyword(const std::string &word);

    inline uint32_t getOffset(const SourceLocation &loc) const;
    inline uint32_t getLineNo() const;
    inline void resetLocation(SourceLocation &loc) const;

private: // members
    UnitBuilder m_helper;

    Internal::TokensQueue m_lookAhead;
    std::vector<uint32_t>& m_lineOffsets;
    mutable uint32_t m_latestLine;
    Token m_ret;

    const char *const m_source;
    const unsigned m_sourceSize;
    const char *m_it;
    int m_state;
    bool m_canAcceptDocString;

    int m_implicitLineJoiningLevel;
    int m_indentCurrent;
    std::vector<unsigned> m_indentStack;
    SourceLocation m_endLocation;
    SourceLocation m_startLocation;
    SourceLocation m_savedLocation;
    std::vector<Lexem> m_braces;
};

class LexerState
{
    uint32_t latestLine;
};

} // namespace pylang

#endif // PYTHON_LEXER_H
