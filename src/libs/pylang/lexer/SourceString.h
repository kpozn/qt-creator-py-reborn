#ifndef SOURCESTRING_H
#define SOURCESTRING_H

#include <string>
#include <QtCore/QString>

namespace pylang {
namespace Internal {

template <class tchar, class tstring>
class SourceString
{
public:
    SourceString(const tchar* text, unsigned length)
        :_text(text)
        ,_textLength(length)
        ,_position(0)
        ,_markedPosition(0)
    {}

    inline void setAnchor()
    {
        _markedPosition = _position;
    }

    inline void move()
    {
        ++_position;
    }

    inline size_t length() const
    {
        return (_position - _markedPosition);
    }

    inline size_t anchor() const
    {
        return (_markedPosition);
    }

    inline bool isEnd() const
    {
        return (_position >= _textLength);
    }

    inline tchar peek(int offset = 0) const
    {
        unsigned pos = static_cast<unsigned>(_position + offset);
        if (pos >= _textLength) {
            return '\0';
        }
        return _text[pos];
    }

    inline tstring value() const
    {
        const tchar* start = _text + _markedPosition;
        return tstring(start, length());
    }

    inline tstring value(unsigned begin, unsigned length) const
    {
        return tstring(_text + begin, length);
    }

private:
    const tchar* _text;
    unsigned _textLength;
    unsigned _position;
    unsigned _markedPosition;
};

typedef SourceString<char, std::string> CSourceString;
typedef SourceString<QChar, QString> QSourceString;

} // namespace Internal
} // namespace pylang

#endif // SOURCESTRING_H
