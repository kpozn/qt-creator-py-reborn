#ifndef PYTHON_TOKEN_H_27394dca_ee56_4c23_b38e_67a41137f62b
#define PYTHON_TOKEN_H_27394dca_ee56_4c23_b38e_67a41137f62b

#include "../pylang_global.h"
#include "../Support/SourceRange.h"
#include "../Support/StringRef.h"
#include "Lexem.h"

namespace pylang {

class StringRef;

/**
 * \brief Keeps an lexem, position in source and pointer to literal
 */
class Token
{
public:
    inline explicit Token()
        : _lexem(T_EndOfFile)
        , _range(SourceLocation(), SourceLocation())
        , _literalPOD(0) { }

    inline explicit Token(Lexem lexem)
        : _lexem(lexem)
        , _range(SourceLocation(), SourceLocation()) { }

    inline ~Token() { }

    inline bool is(Lexem compared) const;
    inline bool isNot(Lexem compared) const;
    inline bool isKeyword() const;

    inline Lexem lexem() const;
    inline void setLexem(Lexem value);

    inline bool operator ==(const Token &other);
    inline bool operator !=(const Token &other);

    inline bool hasLiteral() const;
    inline StringRef getLiteral() const;
    inline void setLiteral(StringRef literal);

    inline bool isGenerated() const;
    inline void setGenerated(bool isGenerated);

    inline const SourceRange &range() const;
    inline void setRange(const SourceRange &value);

private:
    Lexem _lexem;
    SourceRange _range;
    bool _newlineStarted;
    bool _isGenerated;
    union {
        StringRef::PODHandle *_literalPOD;
    };
};

//! Implementation

bool Token::is(Lexem compared) const
{
    return (_lexem == compared);
}

bool Token::isNot(Lexem compared) const
{
    return (_lexem != compared);
}

bool Token::isKeyword() const
{
    return ((_lexem >= T_KwAnd) && (_lexem <= T_KwYield));
}

Lexem Token::lexem() const
{
    return _lexem;
}

void Token::setLexem(Lexem value)
{
    _lexem = value;
}

bool Token::operator ==(const Token &other)
{
    return _lexem == other._lexem;
}

bool Token::operator !=(const Token &other)
{
    return _lexem != other._lexem;
}

bool Token::hasLiteral() const
{
    return is(T_Identifier) && (0 != _literalPOD);
}

StringRef Token::getLiteral() const
{
    return StringRef::create(_literalPOD);
}

void Token::setLiteral(StringRef literal)
{
    _literalPOD = literal.getPODHandle();
}

bool Token::isGenerated() const
{
    return _isGenerated;
}

void Token::setGenerated(bool isGenerated)
{
    _isGenerated = isGenerated;
}

const SourceRange &Token::range() const
{
    return _range;
}

void Token::setRange(const SourceRange &value)
{
    _range = value;
}

} // namespace pylang

#endif // PYTHON_TOKEN_H_27394dca_ee56_4c23_b38e_67a41137f62b
