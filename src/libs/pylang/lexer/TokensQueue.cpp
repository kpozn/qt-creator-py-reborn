#include "TokensQueue.h"
#include <assert.h>

namespace {
const size_t INITIAL_CAPACITY = 16;
}

namespace pylang {
namespace Internal {

TokensQueue::TokensQueue()
    : m_data(new Token[INITIAL_CAPACITY])
    , m_capacity(INITIAL_CAPACITY)
    , m_size(0)
    , m_start(0)
    //, m_end(1)
{
}

TokensQueue::~TokensQueue()
{
    delete[] m_data;
}

Token const& TokensQueue::front() const
{
    assert(m_size && "TokensQueue is empty, but front() called");
    return m_data[m_start];
}

Token const& TokensQueue::operator [](size_t index) const
{
    assert((index < m_size) && "TokensQueue overflow occured");
    return m_data[(m_start + index) % m_capacity];
}

size_t TokensQueue::size() const
{
    return m_size;
}

void TokensQueue::push_back(const pylang::Token &token)
{
    if (m_capacity <= m_size)
    {
        Token *buff = new Token[2 * m_capacity];
        for (size_t i = 0; i < m_size; ++i)
            buff[i] = m_data[(m_start + i) % m_capacity];
        m_start = 0;

        delete[] m_data;
        m_data = buff;
        m_capacity *= 2;
    }

    m_data[(m_start + m_size) % m_capacity] = token;
    ++m_size;
}

void TokensQueue::pop()
{
    assert(m_size && "TokensQueue underflow occured");

    --m_size;
    m_start = (m_start + 1) % m_capacity;
}

} // namespace Internal
} // namespace pylang
