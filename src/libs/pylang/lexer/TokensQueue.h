#ifndef PYLANG_INTERNAL_RINGBUFFER_H
#define PYLANG_INTERNAL_RINGBUFFER_H

#include <stddef.h>
#include "Token.h"

namespace pylang {
namespace Internal {

/**
   @class RingBuffer - implements ring-buffered queue without exceptions
   or silent checks, any logic error will cause assertion failure

   Define NDEBUG macro to turn off those assertions
 */
class TokensQueue
{
    TokensQueue(const TokensQueue&) = delete;
    void operator =(const TokensQueue&) = delete;
public:
    TokensQueue();
    ~TokensQueue();

    const Token& front() const;
    const Token& operator [](size_t index) const;
    size_t size() const;

    void push_back(const Token& token);
    void pop();

private:
    Token *m_data;
    size_t m_capacity;
    size_t m_size;
    size_t m_start; /* index of first token */
};

} // namespace Internal
} // namespace pylang

#endif // PYLANG_INTERNAL_RINGBUFFER_H
