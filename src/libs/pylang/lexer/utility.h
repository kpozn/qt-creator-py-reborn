#ifndef PYTHON_UTILITY_H_32ee7fdc_0546_4305_9d84_04ac11e31a0f
#define PYTHON_UTILITY_H_32ee7fdc_0546_4305_9d84_04ac11e31a0f

/**
 * @file utility.h
 * Contains utility functions for reuse internal lexers code.
 * Each function should be implemented for char/QChar and std::string/QString
 * respectively.
 */

#include <QtCore/QString>
#include <string>
#include <cctype>

namespace pylang {
namespace Internal {

// FIXME: Python3 allows to use some unicode characters outside ascii,
// docs.python.org/release/3.0.1/reference/lexical_analysis.html

inline bool isIdentifierHead(QChar ch)
{
    return ch.isLetter() || (ch == '_');
}

inline bool isIdentifierHead(char ch)
{
    return std::isalpha(ch) || (ch == '_');
}

inline bool isIdentifierMiddle(QChar ch)
{
    return ch.isLetterOrNumber() || (ch == '_');
}

inline bool isIdentifierMiddle(char ch)
{
    return std::isalnum(ch) || (ch == '_');
}

template<class tchar>
inline bool isQuote(tchar ch)
{
    return ch == ('\"') || (ch == '\'');
}

template<class tchar>
inline bool isDecimalDigit(tchar ch)
{
    return std::isdigit(ch);
}

template<>
inline bool isDecimalDigit<QChar>(QChar ch)
{
    return ch.isDigit();
}

template<class tchar>
inline bool isHexDigit(tchar ch)
{
    return (isDecimalDigit(ch)
            || ((ch >= tchar('a')) && (ch <= tchar('f')))
            || ((ch >= tchar('A')) && (ch <= tchar('F'))));
}

template<class tchar>
inline bool isOctalDigit(tchar ch)
{
    return (isDecimalDigit(ch) && (ch != tchar('8')) && (ch != tchar('9')));
}

template<class tchar>
inline bool isBinaryDigit(tchar ch)
{
    return ((ch == '0') || (ch == '1'));
}

template<class tchar>
inline bool isLongSuffix(tchar ch, bool isPython3)
{
    if (isPython3)
        return false;
    return ((ch == 'l') || (ch == 'L'));
}

template<class tchar>
inline bool isComplexSuffix(tchar ch)
{
    return ((ch == 'j') || (ch == 'J'));
}

// TODO: We should distinguish bytesliterals and stringliterals in python3,
// docs.python.org/release/3.0.1/reference/lexical_analysis.html#string-and-bytes-literals
inline bool isStringLiteralPrefix(const std::string &identifier, bool isPython3)
{

    if (identifier.size() == 1)
    {
        const char *allowedPrefixes = isPython3 ? "rb" : "urb";
        char lower = std::tolower(identifier[0]);
        const char *pos = strchr(allowedPrefixes, lower);
        return (pos != NULL) && (*pos != '\0');
    }
    if (!isPython3 && (identifier.size() == 2))
    {
        std::string lower;
        lower.push_back(std::tolower(identifier[0]));
        lower.push_back(std::tolower(identifier[1]));
        const char *pattern = "urbr";
        const char *pos = strstr(pattern, lower.c_str());
        int offset = pattern - pos;
        return ((pos != NULL) && (*pos != '\0') && (offset % 2 == 0));
    }
    return false;
}

// TODO: We should distinguish bytesliterals and stringliterals in python3,
// docs.python.org/release/3.0.1/reference/lexical_analysis.html#string-and-bytes-literals
inline bool isStringLiteralPrefix(const QString &identifier, bool isPython3)
{
    if (identifier.size() == 1)
    {
        QString prefixies(isPython3 ? "rb" : "urb");
        QChar lower = identifier[0].toLower();
        return prefixies.contains(lower);
    }
    if (!isPython3 && (identifier.size() == 2))
    {
        QString lower = identifier.toLower();
        QString pattern = "urbr";
        int index = pattern.indexOf(lower);
        return ((index != -1) && (index % 2 == 0));
    }
    return false;
}

} // namespace Internal
} // namespace pylang

#endif // PYTHON_UTILITY_H_32ee7fdc_0546_4305_9d84_04ac11e31a0f
