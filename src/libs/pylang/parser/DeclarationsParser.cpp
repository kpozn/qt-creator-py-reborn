#include "DeclarationsParser.h"

#include "../ast/exception/parserexception.h"
#include <new>

namespace pylang {

/**
 * @class pylang::DeclarationsParser implements python source code to AST parser
 *
 *   Parses functions definitions (with arguments[1]), classes, decorators[2]
 *   and imports, skips expressions and statements.
 *
 * @see Official grammar specification here:
 * @link http://docs.python.org/reference/grammar.html
 *
 * TODO: 1. add function arguments support to self::parseFunctionDef()
 * TODO: 2. add decorators support
 */

/* ------------------------- Utility classes --------------------------- */

/* ------------------------- Macro definitions ------------------------- */

/* Silents warning for unused variable, but you should check manually
   each applicance of this macro
 */
#define PARSER_UNUSED(arg) ((void)(arg))

#define PARSER_TO_STRING(arg) PARSER_TO_STRING_IMPL(arg)

#define PARSER_TO_STRING_IMPL(arg) #arg

/* enclosing function will throw exception if check failed */
#define PARSER_LOGIC_CHECK(condition, messageOnFailure) \
    if (false == (condition)) { \
        throw ParserException(m_builder.tokenizedUnit(), cursor(), \
                std::string("'") + std::string(messageOnFailure) \
                + std::string("' __LINE__ is ") + PARSER_TO_STRING(__LINE__)); \
    } \

/* enclosing function will return false if check failed */
#define RULE_REQUIRES(condition) \
    if (false == (condition)) return false;

#define RULE_ENDS_ON(condition) \
    if (true == (condition)) { \
        consume(); \
        return true; \
    }

#define RULE_ENDS_BEFORE(condition) \
    if (true  (condition)) return true;

/* -------------------------- Implementation --------------------------- */

DeclarationsParser::DeclarationsParser()
{
}

DeclarationsParser::~DeclarationsParser()
{
}

void DeclarationsParser::operator ()(const UnitBuilder &control,
                                     const QByteArray &source) const
{
    prepare(control, source);
}

bool DeclarationsParser::readStatement(Internal::StmtAst *&ast)
{
    const Lexem ahead(LA());
    switch(ahead) {
    /* parse */
    case T_KwDef:
        return parseFunctionDef(ast);
    case T_KwClass:
        return parseClassDef(ast);
    case T_KwImport:
    case T_KwFrom:
        return parseImport(ast);

    /* skip */
    case T_KwTry:
    case T_KwIf:
    case T_KwFor:
    case T_KwWhile:
    case T_KwElse:
    case T_KwExcept:
    case T_KwElif:
        skipCompoundStatement();
        break;

    default:
        skipSimpleStatement();
        break;
    }

    return false;
}

bool DeclarationsParser::parseClassDef(Internal::StmtAst *&ast)
{
    PARSER_LOGIC_CHECK(T_KwClass == LA(),
                          "Rule for \"def\" called when ahead lexem isn't T_KwDef");
    RULE_REQUIRES(T_Identifier == LA(2));

    auto klass = new (pool()) ClassDefinitionAst;
    ast = klass;
    consume();
    klass->m_identifier = consume();

    skipCompoundStatement();
    return true;
}

bool DeclarationsParser::parseFunctionDef(Internal::StmtAst *&ast)
{
    PARSER_LOGIC_CHECK(T_KwClass == LA(),
                       "Rule for \"def\" called when ahead lexem isn't T_KwDef");
    RULE_REQUIRES(T_Identifier == LA(2));

    auto def = new (pool()) FunctionDefinitionAst;
    ast = def;
    consume();
    def->m_identifier = consume();

    skipCompoundStatement();
    return true;
}

bool DeclarationsParser::parseImport(Internal::StmtAst *&ast)
{
    const Lexem ahead(LA());
    PARSER_LOGIC_CHECK(T_KwFrom == ahead || T_KwImport == ahead,
                       "Imports rule called when ahead lexem isn't"
                       " T_KwFrom or T_KwImport");

    auto import = new (pool()) ImportAst;
    if(ahead == T_KwFrom) {
        import->token_from = consume();
        ExpressionAst *host(0);
        RULE_REQUIRES(parseDottedName(host));
        import->m_host.reset(host);

        RULE_REQUIRES(T_KwImport == LA());
    }

    // NOTE: dotted_as_names cannot have endian comma
    import->token_import = consume();
    if (T_OpMul == LA())
    {
        if (false == import->m_host.isNull())
            import->token_asterix = consume();
        else
            error(consume(), QObject::tr("You can use * only when importing from module"));
    }
    else if (T_LeftParen == LA())
    {
        if (false == import->m_host.isNull())
        {
            consume();
            parseListOfDottedAsNames(import->m_names);
            if (T_RightParen == LA()) {
                consume();
            }
            else {
                error(cursor(), QObject::tr("Expected ')' after import list end"));
            }
        }
    }
    else
    {
        parseListOfDottedAsNames(import->m_names);
    }

    ast = import;
    skipSimpleStatement();
    return true;
}

void DeclarationsParser::skipCompoundStatement()
{
    bool isInline = false;
    for(consume(); true; consume()) {
        const Lexem ahead(LA());
        if(ahead == T_Newline) {
            errorExpectedBefore(cursor(), LexemSpelling[T_Colon], LexemSpelling[T_Newline]);
            consume();
            break;
        }
        else if(ahead == T_Colon) {
            consume();
            if(LA() != T_Newline) {
                isInline = true;
            } else {
                consume();
            }
            break;
        }
    }
    if(LA() != T_Indent)
        isInline = true;
    const Lexem stopping = isInline ? T_Newline : T_Dedent;
    for(consume(); LA() == stopping; consume())
        continue;
    consume();
}

void DeclarationsParser::skipSimpleStatement()
{
    for(consume(); LA() != T_Newline; consume())
        continue;
    consume();
}

bool DeclarationsParser::parseDottedName(ExpressionAst *&ast)
{
    RULE_REQUIRES(T_Identifier == LA());

    auto id = new (pool()) IdentifierAst;
    ast = id;
    id->m_identifier = consume();

    for (;;)
    {
        if (T_OpDot != LA())
            break;

        consume();
        if (T_Identifier != LA())
        {
            errorExpectedAfter(cursor(), "child module name", LexemSpelling[T_OpDot]);
            break;
        }

        auto id = new (pool()) IdentifierAst;
        id->m_identifier = consume();
        auto attrib = new (pool()) AttributeAst;
        ast = attrib;
        attrib->m_object.reset(ast);
        attrib->m_attribute.reset(id);
    }
    return true;
}

bool DeclarationsParser::parseListOfDottedAsNames(PySequence names)
{
    bool hasItems(false);
    for (;;)
    {
        ExpressionAst *item(0);
        if (!parseDottedName(item))
            return hasItems;

        if(T_KwAs == LA() && T_Identifier == LA(2)) {
            consume();
            auto alias = new (pool()) AliasAst;
            alias->m_name.reset(item);
            parseDottedName(item);
            alias->m_alias.reset(item);
            item = alias;
        }

        names.append(item);
        hasItems = true;
        if (T_Comma == LA())
            consume();
        else
            return hasItems;
    }
}

} // namespace pylang















































