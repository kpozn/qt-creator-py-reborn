#ifndef PYLANG_DECLARATIONSPARSER_H
#define PYLANG_DECLARATIONSPARSER_H

#include "ParserBase.h"

namespace pylang {

/**
 * @brief The DeclarationsParser class - skips expressions and statements
 *
 * Parses functions definitions, classes and imports
 * TODO: add decorators support
 */
class DeclarationsParser : public Internal::ParserBase
{
public:
    DeclarationsParser();
    ~DeclarationsParser();

    /*virtual*/ void operator()(const UnitBuilder &builder,
                                const QByteArray &source) const;

    bool readStatement(Internal::StmtAst *&ast);
    bool parseImport(Internal::StmtAst *&ast);
    bool parseFunctionDef(Internal::StmtAst *&ast);
    bool parseClassDef(Internal::StmtAst *&ast);

    void skipSimpleStatement();
    void skipCompoundStatement();

private:
    bool parseDottedName(ExpressionAst *&ast);
    bool parseListOfDottedAsNames(PySequence names);
};

} // namespace pylang

#endif // PYLANG_DECLARATIONSPARSER_H
