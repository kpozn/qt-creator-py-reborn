#include "LalrStack.h"

namespace pylang {

static const unsigned INITIAL_CAPACITY = 128;

LalrStack::LalrStack()
    :_topOfStack(-1)
{
    _data.resize(INITIAL_CAPACITY);
}

LalrStack::~LalrStack()
{
}

void LalrStack::increase()
{
    ++_topOfStack;
    if (_topOfStack >= static_cast<int>(_data.size())) {
        _data.resize(2 * _data.size());
    }
}

void LalrStack::decrease(unsigned amount)
{
    _topOfStack -= static_cast<int>(amount);
#ifdef NEED_RUNTIME_CHECKS
    if (_topOfStack < -1)
        qDebug() << "LalrStack underflow: _topOfStack ==" << _topOfStack;
#endif
}

void LalrStack::storeAction(int state)
{
    node().state = state;
}

void LalrStack::storeValue(StringRef string)
{
    node().string = string;
}

void LalrStack::storeValue(Ast *pAst)
{
    node().pAst = pAst;
}

void LalrStack::storeValue(const PySequence &astList)
{
    node().astList = astList;
}

void LalrStack::storeTokenIndex(quint32 tokenIndex)
{
    node().tokenIndex = tokenIndex;
}

int LalrStack::topIndex() const
{
    return _topOfStack;
}

int LalrStack::getAction(int offset) const
{
    return node(offset).state;
}

StringRef LalrStack::getString(int offset) const
{
    return _data[_topOfStack + offset].string;
}

const PySequence &LalrStack::getAstList(int offset) const
{
    return node(offset).astList;
}

quint32 LalrStack::getTokenIndex(int offset) const
{
    return node(offset).tokenIndex;
}

int LalrStack::saveStackPosition() const
{
    return _topOfStack;
}

void LalrStack::restoreStackPosition(int topOfStack)
{
    _topOfStack = topOfStack;
}

const LalrStack::Node &LalrStack::node(int offset) const
{
    int index = _topOfStack + offset;
    if (index < 0)
    {
#ifdef NEED_RUNTIME_CHECKS
        qDebug() << "LalrStack underflow: passed " << index << " as index";
#endif
        index = 0;
    }
    else if (index > static_cast<int>(_data.size()))
    {
#ifdef NEED_RUNTIME_CHECKS
        qDebug() << "LalrStack overflow: passed " << index << " as index";
#endif
        index = topIndex();
    }
    return _data[_topOfStack + offset];
}

LalrStack::Node &LalrStack::node(int offset)
{
    int index = _topOfStack + offset;
    if (index < 0)
    {
#ifdef NEED_RUNTIME_CHECKS
        qDebug() << "LalrStack underflow: passed " << index << " as index";
#endif
        index = 0;
    }
    else if (index > topIndex())
    {
#ifdef NEED_RUNTIME_CHECKS
        qDebug() << "LalrStack overflow: passed " << index << " as index";
#endif
        index = topIndex();
    }
    return _data[_topOfStack + offset];
}

} // namespace pylang
