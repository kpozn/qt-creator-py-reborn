#ifndef PYTHON_LALRSTACK_H
#define PYTHON_LALRSTACK_H

#include "../ast/AST.h"
#include <vector>

#ifdef NEED_RUNTIME_CHECKS

#include <QtCore/QDebug>
#include <typeinfo>

#ifdef Q_CC_GNU
#include <cxxabi.h>
#endif // Q_CC_GNU

#endif // NEED_RUNTIME_CHECKS

namespace pylang {

/**
 * @brief The PushdownAutomationStack class encapsulates LALR parser stack:
 * it keeps state, location and value in single entry
 *
 * Stack doesn't check values, so you should manually store token index
 * after each increase() call and (optionally)
 * check returned Ast pointer value & PySequence content
 */
class LalrStack
{
public:
    LalrStack();
    ~LalrStack();

    virtual void increase();
    void decrease(unsigned amount);

    /**
     * @brief storeState keeps state at top of stack
     * @param state - representation of LALR parser state
     */
    void storeAction(int state);

    void storeValue(StringRef string);
    void storeValue(Ast *pAst);
    void storeValue(const PySequence &astList);

    int topIndex() const;

    template <class TAst>
    TAst *getAst(int offset) const;

    int getAction(int offset = 0) const;
    StringRef getString(int offset) const;
    const PySequence &getAstList(int offset) const;
    quint32 getTokenIndex(int offset) const;

    int saveStackPosition() const;
    void restoreStackPosition(int topOfStack);

protected:
    void storeTokenIndex(quint32 tokenIndex);

private:
    struct Node {
        int state;
        quint32 tokenIndex;
        StringRef string;
        PySequence astList;
        Ast *pAst;
    };

    const Node &node(int offset = 0) const;
    Node &node(int offset = 0);

    int _topOfStack;
    std::vector<Node> _data;
};

template <class TAst>
TAst *LalrStack::getAst(int offset) const
{
    Ast *casted = node(offset).pAst;
#ifdef NEED_RUNTIME_CHECKS
    TAst *check = dynamic_cast<TAst*>(casted);
    if (check == nullptr)
    {
        const char *dcasted = typeid(casted).name();
        const char *dname = typeid(TAst).name();
#ifdef Q_CC_GNU
        char *gcccasted = abi::__cxa_demangle(dcasted, 0, 0, 0);
        char *gccname = abi::__cxa_demangle(dname, 0, 0, 0);
        dcasted = gcccasted;
        dname = gccname;
#endif
        if (casted == nullptr)
        {
            qDebug() << "Invalid cast from " << dcasted
                     << " to type " << dname
                     << " - argument is nullptr";
        }
        else
        {
            qDebug() << "Invalid cast from " << dcasted
                     << " to type " << dname
                     << " - argument has another type";
        }
#ifdef Q_CC_GNU
        free(gcccasted);
        free(gccname);
#endif
    }
#endif
    return reinterpret_cast<TAst *>(casted);
}

} // namespace pylang

#endif // PYTHON_LALRSTACK_H
