#include "Parser.h"

#include "../ast/exception/parserexception.h"
#include <new>

/**
 * @class pylang::Parser implements python source code to AST parser
 *
 * @see Official grammar specification here:
 * @link http://docs.python.org/reference/grammar.html
 */

/* ------------------------- Utility classes --------------------------- */

/* ------------------------- Macro definitions ------------------------- */

/* Silents warning for unused variable, but you should check manually
   each applicance of this macro
 */
#define PARSER_UNUSED(arg) ((void)(arg))

#define PARSER_TO_STRING(arg) PARSER_TO_STRING_IMPL(arg)

#define PARSER_TO_STRING_IMPL(arg) #arg

/* enclosing function will throw exception if check failed */
#define PARSER_LOGIC_CHECK(condition, messageOnFailure) \
    if (false == (condition)) { \
        throw ParserException(m_builder.tokenizedUnit(), cursor(), \
                std::string("'") + std::string(messageOnFailure) \
                + std::string("' __LINE__ is ") + PARSER_TO_STRING(__LINE__)); \
    } \

/* enclosing function will return false if check failed */
#define RULE_REQUIRES(condition) \
    if (false == (condition)) return false;

#define RULE_ENDS_ON(condition) \
    if (true == (condition)) { \
        consume(); \
        return true; \
    }

#define RULE_ENDS_BEFORE(condition) \
    if (true  (condition)) return true;

#define STRLEN(literal) ((sizeof(literal) / sizeof(char)) - 1u)

/* -------------------------- Implementation --------------------------- */

namespace pylang {

Parser::Parser()
{
}

Parser::~Parser()
{
}

void Parser::operator ()(const UnitBuilder &control, const QByteArray &source) const
{
    prepare(control, source);
    auto tu = new (pool()) TranslationUnitAst;
    while (T_EndOfFile != LA())
    {
        Internal::StmtAst *ast(0);
        if (readStatement(ast))
            tu->m_body.append(ast);
    }
    m_builder.setRootAst(tu);
}

bool Parser::readStatement(Internal::StmtAst *&ast) const
{
    const Lexem firstLexem(LA());
    switch (firstLexem)
    {
    case T_KwDef:
        return parseFunctionDef(ast);
    case T_KwClass:
        return parseClassDef(ast);
    case T_KwTry:
        return parseTryStmt(ast);
    case T_KwIf:
        return parseIfStmt(ast);
    case T_KwFor:
        return parseForStmt(ast);
    case T_KwWhile:
        return parseWhileStmt(ast);
    case T_KwPass:
        return parsePass(ast);
    case T_KwFrom:
    case T_KwImport:
        return parseImport(ast);
    case T_KwReturn:
        return parseReturn(ast);
    case T_KwPrint:
        return parsePrint(ast);
    case T_KwRaise:
        return parseRaiseStmt(ast);

    case T_Identifier:
    case T_OpSub:
        return parseExpressionStmt(ast);

    case T_Newline:
        consume();
        return false;

    default:
        break;
    }

    std::string mess("Unexpected statement start lexem: ");
    mess.append(LexemSpelling[firstLexem]);
    PARSER_LOGIC_CHECK(false, mess.c_str());

    return false;
}

/**
 * @brief Parses compound statement body, should be called right after ':' was consumed
 * @param pDocstring
 * @return null or pointer to allocated Ast
 */
void Parser::parseBody(PySequence &body,
                       /*out*/ AstRef<DocStringAst> *docstring) const
{
    if (LA() == T_Colon)
    {
        consume();
    }
    else if (LA() == T_Newline)
    {
        errorExpectedBefore(cursor(), LexemSpelling[T_Colon], LexemSpelling[T_Newline]);
    }
    else
    {
        errorExpectedAfter(cursor(), "statements list", "new block start");
        skipToNewline();
        return;
    }

    if (LA() == T_Newline)
    {
        consume();
        if (LA() != T_Indent)
        {
            error(cursor(), "Expected aligned statements block");
        }
        else
        {
            consume();

            if (T_DocString == LA())
            {
                if (nullptr != docstring) {
                    docstring->reset(new (pool()) DocStringAst());
                    (*docstring)->m_stringToken = consume();
                } else {
                    error(consume(), QLatin1String("This statement cannot have docstring"));
                }
            }

            parseAlignedBlock(body, true);
        }
    }
    else
    {
        Internal::StmtAst *singleStmt(0);
        if (readStatement(singleStmt))
            body.append(singleStmt);
    }
}

/**
   @brief Reads block that starts with indentation and ends with dedent, example:
   @code
        def printms(count):
            # begins, consumes T_Indent token
            print(count, " ms elapsed")
            # ends, consumes T_Newline and T_Dedent tokens
        # not a part of block
        printms(100)
   @endcode
   @return Always true

   @internal Indentation errors mostly handled in Lexer class
 */
bool Parser::parseAlignedBlock(PySequence &sequence, bool indentConsumed) const
{
    if (false == indentConsumed)
    {
        PARSER_LOGIC_CHECK(T_Indent == LA(),
                "Rule for indented block called when ahead lexem isn't T_Indent");
        consume();
    }

    for (;;)
    {
        const Lexem ahead(LA());
        RULE_ENDS_ON(T_Dedent == ahead);

        PARSER_LOGIC_CHECK((T_EndOfFile != LA()),
                "Unexpected EOF in block, Lexer should pass T_Dedent first");

        Internal::StmtAst *ast(0);
        if (readStatement(ast))
            sequence.append(ast);
    }
    return true;
}

/**
   @brief Reads formal arguments and enclosing parenthesis, example:
   @code
        def myMethod(self, timeElapsed, timeFormat = "milliseconds"):
   @endcode
   (self, timeElapsed, timeFormat = "milliseconds") will be parsed in such code
   @return False if neither ')' nor ':' ends arguments list
 */
bool Parser::parseFormalArguments(PySequence &sequence) const
{
    PARSER_LOGIC_CHECK(T_LeftParen == LA(),
                       "Rule for formal arguments called when ahead lexem isn't '('");

    bool isMultiline(false);
    consume();
    for (;;)
    {
        Lexem lex(LA());

        RULE_REQUIRES(T_EndOfFile != lex);
        RULE_ENDS_ON(T_RightParen == lex);
        RULE_ENDS_ON(tryInsertLexemBefore(T_RightParen, T_Colon));

        if (T_Newline == lex)
        {
            isMultiline = true;
        }

        PARSER_UNUSED(isMultiline);
        PARSER_UNUSED(sequence);
        //! TODO: parse arguments
        consume();
    }
}

bool Parser::parseDottedName(ExpressionAst *&ast) const
{
    RULE_REQUIRES(T_Identifier == LA());

    auto id = new (pool()) IdentifierAst;
    id->m_identifier = consume();
    ast = id;

    for (;;)
    {
        if (T_OpDot != LA())
            break;

        consume();
        if (T_Identifier != LA())
        {
            errorExpectedAfter(cursor(), "child module name", LexemSpelling[T_OpDot]);
            break;
        }

        auto id = new (pool()) IdentifierAst;
        id->m_identifier = consume();
        auto attrib = new (pool()) AttributeAst;
        attrib->m_object.reset(ast);
        attrib->m_attribute.reset(id);
        ast = attrib;
    }
    return true;
}

bool Parser::parseBaseClasses(PySequence &sequence) const
{
    PARSER_LOGIC_CHECK(T_LeftParen == LA(),
                       "Rule for base clases list called "
                       "when ahead lexem isn't T_LeftParen");
    consume();

    parseListOfDottedAsNames(sequence);
    if (LA() == T_RightParen)
        consume();
    else
        errorExpectedAfter(cursor(),
                            LexemSpelling[T_RightParen],
                            "base classes list");

    return true;
}

bool Parser::parseExceptClause(Internal::StmtAst *&ast) const
{
    RULE_REQUIRES(T_KwExcept == LA());
    auto clause = new (pool()) ExceptAst;
    consume();

    // FIXME: temporary stub
    if (LA() == T_Identifier)
    {
        auto id(new (pool()) IdentifierAst);
        id->m_identifier = consume();
        clause->m_exceptionType.reset(id);
    }

    parseBody(clause->m_body);

    ast = clause;
    return true;
}

bool Parser::parseImport(Internal::StmtAst *&ast) const
{
    const Lexem ahead(LA());
    PARSER_LOGIC_CHECK(T_KwFrom == ahead || T_KwImport == ahead,
                          "Rule for \"from\" called when ahead lexem isn't T_KwFrom");

    auto importAst(new (pool()) ImportAst());

    if (T_KwFrom == ahead)
    {
        importAst->token_from = consume();
        ExpressionAst *host(0);
        RULE_REQUIRES(parseDottedName(host));
        importAst->m_host.reset(host);

        RULE_REQUIRES(T_KwImport == LA());
    }

    // NOTE: dotted_as_names cannot have endian comma
    importAst->token_import = consume();
    if (T_OpMul == LA())
    {
        if (false == importAst->m_host.isNull())
            importAst->token_asterix = consume();
        else
            error(consume(), QObject::tr("You can use * only when importing from module"));
    }
    else if (T_LeftParen == LA())
    {
        if (false == importAst->m_host.isNull())
        {
            consume();
            RULE_REQUIRES(parseListOfDottedAsNames(importAst->m_names));
            if (T_RightParen == LA())
            {
                consume();
            }
            else
                error(cursor(), QObject::tr("Expected ')' after import list end"));
        }
    }
    else
    {
        RULE_REQUIRES(parseListOfDottedAsNames(importAst->m_names));
    }

    ast = importAst;
    return true;
}

bool Parser::parseFunctionDef(Internal::StmtAst *&ast) const
{
    PARSER_LOGIC_CHECK(T_KwDef == LA(),
                       "Rule for \"def\" called when ahead lexem isn't T_KwDef");

    RULE_REQUIRES(T_Identifier == LA(2));
    RULE_REQUIRES(T_LeftParen == LA(3));

    auto defAst(new (pool()) FunctionDefinitionAst);
    consume();
    defAst->m_identifier = consume();

    RULE_REQUIRES(parseFormalArguments(defAst->m_arguments));
    parseBody(defAst->m_body, &defAst->m_docstring);

    ast = defAst;
    return true;
}

bool Parser::parseClassDef(Internal::StmtAst *&ast) const
{
    PARSER_LOGIC_CHECK(T_KwClass == LA(),
                          "Rule for \"class\" called when ahead lexem isn't T_KwClass");

    RULE_REQUIRES(T_Identifier == LA(2));

    auto classAst(new (pool()) ClassDefinitionAst());
    consume();
    classAst->m_identifier = consume();

    if (T_LeftParen == LA())
    {
        RULE_REQUIRES(parseBaseClasses(classAst->m_baseClasses));
    }

    parseBody(classAst->m_body, &classAst->m_docstring);

    ast = classAst;
    return true;
}

bool Parser::parseTryStmt(Internal::StmtAst *&ast) const
{
    RULE_REQUIRES(LA(2) == T_Colon && LA(3) == T_Newline);

    auto tryAst(new (pool()) TryStmtAst());
    consume();

    parseBody(tryAst->m_body);

    Internal::StmtAst *stmt(0);
    while (parseExceptClause(stmt)) {
        tryAst->m_excepts.append(stmt);
    }

    ast = tryAst;
    return true;
}

bool Parser::parsePass(Internal::StmtAst *&ast) const
{
    auto passAst(new (pool()) ControlFlowAst(ControlFlowAst::Pass));
    passAst->m_keyword = consume();
    ast = passAst;

    RULE_REQUIRES(consumeStatementTail());

    return true;
}

/**
   @brief Read expression that starts with predicate and finished with ';' or EOL
   Examples:
   @code
    texture = material.normalMap()      # first statement
    texture.bind(); texture.unbind()    # two statements on single line
   @endcode
   @param ast
   @return
 */
bool Parser::parseExpressionStmt(Internal::StmtAst *&ast) const
{
    ast = nullptr;
    ExpressionAst *expr(nullptr);
    bool errorReported(false);
    if(parsePredicate(expr))
    {
        ast = expr;
        // FIXME: testlist (listOfPredicates) should be used instead of predicate
        const Lexem ahead(LA());
        switch (ahead)
        {
        case T_Assign:
        {
            auto assign(new (pool()) AssignmentAst);
            assign->m_expressions.append(expr);

            do
            {
                auto assignLoc = consume();
                if (parsePredicate(expr))
                {
                    assign->m_expressions.append(expr);
                }
                else
                {
                    errorExpectedAfter(assignLoc, "expression", LexemSpelling[T_Assign]);
                }
            }
            while (T_Assign == LA());

            ast = assign;
            break;
        }
        case T_AugAdd:
        case T_AugBitand:
        case T_AugBitor:
        case T_AugBitxor:
        case T_AugDiv:
        case T_AugFloorDiv:
        case T_AugLShift:
        case T_AugMod:
        case T_AugMul:
        case T_AugPower:
        case T_AugRShift:
        case T_AugSub:
        {
            // NOTE: add rule for yield expression
            auto loc(consume());
            ExpressionAst *right(0);
            if (!parsePredicate(right))
            {
                errorReported = true;
                break;
            }
            auto op = new (pool()) BinaryExpressionAst;
            ast = op;
            op->m_kind = BinaryExpressionAst::convertToken(ahead);
            op->m_left.reset(expr);
            op->m_right.reset(right);
            op->m_operator = loc;
            break;
        }
        default:
            break;
        }
    } else {
        errorReported = true;
    }

    if (LA() != T_Newline) {
        if (!errorReported) {
            errorExpectedAfter(cursor(), "Newline", LexemSpelling[LA()]);
        }
        do {
            consume();
            PARSER_LOGIC_CHECK(LA() != T_EndOfFile, "Unexpected end of file");
        } while (LA() != T_Newline);
    }

    return (ast != nullptr);
}

bool Parser::parseReturn(Internal::StmtAst *&ast) const
{
    PARSER_LOGIC_CHECK(T_KwReturn == LA(),
                       "Rule for return stmt called, but ahead lexem "
                       "isn't T_KwReturn");

    consume();
    ExpressionAst *value(0);
    PySequence values;

    const Lexem next(LA());
    if ((next != T_Newline) && (next != T_Semicolon))
    {
        RULE_REQUIRES(parseListOfPredicates(values))
    }

    auto returnAst = new (pool()) ReturnStmtAst;
    returnAst->m_value.reset(value);
    ast = returnAst;
    return true;
}

bool Parser::parsePrint(Internal::StmtAst *&ast) const
{
    PARSER_LOGIC_CHECK(T_KwPrint == LA(),
                       "Rule for return stmt called, but ahead lexem "
                       "isn't T_KwReturn");

    auto printAst = new (pool()) PrintAst;
    printAst->m_keywordPrint = consume();
    const Lexem next(LA());

    if ((next != T_Newline) && (next != T_Semicolon))
    {
        RULE_REQUIRES(parseListOfPredicates(printAst->m_values));
    }

    ast = printAst;
    return true;
}

bool Parser::parseIfStmt(Internal::StmtAst *&ast, bool ignoreElse) const
{
    PARSER_LOGIC_CHECK(T_KwIf == LA() || T_KwElif == LA(),
                       "Rule for if statement called, but ahead lexem isn't T_KwIf");

    auto ifAst = new (pool()) IfStmtAst;
    consume();

    ExpressionAst *cond(0);
    RULE_REQUIRES(parsePredicate(cond));
    ifAst->m_condition.reset(cond);
    parseBody(ifAst->m_body);

    while (T_KwElif == LA())
    {
        Internal::StmtAst *elif(0);
        RULE_REQUIRES(parseIfStmt(elif));
        ifAst->m_elif.append(static_cast<IfStmtAst*>(elif));
    }

    if (!ignoreElse)
        tryParseElseBlock(*ifAst);

    ast = ifAst;
    return true;
}

bool Parser::parseWhileStmt(Internal::StmtAst *&ast) const
{
    PARSER_LOGIC_CHECK(T_KwWhile == LA(),
                       "Rule for while statement called, but ahead lexem isn't T_KwWhile");

    auto qualified = new (pool()) WhileStmtAst;
    ast = qualified;
    consume();

    ExpressionAst *condition(0);
    RULE_REQUIRES(parsePredicate(condition));
    qualified->m_condition.reset(condition);
    parseBody(qualified->m_body);

    tryParseElseBlock(*qualified);

    return true;
}

bool Parser::parseForStmt(Internal::StmtAst *&ast) const
{
    PARSER_LOGIC_CHECK(T_KwFor == LA(),
                       "Rule for for statement called, but ahead lexem isn't T_KwFor");

    auto qualified(new (pool()) ForStmtAst);
    ast = qualified;
    consume();

    // FIXME: correct rules - parseListOfArifmetics and parseListOfPredicates
    ExpressionAst *iterator = exprArifmetic();
    // FIXME: iterator == nullptr not handled properly
    if (nullptr == iterator)
        iterator = new (pool()) NoExpressionAst;
    qualified->m_iterator.reset(iterator);

    RULE_REQUIRES(LA() == T_KwIn);
    consume();

    ExpressionAst *collection(0);
    RULE_REQUIRES(parsePredicate(collection));
    qualified->m_collection.reset(collection);
    parseBody(qualified->m_body);

    tryParseElseBlock(*qualified);

    return true;
}

bool Parser::parseRaiseStmt(Internal::StmtAst *&ast) const
{
    PARSER_LOGIC_CHECK(T_KwRaise == LA(),
                       "Rule for if statement called, but ahead lexem isn't T_KwFor");

    auto raise(new (pool()) RaiseAst);
    raise->m_keywordRaise = consume();
    ast = raise;

    if (LA() == T_Newline)
        return true;

    ExpressionAst *exception(0);
    RULE_REQUIRES(parsePredicate(exception));
    raise->m_exception.reset(exception);

    switch (LA())
    {
    // TODO: add support for python2 syntax: 'raise' [test [, test [, test]]]
    case T_Comma:
    {
        consume();
        ExpressionAst *value(0);
        RULE_REQUIRES(parsePredicate(value));
        auto callAst = new (pool()) FunctionCallAst;
        callAst->m_function.reset(exception);
        callAst->m_arguments.append(value);
        raise->m_exception.reset(exception);

        if (LA() == T_Comma)
        {
            consume();
            ExpressionAst *traceback(0);
            RULE_REQUIRES(parsePredicate(traceback));
            raise->m_traceback.reset(traceback);
        }
    }
        break;
    case T_KwFrom:
    {
        raise->m_keywordFrom = consume();
        ExpressionAst *cause(0);
        RULE_REQUIRES(parsePredicate(cause));
        raise->m_reason.reset(cause);
    }
        break;
    default:
        break;
    }

    return true;
}

bool Parser::tryParseElseBlock(Internal::OtherwisedAst &ast) const
{
    if (T_KwElse != LA())
        return false;

    consume();
    if (T_Colon != LA())
    {
        tryInsertLexemBefore(T_Colon, T_Newline);
    }
    else
    {
        consume();
    }

    RULE_REQUIRES(LA() == T_Newline && LA(2) == T_Indent);
    consume();
    RULE_REQUIRES(parseAlignedBlock(ast.m_bodyElse));

    return true;
}

bool Parser::tryParseTrailer(ExpressionAst *&ast) const
{
    const Lexem ahead(LA());
    if (ahead == T_OpDot)
    {
        SourceLocation dotLoc(consume());
        if (LA() == T_Identifier)
        {
            auto id = new (pool()) IdentifierAst;
            id->m_identifier = consume();
            auto attrib = new (pool()) AttributeAst;
            attrib->m_object.reset(ast);
            attrib->m_attribute.reset(id);
            ast = attrib;
        }
        else
        {
            errorExpectedAfter(dotLoc, "expression", LexemSpelling[T_OpDot]);
        }
    }
    else if (ahead == T_LeftParen) {
        SourceLocation openingLoc = consume();
        PySequence arguments;
        // FIXME: incorrect rule called here
        if (LA() != T_RightParen)
        {
            RULE_REQUIRES(parseListOfPredicates(arguments));
        }
        auto call = new (pool()) FunctionCallAst;
        call->m_function.reset(ast);
        call->m_arguments = arguments;
        ast = call;
        call->m_openingBrace = openingLoc;
        if (LA() == T_RightParen)
        {
            call->m_closingBrace = consume();
        }
        else
        {
            errorExpectedAfter(cursor(), LexemSpelling[T_RightParen], "arguments list");
        }
    }
    else if (ahead == T_LeftBracket) {
        PySequence subscripts;
        // FIXME: incorrect rule called here
        auto openingLoc = consume();
        RULE_REQUIRES(parseListOfSubscripts(subscripts));
        auto slice = new (pool()) SliceAst;
        slice->m_array.reset(ast);
        slice->m_subscripts = subscripts;
        slice->m_openingBracket = openingLoc;
        if (LA() == T_RightBracket)
        {
            slice->m_closingBracket = consume();
        }
        else
        {
            errorExpectedAfter(cursor(), LexemSpelling[T_RightBracket], "");
        }

        ast = slice;
    }
    else
        return false;
    return true;
}

/**
   @brief Reads comma-separated list of dotted names with optional enclosing comma
   @return true if at least one name parsed
 */
bool Parser::parseListOfDottedAsNames(PySequence &sequence) const
{
    bool hasItems(false);
    for (;;)
    {
        ExpressionAst *item(0);
        if (parseDottedName(item))
            sequence.append(item);
        else
            return hasItems;

        hasItems = true;
        if (T_Comma == LA())
            consume();
        else
            return hasItems;
    }
}

/**
   @brief Reads comma-separated list of logical predicates with optional enclosing comma
   @note predicate is named 'test' in official grammar specification
   @return true if at least one expression parsed
 */
bool Parser::parseListOfPredicates(PySequence &sequence) const
{
    bool hasItems(false);
    for (;;)
    {
        ExpressionAst *item(0);
        if (parsePredicate(item))
            sequence.append(item);
        else
            return hasItems;

        hasItems = true;
        if (T_Comma == LA())
            consume();
        else
            return hasItems;
    }
}

/**
   @brief Reads comma-separated list of arifm expressions with optional enclosing comma
   @return true if at least one expression parsed
 */
bool Parser::parseListOfArifmetics(PySequence &sequence) const
{
    bool hasItems(false);
    for (;;)
    {
        ExpressionAst *item = exprArifmetic();
        if (nullptr != item)
            sequence.append(item);
        else
            return hasItems;

        hasItems = true;
        if (T_Comma == LA())
            consume();
        else
            return hasItems;
    }
}

/**
   @brief Reads comma-separated list of subscriptions with optional enclosing comma
   @return true if at least one subscription parsed
 */
bool Parser::parseListOfSubscripts(PySequence &sequence) const
{
    bool hasItems(false);
    for (;;)
    {
        ExpressionAst *item(0);
        if (parseSubscript(item))
            sequence.append(item);
        else
            return hasItems;

        hasItems = true;
        if (T_Comma == LA())
            consume();
        else
            return hasItems;
    }
}

void Parser::parseDictInitializerTail(PySequence &expressions) const
{
    if (T_Comma == LA())
    {
        consume();
        for (;;)
        {
            ExpressionAst *item(0);
            if (parseDictPair(item))
                expressions.append(item);
            else
                break;

            if (T_Comma == LA())
                consume();
            else
                break;
        }
    }
}

bool Parser::parseDictOrSetInitializer(PySequence &expressions) const
{
    ExpressionAst *key(0);
    if (!parsePredicate(key))
        return false;

    if (LA() == T_Colon)
    {
        consume();
        auto dict = new (pool()) DictPairAst;
        dict->m_key.reset(key);
        ExpressionAst *value(0);
        if (!parsePredicate(value))
        {
            value = new (pool()) NoExpressionAst;
        }
        dict->m_value.reset(value);
        expressions.append(dict);

        parseDictInitializerTail(expressions);
    }
    else
    {
        parseListOfPredicates(expressions);
    }
    return true;
}

bool Parser::parseDictPair(ExpressionAst *&ast) const
{
    RULE_REQUIRES(parsePredicate(ast));

    switch (LA())
    {
    case T_Comma:
    {
        errorExpectedAfter(cursor(), "value", "dictionary key");
        auto pair = new (pool()) DictPairAst;
        pair->m_key.reset(ast);
        pair->m_value.reset(new (pool()) NoExpressionAst);
        ast = pair;
        break;
    }
    case T_Colon:
    {
        consume();
        auto pair(new (pool()) DictPairAst);
        pair->m_key.reset(ast);
        ast = pair;
        ExpressionAst *value(0);
        if (!parsePredicate(value))
        {
            value = new (pool()) NoExpressionAst;
            errorExpectedAfter(cursor(), "value", LexemSpelling[T_Colon]);
        }
        pair->m_value.reset(value);
        break;
    }
    default:
        return false;
    }
    return true;
}

bool Parser::consumeStatementTail() const
{
    while (T_Semicolon == LA())
        consume();

    RULE_ENDS_ON(T_Newline == LA());
    return false;
}

bool Parser::tryInsertLexemBefore(Lexem lexToInsert, Lexem lexToTrigger) const
{
    if (LA() == lexToTrigger)
    {
        error(cursor(), QString("Expected %1 before %2")
              .arg(LexemSpelling[lexToInsert])
              .arg(LexemSpelling[lexToTrigger]));
        return true;
    }
    return false;
}

bool Parser::parseExpressionUpToComma(ExpressionAst *&ast) const
{
    ExpressionAst *expr(0);
    if (parsePredicate(expr))
    {
        ast = expr;
        return true;
    }
    return false;
}

/**
 * @brief Handles rule for "test" in official grammar specification
 * @param ast
 * @return true if ast was created
 */
bool Parser::parsePredicate(ExpressionAst *&ast) const
{
    RULE_REQUIRES(exprLogicalOr(ast));

    if (LA() == T_KwIf) {
        ExpressionAst *cond(0);
        SourceLocation ifLoc = consume();
        if (!exprLogicalOr(cond)) {
            errorExpectedAfter(ifLoc, "expression", LexemSpelling[T_KwIf]);
        }
        if (LA() != T_KwElse) {
            errorExpectedAfter(cursor(), "else", "if statement in expression");
            return true;
        }
        ExpressionAst *otherwise(0);
        SourceLocation elseLoc = consume();
        if (!parsePredicate(otherwise)) {
            errorExpectedAfter(elseLoc, "expression", LexemSpelling[T_KwElse]);
            return true;
        }

        auto ifAst = new (pool()) IfExpressionAst;
        ifAst->m_condition.reset(cond);
        ifAst->m_then.reset(ast);
        ifAst->m_otherwise.reset(otherwise);
        ast = ifAst;
    }

    return true;
}

bool Parser::exprLogicalOr(ExpressionAst *&ast) const
{
    RULE_REQUIRES(exprLogicalAnd(ast));
    PARSER_LOGIC_CHECK(ast != nullptr, "ast cannot be null here");

    const Lexem ahead(LA());
    if (ahead == T_KwOr) {
        ExpressionAst *rhs(0);
        SourceLocation opLoc = consume();
        if (exprLogicalOr(rhs)) {
            auto op = new(pool()) BinaryExpressionAst;
            op->m_kind = BinaryExpressionAst::convertToken(ahead);
            op->m_left.reset(ast);
            op->m_right.reset(rhs);
            ast = op;
        } else {
            errorExpectedAfter(opLoc, "expression", LexemSpelling[ahead]);
            //! TODO: add error recovery
        }
    }

    return true;
}

bool Parser::exprLogicalAnd(ExpressionAst *&ast) const
{
    RULE_REQUIRES(exprLogicalNot(ast));
    PARSER_LOGIC_CHECK(ast != nullptr, "ast cannot be null here");

    const Lexem ahead(LA());
    if (ahead == T_KwAnd) {
        ExpressionAst *rhs(0);
        SourceLocation opLoc = consume();
        if (exprLogicalAnd(rhs)) {
            auto op = new(pool()) BinaryExpressionAst;
            op->m_kind = BinaryExpressionAst::convertToken(ahead);
            op->m_left.reset(ast);
            op->m_right.reset(rhs);
            ast = op;
        } else {
            errorExpectedAfter(opLoc, "expression", LexemSpelling[ahead]);
            //! TODO: add error recovery
        }
    }

    return true;
}

bool Parser::exprLogicalNot(ExpressionAst *&ast) const
{
    if (LA() == T_KwNot) {
        consume();
        RULE_REQUIRES(exprLogicalNot(ast));
        auto op = new(pool()) UnaryExpressionAst;
        op->m_kind = UnaryExpressionAst::LogicalNot;
        op->m_child.reset(ast);
        ast = op;
    } else {
        RULE_REQUIRES(parseComparison(ast));
    }
    return true;
}

/**
 * @brief Handles rule for "comparison" in official grammar specification
 * @param ast
 * @return true if ast was created
 */
bool Parser::parseComparison(ExpressionAst *&ast) const
{
    ast = exprArifmetic();
    if (nullptr == ast)
        return false;

    BinaryExpressionAst::Kind kind(BinaryExpressionAst::Equal);
    SourceLocation kindLoc;
    switch (LA())
    {
    case T_KwIs:
    {
        kind = BinaryExpressionAst::IsThis;
        if (LA(2) == T_KwNot)
        {
            SourceLocation notLoc;
            kindLoc = consume();
            notLoc = consume();

            ExpressionAst *right(0);
            RULE_REQUIRES(parseComparison(right));
            auto isExpr = new (pool()) BinaryExpressionAst;
            isExpr->m_kind = kind;
            isExpr->m_left.reset(ast);
            isExpr->m_right.reset(right);
            isExpr->m_operator = kindLoc;

            auto notExpr = new (pool()) UnaryExpressionAst;
            notExpr->m_child.reset(isExpr);
            notExpr->m_kind = UnaryExpressionAst::LogicalNot;
            ast = notExpr;
            return true;
        }
        break;
    }
    case T_KwIn:
    {
        kind = BinaryExpressionAst::InCollection;
        break;
    }
    case T_OpLT:
    {
        kind = BinaryExpressionAst::LesserThan;
        break;
    }
    case T_OpEQ:
    {
        kind = BinaryExpressionAst::Equal;
        break;
    }
    default:
    {
        return true;
    }
    }

    kindLoc = consume();

    ExpressionAst *right(0);
    RULE_REQUIRES(parseComparison(right));

    auto expr = new (pool()) BinaryExpressionAst;
    expr->m_kind = kind;
    expr->m_operator = kindLoc;
    expr->m_left.reset(ast);
    expr->m_right.reset(right);
    ast = expr;
    return true;
}

/**
 * @brief Parses binary expression separated by arifmetic and bitwise operators
 */
ExpressionAst *Parser::exprArifmetic() const
{
    auto left = exprBitwiseXor();

    const Lexem ahead(LA());
    if (ahead == T_OpBitwiseOr) {
        auto expr = new (pool()) BinaryExpressionAst;
        expr->m_kind = BinaryExpressionAst::convertToken(ahead);
        expr->m_operator = consume();

        if (nullptr == left) {
            errorExpectedBefore(expr->m_operator, "expression", LexemSpelling[ahead]);
            left = new (pool()) NoExpressionAst;
        }
        expr->m_left.reset(left);

        auto right = exprArifmetic();
        if (nullptr == right) {
            errorExpectedAfter(cursor(), "expression", LexemSpelling[ahead]);
            right = new (pool()) NoExpressionAst;
        }
        expr->m_right.reset(right);
        return expr;
    }

    return left;
}

ExpressionAst *Parser::exprBitwiseXor() const
{
    auto left = exprBitwiseAnd();

    const Lexem ahead(LA());
    if (ahead == T_OpBitwiseXor) {
        auto expr = new (pool()) BinaryExpressionAst;
        expr->m_kind = BinaryExpressionAst::convertToken(ahead);
        expr->m_operator = consume();

        if (nullptr == left) {
            errorExpectedBefore(expr->m_operator, "expression", LexemSpelling[ahead]);
            left = new (pool()) NoExpressionAst;
        }
        expr->m_left.reset(left);

        auto right = exprBitwiseXor();
        if (nullptr == right) {
            errorExpectedAfter(cursor(), "expression", LexemSpelling[ahead]);
            right = new (pool()) NoExpressionAst;
        }
        expr->m_right.reset(right);
        return expr;
    }

    return left;
}

ExpressionAst *Parser::exprBitwiseAnd() const
{
    auto left = exprBitwiseShift();

    const Lexem ahead(LA());
    if (ahead == T_OpBitwiseAnd) {
        auto expr = new (pool()) BinaryExpressionAst;
        expr->m_operator = consume();
        expr->m_kind = BinaryExpressionAst::convertToken(ahead);

        if (nullptr == left) {
            errorExpectedBefore(expr->m_operator, "expression", LexemSpelling[ahead]);
            left = new (pool()) NoExpressionAst;
        }
        expr->m_left.reset(left);

        auto right = exprBitwiseAnd();
        if (nullptr == right) {
            errorExpectedAfter(cursor(), "expression", LexemSpelling[ahead]);
            right = new (pool()) NoExpressionAst;
        }
        expr->m_right.reset(right);
        return expr;
    }

    return left;
}

ExpressionAst *Parser::exprBitwiseShift() const
{
    auto left = exprAdd();

    const Lexem ahead(LA());
    if ((ahead == T_OpLeftShift) || (ahead == T_OpRightShift)) {
        auto expr = new (pool()) BinaryExpressionAst;
        expr->m_kind = BinaryExpressionAst::convertToken(ahead);
        expr->m_operator = consume();

        if (nullptr == left) {
            errorExpectedBefore(expr->m_operator, "expression", LexemSpelling[ahead]);
            left = new (pool()) NoExpressionAst;
        }
        expr->m_left.reset(left);

        auto right = exprBitwiseShift();
        if (nullptr == right) {
            errorExpectedAfter(cursor(), "expression", LexemSpelling[ahead]);
            right = new (pool()) NoExpressionAst;
        }
        expr->m_right.reset(right);
        return expr;
    }

    return left;
}

ExpressionAst *Parser::exprAdd() const
{
    auto left = exprMultiply();

    /* Cannot recover here, unary ± was handled in exprFactor() */
    if (nullptr != left) {
        const Lexem ahead(LA());
        if ((ahead == T_OpAdd) || (ahead == T_OpSub)) {
            auto ast = new (pool()) BinaryExpressionAst;
            ast->m_kind = BinaryExpressionAst::convertToken(ahead);
            ast->m_operator = consume();
            ast->m_left.reset(left);

            ExpressionAst *right = exprAdd();
            if (nullptr == right) {
                errorExpectedAfter(cursor(), "expression", LexemSpelling[ahead]);
                right = new (pool()) NoExpressionAst;
            }
            ast->m_right.reset(right);
            return ast;
        }
    }

    return left;
}

ExpressionAst *Parser::exprMultiply() const
{
    auto left = exprFactor();

    const Lexem ahead(LA());
    switch (ahead) {
    case T_OpMul:
    case T_OpDiv:
    case T_OpFloorDiv:
    case T_OpMod: {
        auto ast = new (pool()) BinaryExpressionAst;
        ast->m_kind = BinaryExpressionAst::convertToken(ahead);
        ast->m_operator = consume();
        if (nullptr == left) {
            errorExpectedBefore(ast->m_operator, "expression", LexemSpelling[ahead]);
            left = new (pool()) NoExpressionAst;
        }
        ast->m_left.reset(left);
        auto right = exprMultiply();
        if (nullptr == right) {
            errorExpectedAfter(ast->m_operator.locationAtRight(strlen(LexemSpelling[ahead])),
                               "expression",
                               LexemSpelling[ahead]);
            right = new (pool()) NoExpressionAst;
        }
        ast->m_right.reset(right);
        return ast;
    }
    default:
        break;
    }

    return left;
}

ExpressionAst *Parser::exprFactor() const
{
    UnaryExpressionAst::Kind kind = UnaryExpressionAst::UnaryPlus;

    const Lexem ahead = LA();
    switch (ahead) {
    case T_OpAdd:
        kind = UnaryExpressionAst::UnaryPlus;
        break;
    case T_OpSub:
        kind = UnaryExpressionAst::UnaryMinus;
        break;
    case T_OpComplement:
        kind = UnaryExpressionAst::BitwiseNot;
        break;

    default:
        return exprPower();
    }

    auto ast = new (pool()) UnaryExpressionAst;
    ast->m_kind = kind;
    auto locationOfKind = consume();

    ExpressionAst *child = exprPower();
    if (nullptr == child) {
        errorExpectedAfter(locationOfKind, "expression", LexemSpelling[ahead]);
        child = new (pool()) NoExpressionAst;
    }
    ast->m_child.reset(child);
    return ast;
}

ExpressionAst *Parser::exprPower() const
{
    auto left = parseAtomicExpression();

    if (nullptr != left) {
        while (tryParseTrailer(left))
            ; // pass
    }

    const Lexem ahead(LA());
    if (ahead == T_OpPower) {
        auto expr = new (pool()) BinaryExpressionAst;
        expr->m_kind = BinaryExpressionAst::Power;
        expr->m_operator = consume();
        ExpressionAst *degree = exprFactor();

        if (nullptr == left) {
            errorExpectedBefore(expr->m_operator,
                                 "expression",
                                 LexemSpelling[T_OpPower]);
            left = new (pool()) NoExpressionAst;
        }
        expr->m_left.reset(left);
        if (nullptr == degree) {
            errorExpectedAfter(expr->m_operator.locationAtRight(STRLEN("**")),
                               "expression",
                               LexemSpelling[T_OpPower]);
            degree = new (pool()) NoExpressionAst;
        }
        expr->m_right.reset(degree);
        return expr;
    }

    return left;
}

// TODO: refactor it, too many returns
bool Parser::parseSubscript(ExpressionAst *&ast) const
{
    if (LA() == T_OpDot && LA(2) == T_OpDot && LA(3) == T_OpDot)
    {
        PARSER_LOGIC_CHECK(false,
                           "This subscript type not implemented: array[...]");
    }

    auto item(new (pool()) SubscriptAst());

    ExpressionAst *predicate(0);
    if (T_Colon != LA())
    {
        RULE_REQUIRES(parsePredicate(predicate));
        item->m_begin.reset(predicate);
        if (T_Colon != LA())
        {
            ast = item;
            return true;
        }
    }
    consume();

    if (T_Colon != LA())
    {
        if (T_RightBracket == LA())
        {
            ast = item;
            return true;
        }
        else if (parsePredicate(predicate))
        {
            item->m_end.reset(predicate);
            if (T_Colon != LA())
            {
                ast = item;
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    consume();

    if (parsePredicate(predicate))
        item->m_step.reset(predicate);

    ast = item;
    return true;
}

// TODO: let's study it to parse the following illegal code:
//      my_list = 'helen', 'ann', 'victoria']; # no starting bracket
ExpressionAst *Parser::parseAtomicExpression() const
{
    const Lexem ahead(LA());
    switch (ahead)
    {
    case T_DocString: {
        auto ast = new (pool()) LiteralExpressionAst;
        ast->m_literal = consume();
        ast->m_tokenKind = T_String;
        return ast;
    }

    case T_None:
    case T_True:
    case T_False:
    case T_String:
    case T_FloatValue:
    case T_IntegerValue:
    case T_ComplexValue: {
        auto ast = new (pool()) LiteralExpressionAst;
        ast->m_literal = consume();
        ast->m_tokenKind = ahead;
        return ast;
    }

    case T_Identifier: {
        auto ast = new (pool()) IdentifierAst;
        ast->m_identifier = consume();
        return ast;
    }

    case T_LeftBracket:
    {
        if (T_RightBracket == LA(2))
        {
            auto ast = new (pool()) ListExpressionAst;
            ast->m_kind = ListExpressionAst::List;
            ast->m_openingToken = consume();
            ast->m_closingToken = consume();
            return ast;
        }
        else
        {
            const auto startLoc = consume();
            PySequence expressions;
            RULE_REQUIRES(parseListOfPredicates(expressions));

            auto ast = new (pool()) ListExpressionAst;
            ast->m_kind = ListExpressionAst::List;
            ast->m_items = expressions;
            ast->m_openingToken = startLoc;
            if (T_RightBracket == LA())
            {
                ast->m_closingToken = consume();
            }
            else
            {
                errorExpectedAfter(cursor(), LexemSpelling[T_RightBracket],
                                    "last expression in list");
            }
            return ast;
        }
    }

    case T_LeftBrace:
    {
        if (T_RightBrace == LA(2))
        {
            auto list = new (pool()) ListExpressionAst;
            list->m_kind = ListExpressionAst::Dict;
            list->m_openingToken = consume();
            list->m_closingToken = consume();
            return list;
        }
        else
        {
            const auto startLoc = consume();
            PySequence expressions;
            RULE_REQUIRES(parseDictOrSetInitializer(expressions));

            auto list = new (pool()) ListExpressionAst;
            list->m_kind = ListExpressionAst::Dict;
            list->m_items = expressions;
            list->m_openingToken = startLoc;
            if (T_RightBrace == LA())
            {
                list->m_closingToken = consume();
            }
            else
            {
                errorExpectedAfter(cursor(), LexemSpelling[T_RightBrace],
                                    "last expression in dict or set");
            }
            return list;
        }
    }

        // TODO: should be '(' yield_expr|testlist_comp ')'
    case T_LeftParen:
    {
        if (T_RightParen == LA(2))
        {
            auto list = new (pool()) ListExpressionAst;
            list->m_kind = ListExpressionAst::Tuple;
            list->m_openingToken = consume();
            list->m_closingToken = consume();
            return list;
        }
        else
        {
            const auto startLoc = consume();
            PySequence expressions;
            RULE_REQUIRES(parseListOfPredicates(expressions));
            auto list = new (pool()) ListExpressionAst;
            list->m_items = expressions;
            list->m_kind = ListExpressionAst::Tuple;
            list->m_openingToken = startLoc;
            if (T_RightParen == LA())
            {
                list->m_closingToken = consume();
            }
            else
            {
                errorExpectedAfter(cursor(), LexemSpelling[T_RightParen],
                                    "last expression in dict or set");
            }
            return list;
        }
    }

    default:
        break;
    }
    return nullptr;
}

void Parser::skipToNewline() const
{
    while (LA() != T_Newline) {
        PARSER_LOGIC_CHECK(LA() != T_EndOfFile, "Unexprected end of file on error recovering");
        consume();
    }
}

} // namespace pylang
