#ifndef PYLANG_PARSER_H
#define PYLANG_PARSER_H

#include "ParserBase.h"

namespace pylang {

class Parser : public Internal::ParserBase
{
public:
    virtual void operator()(UnitBuilder const &control,
                            const QByteArray &source) const;

    Parser();
    ~Parser();

private:
    /**
       @brief Reads complex or simple statement
       @param ast - output variable
       @return True if statement successfully parsed
     */
    bool readStatement(Internal::StmtAst *&ast) const;

    void parseBody(PySequence &body, AstRef<DocStringAst> *pDocstring =  nullptr) const;
    bool parseAlignedBlock(PySequence &sequence, bool indentConsumed = false) const;
    bool parseFormalArguments(PySequence &sequence) const;
    bool parseDottedName(ExpressionAst *&ast) const;
    bool parseBaseClasses(PySequence &sequence) const;
    bool parseExceptClause(Internal::StmtAst *&ast) const;

    bool parseImport(Internal::StmtAst *&ast) const;
    bool parseFunctionDef(Internal::StmtAst *&ast) const;
    bool parseClassDef(Internal::StmtAst *&ast) const;
    bool parseTryStmt(Internal::StmtAst *&ast) const;
    bool parsePass(Internal::StmtAst *&ast) const;
    bool parseExpressionStmt(Internal::StmtAst *&ast) const;
    bool parseReturn(Internal::StmtAst *&ast) const;
    bool parsePrint(Internal::StmtAst *&ast) const;
    bool parseIfStmt(Internal::StmtAst *&ast, bool ignoreElse = false) const;
    bool parseWhileStmt(Internal::StmtAst *&ast) const;
    bool parseForStmt(Internal::StmtAst *&ast) const;
    bool parseRaiseStmt(Internal::StmtAst *&ast) const;

    bool tryParseElseBlock(Internal::OtherwisedAst &ast) const;
    bool tryParseTrailer(ExpressionAst *&ast) const;

    bool parseListOfDottedAsNames(PySequence &sequence) const;
    bool parseListOfPredicates(PySequence &sequence) const;
    bool parseListOfArifmetics(PySequence &sequence) const;
    bool parseListOfSubscripts(PySequence &sequence) const;
    void parseDictInitializerTail(PySequence &expressions) const;
    bool parseDictOrSetInitializer(PySequence &expressions) const;
    inline bool parseDictPair(ExpressionAst *&ast) const;

    bool consumeStatementTail() const;

    bool tryInsertLexemBefore(Lexem lexToInsert, Lexem lexToTrigger) const;

    bool parseExpressionUpToComma(ExpressionAst *&ast) const;

    bool parsePredicate(ExpressionAst *&ast) const;
    bool exprLogicalOr(ExpressionAst *&ast) const;
    bool exprLogicalAnd(ExpressionAst *&ast) const;
    bool exprLogicalNot(ExpressionAst *&ast) const;
    bool parseComparison(ExpressionAst *&ast) const;
    ExpressionAst *exprArifmetic() const;
    ExpressionAst *exprBitwiseXor() const;
    ExpressionAst *exprBitwiseAnd() const;
    ExpressionAst *exprBitwiseShift() const;
    ExpressionAst *exprAdd() const;
    ExpressionAst *exprMultiply() const;
    ExpressionAst *exprFactor() const;
    ExpressionAst *exprPower() const;

    bool parseSubscript(ExpressionAst *&ast) const;
    ExpressionAst *parseAtomicExpression() const;

    void skipToNewline() const;
};

} // namespace pylang

#endif // PYLANG_PARSER_H
