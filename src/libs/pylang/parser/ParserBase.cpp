#include "ParserBase.h"
#include "../ast/exception/parserexception.h"

namespace pylang {
namespace Internal {

/* ------------------------- Macro definitions ------------------------- */

/* Silents warning for unused variable, but you should check manually
   each applicance of this macro
 */
#define PARSER_UNUSED(arg) ((void)(arg))

#define PARSER_TO_STRING(arg) PARSER_TO_STRING_IMPL(arg)

#define PARSER_TO_STRING_IMPL(arg) #arg

/* enclosing function will throw exception if check failed */
#define PARSER_LOGIC_CHECK(condition, messageOnFailure) \
    if (false == (condition)) { \
        throw ParserException(m_builder.tokenizedUnit(), cursor(), \
                std::string("'") + std::string(messageOnFailure) \
                + std::string("' __LINE__ is ") + PARSER_TO_STRING(__LINE__)); \
    } \

/* enclosing function will return false if check failed */
#define RULE_REQUIRES(condition) \
    if (false == (condition)) return false;

#define RULE_ENDS_ON(condition) \
    if (true == (condition)) { \
        consume(); \
        return true; \
    }

#define RULE_ENDS_BEFORE(condition) \
    if (true  (condition)) return true;

/* -------------------------- Implementation --------------------------- */

void ParserBase::prepare(const UnitBuilder &builder, const QByteArray &source) const
{
    m_aheadTokenIndex = 0;
    m_builder = builder;
    m_tunit = &(m_builder.tokenizedUnit());

    Lexer lexer(builder, source.data(), source.size());
    lexer.init();

    Token tok;
    do {
        tok = lexer.read();
        m_tunit->pushToken(tok);
    }
    while (tok.lexem() != T_EndOfFile);
}

ParserBase::ParserBase()
    : m_aheadTokenIndex(0)
{
}

ParserBase::~ParserBase()
{
}

MemoryPool &ParserBase::pool() const
{
    return m_builder.pool();
}

Lexem ParserBase::LA(unsigned step) const
{
    PARSER_LOGIC_CHECK(step + m_aheadTokenIndex < m_tunit->tokensCount(), "LA() overflow");
    return m_tunit->tokenAt(step + m_aheadTokenIndex).lexem();
}

SourceLocation ParserBase::consume() const
{
    PARSER_LOGIC_CHECK(m_aheadTokenIndex < m_tunit->tokensCount(), "consume() overflow");
    SourceLocation ret = m_tunit->tokenAt(m_aheadTokenIndex).range().first();
    ++m_aheadTokenIndex;
    return ret;
}

SourceLocation ParserBase::cursor() const
{
    PARSER_LOGIC_CHECK(m_aheadTokenIndex < m_tunit->tokensCount(), "cursor() overflow");
    return m_tunit->tokenAt(m_aheadTokenIndex).range().first();
}

void ParserBase::error(SourceLocation const& loc, QString const& message) const
{
    if (m_builder.isDiagnosticEnabled())
    {
        Diagnostic diagnostic;
        diagnostic.setKind(Diagnostic::Error);
        diagnostic.setLocation(loc);
        diagnostic.setMessage(message);
        m_builder.pushDiagnostic(diagnostic);
    }
}

void ParserBase::warning(SourceLocation const& loc, QString const& message) const
{
    if (m_builder.isDiagnosticEnabled())
    {
        Diagnostic diagnostic;
        diagnostic.setKind(Diagnostic::Warning);
        diagnostic.setLocation(loc);
        diagnostic.setMessage(message);
        m_builder.pushDiagnostic(diagnostic);
    }
}

void ParserBase::errorExpectedAfter(const SourceLocation &loc,
                                 const char *expected, const char *after) const
{
    error(loc, QObject::trUtf8("Expected %1 after %2")
          .arg(expected)
          .arg(after));
}

void ParserBase::errorExpectedBefore(const SourceLocation &loc,
                                  const char *expected, const char *after) const
{
    error(loc, QObject::trUtf8("Expected %1 before %2")
          .arg(expected)
          .arg(after));
}

} // namespace Internal
} // namespace pylang
