#ifndef PYLANG_INTERNAL_PARSERBASE_H
#define PYLANG_INTERNAL_PARSERBASE_H

#include "../diagnostic/IDiagnosticClient.h"
#include "../diagnostic/DiagnosticMessage.h"
#include "../ast/AST.h"
#include "../ast/UnitBuilder.h"
#include "../lexer/Lexer.h"
#include "../Support/TokenizedUnit.h"
#include <QByteArray>

namespace pylang {
namespace Internal {

class ParserBase
{
public:
    /**
     * @brief operator ()
     * @param control - provides access to building translation unit
     * @param source - source in utf8 encoding
     * @return pointer to translation unit AST root node, never returns null
     */
    virtual void operator()(UnitBuilder const &builder,
                            const QByteArray &source) const = 0;

    /**
     * @brief Splits unit to tokens before parsing
     * @note Do not forget call this operator from derived class, otherwise
     *       unit will be not properly initialized and LA() can behave
     *       unexpectedly
     */
    void prepare(UnitBuilder const &builder,
                 const QByteArray &source) const;

protected:
    ParserBase();
    ~ParserBase();

    MemoryPool &pool() const;
    Lexem LA(unsigned step = 1) const;

    /**
       @brief Consumes token from Lexer
       @return index of token, 0 is invalid index
     */
    SourceLocation consume() const;
    SourceLocation cursor() const;

    void error(SourceLocation const& loc, QString const& message) const;
    void warning(SourceLocation const& loc, QString const& message) const;
    void errorExpectedAfter(SourceLocation const& loc,
                             const char *expected, const char *after) const;
    void errorExpectedBefore(SourceLocation const& loc,
                              const char *expected, const char *after) const;

protected: // attributes
    mutable UnitBuilder m_builder;
    mutable TokenizedUnit *m_tunit;
    mutable unsigned m_aheadTokenIndex;
};

} // namespace Internal
} // namespace pylang

#endif // PYLANG_INTERNAL_PARSERBASE_H
