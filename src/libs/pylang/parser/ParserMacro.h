#ifndef PYTHON_PARSERMACRO_H
#define PYTHON_PARSERMACRO_H

#ifdef NEED_RUNTIME_CHECKS

#include <QtCore/QDebug>
#include <typeinfo>
#include "ast/AST_pwd.h"

#ifdef Q_CC_GNU
#include <cxxabi.h>
#endif // Q_CC_GNU

#endif // NEED_RUNTIME_CHECKS

namespace pylang {

/**
 * \brief ast_cast - casts pointer from base type Ast to specified type,
 * checks result using dynamic_cast in development version
 */
template <class TAst>
TAst *ast_cast(Ast *casted)
{
#ifdef NEED_RUNTIME_CHECKS
    TAst *check = dynamic_cast<TAst*>(casted);
    if (check == nullptr)
    {
        const char *dcasted = typeid(casted).name();
        const char *dname = typeid(TAst).name();
#ifdef Q_CC_GNU
        char *gcccasted = abi::__cxa_demangle(dcasted, 0, 0, 0);
        char *gccname = abi::__cxa_demangle(dname, 0, 0, 0);
        dcasted = gcccasted;
        dname = gccname;
#endif
        if (casted == nullptr)
        {
            qDebug() << "Invalid cast from " << dcasted
                     << " to type " << dname
                     << " - argument is nullptr";
        }
        else
        {
            qDebug() << "Invalid cast from " << dcasted
                     << " to type " << dname
                     << " - argument has another type";
        }
#ifdef Q_CC_GNU
        free(gcccasted);
        free(gccname);
#endif
    }
#endif
    return reinterpret_cast<TAst *>(casted);
}

} // namespace pylang

#define HANDLE_BINARY_EXPRESSION2(ruleNumber, BinaryKind)                       \
    case ruleNumber:                                                            \
    {                                                                           \
        ExpressionAst *left = h.getAst<ExpressionAst>(0);                       \
        ExpressionAst *right = h.getAst<ExpressionAst>(2);                      \
        BinaryExpressionAst *ast = new (h.pool()) BinaryExpressionAst(          \
                    left,                                                       \
                    right,                                                      \
                    BinaryExpressionAst::BinaryKind                             \
                    );                                                          \
        h.storeValue(ast);                                                      \
        break;                                                                  \
    }

#define HANDLE_PREFIX_UNARY_EXPRESSION(ruleNumber, UnaryKind)                   \
    case ruleNumber:                                                            \
    {                                                                           \
        ExpressionAst *child = h.getAst<ExpressionAst>(1);                      \
        UnaryExpressionAst *ast = new (h.pool()) UnaryExpressionAst(            \
                child, UnaryExpressionAst::UnaryKind);                          \
        h.storeValue(ast);                                                      \
        break;                                                                  \
    }

#define HANDLE_LIST_CREATE(ruleNumber, ASTType) \
    case ruleNumber: \
    { \
        PySequence seq; \
        seq.append(h.getAst<ASTType>(0)); \
        h.storeValue(seq); \
        break; \
    }

#ifdef NEED_RUNTIME_CHECKS

#define HANDLE_LIST_APPEND_NO_CHECK(ruleNumber, ASTType, astOffset) \
    case ruleNumber: \
    { \
        PySequence seq = h.getAstList(0); \
        seq.append(h.getAst<ASTType>(int(astOffset))); \
        h.storeValue(seq); \
        break; \
    }

#define HANDLE_LIST_APPEND(ruleNumber, ASTType, astOffset) \
    case ruleNumber: \
    { \
        PySequence seq = h.getAstList(0); \
        if (seq.isEmpty()) \
            qDebug() << "HANDLE_LIST_APPEND macro reports that PySequence is empty"; \
        seq.append(h.getAst<ASTType>(int(astOffset))); \
        h.storeValue(seq); \
        break; \
    }

#else // NEED_RUNTIME_CHECKS

#define HANDLE_LIST_APPEND(ruleNumber, ASTType, astOffset) \
    case ruleNumber: \
    { \
        PySequence seq = h.getAstList(0); \
        seq.append(h.getAst<ASTType>(int(astOffset))); \
        h.storeValue(seq); \
        break; \
    }

#define HANDLE_LIST_APPEND_NO_CHECK(ruleNumber, ASTType, astOffset) \
    HANDLE_LIST_APPEND(ruleNumber, ASTType, astOffset)

#endif // NEED_RUNTIME_CHECKS

/**
 * Macro for common situations
 */

#define UNITE_TO_TUPLE_IF_NOT_SINGLE(resultName, offset) \
    const PySequence &macroSeq = h.getAstList(offset); \
    ExpressionAst *resultName = 0; \
    if (macroSeq.size() == 1) \
        resultName = ast_cast<ExpressionAst>(macroSeq.front()); \
    else \
        resultName = new (h.pool()) ListExpressionAst( \
                macroSeq, \
                ListExpressionAst::Tuple \
            );

#endif // PYTHON_PARSERMACRO_H
