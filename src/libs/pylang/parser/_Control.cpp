//#include "Control.h"
////#include "pythongrammar_p.h"

//#include "../symbols/Symbol.h"

//namespace Pylang {

//TranslationUnit::TranslationUnit(Language sourceLanguage)
//    : _language(sourceLanguage)
//    , _client(0)
//    , _table(0)
//    , _translationUnit(0)
//    , _pool(0)
//{
//    _pool = new MemoryPool();
//    _table = new (*_pool) StringsHash(*_pool);

//    Token invalidToken(T_Invalid);
//    invalidToken.setGenerated(true);
//    _tokens.push_back(invalidToken);

//    _wordEmpty = _table->findOrInsertLiteral("", 0);
//}

//TranslationUnit::~TranslationUnit()
//{
//    delete _table;
//    delete _pool;
//}

//TranslationUnit::Language TranslationUnit::language() const
//{
//    return _language;
//}

///**
// * \brief Gives token by it's index. AST keeps tokens indicies in every node,
// * however, if token index in AST is 0 than appropriate token does not exists
// * in document source
// * \param index - index between 1..tokensCount, index 0 will return invalid token
// * \return
// */
//const Token &TranslationUnit::tokenAt(unsigned index) const
//{
//    return _tokens.at(index);
//}

///**
// * \brief Gives literal by token's index. AST keeps tokens indicies in every node,
// * however, if token index in AST is 0 than appropriate token does not exists
// * in document source
// * \param index - index between 1..tokensCount, index 0 will return invalid token
// * \return literal for identifier tokens, empty literal otherwise
// */
//StringRef TranslationUnit::literalAt(quint32 index) const
//{
//    const Token &tk = _tokens.at(index);
//    if (tk.hasLiteral())
//        return tk.getLiteral();
//    return wordEmpty();
//}

//StringRef TranslationUnit::literal(const Symbol &symbol) const
//{
//    const Token &tk = _tokens.at(symbol.token_name);
//    if (tk.is(T_Identifier))
//        return literalAt(symbol.token_name);
//    return wordEmpty();
//}

//const StringsHash &TranslationUnit::table() const
//{
//    return *_table;
//}

//TranslationUnitAst &TranslationUnit::translationUnit() const
//{
//    return *_translationUnit;
//}

//IDiagnosticClient *TranslationUnit::client() const
//{
//    return _client;
//}

//StringsHash &TranslationUnit::table()
//{
//    return *_table;
//}

//MemoryPool &TranslationUnit::pool()
//{
//    return *_pool;
//}

//void TranslationUnit::setClient(IDiagnosticClient *client)
//{
//    _client = client;
//}

//StringRef TranslationUnit::wordEmpty() const
//{
//    return _wordEmpty;
//}

//} // namespace pylang
