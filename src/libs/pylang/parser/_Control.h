//#ifndef PYTHON_PARSERENGINE_H
//#define PYTHON_PARSERENGINE_H

//#include "../Support/StringsHash.h"

//#include "../lexer/Token.h"
//#include "../ast/AST_pwd.h"

//namespace Pylang {

//class IDiagnosticClient;
//class Symbol;

//class PYLANG_EXPORT TranslationUnit
//{
//public:
//    enum Language
//    {
//        Python2,
//        Python3,
//        RestrictedPython
//    };

//    TranslationUnit(Language sourceLanguage);
//    ~TranslationUnit();

//    Language language() const;

//    //! token info
//    const Token &tokenAt(quint32 index) const;
//    StringRef literalAt(quint32 index) const;
//    StringRef literal(const Symbol &symbol) const;

//    //! other
//    const StringsHash &table() const;
//    TranslationUnitAst &translationUnit() const;

//    IDiagnosticClient *client() const;

//    StringsHash &table();
//    MemoryPool &pool();
//    void setClient(IDiagnosticClient *client);

//private:
//    // TODO: ParserHelper should not be friend
//    friend class ParserHelper;
//    friend class Parser;
//    StringRef wordEmpty() const;

//    Language _language;
//    std::vector<Token> _tokens;
//    IDiagnosticClient *_client;
//    StringsHash *_table;
//    TranslationUnitAst *_translationUnit;
//    MemoryPool *_pool;

//    StringRef _wordEmpty;
//};

//} // namespace pylang

//#endif // PYTHON_PARSERENGINE_H
