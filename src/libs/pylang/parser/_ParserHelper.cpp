//#include "ParserHelper.h"
//#include "../ast/TranslationUnit.h"
//#include "../diagnostic/DiagnosticMessage.h"
//#include "../diagnostic/IDiagnosticClient.h"

////#include "pythongrammar_p.h"

//#include <cstring>
//#include <algorithm>

//#include <QDebug>

//namespace Pylang {

//static inline void emitMessage(IDiagnosticClient *client,
//                               const QString &message,
//                               const SourceLocation &location,
//                               Diagnostic::Kind kind)
//{
//    if (client)
//    {
//        Diagnostic mess;
//        mess.setKind(kind);
//        mess.setMessage(message);
//        mess.location() = location;
//        client->diagnose(mess);
//    }
//}

//ParserHelper::ParserHelper(TranslationUnit &control)
//    : _control(control)
//{
//    _false = store("False");
//    _true = store("True");
//    _none = store("None");

//    increase();
//    storeAction(0); // initial state
//}

//TranslationUnitAst *ParserHelper::getTranslationUnit()
//{
//    TranslationUnitAst *ret = getAst<TranslationUnitAst>(0);
//    _control._translationUnit = ret;
//    return ret;
//}

//TranslationUnit &ParserHelper::control()
//{
//    return _control;
//}

//MemoryPool &ParserHelper::pool()
//{
//    return _control.pool();
//}

///**
// * \brief Overloaded stack inrease function, stores token index
// */
//void ParserHelper::increase()
//{
//    LalrStack::increase();
//    storeTokenIndex(_control._tokens.size() - 1);
//}

///**
// * \brief store - stores string value in repository and returns new reference,
// * recommended way to add new identifier or literal to created Ast*
// * \param value - identifier of literal
// * \return reference to object in hash table
// */
//StringRef ParserHelper::store(const char *string)
//{
//    return _control.table().findOrInsertLiteral(string, strlen(string));
//}

//StringRef ParserHelper::store(const char *string, int size)
//{
//    return _control.table().findOrInsertLiteral(string, size);
//}

//quint32 ParserHelper::addToken(const Token &token, bool isGenerated)
//{
//    _control._tokens.push_back(token);
//    _control._tokens.back().setGenerated(isGenerated);
//    return (_control._tokens.size() - 1);
//}

//StringRef ParserHelper::wordFalse() const
//{
//    return _false;
//}

//StringRef ParserHelper::wordTrue() const
//{
//    return _true;
//}

//StringRef ParserHelper::wordNone() const
//{
//    return _none;
//}

//StringRef ParserHelper::literal(const char *source, const SourceLocation &location)
//{
//    const char *start = source + location.begin();
//    return store(start, location.length());
//}

//void ParserHelper::emitError(const QString &message, const SourceLocation &location)
//{
//    emitMessage(_control.client(), message, location,
//                Diagnostic::Error);
//}

//void ParserHelper::emitPython3Error(const QString &message, const SourceLocation &location)
//{
//    emitMessage(_control.client(), message, location,
//                Diagnostic::Error_Python3);
//}

//void ParserHelper::emitRPythonError(const QString &message, const SourceLocation &location)
//{
//    emitMessage(_control.client(), message, location,
//                Diagnostic::Error_RPython);
//}

//void ParserHelper::emitWarning(const QString &message, const SourceLocation &location)
//{
//    emitMessage(_control.client(), message, location,
//                Diagnostic::Warning);
//}

//QString ParserHelper::substring(const char *source, const SourceLocation &location) const
//{
//    if (location.length() == 0)
//        return QString();

//    const char *start = source + location.begin();
//    return QString::fromAscii(start, location.length());
//}

//} // namespace pylang
