//#ifndef PYTHON_PARSERHELPER_H
//#define PYTHON_PARSERHELPER_H

//#include "../Support/StringRef.h"
//#include "LalrStack.h"

//namespace Pylang {

//class TranslationUnit;
//class Lexer;
//class Token;

//class ParserHelper : public LalrStack
//{
//public:
//    ParserHelper(TranslationUnit &control);

//    //! Called when parser stops
//    TranslationUnitAst *getTranslationUnit();

//    TranslationUnit &control();
//    MemoryPool &pool();

//    void increase();

//    StringRef store(const char *string);
//    StringRef store(const char *string, int size);
//    quint32 addToken(const Token &token, bool isGenerated);

//    StringRef wordFalse() const;
//    StringRef wordTrue() const;
//    StringRef wordNone() const;
//    StringRef literal(const char *source, const SourceLocation &location);

//    void emitError(const QString &message, const SourceLocation &location);
//    void emitPython3Error(const QString &message, const SourceLocation &location);
//    void emitRPythonError(const QString &message, const SourceLocation &location);
//    void emitWarning(const QString &message, const SourceLocation &location);

//    QString substring(const char *source, const SourceLocation &location) const;

//private:
//    TranslationUnit &_control;
//    StringRef _false;
//    StringRef _true;
//    StringRef _none;
//};

//} // namespace pylang

//#endif // PYTHON_PARSERHELPER_H
