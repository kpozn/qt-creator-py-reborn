//#include "QlalrParser.h"
//#include "Control.h"

//#ifdef NEED_RUNTIME_CHECKS
//#include <stdexcept>
//#endif

////#define TRACE_PYTHON_PARSING

//namespace Pylang {

//namespace /*anonymous*/ {

//}

//    /**
//     * \brief QLalrParser
//     * \param control
//     * \param source - utf8 source code
//     * \param sourceSize - size of source code string in bytes
//     */
//QLalrParser::QLalrParser(Control &control,
//                         const char *source, unsigned size)
//    :h(control)
//    ,_lexer(control, source, size)
//    ,_source(source)
//{
//}

//QLalrParser::~QLalrParser()
//{
//}

//TranslationUnitAst *QLalrParser::parse()
//{
//    _silentedTokensCount = 0;
//    _current.setLexem(INVALID_TOKEN);

//    forever
//    {
//        if (_current.is(INVALID_TOKEN)
//            && (-TERMINAL_COUNT != action_index[h.getAction()]))
//        {
//            _current = _lexer.read();
//            h.addToken(_current, false);
//            if (_silentedTokensCount)
//                _silentedTokensCount -= 1;
//        }

//        int action = t_action(h.getAction(), _current.lexem());
//        if (action == ACCEPT_STATE)
//        {
//            return h.getTranslationUnit();
//        }
//        else if (action > 0)
//        {
//            h.increase();
//            h.storeAction(action);
//            _current.setLexem(INVALID_TOKEN);
//        }
//        else if (action < 0)
//        {
//            int rule = -(action + 1);
//#ifdef TRACE_PYTHON_PARSING
//            traceParsingProcess(rule);
//#endif
//            h.decrease(rhs[rule]);
//            action = h.getAction();
//            h.increase();
//            h.storeAction(nt_action (action, lhs[rule] - TERMINAL_COUNT));

//            onReduce(rule);
//        }
//        else // action == 0, error recovery
//        {
//            /** Error recovery here */
//            const int topOfStack = h.saveStackPosition();
//            bool ok = tryRejectToken();

//            if (!ok)
//            {
//                ok = tryInsertToken();
//            }

//            if (!ok)
//            {
//#ifdef TRACE_PYTHON_PARSING
//                if (_current.isNot(INVALID_TOKEN))
//                    qDebug() << "This token causes panic-mode recovery: "
//                             << spell[_current.lexem()];
//#endif
//                h.restoreStackPosition(topOfStack);
//                ok = tryRecoveryInPanic();
//            }

//            if (!ok)
//            {
//#ifdef TRACE_PYTHON_PARSING
//                if (_current.isNot(INVALID_TOKEN))
//                    qDebug() << "Cannot recover parser in token: "
//                             << spell[_current.lexem()];
//#endif
//                if (_current.lexem() != EOF_SYMBOL)
//                {
//                    _current.setLexem(INVALID_TOKEN);
//                }
//                else
//                {
//                    return (TranslationUnitAst *)0;
//                }
//            }
//        }
//    }
//}

//    /**
//     * \brief tryRejectToken - attempts to recover parser from error state by
//     * rejecting token, if it possible in current parser state.
//     * \return true if parser recovered successfully
//     */
//bool QLalrParser::tryRejectToken()
//{
//    // If there are no braces, any right brace should be rejected
//    if (_lexer.bracesStack().empty())
//    {
//        static const short rejectable[] = {
//            T_DL_RIGHT_PAREN, T_DL_RIGHT_BRACE, T_DL_RIGHT_BRACKET
//        };
//        for (const short *tptr = rejectable;
//             tptr < rejectable + sizeof(rejectable) / sizeof(short);
//             ++tptr)
//        {
//            if (_current.is(*tptr))
//            {
//                _current.setLexem(INVALID_TOKEN);
//                h.emitError(QString("Unexpected brace \'%1\'")
//                            .arg(spell[*tptr]),
//                            _lexer.location());
//                return true;
//            }
//        }
//    }
//    // Otherwise unmatching brace should be rejected
//    else
//    {
//        int realBraceKind = 0;
//        switch (_current.lexem())
//        {
//            case T_DL_RIGHT_PAREN:
//                realBraceKind = T_DL_LEFT_PAREN;
//                break;
//            case T_DL_RIGHT_BRACE:
//                realBraceKind = T_DL_LEFT_BRACE;
//                break;
//            case T_DL_RIGHT_BRACKET:
//                realBraceKind = T_DL_LEFT_BRACKET;
//                break;
//        }
//        int braceKind = _lexer.bracesStack().back();
//        if (realBraceKind && (braceKind != realBraceKind))
//        {
//            _current.setLexem(INVALID_TOKEN);
//            h.emitError(QString("Unmatching brace \'%1\'")
//                        .arg(spell[realBraceKind]),
//                        _lexer.location());
//            return true;
//        }
//    }

//    return false;
//}

////bool QLalrParser::tryInsertOnImplicitLineJoining()
////{
////    if (_lexer.implicitLineJoiningOn() && _lexer.newLineStarted())
////    {
////    }
////    return false;
////}

///*
// * TODO: add recovering through inserting new brace or identifier
// * should be possible only if _silentedTokensCount is zero
// * T_DL_LEFT_PAREN, T_DL_LEFT_BRACE, T_DL_LEFT_BRACKET
// */

///**
// * \brief Returns Token(T_UNRESOLVED) if new token inserted
// */
//Token QLalrParser::tryInsertTokenHelper(int action)
//{
//    static const unsigned electric[] = {
//        T_DL_RIGHT_PAREN, T_DL_RIGHT_BRACE, T_DL_RIGHT_BRACKET,
//        T_DL_COMMA, T_DL_COLON
//    };
//    const unsigned electricSize = sizeof(electric) / sizeof(unsigned);
//    const unsigned limit = (_current.lexem() == T_DL_INDENT) ? electricSize : (electricSize - 1);

//    Token ret(T_UNRESOLVED);

//    for (;;)
//    {
//        for (const unsigned *tptr = electric; tptr < electric + limit; ++tptr)
//        {
//            const int next = t_action(action, *tptr);
//            if (next > 0)
//            {
//                ret.setLexem(*tptr);
//                _lexer.insertBrace(*tptr);

//          //      h.increase();
//          //      h.storeAction(next);
//          //      h.storeValue(h.addToken(ret, true));
//                continue;
//            }
//        }
//        break;
//    }

//    return ret;
//}

///**
// * \brief Attempts to recover parser from error state by
// * inserting token that silents this error.
// * \param errorReported - optional, true if error message already emitted
// * \return true if parser recovered successfully
// *
// * \internal
// * _silentedTokensCount is main sign of the error, if it's not zero,
// * than only possible recovering is closing the last sequence
// */
//bool QLalrParser::tryInsertToken()
//{
//    const Token erroneousToken = _current;

//    for (; h.topIndex() >= 0; h.decrease(1))
//    {
//        int action = h.getAction();
//        Token inserted(T_UNRESOLVED);
//        do
//        {
//            inserted = tryInsertTokenHelper(action);
//            action = t_action(action, inserted.lexem());
//        } while (inserted.isNot(T_UNRESOLVED) && (action == 0));

//        if (0 == _silentedTokensCount)
//        {
//            //! TODO: check specific case when inserted == erroneousToken
//            if (0 == action)
//            {
//                h.emitError(QString("Expected \'%1\' instead of \'%2\'")
//                            .arg(QLatin1String(spell[inserted.lexem()]))
//                            .arg(QLatin1String(spell[erroneousToken.lexem()])),
//                            _lexer.location());
//                _current.setLexem(INVALID_TOKEN);
//            }
//            else
//            {
//                h.emitError(QString("Expected \'%1\' before \'%2\'")
//                            .arg(QLatin1String(spell[inserted.lexem()]))
//                            .arg(QLatin1String(spell[erroneousToken.lexem()])),
//                            _lexer.location());
//            }
//        }

//        if (inserted.isNot(T_UNRESOLVED))
//        {
//        }

//#ifdef TRACE_PYTHON_PARSING
//        qDebug() << "Decreasing stack:";
//        traceParsingProcess(h.getAction());
//#endif
//    }

//    QString message = getUnexpectedTokenMessage();
//    if (_current.is(T_IDENTIFIER))
//        message.append(", value is "
//                       + h.substring(_source, _lexer.location()));
//    h.emitError(message, _lexer.location());

//    return false;
//}

//    /**
//     * \brief tryRecoveryInPanic - attempts to recover parser from error state by
//     * searching next T_DL_NEWLINE token and clearing LalrStack to consume T_DL_NEWLINE.
//     * \return true if parser recovered successfully
//     */
//bool QLalrParser::tryRecoveryInPanic()
//{
//    while ((_current.lexem() != T_DL_NEWLINE)
//            && (_current.lexem() != EOF_SYMBOL)) // paranoia
//    {
//        _current = _lexer.read();
//        if (_silentedTokensCount)
//            _silentedTokensCount -= 1;
//    }
//    for (; h.topIndex() >= 0; h.decrease(1))
//    {
//        const int erroneousAction = h.getAction();
//        const int next = t_action(erroneousAction, _current.lexem());
//        if (next > 0)
//        {
//            return true;
//        }
//    }
//    return false;
//}

//QString QLalrParser::getUnexpectedTokenMessage()
//{
//    QString message = QLatin1String("Syntax error");
//    if (!_current.is(INVALID_TOKEN))
//    {
//        const QLatin1String spelled(spell[_current.lexem()]);
//        message = QString("Unexpected token \'%1\'").arg(spelled);
//    }
//    return message;
//}

//    /**
//     * \brief traceParsingProcess - traces each reduce step to QDebug, doesn't work when
//     * compiling this library as part of QtCreator
//     */
//void QLalrParser::traceParsingProcess(int ruleNo)
//{
//#ifdef NEED_RUNTIME_CHECKS
//    if (ruleNo < 0 || (ruleNo) + 1 >= RULE_COUNT)
//        return;

//    int ruleDebugIndex = rule_index[ruleNo];

//    QString result;
//    result += "*** reduce using rule #";
//    result += QString::number(ruleNo + 1);
//    result += " ";
//    result += spell[rule_info[ruleDebugIndex]];
//    result += " ::= ";
//    ++ruleDebugIndex;
//    for (int i = ruleDebugIndex; i < ruleDebugIndex + rhs[ruleNo]; ++i)
//    {
//        int symbol = rule_info[i];
//        if (const char *name = spell[symbol])
//            result += name;
//        else
//            result += QString::number(symbol);
//        result += " ";
//    }
//    qDebug() << qPrintable(result) << ", stack size " << (h.topIndex() + 1);
//#else
//    Q_UNUSED(ruleNo);
//#endif
//}

//} // namespace pylang
