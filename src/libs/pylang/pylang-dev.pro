# This project created ONLY for testing and development
# Use pylang.pro as default library project

# boost library required for this project
# C++11 required too

### Useful clang build options:
##
##  -faddress-sanitizer : a memory error detector, slows compilation x2 times

DEFINES += NEED_RUNTIME_CHECKS

include(pylang-lib.pri)

debug {
    LIBS += -rdynamic
}

LIBS += -lboost_unit_test_framework
QMAKE_CXXFLAGS += -std=c++0x -Wall -Wextra -pedantic

HEADERS += \
    test/qdebugdiagnosticclient.h \
    test/errorscounterclient.h \
    test/sourcefile.h \
    test/Revision/AstRevisor.h

SOURCES += \
    test/test_parser.cpp \
    test/test_lexer.cpp \
    test/qdebugdiagnosticclient.cpp \
    test/main.cpp \
    test/errorscounterclient.cpp \
    test/sourcefile.cpp \
    test/test_document.cpp \
    test/test_literalstable.cpp \
    test/test_token_queue.cpp \
    test/test_lexer_errors.cpp \
    test/Revision/AstRevisor.cpp \
    test/parsing/test_parser_ast.cpp

OTHER_FILES += \
    test/data/TEST-README.txt \
    test/data/ok_script_lvl_9.py \
    test/data/ok_script_lvl_8.py \
    test/data/ok_script_lvl_7.py \
    test/data/ok_script_lvl_6.py \
    test/data/ok_script_lvl_4.py \
    test/data/ok_script_lvl_5.py \
    test/data/ok_script_lvl_3.py \
    test/data/crash_script_lvl_8.py \
    test/data/ok_script_lvl_2.py \
    test/data/ok_script_lvl_1.py \
    test/data/crash_script_lvl_2.py \
    test/data/crash_script_lvl_1.py \
    test/data/ok_script_lvl_25.py \
    test/data/ok_script_lvl_10.py \
    test/data/ok_script_lvl_11.py \
    test/data/ok_script_lvl_12.py \
    test/data/crash_script_lvl_12.py \
    test/data/crash_script_lvl_25.py \
    test/data/crash_script_lvl_3.py \
    test/data/crash_script_lvl_4.py \
    test/data/ok_script_lvl_13.py \
    test/data/ok_script_lvl_14.py \
    test/data/crash_script_lvl_14.py \
    test/data/ok_script_lvl_15.py \
    test/data/ok_script_lvl_16.py \
    test/data/ok_script_lvl_17.py \
    test/data/ok_script_lvl_18.py \
    test/data/crash_script_lvl_18.py \
    test/3rdparty-pynoto/ClassCollector.py \
    test/3rdparty-pynoto/LintConfig.py \
    test/3rdparty-pynoto/RopeSrv.py \
    test/3rdparty-pynoto/README \
    test/3rdparty-pynoto/logilab/__init__.py \
    test/3rdparty-pynoto/logilab/common/xmlutils.py \
    test/3rdparty-pynoto/logilab/common/xmlrpcutils.py \
    test/3rdparty-pynoto/logilab/common/visitor.py \
    test/3rdparty-pynoto/logilab/common/vcgutils.py \
    test/3rdparty-pynoto/logilab/common/urllib2ext.py \
    test/3rdparty-pynoto/logilab/common/umessage.py \
    test/3rdparty-pynoto/logilab/common/tree.py \
    test/3rdparty-pynoto/logilab/common/textutils.py \
    test/3rdparty-pynoto/logilab/common/testlib.py \
    test/3rdparty-pynoto/logilab/common/tasksqueue.py \
    test/3rdparty-pynoto/logilab/common/table.py \
    test/3rdparty-pynoto/logilab/common/sphinxutils.py \
    test/3rdparty-pynoto/logilab/common/sphinx_ext.py \
    test/3rdparty-pynoto/logilab/common/shellutils.py \
    test/3rdparty-pynoto/logilab/common/pytest.py \
    test/3rdparty-pynoto/logilab/common/pyro_ext.py \
    test/3rdparty-pynoto/logilab/common/proc.py \
    test/3rdparty-pynoto/logilab/common/pdf_ext.py \
    test/3rdparty-pynoto/logilab/common/optparser.py \
    test/3rdparty-pynoto/logilab/common/optik_ext.py \
    test/3rdparty-pynoto/logilab/common/modutils.py \
    test/3rdparty-pynoto/logilab/common/logging_ext.py \
    test/3rdparty-pynoto/logilab/common/interface.py \
    test/3rdparty-pynoto/logilab/common/hg.py \
    test/3rdparty-pynoto/logilab/common/graph.py \
    test/3rdparty-pynoto/logilab/common/fileutils.py \
    test/3rdparty-pynoto/logilab/common/deprecation.py \
    test/3rdparty-pynoto/logilab/common/decorators.py \
    test/3rdparty-pynoto/logilab/common/debugger.py \
    test/3rdparty-pynoto/logilab/common/dbf.py \
    test/3rdparty-pynoto/logilab/common/date.py \
    test/3rdparty-pynoto/logilab/common/daemon.py \
    test/3rdparty-pynoto/logilab/common/corbautils.py \
    test/3rdparty-pynoto/logilab/common/contexts.py \
    test/3rdparty-pynoto/logilab/common/configuration.py \
    test/3rdparty-pynoto/logilab/common/compat.py \
    test/3rdparty-pynoto/logilab/common/cli.py \
    test/3rdparty-pynoto/logilab/common/clcommands.py \
    test/3rdparty-pynoto/logilab/common/changelog.py \
    test/3rdparty-pynoto/logilab/common/cache.py \
    test/3rdparty-pynoto/logilab/common/__pkginfo__.py \
    test/3rdparty-pynoto/logilab/common/__init__.py \
    test/3rdparty-pynoto/logilab/astng/utils.py \
    test/3rdparty-pynoto/logilab/astng/scoped_nodes.py \
    test/3rdparty-pynoto/logilab/astng/rebuilder.py \
    test/3rdparty-pynoto/logilab/astng/raw_building.py \
    test/3rdparty-pynoto/logilab/astng/protocols.py \
    test/3rdparty-pynoto/logilab/astng/nodes.py \
    test/3rdparty-pynoto/logilab/astng/node_classes.py \
    test/3rdparty-pynoto/logilab/astng/mixins.py \
    test/3rdparty-pynoto/logilab/astng/manager.py \
    test/3rdparty-pynoto/logilab/astng/inspector.py \
    test/3rdparty-pynoto/logilab/astng/inference.py \
    test/3rdparty-pynoto/logilab/astng/exceptions.py \
    test/3rdparty-pynoto/logilab/astng/builder.py \
    test/3rdparty-pynoto/logilab/astng/bases.py \
    test/3rdparty-pynoto/logilab/astng/as_string.py \
    test/3rdparty-pynoto/logilab/astng/__pkginfo__.py \
    test/3rdparty-pynoto/logilab/astng/__init__.py

