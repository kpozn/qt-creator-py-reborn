HEADERS += \
    lexer/utility.h \
    ast/detail/pyastcommons.h \
    ast/detail/pyastvisitor.h \
    ast/detail/pytypes.h \
    ast/AST.h \
    ast/AST_pwd.h \
    ast/BaseVisitor.h \
    ast/PySequence.h \
    wizards/SourceGenerator.h \
    pylang_global.h \
    parser/LalrStack.h \
    diagnostic/DiagnosticMessage.h \
    lexer/HighlightLexer.h \
    lexer/HighlightToken.h \
    lexer/Lexer.h \
    lexer/Token.h \
    lexer/SourceString.h \
    diagnostic/IDiagnosticClient.h \
    parser/ParserMacro.h \
    pythonmodel/Document.h \
    pythonmodel/DocumentInclude.h \
    pythonmodel/SourceDocumentTraits.h \
    parser/Parser.h \
    lexer/Lexem.h \
    parser/_QlalrParser.h \
    parser/_pythongrammar_p.h \
    Support/StringRef.h \
    Support/MemoryPool.h \
    Support/StringsHash.h \
    lexer/TokensQueue.h \
    Support/SourceLocation.h \
    Support/SourceRange.h \
    ast/TranslationUnit.h \
    ast/CodeDomInterfaces.h \
    ast/UnitBuilder.h \
    parser/_Control.h \
    parser/_ParserHelper.h \
    symbols/_Symbol.h \
    symbols/_SymbolTable.h \
    ast/_Type.h \
    ast/_Type_PythonCore.h \
    ast/_TypeInferencer.h \
    Support/TokenizedUnit.h \
    Drivers/Pipeline_i.h \
    Index.h \
    parser/DeclarationsParser.h \
    parser/ParserBase.h

SOURCES += \
    ast/detail/pyastcommons.cpp \
    ast/detail/pytypes.cpp \
    pythonmodel/Document.cpp \
    pythonmodel/DocumentInclude.cpp \
    pythonmodel/SourceDocumentTraits.cpp \
    wizards/SourceGenerator.cpp \
    symbols/Symbol.cpp \
    symbols/SymbolTable.cpp \
    parser/LalrStack.cpp \
    lexer/Lexer.cpp \
    lexer/HighlightLexer.cpp \
    diagnostic/DiagnosticMessage.cpp \
    ast/AST.cpp \
    ast/BaseVisitor.cpp \
    ast/PySequence.cpp \
    ast/Type.cpp \
    ast/Type_PythonCore.cpp \
    ast/TypeInferencer.cpp \
    parser/Parser.cpp \
    lexer/Lexem.cpp \
    parser/_QlalrParser.cpp \
    parser/_QlalrParser_core.cpp \
    parser/_pythongrammar.cpp \
    Support/StringRef.cpp \
    Support/MemoryPool.cpp \
    Support/StringsHash.cpp \
    ast/TranslationUnit.cpp \
    ast/UnitBuilder.cpp \
    parser/_Control.cpp \
    parser/_ParserHelper.cpp \
    ast/AST_pwd.cpp \
    Support/TokenizedUnit.cpp \
    lexer/TokensQueue.cpp \
    parser/DeclarationsParser.cpp \
    parser/ParserBase.cpp

OTHER_FILES += \
    parser/python_parser.g \
    lexer/tokens.def \
    code_model_design.txt

HEADERS += \
    ast/exception/parserexception.h

SOURCES += \
    ast/exception/parserexception.cpp

HEADERS += \
    ast/exception/exceptionwithbacktrace.h

SOURCES += \
    ast/exception/exceptionwithbacktrace.cpp

HEADERS += \
    ast/exception/exceptionwithmessage.h

SOURCES += \
    ast/exception/exceptionwithmessage.cpp
