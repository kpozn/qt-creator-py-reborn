TEMPLATE = lib
TARGET = Pylang
DEFINES += PYLANG_BUILD_LIB QT_CREATOR

QMAKE_CXXFLAGS = -std=c++0x

include(../../qtcreatorlibrary.pri)
include(pylang-lib.pri)
include(../utils/utils.pri)
include(../cplusplus/cplusplus.pri)

