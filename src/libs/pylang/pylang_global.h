#ifndef PYGLOBAL_H
#define PYGLOBAL_H

#include <QtGlobal>

#if defined(PYLANG_BUILD_LIB)
#  define PYLANG_EXPORT Q_DECL_EXPORT
#elif defined(PYTHON_BUILD_STATIC_LIB)
#  define PYLANG_EXPORT
#else
#  define PYLANG_EXPORT Q_DECL_IMPORT
#endif

#endif // PYGLOBAL_H
