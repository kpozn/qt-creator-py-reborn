#include "Document.h"

#include "../parser/DeclarationsParser.h"
#include "../ast/TranslationUnit.h"

#include <QtCore/QFileInfo>
#include <QtCore/QDebug>

namespace pylang {

class EmptyDiagClient : pylang::IDiagnosticClient
{
public:
    void diagnose(const Diagnostic &message)
    {
        qDebug() << message.message();
    }
};

Document::Document(const QString &filePath)
    : Internal::SourceDocumentTraits(filePath)
    , _control(0)
{
}

Document::~Document()
{
    delete _control;
}

void Document::renewCodeDom(const QByteArray &source)
{
    (void)source;
    TranslationUnit *control = new TranslationUnit();
    // TODO: control->setClient();

    DeclarationsParser parser;
    parser(UnitBuilder(*control), source);

    {
        delete _control;
        _control = control;
    }
}

///**
// * \brief codeDom - start point for all visitors inherited from AstVisitor
// * \return reference to root node of abstract syntax trees
// */
//TranslationUnitAst &Document::codeDom() const
//{
//    return *_codeDom;
//}

TranslationUnit &Document::control() const
{
    return *_control;
}

Document::MutablePtr Document::create(const QString &filePath)
{
    return Document::MutablePtr(new Document(filePath));
}

} // namespace pylang
