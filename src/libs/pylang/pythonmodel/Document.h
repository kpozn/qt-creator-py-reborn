#ifndef PYTHON_PYTHONDOCUMENT_H
#define PYTHON_PYTHONDOCUMENT_H

#include "../pylang_global.h"
#include "SourceDocumentTraits.h"
#include "../ast/AST_pwd.h"

#include "../ast/TranslationUnit.h"

#include <QtCore/QSharedPointer>

namespace pylang {

class TranslationUnit;

class PYLANG_EXPORT Document : public Internal::SourceDocumentTraits
{
public: // types
    typedef QSharedPointer<const Document> Ptr;
    typedef QSharedPointer<Document> MutablePtr;

public: // methods
    static MutablePtr create(const QString &filePath);

    void renewCodeDom(const QByteArray &source);

    TranslationUnit &control() const;

    virtual ~Document();

private: // attributes
    TranslationUnit *_control;

private: // methods
    Document(const QString &filePath);
    Document(const Document &other);
    void operator =(const Document &other);
};

} // namespace pylang

#endif // PYTHON_PYTHONDOCUMENT_H
