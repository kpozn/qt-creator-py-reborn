#include "DocumentInclude.h"
#include <QtCore/QFileInfo>
#include <QtCore/QHash>

namespace pylang {

DocumentInclude::DocumentInclude()
    : _line(0)
    , _resolveStatus(Unknown)
{
}

DocumentInclude::DocumentInclude(const QString &path, unsigned line)
    : _path(path)
    , _line(line)
    , _resolveStatus(Unknown)
{
}

const QString &DocumentInclude::path() const
{
    return _path;
}

unsigned DocumentInclude::line() const
{
    return _line;
}

bool DocumentInclude::resolved() const
{
    bool ret;
    switch (_resolveStatus)
    {
    default:
        ret = QFileInfo(_path).isAbsolute();
        if (ret)
            _resolveStatus = Resolved;
        else
            _resolveStatus = Unresolved;
        break;
    case Resolved:
        return true;
    case Unresolved:
        return false;
    }
    return ret;
}

bool DocumentInclude::operator ==(const DocumentInclude &doc) const
{
    return (_path == doc._path);
}

} // namespace pylang

uint qHash(const pylang::DocumentInclude &doc)
{
    return qHash(doc.path());
}
