#ifndef PYTHON_INTERNAL_DOCUMENTINCLUDE_H
#define PYTHON_INTERNAL_DOCUMENTINCLUDE_H

#include "../pylang_global.h"
#include <QtCore/QString>

namespace pylang {

class PYLANG_EXPORT DocumentInclude
{
public:
    DocumentInclude();
    DocumentInclude(const QString &path, unsigned line);
    const QString &path() const;
    unsigned line() const;
    bool resolved() const;

    /* Compares only path */
    bool operator ==(const DocumentInclude &doc) const;

private: // attributes
    QString _path;
    unsigned _line;
    mutable enum {
        Resolved,
        Unresolved,
        Unknown
    } _resolveStatus;
};

} // namespace pylang

uint qHash(const pylang::DocumentInclude &doc);

#endif // PYTHON_INTERNAL_DOCUMENTINCLUDE_H
