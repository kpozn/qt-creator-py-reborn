#include "SourceDocumentTraits.h"

#include <QtCore/QFileInfo>
#include <QtCore/QDir>

namespace pylang {
namespace Internal {

const QString &SourceDocumentTraits::filePath() const
{
    return _filePath;
}

const QSet<DocumentInclude> &SourceDocumentTraits::includedFiles() const
{
    return _includes;
}

void SourceDocumentTraits::addIncludeFile(const QString &fileName, unsigned line)
{
    DocumentInclude temp(fileName, line);
    QSet<DocumentInclude>::iterator it = _includes.find(temp);
    if (it == _includes.end())
    {
        _includes.insert(temp);
    }
    else if (it->line() > temp.line())
    {
        _includes.erase(it);
        _includes.insert(temp);
    }
}

const QByteArray &SourceDocumentTraits::utf8Source() const
{
    return _utf8Source;
}

/**
 * \brief setUtf8Source - sets source code utf8 string, can be overloaded
 * to perform action after setting, e.g. we can renew code model
 * \param utf8Source
 */
void SourceDocumentTraits::setUtf8Source(const QByteArray &utf8Source)
{
    _utf8Source = utf8Source;
}

unsigned SourceDocumentTraits::revision() const
{
    return _revision;
}

void SourceDocumentTraits::setRevision(unsigned revision)
{
    _revision = revision;
}

unsigned SourceDocumentTraits::editorRevision() const
{
    return _revision;
}

void SourceDocumentTraits::setEditorRevision(unsigned editorRevision)
{
    _editorRevision = editorRevision;
}

SourceDocumentTraits::SourceDocumentTraits()
    : _revision(0)
    , _editorRevision(0)
{
}

SourceDocumentTraits::SourceDocumentTraits(const QString &filePath)
    : _filePath(QDir::cleanPath(filePath))
    , _revision(0)
    , _editorRevision(0)
{
}

} // namespace Internal
} // namespace pylang
