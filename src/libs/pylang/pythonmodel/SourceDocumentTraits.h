#ifndef PYTHON_INTERNAL_BASESOURCEDOCUMENT_H
#define PYTHON_INTERNAL_BASESOURCEDOCUMENT_H

#include "DocumentInclude.h"

#include <QtCore/QString>
#include <QtCore/QDateTime>
#include <QtCore/QSet>

namespace pylang {
namespace Internal {

/**
 * @brief The BaseSourceDocument class contains basic traits
 * that are applicable for source document with any programming language
 *
 * Source document has internal revision number and real file path
 *
 * We also guess that any language has headers, modules, packages, etc.
 * - and includedFiles() returns set of those packages
 *
 */

class SourceDocumentTraits
{
public:
    const QString &filePath() const;

    unsigned revision() const;
    void setRevision(unsigned revision);

    unsigned editorRevision() const;
    void setEditorRevision(unsigned editorRevision);

    QDateTime lastModified() const;
    void setLastModified(const QDateTime &lastModified);

    const QSet<DocumentInclude> &includedFiles() const;
    void addIncludeFile(const QString &fileName, unsigned line);

    const QByteArray &utf8Source() const;

    virtual void setUtf8Source(const QByteArray &utf8Source);

protected:
    SourceDocumentTraits();
    SourceDocumentTraits(const QString &filePath);

private:
    QString _filePath;
    unsigned _revision;
    unsigned _editorRevision;
    QDateTime _lastModified;
    QSet<DocumentInclude> _includes;

    QByteArray _utf8Source;
};

} // namespace Internal
} // namespace pylang

//uint qHash(const Python::Internal::BaseSourceDocument::Include &inc);

#endif // PYTHON_INTERNAL_BASESOURCEDOCUMENT_H
