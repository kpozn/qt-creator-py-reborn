//#include "SymbolTable.h"
//#include "Symbol.h"
//#include "../ast/UnitBuilder.h"

//#include <cstring>

//namespace Pylang {

//SymbolTable::SymbolTable()
//{
//}

//SymbolTable::~SymbolTable()
//{
//}

//bool SymbolTable::add(UnitBuilder &helper, Symbol &symbol)
//{
//    if (_buckets)
//    {
//        StringRef literal = helper.literalFor(symbol);
//        unsigned h = literal.hashCode();
//        Entry *ptr = _buckets[h % _bucketsCapacity];
//        for (; ptr; ptr = ptr->next)
//        {
//            if (helper.literalFor(ptr->symbol).equalTo(literal))
//                return false;
//        }
//    }

//    Entry *entry = new (helper) Entry(symbol);
//    entry->next = 0;

//    if (++_listSize == _listCapacity)
//    {
//        if (! _listCapacity)
//            _listCapacity = 4;
//        else
//            _listCapacity <<= 1;

//        size_t memsize = sizeof(Entry *) * _listCapacity;
//        _list = reinterpret_cast<Entry **>(
//                    helper.pool().reallocate_custom_block(_list, memsize));
//        for (int index = _listSize; index < _listCapacity; ++index)
//            _list[index] = 0;
//    }

//    _list[_listSize - 1] = entry;

//    if (! _buckets || _listSize * 5 >= _bucketsCapacity * 3)
//        rehash(helper);
//    else {
//        unsigned h = helper.literalFor(entry->symbol).hashCode() % _bucketsCapacity;
//        entry->next = _buckets[h];
//        _buckets[h] = entry;
//    }

//    return true;
//}

//Symbol *SymbolTable::get(UnitBuilder &helper, const StringRef &identifier) const
//{
//    if (_buckets)
//    {
//        unsigned h = identifier.hashCode();
//        Entry *entry = _buckets[h % _bucketsCapacity];
//        for (; entry; entry = entry->next)
//        {
//            if (helper.literalFor(entry->symbol).equalTo(identifier))
//                return &(entry->symbol);
//        }
//    }

//    return 0;
//}

//void SymbolTable::rehash(UnitBuilder &helper)
//{
//   if (_buckets)
//       helper.pool().free_custom_block(_buckets);

//   if (! _bucketsCapacity)
//       _bucketsCapacity = 12;
//   else
//       _bucketsCapacity <<= 1;

//   size_t memsize = _bucketsCapacity * sizeof(Entry *);
//   _buckets = (Entry **)  helper.pool().allocate_custom_block(memsize);
//   std::memset(_buckets, 0, memsize);

//   Entry **end = _list + _listSize;

//   for (Entry **it = _list; it != end; ++it) {
//       Entry *entry = *it;
//       unsigned h = helper.literalFor(entry->symbol).hashCode() % _bucketsCapacity;

//       entry->next = 0;
//       _buckets[h] = entry;
//   }
//}

//} // namespace pylang
