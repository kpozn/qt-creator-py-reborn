//#ifndef PYTHON_SYMBOL_H
//#define PYTHON_SYMBOL_H

//#include "../pylang_global.h"
//#include "../Support/StringRef.h"

//namespace Pylang {

//class Ast;

//class PYLANG_EXPORT Symbol : public Managed
//{
//public:
//    Symbol(quint32 tokenName = 0);
//    ~Symbol();

//    bool isUndefined() const;

//    Ast *getAst() const;
//    void setAst(Ast *pointer);

//public: // attributes
//    quint32 token_name;

//private:
//    // data
//    Ast *_astPointer;

//    // non-copyable
//    Symbol(const Symbol &symbol);
//    void operator=(const Symbol &symbol);
//};

//} // namespace pylang

//#endif // PYTHON_SYMBOL_H
