//#ifndef PYTHON_SYMBOLTABLE_H
//#define PYTHON_SYMBOLTABLE_H

//#include "../pylang_global.h"
//#include "../Support/StringRef.h"

//namespace Pylang {
//class Symbol;
//class UnitBuilder;

//class PYLANG_EXPORT SymbolTable : public Managed
//{
//public:
//    SymbolTable();
//    ~SymbolTable();

//    /**
//     * @brief add
//     * @param symbol
//     * @return false if symbol with such identifier already exists
//     */
//    bool add(UnitBuilder &pool, Symbol &symbol);

//    /**
//     * @brief get
//     * @param identifier - name of symbol
//     * @return nullptr if symbol with given name doesn't exist yet
//     */
//    Symbol *get(UnitBuilder &helper, const StringRef &identifier) const;

//private:
//    void rehash(UnitBuilder &helper);

//    struct Entry : Managed
//    {
//        Symbol &symbol;
//        Entry *next;

//        Entry(Symbol &symbol_) : symbol(symbol_) { }
//    };

//    Entry **_list;
//    int _listCapacity;
//    int _listSize;

//    Entry **_buckets;
//    int _bucketsCapacity;
//};

//} // namespace pylang

//#endif // PYTHON_SYMBOLTABLE_H
