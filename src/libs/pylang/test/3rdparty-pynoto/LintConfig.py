import configparser
import tempfile

def strip(value):
    if value[0] == '"':
        return value[1:-1]
    return value

def createConfig(prjFile):
    prj = configparser.ConfigParser()
    prj.read(prjFile)

    if not "lint" in prj:
        prj["lint"] = {}
    
    out = configparser.ConfigParser()
    
    out["MASTER"] = {
        "profile"      : "no",
        "ignore"       : "CVS, .git, .svn",
        "persistent"   : "yes",
        "load-plugins" : "",
    }
    
    enable = ""
    disable = "C0111"
    out["MESSAGES CONTROL"] = {
        "enable" if enable else "#enable"    : enable,
        "disable" if disable else "#disable" : disable
    }

    out["REPORTS"] = {
        "include-ids"  : "no",
        "files-output" : "no",
        "reports"      : "no",
        "evaluation"   : "10.0 - ((float(5 * error + warning + refactor + convention) / statement) * 10)",
        "comment"      : "no",
    }

    out["SIMILARITIES"] = {
        "min-similarity-lines" : "4",
        "ignore-comments"      : "yes",
        "ignore-docstrings"    : "yes",
    }

    out["VARIABLES"] = {
        "init-import"         : "no",
        "dummy-variables-rgx" : prj["lint"].get("dummyVar", "_|dummy"),
        "additional-builtins" : "PyQt4, PyQt4.QtCore",
    }

    out["MISCELLANEOUS"] = {
        "notes" : "FIXME, XXX, TODO",
    }

    out["TYPECHECK"] = {
        "ignore-mixin-members" : "yes",
        "ignored-classes"      : "SQLObject",
        "zope"                 : "no",
        "generated-members"    : "REQUEST, acl_users, aq_parent"
    }
    

    out["BASIC"] = {
        "required-attributes" : "",
        "bad-functions"       : prj["lint"].get("badFunc", "map, filter, apply, input"),
        "module-rgx"          : strip(prj["lint"].get("checkModule", "(([a-z_][a-z0-9_]*)|([A-Z][a-zA-Z0-9]+))$")),
        "const-rgx"           : strip(prj["lint"].get("checkConst", "(([A-Z_][A-Z0-9_]*)|(__.*__))$")),
        "class-rgx"           : strip(prj["lint"].get("checkClass", "[A-Z_][a-zA-Z0-9]+$")),
        "function-rgx"        : strip(prj["lint"].get("checkFunc", "[a-z_][a-z0-9_]{2,30}$")),
        "method-rgx"          : strip(prj["lint"].get("checkMethod", "[a-z_][a-z0-9_]{2,30}$")),
        "attr-rgx"            : strip(prj["lint"].get("checkAttr", "[a-z_][a-z0-9_]{2,30}$")),
        "argument-rgx"        : strip(prj["lint"].get("checkArg", "[a-z_][a-z0-9_]{2,30}$")),
        "variable-rgx"        : strip(prj["lint"].get("checkVar", "[a-z_][a-z0-9_]{2,30}$")),
        "inlinevar-rgx"       : strip(prj["lint"].get("checkInlineVar", "[A-Za-z_][A-Za-z0-9_]*$")),
        "good-names"          : strip(prj["lint"].get("goodNames", "i, j, k, ex, Run, _")),
        "bad-names"           : strip(prj["lint"].get("badNames", "foo, bar, baz")),
        "no-docstring-rgx"    : "__.*__"
    }

    out["FORMAT"] = {
        "max-line-length"  : prj["lint"].get("maxLineLength", "80"),
        "max-module-lines" : prj["lint"].get("maxModLines", "1000"),
        "indent-string"    : "'    '"
    }

    out["IMPORTS"] = {
        "deprecated-modules" : "regsub, string, TERMIOS, Bastion, rexec",
        "import-graph"       : "",
        "ext-import-graph"   : "",
        "int-import-graph"   : ""
    }

    out["DESIGN"] = {
        "max-args"               : prj["lint"].get("maxArgs", "5"),
        "ignored-argument-names" : prj["lint"].get("ignArgNames", "_.*"),
        "max-locals"             : prj["lint"].get("maxLocals", "15"),
        "max-returns"            : prj["lint"].get("maxReturns", "6"),
        "max-branchs"            : prj["lint"].get("maxBranchs", "12"),
        "max-statements"         : prj["lint"].get("maxStats", "50"),
        "max-parents"            : prj["lint"].get("maxParents", "7"),
        "max-attributes"         : prj["lint"].get("maxAttrs", "7"),
        "min-public-methods"     : prj["lint"].get("minPublic", "2"),
        "max-public-methods"     : prj["lint"].get("maxPublic", "20"),
    }


    out["CLASSES"] = {
        "ignore-iface-methods"        : "isImplementedBy, deferred, extends, names, namesAndDescriptions, queryDescriptionFor, getBases, getDescriptionFor, getDoc, getName, getTaggedValue, getTaggedValueTags, isEqualOrExtendedBy, setTaggedValue, isImplementedByInstancesOf, adaptWith, is_implemented_by",
        "defining-attr-methods"       : "__init__, __new__, setUp",
        "valid-classmethod-first-arg" : "cls",
    }

    out["EXCEPTIONS"] = {
        "overgeneral-exceptions" : "Exception",
    }

    name = ""
    with tempfile.NamedTemporaryFile(mode='w+t', delete=False) as configfile:
        out.write(configfile)
        name = configfile.name
        
    return name
    
#createConfig("/home/jes/workspace/pynotosup/pynoto.pynotoprj")