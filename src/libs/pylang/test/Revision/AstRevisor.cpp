#include "AstRevisor.h"
#include <ast/BaseVisitor.h>

using namespace pylang;

namespace test {

class NodeFound {
public:
    NodeFound(Ast *ast = 0) : m_ast(ast) {}
    Ast *m_ast;
};

class FindInAstVisitor : public BaseVisitor<true>
{
public:
    AstPredicate m_predicate;
    SourceLocation m_from;

    bool check(Ast *ast) {
        if (true == m_predicate(*ast))
            throw NodeFound(ast);

        // TODO: uncomment code below, teach Parser to keep AST range
//        if (ast->m_range.first().isInvalid()
//                || ast->m_range.last() <= m_from)
//            return false;

//        if (ast->m_range.first() >= m_from
//                && true == m_predicate(*ast))
//            throw NodeFound(ast);

        return true;
    }

    bool visit(TranslationUnitAst &ast) { return check(&ast);  }
    bool visit(ClassDefinitionAst &ast) { return check(&ast);  }
    bool visit(FunctionDefinitionAst &ast) { return check(&ast);  }
    bool visit(IfStmtAst &ast) { return check(&ast);  }
    bool visit(WhileStmtAst &ast) { return check(&ast);  }
    bool visit(ForStmtAst &ast) { return check(&ast);  }
    bool visit(WithStmtAst &ast) { return check(&ast);  }
    bool visit(TryStmtAst &ast) { return check(&ast);  }
    bool visit(ReturnStmtAst &ast) { return check(&ast);  }
    bool visit(ImportAst &ast) { return check(&ast);  }
    bool visit(ControlFlowAst &ast) { return check(&ast);  }
    bool visit(RaiseAst &ast) { return check(&ast);  }
    bool visit(PrintAst &ast) { return check(&ast);  }
    bool visit(DelAst &ast) { return check(&ast);  }
    bool visit(ExecAst &ast) { return check(&ast);  }
    bool visit(GlobalAst &ast) { return check(&ast);  }
    bool visit(AssertAst &ast) { return check(&ast);  }
    bool visit(IdentifierAst &ast) { return check(&ast);  }
    bool visit(LiteralExpressionAst &ast) { return check(&ast);  }
    bool visit(BinaryExpressionAst &ast) { return check(&ast);  }
    bool visit(UnaryExpressionAst &ast) { return check(&ast);  }
    bool visit(AssignmentAst &ast) { return check(&ast);  }
    bool visit(LambdaAst &ast) { return check(&ast);  }
    bool visit(ConditionExpressionAst &ast) { return check(&ast);  }
    bool visit(IfExpressionAst &ast) { return check(&ast);  }
    bool visit(ForExpressionAst &ast) { return check(&ast);  }
    bool visit(ListExpressionAst &ast) { return check(&ast);  }
    bool visit(FunctionCallAst &ast) { return check(&ast);  }
    bool visit(SliceAst &ast) { return check(&ast);  }
    bool visit(SubscriptAst &ast) { return check(&ast);  }
    bool visit(DictPairAst &ast) { return check(&ast);  }
    bool visit(AttributeAst &ast) { return check(&ast);  }
    bool visit(AliasAst &ast) { return check(&ast);  }
    bool visit(NoExpressionAst &ast) { return check(&ast); }
    bool visit(FormalParameterAst &ast) { return check(&ast);  }
    bool visit(ExceptAst &ast) { return check(&ast);  }
    bool visit(DocStringAst &ast) { return check(&ast);  }
};

AstRevisor::AstRevisor()
{
}

AstRevisor::~AstRevisor()
{
}

void AstRevisor::setRoot(TranslationUnitAst *ast)
{
    m_root.reset(ast);
}

AstRef<Ast> AstRevisor::find(AstPredicate finder)
{
    SourceLocation from(0, 0);
    return find(finder, from);
}

AstRef<Ast> AstRevisor::find(AstPredicate finder, SourceLocation from)
{
    if (from.isInvalid())
        from = SourceLocation(0, 0);

    try {
        FindInAstVisitor visitor;
        visitor.m_predicate = finder;
        m_root->accept(visitor);
    } catch (const NodeFound &ex) {
        return AstRef<Ast>(ex.m_ast);
    }
    return AstRef<Ast>();
}

AstPredicate AstRevisor::astPredicateAny()
{
    return [](const Ast &) -> bool { return true; };
}

AstPredicate AstRevisor::astPredicateForRange(const SourceRange &range)
{
    return [range] (const Ast &ast) -> bool {
            return (ast.m_range.first() >= range.first()
                    && ast.m_range.last() <= range.last());
        };
}

} // namespace text
