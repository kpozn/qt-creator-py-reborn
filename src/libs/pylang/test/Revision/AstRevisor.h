#ifndef TEXT_ASTREVISOR_H
#define TEXT_ASTREVISOR_H

#include <ast/AST.h>
#include <functional>

namespace test {

typedef std::function<bool(const pylang::Ast &ast)> AstPredicate;

/**
 * @brief Ast revisor checks only logical structure of code
 */
class AstRevisor
{
public:
    AstRevisor();
    ~AstRevisor();
    void setRoot(pylang::TranslationUnitAst *ast);

    /**
     * @brief Looks for first node on which predicate returned true
     * @param finder - predicate to determine correct node
     * @return reference to ast or null reference
     */
    pylang::AstRef<pylang::Ast> find(AstPredicate finder);

    /**
     * @brief Looks for first node on which predicate returned true
     * @param finder - predicate to determine correct node
     * @return reference to ast or null reference
     * @param from - Source location where to start search
     */
    pylang::AstRef<pylang::Ast> find(AstPredicate finder, pylang::SourceLocation from);

    static AstPredicate astPredicateAny();

    static AstPredicate astPredicateForRange(const pylang::SourceRange &range);

    template <class TAstNode>
    static AstPredicate astPredicateForType()
    {
        return [](const pylang::Ast &ast) -> bool {
                return (nullptr != dynamic_cast<const TAstNode*>(&ast));
            };
    }

private:
    pylang::AstRef<pylang::TranslationUnitAst> m_root;
};

} // namespace text

#endif // TEXT_ASTREVISOR_H
