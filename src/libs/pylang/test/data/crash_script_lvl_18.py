# -*- coding: utf-8 -*-

class foo:
    def __init__(self, id):
        assert type(id) is IntType, "id is not an integer: %s" % `id`
        pass

    def add(self, new_queue):
        assert new_queue not in self._list, \
            "%s is already in %s" % (`self, `new_queue`)
        assert isinstance(new_queue, PrintQueue), \
            "%s is not a print queue" % `new_queue`
        pass
