# -*- coding: utf-8 -*-

import random

class DataStructsDemo:
    def __init__(self):
        pass

    def lists(self):
        """
        Demonstrates some features of python lists
        """
        fruits = ['apple', 'orange', 'pear', 'pineapple', 'grapefruit', 'avocado']
        print fruits
        cpy = fruits[:]
        if fruits is not cpy:
            print 'successfully copied fruits list!'
        print "Random element from list: ", random.choice(fruits)

    def dicts(self #):
        def mergeWithoutOverlap(oneDict, otherDict):
            newDict = oneDict.copy()
            for key in otherDict.keys():
                if key in oneDict.keys():
                    raise ValueError, "the two dictionaries share the same key \"" + oneDict[key] + "\""
                newDict[key] = otherDict[key]
            return newDict

        pets = {'Kate': 'dog', 'Ashley': 'parrot', 'Alexa': 'python', 'Allison': 'cat', 'Sara': 'goldfish', 'Samantha': 'guinea pig'}
        for key in pets.keys():
            print pets[key],
        print

        colors = {'Jordan': 'pink', 'Kaylee': 'gray', 'Amelia': 'green', 'Brianna': 'red', 'Destiny': 'blue', 'Allison': 'yellow'}
        d = mergeWithoutOverlap(pets, colors)
        for key in d.keys():
            print d[key],
        print
