#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
try:
    from PySide import QtGui
    from PySide import QtCore
except:
    from PyQt4.QtCore import pyqtSlot as Slot
    from PyQt4 import QtGui
    from PyQt4 import QtCore
from mainwindow import MainWindow
from datastructsdemo import DataStructsDemo

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    win = MainWindow()
    win.setWindowTitle(u'language_features')
    win.setGeometry(200, 200, 800, 600)
    win.show()

    try:
        d = DataStructsDemo()
        d.lists()
        d.dicts()
    except ValueError, e:
        print 'DataStructsDemo throws an exception: ' + str(e)
    app.exec_()
