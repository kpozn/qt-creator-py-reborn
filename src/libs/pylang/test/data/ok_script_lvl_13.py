# -*- coding: utf-8 -*-

class Yeah:
    def __init__(self):
        pass

class Noo:
    def __init__(self):
        pass

class Foo:
    def __init__(self):
        self.noo = Noo()
        self.yeah = Yeah()

foo = Foo()

del foo.noo
del foo.yeah
del foo
foo = None

dictionary = {"alpha" : "greek", "jay": "latin"}
del dictionary["alpha"]

