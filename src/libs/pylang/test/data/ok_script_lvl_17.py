# -*- coding: utf-8 -*-

def by_name(self, name):
    id = self._name2id_map[name]
    assert self._id2name_map[id] == name
    return id

