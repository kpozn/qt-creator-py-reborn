#!/usr/bin/env python3

#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor,
#Boston, MA  02110-1301, USA.

#-----------------------------------------------------------------
#Copyright (C) 2012, Andrey Inishev <inish777@gmail.com>

from gi.repository import Gtk
import os
import subprocess
import json

class Parser():
    def __init__(self):
        self.cpu_info = [{}]
        self.os_info = {}
        self.core_exe = "/usr/local/bin/hardinform"
    def parse_cpu_info(self):
        pipe = subprocess.Popen([self.core_exe, '-c'], 0, None, None, subprocess.PIPE)
        lines = pipe.stdout.readlines()
        x = 0
        json_str = ''
        while x < len(lines):
            json_str += lines[x].decode('utf-8').strip()
            x += 1
        decoder = json.decoder.JSONDecoder()
        self.cpu_info = decoder.decode(json_str)
        return self.cpu_info
    def parse_os_info(self):
        pipe = subprocess.Popen([self.core_exe, '-o'], 0, None, None, subprocess.PIPE)
        lines = pipe.stdout.readlines()
        x = 0
        json_str = ''
        while x < len(lines):
            json_str += lines[x].decode('utf-8').strip()
            x += 1
        decoder = json.decoder.JSONDecoder()
        self.os_info = decoder.decode(json_str)
        return self.os_info
    def parse(self):
        self.parse_cpu_info()
        self.parse_os_info()
        
class Handler():
    def __init__(self, user_data):
        self.data = user_data
        self.parser = Parser()
    def add_items_to_categories_tree(self, widget):
        treestore = self.data['categories_tree_store']
        cpu = treestore.append(None, ('CPU',)) 
        os = treestore.append(None, ('OS',))
        users = treestore.append(os, ('Users',))
        kernel = treestore.append(os, ('Kernel',))
    def on_status_icon_activated(self):
        pass
    def on_status_icon_popup_menu(self, status_icon, button, activate_time):
        self.data['status_icon_context_menu'].popup(None, None , self.data['status_icon'].position_menu, self.data['status_icon'], button, activate_time)
    def on_main_window_delete(self, widget, event):
        Gtk.main_quit()
    def cpu_item_selected(self):
        box = self.data['main_hbox']
        self.data['place_for_widgets'].destroy()
        place = self.data['place_for_widgets'] = Gtk.ScrolledWindow()
        box.add(place)
        treestore = Gtk.TreeStore(str,str)
        treeview = Gtk.TreeView(treestore)
        cpu_info = self.parser.parse_cpu_info()
        x = 0
        while x < len(cpu_info):
            cpu = treestore.append(None, ('CPU', cpu_info[x]['CPU']))
            treestore.append(cpu, ('Processor type', cpu_info[x]['Processor type']))
            treestore.append(cpu, ('Model name', cpu_info[x]['Model name']))
            treestore.append(cpu, ('Current speed in MHz', cpu_info[x]['Current speed in MHz']))
            treestore.append(cpu, ('Cache size', cpu_info[x]['Cache size']))
            treestore.append(cpu, ('Current speed in bogomips', cpu_info[x]['Current speed in bogomips']))
            treestore.append(cpu, ('Temperature', cpu_info[x]['Temperature']))
            treestore.append(cpu, ('Critical temperature', cpu_info[x]['Critical temperature']))
            x += 1
        column = Gtk.TreeViewColumn()
        first = Gtk.CellRendererText()
        second = Gtk.CellRendererText()
        column.pack_start(first, True)
        column.pack_start(second, True)
        column.add_attribute(first, "text", 0)
        column.add_attribute(second, "text", 1)
        treeview.append_column(column)
        place.add(treeview)
        self.data['main_window'].show_all()
    def os_item_selected(self):
        box = self.data['main_hbox']
        self.data['place_for_widgets'].destroy()
        place = self.data['place_for_widgets'] = Gtk.ScrolledWindow()
        box.add(place)
        treestore = Gtk.TreeStore(str,str)
        treeview = Gtk.TreeView(treestore)
        os_info = self.parser.parse_os_info()
        treestore.append(None, ('Distro', os_info['Distro']))
        treestore.append(None, ('Uptime', os_info['Uptime']))
        env = treestore.append(None, ('List of environment variables', None))
        x = 0
        while x < len(os_info['Environment variables']):
            treestore.append(env,  (os_info['Environment variables'][x].split('=')[0], os_info['Environment variables'][x].split('=')[1]))
            x += 1
        column = Gtk.TreeViewColumn()
        first = Gtk.CellRendererText()
        second = Gtk.CellRendererText()
        column.pack_start(first, True)
        column.pack_start(second, True)
        column.add_attribute(first, "text", 0)
        column.add_attribute(second, "text", 1)
        treeview.append_column(column)
        place.add(treeview)
        self.data['main_window'].show_all()
    def kernel_item_selected(self):
        box = self.data['main_hbox']
        self.data['place_for_widgets'].destroy()
        place = self.data['place_for_widgets'] = Gtk.ScrolledWindow()
        box.add(place)
        treestore = Gtk.TreeStore(str,str)
        treeview = Gtk.TreeView(treestore)
        kernel_info = self.parser.parse_os_info()
        treestore.append(None, ('Linux version', kernel_info['Linux version']))
        treestore.append(None, ('Boot options', kernel_info['Boot options']))
        mod = treestore.append(None, ('List of loaded modules',None))
        x = 0
        kernel_info['List of loaded modules'].sort()
        while x < len(kernel_info['List of loaded modules']):           
            treestore.append(mod, (kernel_info['List of loaded modules'][x],None))
            x += 1
        column = Gtk.TreeViewColumn()
        first = Gtk.CellRendererText()
        second = Gtk.CellRendererText()
        column.pack_start(first, True)
        column.pack_start(second, True)
        column.add_attribute(first, "text", 0)
        column.add_attribute(second, "text", 1)
        treeview.append_column(column)
        place.add(treeview)
        self.data['main_window'].show_all()
    def users_item_selected(self):
        box = self.data['main_hbox']
        self.data['place_for_widgets'].destroy()
        place = self.data['place_for_widgets'] = Gtk.ScrolledWindow()
        box.add(place)
        treestore = Gtk.TreeStore(str,str)
        treeview = Gtk.TreeView(treestore)
        users_info = self.parser.parse_os_info()['Users']
        x = 0
        while x < len(users_info):
            user = treestore.append(None, ('User', users_info[x]['User']))
            treestore.append(user, ('UID', users_info[x]['UID']))
            treestore.append(user, ('GID', users_info[x]['GID']))
            treestore.append(user, ('Home directory', users_info[x]['Home directory']))
            treestore.append(user, ('Shell', users_info[x]['Shell']))
            if users_info[x].__contains__('Comment'):
                treestore.append(user, ('Comment', users_info[x]['Comment']))
            x += 1
        column = Gtk.TreeViewColumn()
        first = Gtk.CellRendererText()
        second = Gtk.CellRendererText()
        column.pack_start(first, True)
        column.pack_start(second, True)
        column.add_attribute(first, "text", 0)
        column.add_attribute(second, "text", 1)
        treeview.append_column(column)
        place.add(treeview)
        self.data['main_window'].show_all()
    def on_categories_tree_cursor_changed(self, w):
        text = self.data['categories_tree_curr_text'].props.text
        if text == 'CPU':
            self.cpu_item_selected()
        elif text == 'OS':
            self.os_item_selected()
        elif text == 'Kernel':
            self.kernel_item_selected()
        elif text == 'Users':
            self.users_item_selected()

class Main():
    def __init__(self):
        user_data = {}
        gtk_builder = Gtk.Builder()
        gtk_builder.add_from_file("/usr/local/share/hardinform-gui/hardinform-gui.ui")
        user_data['main_window'] = main_window = gtk_builder.get_object("main_window")
        user_data['categories_tree_curr_text'] = gtk_builder.get_object('curr_categories_tree_text')
        user_data['categories_tree_store'] = gtk_builder.get_object('categories_tree_store')
        user_data['information_tree_store'] = gtk_builder.get_object('information_tree_store')
        user_data['place_for_widgets'] = gtk_builder.get_object('place_for_widgets')
        user_data['status_icon'] = gtk_builder.get_object('hardinform_status_icon')
        user_data['status_icon_context_menu'] = gtk_builder.get_object('status_icon_context_menu')
        user_data['main_hbox'] = gtk_builder.get_object('main_hbox')
        gtk_builder.connect_signals(Handler(user_data))
        main_window.show_all()
        Gtk.main()
Main()
