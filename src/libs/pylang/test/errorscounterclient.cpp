#include "errorscounterclient.h"
#include "../diagnostic/DiagnosticMessage.h"
#include <QtCore/QDebug>

ErrorsCounterClient::ErrorsCounterClient()
    :_errorsCount(0)
    ,_warningsCount(0)
    ,_notesCount(0)
    ,_fileName(nullptr)
{
}

ErrorsCounterClient::ErrorsCounterClient(const char *fileName)
    :_errorsCount(0)
    ,_warningsCount(0)
    ,_notesCount(0)
    ,_fileName(fileName)
{
}

ErrorsCounterClient::~ErrorsCounterClient()
{
//    if (_fileName == nullptr)
//        _fileName = "source code";
//    qDebug() << "Parser found" << _errorsCount << "errors"
//             << "and" << _warningsCount << "warnings"
//             << "and" << _notesCount << "notes"
//             << "in" << _fileName;
}

void ErrorsCounterClient::diagnose(const pylang::Diagnostic &message)
{
    if (message.kind() == pylang::Diagnostic::Warning)
        ++_warningsCount;
    else if (message.kind() == pylang::Diagnostic::Error)
        ++_errorsCount;
    else
        ++_notesCount;
}

unsigned ErrorsCounterClient::errorsCount() const
{
    return _errorsCount;
}

unsigned ErrorsCounterClient::overallCount() const
{
    return _errorsCount + _warningsCount + _notesCount;
}
