#ifndef ERRORSCOUNTERCLIENT_H
#define ERRORSCOUNTERCLIENT_H

#include "../diagnostic/IDiagnosticClient.h"

class ErrorsCounterClient : public pylang::IDiagnosticClient
{
public:
    ErrorsCounterClient();
    ErrorsCounterClient(const char *fileName);
    ~ErrorsCounterClient();

    void diagnose(const pylang::Diagnostic &message);

    unsigned errorsCount() const;
    unsigned overallCount() const;

private:
    unsigned _errorsCount;
    unsigned _warningsCount;
    unsigned _notesCount;
    const char *_fileName;
};

#endif // ERRORSCOUNTERCLIENT_H
