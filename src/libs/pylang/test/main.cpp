/*
 * Tests for this library
 */

#define BOOST_TEST_MODULE MainTest
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>
#include <boost/test/auto_unit_test.hpp>

#include <parser/Parser.h>
#include <test/sourcefile.h>

#include <fstream>

using namespace boost::test_tools;
using namespace boost::unit_test;

using namespace pylang;

#include <iostream>

BOOST_AUTO_TEST_SUITE(TestNothing)

//void prettyPrint(const std::string &path)
//{
//    const std::string CODE = SourceFile::read(path);

//    Pylang::TranslationUnit unit;
//    Pylang::UnitBuilder builder(unit);

//    {
//        Pylang::Parser parser(builder, QByteArray(CODE.c_str(), CODE.size()));
//        parser.parse();
//    }

//    std::ofstream stream;
//    stream.open("prettyprinted.py");
//    BOOST_ASSERT(stream.is_open());

//    NonPrettyPrinter printer(unit, stream);
//    unit.enterAst(printer);
//}

BOOST_AUTO_TEST_CASE(testIfTrueIsTrue)
{
    //prettyPrint("data/ok_script_lvl_4.py");
    BOOST_CHECK(true);
}

BOOST_AUTO_TEST_SUITE_END()
