

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>
#include <boost/test/auto_unit_test.hpp>
#include <QtCore/QDebug>

#include "../../parser/Parser.h"
#include "../../ast/TranslationUnit.h"
#include "../errorscounterclient.h"
#include "../qdebugdiagnosticclient.h"
#include "../sourcefile.h"

#include "../Revision/AstRevisor.h"

using namespace pylang;

BOOST_AUTO_TEST_SUITE(TestParserAst)

typedef std::auto_ptr<TranslationUnit> TUPointer;

static
TranslationUnit *parseFile(const char *path)
{
    const std::string CODE(SourceFile::read(path));
    TUPointer unit(new TranslationUnit);
    UnitBuilder builder(*unit);
    pylang::Parser parser;
    parser(builder, QByteArray(CODE.c_str()));

    return unit.release();
}

//static bool isSuchLiteral(
//        const TranslationUnit &unit,
//        SourceLocation literalLoc,
//        const char *requiredLiteral)
//{
//    (void)requiredLiteral;
//    BOOST_ASSERT(false == literalLoc.isInvalid());
//    StringRef ref = unit.literalAt(literalLoc);
//    if (ref.isNull())
//        return false;
//    return true;
////    return std::string(ref.chars()) == std::string(requiredLiteral);
//}

//static bool isSuchIdentifier(
//        const TranslationUnit &unit,
//        SourceLocation literalLoc,
//        const char *requiredName)
//{
//    (void)requiredName;
//    BOOST_ASSERT(false == literalLoc.isInvalid());
//    StringRef ref = unit.identifierAt(literalLoc);
//    if (ref.isNull())
//        return false;
//    return true;
////    return std::string(ref.chars()) == std::string(requiredName);
//}

BOOST_AUTO_TEST_CASE(okTestLevel1)
{
    TUPointer unit(parseFile("data/ok_script_lvl_1.py"));
    test::AstRevisor revisor;
    revisor.setRoot(unit->astRoot());

    {
        auto pred = test::AstRevisor::astPredicateAny();
        auto ref = revisor.find(pred);
        BOOST_ASSERT(false == ref.isNull());
        BOOST_ASSERT(false == ref.up_cast<TranslationUnitAst>().isNull());
    } {
        auto pred = test::AstRevisor::astPredicateForType<TranslationUnitAst>();
        BOOST_ASSERT(false == revisor.find(pred).isNull());
    } {
        auto pred = test::AstRevisor::astPredicateForType<ImportAst>();
        BOOST_ASSERT(false == revisor.find(pred).isNull());
    } {
        auto pred = test::AstRevisor::astPredicateForType<ClassDefinitionAst>();
        BOOST_ASSERT(false == revisor.find(pred).isNull());
    } {
        auto pred = test::AstRevisor::astPredicateForType<FunctionDefinitionAst>();
        BOOST_ASSERT(false == revisor.find(pred).isNull());
    } {
        auto pred = test::AstRevisor::astPredicateForType<IdentifierAst>();
        BOOST_ASSERT(false == revisor.find(pred).isNull());
    } {
        auto pred = test::AstRevisor::astPredicateForType<AttributeAst>();
        BOOST_ASSERT(false == revisor.find(pred).isNull());
    } {
        auto pred = test::AstRevisor::astPredicateForType<ControlFlowAst>();
        BOOST_ASSERT(false == revisor.find(pred).isNull());
    }
}

BOOST_AUTO_TEST_CASE(okTestLevel1_Extended)
{
    TUPointer unit(parseFile("data/ok_script_lvl_1.py"));
    test::AstRevisor revisor;
    revisor.setRoot(unit->astRoot());
//    const TranslationUnit &u(*unit);

    {
        auto pred = test::AstRevisor::astPredicateForType<ClassDefinitionAst>();
        auto ref = revisor.find(pred).up_cast<ClassDefinitionAst>();
        BOOST_ASSERT(false == ref.isNull());
//        BOOST_ASSERT(isSuchIdentifier(u, ref->m_identifier, "MainWindow"));
    } {
        auto pred = test::AstRevisor::astPredicateForType<FunctionDefinitionAst>();
        BOOST_ASSERT(false == revisor.find(pred).isNull());
    }
}

BOOST_AUTO_TEST_CASE(okTestLevel3)
{
    TUPointer unit(parseFile("data/ok_script_lvl_3.py"));
    test::AstRevisor revisor;
    revisor.setRoot(unit->astRoot());

    {
        auto pred = test::AstRevisor::astPredicateAny();
        auto ref = revisor.find(pred);
        BOOST_ASSERT(false == ref.isNull());
        BOOST_ASSERT(false == ref.up_cast<TranslationUnitAst>().isNull());
    } {
        auto pred = test::AstRevisor::astPredicateForType<DocStringAst>();
        BOOST_ASSERT(false == revisor.find(pred).isNull());
    } {
        auto pred = test::AstRevisor::astPredicateForType<LiteralExpressionAst>();
        BOOST_ASSERT(false == revisor.find(pred).isNull());
    } {
        auto pred = test::AstRevisor::astPredicateForType<FunctionCallAst>();
        BOOST_ASSERT(false == revisor.find(pred).isNull());
    } {
        auto pred = test::AstRevisor::astPredicateForType<ForStmtAst>();
        BOOST_ASSERT(false == revisor.find(pred).isNull());
    }
}

BOOST_AUTO_TEST_SUITE_END()
