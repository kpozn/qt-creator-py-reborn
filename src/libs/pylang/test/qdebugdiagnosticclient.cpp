#include "qdebugdiagnosticclient.h"
#include "../diagnostic/DiagnosticMessage.h"
#include <QtCore/QDebug>

static const char *spellDiagnosticKind(pylang::Diagnostic::Kind kind)
{
    switch (kind)
    {
    default: break;
    case pylang::Diagnostic::Error:
        return "ERROR";
    case pylang::Diagnostic::Warning:
        return "WARNING";
    case pylang::Diagnostic::Deprecation:
        return "DEPRECATION";
    case pylang::Diagnostic::TypesMismatch:
        return "TYPES MISMATCH";
    }
    return "UNKNOWN PROBLEM";
}

void QDebugDiagnosticClient::diagnose(const pylang::Diagnostic &diagnostics)
{
    qDebug() << spellDiagnosticKind(diagnostics.kind())
             << ": "
             << diagnostics.message()
             << "at line" << diagnostics.location().line() + 1
             << "column" << diagnostics.location().column() + 1;
}
