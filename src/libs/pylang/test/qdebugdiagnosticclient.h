#ifndef QDEBUGDIAGNOSTICCLIENT_H
#define QDEBUGDIAGNOSTICCLIENT_H

#include "../diagnostic/IDiagnosticClient.h"

class QDebugDiagnosticClient : public pylang::IDiagnosticClient
{
public:
    QDebugDiagnosticClient() {}
    QDebugDiagnosticClient(const char *) {}
    void diagnose(const pylang::Diagnostic &message);
};

#endif // QDEBUGDIAGNOSTICCLIENT_H
