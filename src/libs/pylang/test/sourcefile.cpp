#include "sourcefile.h"

#include <stdio.h>
#include <stdexcept>

std::string SourceFile::read(const std::string &path)
{
    std::string ret;
    char buffer[4100];
    FILE *file = fopen(path.c_str(), "r");
    if (!file)
        throw std::runtime_error("cannot open source file " + path);

    int actual;
    do {
        actual = fread(buffer, sizeof(char), 4096, file);
        ret.append(buffer, actual);
    } while (actual > 0);

    return ret;
}
