#ifndef PYTHON_SOURCEFILE_H
#define PYTHON_SOURCEFILE_H

#include <string>

class SourceFile
{
public:
    static std::string read(const std::string &path);
};

#endif // PYTHON_SOURCEFILE_H
