
#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>
#include <boost/test/auto_unit_test.hpp>

#include <vector>
#include <string>

#include "../lexer/Lexer.h"
#include "../ast/TranslationUnit.h"
#include "../ast/UnitBuilder.h"
#include "qdebugdiagnosticclient.h"

using namespace pylang;

/**
 * @brief checkReceivedTokens - compares given source code with tokens
 * sequence that DOES NOT contain EOF_SYMBOL token
 * @param code
 * @param sequence
 */
static void checkReceivedTokens(const std::string &code,
                                const std::vector<unsigned> &sequence)
{
    QDebugDiagnosticClient client;
    TranslationUnit unit;
    UnitBuilder builder(unit, client);
    Lexer lex(builder, code.c_str(), code.size());
    lex.init();
    Token tk;

    std::vector<unsigned> tokens;
    while ((tk = lex.read()).isNot(T_EndOfFile))
    {
        tokens.push_back(tk.lexem());
    }
    BOOST_CHECK_EQUAL_COLLECTIONS(tokens.begin(), tokens.end(),
                                  sequence.begin(), sequence.end());
}

BOOST_AUTO_TEST_SUITE(TestLexer)

BOOST_AUTO_TEST_CASE(testSingleComment)
{
    const std::string CODE = "#!/usr/bin/env python\n";
    const std::vector<unsigned> SEQUENCE = {
        T_Newline,
        T_Newline
    };
    checkReceivedTokens(CODE, SEQUENCE);
}

BOOST_AUTO_TEST_CASE(testSingleId)
{
    const std::string CODE = "test";
    const std::vector<unsigned> SEQUENCE = {
        T_Identifier,
        T_Newline
    };
    checkReceivedTokens(CODE, SEQUENCE);
}

BOOST_AUTO_TEST_CASE(testBooleanLiterals)
{
    const std::string CODE = "True False";
    const std::vector<unsigned> SEQUENCE = {
        T_True,
        T_False,
        T_Newline
    };
    checkReceivedTokens(CODE, SEQUENCE);
}

BOOST_AUTO_TEST_CASE(testIntegers)
{
    const std::string CODE = "0 1 2 1024 719l 12L 0x16L 0b101";
    const std::vector<unsigned> SEQUENCE = {
        T_IntegerValue,
        T_IntegerValue,
        T_IntegerValue,
        T_IntegerValue,
        T_IntegerValue,
        T_IntegerValue,
        T_IntegerValue,
        T_IntegerValue,
        T_Newline
    };
    checkReceivedTokens(CODE, SEQUENCE);
}

BOOST_AUTO_TEST_CASE(testFloats)
{
    const std::string CODE = "0.412 .51 6.152 1e+5 2e6 3E-7";
    const std::vector<unsigned> SEQUENCE = {
        T_FloatValue,
        T_FloatValue,
        T_FloatValue,
        T_FloatValue,
        T_FloatValue,
        T_FloatValue,
        T_Newline
    };
    checkReceivedTokens(CODE, SEQUENCE);
}

BOOST_AUTO_TEST_CASE(testComplex)
{
    const std::string CODE = "0.412j .51J 6.152J 1e+5j 2e6J 3e-7j 1j 0j";
    const std::vector<unsigned> SEQUENCE = {
        T_ComplexValue,
        T_ComplexValue,
        T_ComplexValue,
        T_ComplexValue,
        T_ComplexValue,
        T_ComplexValue,
        T_ComplexValue,
        T_ComplexValue,
        T_Newline
    };
    checkReceivedTokens(CODE, SEQUENCE);
}

BOOST_AUTO_TEST_CASE(testIndentation)
{
    const std::string CODE =
            "class A:           \n"
            "   b = 1           \n"
            "   def get():      \n"
            "       return b\n"
            "someIdentifer";
    const std::vector<unsigned> SEQUENCE = {
        T_KwClass,
        T_Identifier,
        T_Colon,
        T_Newline,
        T_Indent,
        T_Identifier,
        T_Assign,
        T_IntegerValue,
        T_Newline,
        T_KwDef,
        T_Identifier,
        T_LeftParen,
        T_RightParen,
        T_Colon,
        T_Newline,
        T_Indent,
        T_KwReturn,
        T_Identifier,
        T_Newline,
        T_Dedent,
        T_Dedent,
        T_Identifier,
        T_Newline
    };
    checkReceivedTokens(CODE, SEQUENCE);
}

BOOST_AUTO_TEST_CASE(testUnexpectedEOF)
{
    const std::string CODE =
            "class A:           \n"
            "   b = 1           \n"
            "                   \n"
            "   def get():      \n"
            "       return b";
    const std::vector<unsigned> SEQUENCE = {
        T_KwClass,
        T_Identifier,
        T_Colon,
        T_Newline,
        T_Indent,
        T_Identifier,
        T_Assign,
        T_IntegerValue,
        T_Newline,
        T_Newline,
        T_KwDef,
        T_Identifier,
        T_LeftParen,
        T_RightParen,
        T_Colon,
        T_Newline,
        T_Indent,
        T_KwReturn,
        T_Identifier,
        T_Newline,
        T_Dedent,
        T_Dedent
    };
    checkReceivedTokens(CODE, SEQUENCE);
}

BOOST_AUTO_TEST_CASE(testPythonEditorGeneratedClass)
{
    const std::string CODE =
            "# -*- coding: utf-8 -*-            \n"
            "                                   \n"
            "from PySide import QtCore, QtGui   \n"
            "                                   \n"
            "class MainWindow(QtGui.QWidget):   \n"
            "    def __init__(self):\n"
            "        pass\n";
    const std::vector<unsigned> SEQUENCE = {
        T_Newline,
        T_Newline,
        T_KwFrom,
        T_Identifier,
        T_KwImport,
        T_Identifier,
        T_Comma,
        T_Identifier,
        T_Newline,
        T_Newline,
        T_KwClass,
        T_Identifier,
        T_LeftParen,
        T_Identifier,
        T_OpDot,
        T_Identifier,
        T_RightParen,
        T_Colon,
        T_Newline,
        T_Indent,
        T_KwDef,
        T_Identifier,
        T_LeftParen,
        T_Identifier,
        T_RightParen,
        T_Colon,
        T_Newline,
        T_Indent,
        T_KwPass,
        T_Newline,
        T_Newline,
        T_Dedent,
        T_Dedent
    };
    checkReceivedTokens(CODE, SEQUENCE);
}

BOOST_AUTO_TEST_CASE(testStringLiteralPrefixes)
{
    const std::string CODE =
            "r\"string\"    "
            "u\"string\"    "
            "ur\"string\"   "
            "Ur\"string\"   "
            "uR\"text\"     "
            "b\"bintext\"   "
            "br\"text\"     "
            "B\"text\"      "
            "BR\"hello\"    ";
    const std::vector<unsigned> SEQUENCE = {
        T_String,
        T_String,
        T_String,
        T_String,
        T_String,
        T_String,
        T_String,
        T_String,
        T_String,
        T_Newline
    };
    checkReceivedTokens(CODE, SEQUENCE);
}

BOOST_AUTO_TEST_CASE(testStringLiteralEscapes)
{
    const std::string CODE =
            "r\"st\\\"ring\"    "
            "u\"string\"    "
            "\"string\\\"\"   "
            "\"st\\\\ring\"   "
            "br\"text\"     ";
    const std::vector<unsigned> SEQUENCE = {
        T_String,
        T_String,
        T_String,
        T_String,
        T_String,
        T_Newline
    };
    checkReceivedTokens(CODE, SEQUENCE);
}

BOOST_AUTO_TEST_CASE(testMultiLineStrings)
{
    const std::string CODE =
            "r\"\"\"st\\\"ring\"\"\"    "
            "if True:"
            "\"\"\"string\"\"\"    "
            "\"string\\\"\"   "
            "\"st\\\\ring\"   "
            "br\"text\"     ";
    const std::vector<unsigned> SEQUENCE = {
        T_String,
        T_KwIf,
        T_True,
        T_Colon,
        T_DocString,
        T_String,
        T_String,
        T_String,
        T_Newline
    };
    checkReceivedTokens(CODE, SEQUENCE);
}

BOOST_AUTO_TEST_CASE(testLookAhead)
{
    const std::string CODE =
            "class A:           \n"
            "   b = 1           \n"
            "   def get():      \n"
            "       return b\n"
            "someIdentifer";

    QDebugDiagnosticClient client;
    TranslationUnit unit;
    UnitBuilder builder(unit, client);
    Lexer lex(builder, CODE.c_str(), CODE.size());
    lex.init();
    Token tk;

    BOOST_ASSERT(lex.lookAhead(1) == T_KwClass);
    BOOST_ASSERT(lex.lookAhead(2) == T_Identifier);
    BOOST_ASSERT(lex.lookAhead(3) == T_Colon);
    BOOST_ASSERT(lex.lookAhead(5) == T_Indent);
    BOOST_ASSERT(lex.lookAhead(1) == T_KwClass);

    BOOST_ASSERT(lex.lookAhead(4) == T_Newline);
    BOOST_ASSERT(lex.lookAhead(8) == T_IntegerValue);
    BOOST_ASSERT(lex.lookAhead(5) == T_Indent);

    lex.read();
    lex.read();
    BOOST_ASSERT(lex.lookAhead(1) == T_Colon);
    BOOST_ASSERT(lex.lookAhead(3) == T_Indent);
    BOOST_ASSERT(lex.lookAhead(6) == T_IntegerValue);
    BOOST_ASSERT(lex.lookAhead(2) == T_Newline);

//    const std::vector<unsigned> SEQUENCE = {
//        T_KwClass,
//        T_Identifier,
//        T_Colon,
//        T_Newline,
//        T_Indent,
//        T_Identifier,
//        T_Assign,
//        T_IntegerValue,
//        T_Newline,
//        T_KwDef,
//        T_Identifier,
//        T_LeftParen,
//        T_RightParen,
//        T_Colon,
//        T_Newline,
//        T_Indent,
//        T_KwReturn,
//        T_Identifier,
//        T_Newline,
//        T_Dedent,
//        T_Dedent,
//        T_Identifier,
//        T_Newline
//    };
//    checkReceivedTokens(CODE, SEQUENCE);
}

BOOST_AUTO_TEST_CASE(testLookAheadWithCommentOnFirstLine)
{
    const std::string CODE = "#!/usr/bin/env python\n";

    QDebugDiagnosticClient client;
    TranslationUnit unit;
    UnitBuilder builder(unit, client);
    Lexer lex(builder, CODE.c_str(), CODE.size());
    lex.init();
    Token tk;

    BOOST_ASSERT(lex.lookAhead(1) == T_Newline);
    BOOST_ASSERT(lex.lookAhead(2) == T_Newline);
    BOOST_ASSERT(lex.lookAhead(4) == T_EndOfFile);
    BOOST_ASSERT(lex.lookAhead(5) == T_EndOfFile);
    BOOST_ASSERT(lex.lookAhead(6) == T_EndOfFile);
    BOOST_ASSERT(lex.lookAhead(10) == T_EndOfFile);
    BOOST_ASSERT(lex.lookAhead(1) == T_Newline);
}

BOOST_AUTO_TEST_SUITE_END()
