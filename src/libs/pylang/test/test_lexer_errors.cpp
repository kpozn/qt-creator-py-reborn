
#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>
#include <boost/test/auto_unit_test.hpp>

#include "../lexer/Lexer.h"
#include "../ast/TranslationUnit.h"
#include "../ast/UnitBuilder.h"
#include "errorscounterclient.h"
#include "sourcefile.h"

using namespace pylang;

enum TestType {
    IsAbsolutelyClean,
    HasNoErrors,
    HasErrors
};

static void testSourceFromFile(const std::string &filePath, TestType testType)
{
    const std::string CODE(SourceFile::read(filePath));

    {
        ErrorsCounterClient client(filePath.c_str());
        pylang::TranslationUnit unit;
        pylang::UnitBuilder builder(unit, client);
        pylang::Lexer lexer(builder, CODE.c_str(), CODE.size());
        lexer.init();

        for (size_t tokenIndex(0); tokenIndex < CODE.size(); ++tokenIndex)
        {
            if (T_EndOfFile == lexer.lookAhead(1))
                break;
            lexer.read();
        }
        BOOST_ASSERT(T_EndOfFile == lexer.lookAhead(1));

        switch (testType)
        {
            case IsAbsolutelyClean:
                BOOST_ASSERT(0 == client.overallCount());
                break;
            case HasNoErrors:
                BOOST_ASSERT(0 == client.errorsCount());
                break;
            case HasErrors:
                BOOST_ASSERT(0 != client.errorsCount());
                break;
        }
    }
}

BOOST_AUTO_TEST_SUITE(TestLexerErrors)

BOOST_AUTO_TEST_CASE(lexCleanTest1)
{
    testSourceFromFile("data/ok_script_lvl_1.py", IsAbsolutelyClean);
}

BOOST_AUTO_TEST_CASE(lexDirtyTest1)
{
    /* symbol $ in program */
    testSourceFromFile("data/crash_script_lvl_1.py", HasErrors);
}

BOOST_AUTO_TEST_CASE(lexCleanTest2)
{
    /* TODO: fails with error
    ERROR :  "should be indented up to 0 spaces" at line 28 column 4
    */
    testSourceFromFile("data/ok_script_lvl_2.py", IsAbsolutelyClean);
}

BOOST_AUTO_TEST_SUITE_END()
