#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>
#include <boost/test/auto_unit_test.hpp>

#include <QtCore/QDebug>
#include <QtCore/QString>

#include "../Support/StringsHash.h"

BOOST_AUTO_TEST_SUITE(TestLiteralsTable)

BOOST_AUTO_TEST_CASE(testCreation)
{
    pylang::MemoryPool pool;
    pylang::StringsHash table(pool);
    {
        pylang::StringRef first = table.findOrInsertLiteral("hello!", 6);
        pylang::StringRef second = table.findOrInsertLiteral("hello!", 6);
        BOOST_ASSERT(first.equalTo(second));
    }
    {
        const pylang::StringRef &first = table.findOrInsertLiteral("hello!", 6);
        BOOST_ASSERT(first.size() > 0);
    }
    {
        table.findOrInsertLiteral("kekeke", 6);
        table.findOrInsertLiteral("abrakadabra", 11);
        table.findOrInsertLiteral("no", 2);
        table.findOrInsertLiteral("halo", 4);
        table.findOrInsertLiteral("nope", 4);
    }
    {
        const pylang::StringRef *result = table.findLiteral("testing", 7);
        BOOST_ASSERT(result == 0);

        result = table.findLiteral("kekeke", 6);
        BOOST_ASSERT(result != 0);
        BOOST_ASSERT(strcmp(result->chars(), "kekeke") == 0);
        BOOST_ASSERT(strcmp(result->chars(), "another") != 0);
    }
}

BOOST_AUTO_TEST_SUITE_END()

