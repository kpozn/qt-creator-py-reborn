/**
 * \file test_parser.cpp contains tests for python parser generated with Qlalr utility
 *
 * If your test fails, change diagnostic client from ErrorsCounterClient to
 * QDebugDiagnosticClient for better diagnostic output.
 *
 */

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>
#include <boost/test/auto_unit_test.hpp>

#include <string>

#include "../lexer/Lexer.h"
//#include "../parser/QlalrParser.h"
#include "../parser/Parser.h"
#include "../ast/TranslationUnit.h"
#include "errorscounterclient.h"
#include "qdebugdiagnosticclient.h"
#include "sourcefile.h"
#include <QtCore/QDebug>

#include "../ast/exception/parserexception.h"

#include <QDir>

enum TestType {
    IsAbsolutelyClean,
    HasNoErrors,
    HasErrors
};

static void testSourceFromFile(const QString &path, TestType type)
{
    const std::string CODE(SourceFile::read(path.toStdString()));

    {
        ErrorsCounterClient client(path.toStdString().c_str());
        pylang::TranslationUnit unit;
        pylang::UnitBuilder builder(unit, client);
        pylang::Parser parser;

        try {
            parser(builder, QByteArray(CODE.c_str()));
        } catch (const pylang::ExceptionWithBacktrace &ex) {
            ex.prettyPrint();
            BOOST_ERROR("Parser exception thrown - it means logic error");
        }

        switch (type)
        {
            case IsAbsolutelyClean:
                BOOST_ASSERT(0 == client.overallCount());
                break;
            case HasNoErrors:
                BOOST_ASSERT(0 == client.errorsCount());
                break;
            case HasErrors:
                BOOST_ASSERT(0 != client.errorsCount());
                break;
        }
    }
}

static void testSourceFromFileWithFullOutput(const QString &path, TestType type)
{
    (void)type;
    const std::string CODE = SourceFile::read(path.toStdString());

    {
        QDebugDiagnosticClient client(path.toStdString().c_str());
        pylang::TranslationUnit unit;
        pylang::UnitBuilder builder(unit, client);
        pylang::Parser parser;

        try {
            parser(builder, QByteArray(CODE.c_str()));
        } catch (const pylang::ExceptionWithBacktrace &ex) {
            ex.prettyPrint();
            BOOST_ERROR("Parser exception thrown - it means logic error");
        }
    }
}


static void testDirectory(QString parent)
{
    qDebug() << "Entering directory" << parent;
    QDir dir(parent);
    QStringList entries = dir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot);
    for (auto &path : entries)
    {
        if (path == "bases.py" || path == "inference.py" || path == "inspector.py")
            continue;
        QString fullPath = parent + QLatin1String("/") + path;
        qDebug() << fullPath;
        QFileInfo finfo(fullPath);
        if (finfo.isDir())
            testDirectory(fullPath);
        else if (finfo.suffix() == "py")
            testSourceFromFile(fullPath, HasNoErrors);
    }
}

BOOST_AUTO_TEST_SUITE(TestParser)


BOOST_AUTO_TEST_CASE(okTestLevel1)
{
    testSourceFromFile("data/ok_script_lvl_1.py", IsAbsolutelyClean);
}

BOOST_AUTO_TEST_CASE(crashTestLevel1)
{
//    testSourceFromFileWithFullOutput("data/crash_script_lvl_1.py", HasErrors);
}

BOOST_AUTO_TEST_CASE(okTestLevel2)
{
//    testSourceFromFileWithFullOutput("data/ok_script_lvl_2.py", IsAbsolutelyClean);
}

#if 0
BOOST_AUTO_TEST_CASE(crashTestLevel2)
{
    testSourceFromFile("data/crash_script_lvl_2.py", HasErrors);
}
#endif

BOOST_AUTO_TEST_CASE(okTestLevel3)
{
    testSourceFromFile("data/ok_script_lvl_3.py", HasNoErrors);
}

#if 0
BOOST_AUTO_TEST_CASE(crashTestLevel3)
{
    testSourceFromFile("data/crash_script_lvl_3.py", HasErrors);
}
#endif

BOOST_AUTO_TEST_CASE(okTestLevel4)
{
    testSourceFromFile("data/ok_script_lvl_4.py", IsAbsolutelyClean);
}

#if 0
BOOST_AUTO_TEST_CASE(crashTestLevel4)
{
    testSourceFromFile("data/crash_script_lvl_4.py", HasErrors);
}
#endif

BOOST_AUTO_TEST_CASE(okTestLevel6)
{
    testSourceFromFile("data/ok_script_lvl_5.py", IsAbsolutelyClean);
}

BOOST_AUTO_TEST_CASE(okTestLevel5)
{
    testSourceFromFile("data/ok_script_lvl_6.py", IsAbsolutelyClean);
}

BOOST_AUTO_TEST_CASE(okTestLevel7)
{
    testSourceFromFile("data/ok_script_lvl_7.py", IsAbsolutelyClean);
}

#if 0
BOOST_AUTO_TEST_CASE(okTestLevel8)
{
    testSourceFromFile("data/ok_script_lvl_8.py", IsAbsolutelyClean);
}
#endif

#if 0
BOOST_AUTO_TEST_CASE(crashTestLevel8)
{
    testSourceFromFile("data/crash_script_lvl_8.py", HasErrors);
}

BOOST_AUTO_TEST_CASE(okTestLevel9)
{
    testSourceFromFile("data/ok_script_lvl_9.py", IsAbsolutelyClean);
}

BOOST_AUTO_TEST_CASE(okTestLevel10)
{
    //! exceptions syntax is incompatible with python3
    testSourceFromFile("data/ok_script_lvl_10.py", HasNoErrors);
}

BOOST_AUTO_TEST_CASE(okTestLevel11)
{
    testSourceFromFile("data/ok_script_lvl_11.py", HasNoErrors);
}

BOOST_AUTO_TEST_CASE(okTestLevel12)
{
    testSourceFromFile("data/ok_script_lvl_12.py", IsAbsolutelyClean);
}

BOOST_AUTO_TEST_CASE(crashTestLevel12)
{
    testSourceFromFile("data/crash_script_lvl_12.py", HasErrors);
}

BOOST_AUTO_TEST_CASE(okTestLevel13)
{
    testSourceFromFile("data/ok_script_lvl_13.py", IsAbsolutelyClean);
}

// TODO: add crash test 13

BOOST_AUTO_TEST_CASE(okTestLevel14)
{
    testSourceFromFile("data/ok_script_lvl_14.py", IsAbsolutelyClean);
}

BOOST_AUTO_TEST_CASE(crashTestLevel14)
{
    testSourceFromFile("data/crash_script_lvl_14.py", HasErrors);
}

BOOST_AUTO_TEST_CASE(okTestLevel15)
{
    testSourceFromFile("data/ok_script_lvl_15.py", IsAbsolutelyClean);
}

// TODO: add crash test 15

BOOST_AUTO_TEST_CASE(okTestLevel16)
{
    testSourceFromFile("data/ok_script_lvl_16.py", IsAbsolutelyClean);
}

BOOST_AUTO_TEST_CASE(okTestLevel17)
{
    testSourceFromFile("data/ok_script_lvl_17.py", IsAbsolutelyClean);
}

BOOST_AUTO_TEST_CASE(okTestLevel18)
{
    testSourceFromFile("data/ok_script_lvl_18.py", IsAbsolutelyClean);
}

BOOST_AUTO_TEST_CASE(crashTestLevel18)
{
    testSourceFromFile("data/crash_script_lvl_18.py", HasErrors);
}

BOOST_AUTO_TEST_CASE(okTestLevel25)
{
    testSourceFromFile("data/ok_script_lvl_25.py", IsAbsolutelyClean);
}
#endif

#if 0
BOOST_AUTO_TEST_CASE(crashTestLevel25)
{
    testSourceFromFile("data/crash_script_lvl_25.py", HasErrors);
}
#endif

#if 0
BOOST_AUTO_TEST_CASE(okTestLevel50)
{
  //  testDirectory("3rdparty-pynoto");
   // testSourceFromFile("3rdparty-pynoto/RopeSrv.py", IsAbsolutelyClean);
}
#endif

BOOST_AUTO_TEST_SUITE_END()
