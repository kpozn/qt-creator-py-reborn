
#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>
#include <boost/test/auto_unit_test.hpp>

#include "../lexer/TokensQueue.h"

using namespace pylang;

BOOST_AUTO_TEST_SUITE(TestTokensQueue)

BOOST_AUTO_TEST_CASE(testSingleToken)
{
    Token token(T_Identifier);
    Internal::TokensQueue queue;
    queue.push_back(token);
    BOOST_ASSERT(queue[0].lexem() == T_Identifier);
}

BOOST_AUTO_TEST_CASE(testTenTokens)
{
    Token klass(T_KwClass);
    Token token(T_Identifier);
    Internal::TokensQueue queue;
    queue.push_back(klass);
    for (int i = 1; i < 10; ++i)
    {
        queue.push_back(token);
    }
    BOOST_ASSERT(queue.size() == 10);
    BOOST_ASSERT(queue.front().lexem() == T_KwClass);
    BOOST_ASSERT(queue[0].lexem() == T_KwClass);
    for (int i = 1; i < 10; ++i)
    {
        BOOST_ASSERT(queue[i].lexem() == T_Identifier);
    }
}

BOOST_AUTO_TEST_CASE(testPushPop)
{
    Token token(T_Identifier);
    Internal::TokensQueue queue;
    for (int i = 0; i < 256; ++i)
    {
        queue.push_back(token);
    }
    BOOST_ASSERT(queue.size() == 256);
    BOOST_ASSERT(queue.front().lexem() == T_Identifier);
    for (int i = 0; i < 256; ++i)
    {
        BOOST_ASSERT(queue[i].lexem() == T_Identifier);
    }
    /////////////////////////////////////
    for (int i = 0; i < 128; ++i)
    {
        queue.pop();
    }
    BOOST_ASSERT(queue.size() == 128);
    /////////////////////////////////////
    for (int i = 0; i < 128; ++i)
    {
        queue.pop();
    }
    BOOST_ASSERT(queue.size() == 0);
    /////////////////////////////////////
    token.setLexem(T_KwClass);
    for (int i = 0; i < 256; ++i)
    {
        queue.push_back(token);
    }
    BOOST_ASSERT(queue.size() == 256);
    BOOST_ASSERT(queue.front().lexem() == T_KwClass);
    for (int i = 0; i < 256; ++i)
    {
        BOOST_ASSERT(queue[i].lexem() == T_KwClass);
    }
}

BOOST_AUTO_TEST_SUITE_END()

