#if defined(QT_CREATOR)

#include "SourceGenerator.h"

// Qt library
#include <QtCore/QSet>

namespace pylang {

static const char BASH_RUN_HEADER[] = "#!/usr/bin/env python\n";
static const char ENCODING_HEADER[] = "# -*- coding: utf-8 -*-\n";

SourceGenerator::SourceGenerator()
    :_binding(PySide)
{
}

SourceGenerator::~SourceGenerator()
{
}

void SourceGenerator::setBinding(QtBinding binding)
{
    _binding = binding;
}

QString SourceGenerator::generateEmpty()
{
    return QLatin1String(ENCODING_HEADER);
}

QString SourceGenerator::generateClass(const QString &className,
                                       QString baseClass,
                                       Utils::NewClassWidget::ClassType classType) const
{
    QSet<QString> modules;
    bool hasUserBaseClass = !baseClass.isEmpty();
    // euristic
    bool hasInheritedFromQt = !baseClass.isEmpty() && (baseClass[0] == 'Q');

    switch (classType)
    {
    case Utils::NewClassWidget::NoClassType:
        break;
    case Utils::NewClassWidget::SharedDataClass:
        break;
    case Utils::NewClassWidget::ClassInheritsQObject:
        hasInheritedFromQt = true;
        modules.insert("QtCore");
        if (!hasUserBaseClass)
            baseClass = "QtCore.QObject";
        break;

    case Utils::NewClassWidget::ClassInheritsQWidget:
        hasInheritedFromQt = true;
        modules.insert("QtCore");
        modules.insert("QtGui");
        if (!hasUserBaseClass)
            baseClass = "QtGui.QWidget";
        break;

    case Utils::NewClassWidget::ClassInheritsQDeclarativeItem:
        hasInheritedFromQt = true;
        modules.insert("QtCore");
        modules.insert("QtDeclarative");
        if (!hasUserBaseClass)
            baseClass = "QtDeclarative.QDeclarative";
        break;
    }

    QString nonQtModule; // empty
    if (hasUserBaseClass)
    {
        int dotIndex = baseClass.lastIndexOf('.');
        if (dotIndex != -1)
        {
            if (hasInheritedFromQt)
                modules.insert(baseClass.left(dotIndex));
            else
                nonQtModule = baseClass.left(dotIndex);
        }
    }

    QString ret;
    ret.reserve(1024);
    ret.append(ENCODING_HEADER);
    ret.append(QChar('\n'));

    if (!modules.isEmpty())
    {
        ret.append(getQtModulesImport(modules));
        ret.append(QChar('\n'));
    }

    if (!nonQtModule.isEmpty())
    {
        ret.append(QString("import %1\n"
                           "\n").arg(nonQtModule));
    }

    if (baseClass.isEmpty())
        ret += QString("class %1:\n").arg(className);
    else
        ret += QString("class %1(%2):\n").arg(className).arg(baseClass);

    ret.append("    def __init__(self):\n");
    if (hasInheritedFromQt)
        ret.append(QString("        %1.__init__(self)\n").arg(baseClass));
    ret.append("        pass\n");

    return ret;
}

QString SourceGenerator::generateQtMain(const QString &windowTitle) const
{
    QSet<QString> qtModules;
    qtModules.insert("QtCore");
    qtModules.insert("QtGui");

    QString ret;
    ret.reserve(1024);
    ret.append(BASH_RUN_HEADER);
    ret.append(ENCODING_HEADER);
    ret.append(QChar('\n'));
    ret.append("import sys\n");
    ret.append(getQtModulesImport(qtModules));
    ret.append("from mainwindow import MainWindow\n");
    ret.append(QChar('\n'));

    ret += QString(
                "if __name__ == \'__main__\':\n"
                "    app = QtGui.QApplication(sys.argv)\n"
                "    win = MainWindow()\n"
                "    win.setWindowTitle(u\'%1\')\n"
                "    win.show()\n"
                "    app.exec_()\n"
                ).arg(windowTitle);

    return ret;
}

QString SourceGenerator::getQtModulesImport(const QSet<QString> &modules) const
{
    const char *slotsImport = "";
    if (modules.contains("QtCore"))
        slotsImport = "    from PyQt4.QtCore import pyqtSlot as Slot\n";

    const char *defaultLib = "PySide";
    const char *fallbackLib = "PyQt4";
    if (_binding == PyQt4)
        qSwap(defaultLib, fallbackLib);

    QString ret;
    ret.reserve(256);
    ret += QString("try:\n");
    if (_binding == PyQt4)
        ret.append(slotsImport);
    foreach (const QString &name, modules)
        ret += QString("    from %1 import %2\n")
                .arg(defaultLib)
                .arg(name);

    ret += QString("except:\n");
    if (_binding != PyQt4)
        ret.append(slotsImport);
    foreach (const QString &name, modules)
        ret += QString("    from %1 import %2\n")
                .arg(fallbackLib)
                .arg(name);

    return ret;
}

} // namespace pylang

#endif
