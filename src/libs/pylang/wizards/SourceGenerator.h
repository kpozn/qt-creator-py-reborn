#ifndef PYTHON_SOURCEGENERATOR_H
#define PYTHON_SOURCEGENERATOR_H

#if defined(QT_CREATOR)

#include "../pylang_global.h"
// QtCreator platform
#include <utils/newclasswidget.h>
#include <QSet>

// Qt library
#include <QtCore/QString>

namespace pylang {

class PYLANG_EXPORT SourceGenerator
{
public:
    enum QtBinding {
        PySide,
        PyQt4
    };

    static QString generateEmpty();

    SourceGenerator();
    ~SourceGenerator();

    void setBinding(QtBinding binding);

    QString generateClass(const QString &className,
                          QString baseClass,
                          Utils::NewClassWidget::ClassType classType) const;

     /**
     * @brief generateQtMain - this method guesses that python class MainWindow
     * already defined in "mainwindow.*" file and derived from QWidget.
     * @param windowTitle - title for created window instance
     * @return
     */
    QString generateQtMain(const QString &windowTitle) const;

private:
    QString getQtModulesImport(const QSet<QString> &modules) const;

    QtBinding _binding;
};

} // namespace pylang

#endif

#endif // PYTHON_SOURCEGENERATOR_H
