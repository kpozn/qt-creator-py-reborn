#include "classnamevalidator.h"
#include <utils/qtcassert.h>

namespace Utils {

ClassNameValidator::ClassNameValidator()
    :m_namespacesEnabled(true)
{
    setNamesDelimiter(CxxNamespaceDelimiter);
}

bool ClassNameValidator::namespacesEnabled() const
{
    return m_namespacesEnabled;
}

void ClassNameValidator::setNamespacesEnabled(bool b)
{
    m_namespacesEnabled = b;
}

NamesDelimiter ClassNameValidator::namesDelimiter() const
{
    return m_namesDelimiter;
}

void ClassNameValidator::setNamesDelimiter(NamesDelimiter delimiter)
{
    m_namesDelimiter = delimiter;
    m_namesDelimiterValue.clear();
    switch (delimiter)
    {
        case CxxNamespaceDelimiter:
            m_namesDelimiterValue = "::";
            break;
        case DotNamesDelimiter:
            m_namesDelimiterValue = ".";
            break;
    }
    if (m_namesDelimiterValue.isEmpty())
        m_namesDelimiterValue = "::";

    m_nameRegexp = QRegExp(QString("[a-zA-Z_][a-zA-Z0-9_]*("
                                   "%1"
                                   "[a-zA-Z_][a-zA-Z0-9_]*)*")
            .arg(m_namesDelimiterValue));
    QTC_ASSERT(m_nameRegexp.isValid(), return);
}

const QString &ClassNameValidator::namesDelimiterString() const
{
    return m_namesDelimiterValue;
}

bool ClassNameValidator::validate(const QString &value) const
{
    return m_nameRegexp.exactMatch(value);
}

} // namespace Utils
