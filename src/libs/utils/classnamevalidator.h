#ifndef UTILS_CLASSNAMEVALIDATOR_H
#define UTILS_CLASSNAMEVALIDATOR_H

#include "utils_global.h"
#include <QtCore/QRegExp>
#include "newclasswidget.h"

namespace Utils {

/**
 * @brief The ClassNameValidator class - checks if string is valid class
 * name using given conventions about modules (namespaces in C++)
 * and names delimiters
 */
class QTCREATOR_UTILS_EXPORT ClassNameValidator
{
public:
    ClassNameValidator();

    bool namespacesEnabled() const;
    void setNamespacesEnabled(bool b);

    NamesDelimiter namesDelimiter() const;
    void setNamesDelimiter(NamesDelimiter delim);
    const QString &namesDelimiterString() const;

    bool validate(const QString &value) const;

private:
    QRegExp m_nameRegexp;
    QString m_namesDelimiterValue;
    NamesDelimiter m_namesDelimiter;
    bool m_namespacesEnabled;
};

} // namespace Utils

#endif // UTILS_CLASSNAMEVALIDATOR_H
