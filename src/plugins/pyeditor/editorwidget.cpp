/**
  \class EditorWidget
  Graphical representation and parent widget for PyEditor::Editor class.
  Represents editor as plain text editor.
  */

// QtCreator platform & other plugins
#include <texteditor/fontsettings.h>
#include <texteditor/texteditorconstants.h>
#include <texteditor/basetextdocument.h>
#include <texteditor/indenter.h>
#include <texteditor/autocompleter.h>

// Self headers
#include "pluginfeatures.h"
#include "editor.h"
#include "editorwidget.h"

#include <QDebug>

//! Uncommend to update CodeDom
//#define EXPERIMENTAL_CODE_DOM

namespace {

enum {
    UPDATE_DOCUMENT_DEFAULT_INTERVAL = 150
};

} // end of anonymous namespace

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

namespace PyEditor {

EditorWidget::EditorWidget(QWidget *parent)
    :TextEditor::BaseTextEditorWidget(parent)
{
    _commentDefinition.setMultiLineStart("");
    _commentDefinition.setMultiLineEnd("");
    _commentDefinition.setSingleLine(QLatin1String("#"));

    setRevisionsVisible(true);
    setMarksVisible(true);
    setRequestMarkEnabled(false);
    setLineSeparatorsAllowed(true);

    // unused
    setParenthesesMatchingEnabled(true);
    setCodeFoldingSupported(true);

    // create document
    _document = pylang::Document::create("");

    // timer for updating code document object model

#if defined(EXPERIMENTAL_CODE_DOM)
    _updateCodeDomTimer.setInterval(UPDATE_DOCUMENT_DEFAULT_INTERVAL);
    _updateCodeDomTimer.setSingleShot(true);
    connect(&_updateCodeDomTimer, SIGNAL(timeout()), this, SLOT(updateCodeDomNow()));
    connect(this, SIGNAL(textChanged()), this, SLOT(updateCodeDom()));
#endif

#ifdef PYEDITOR_HOVERHANDLER_H
    setAutoCompleter(new Autocompleter());
#endif

#ifdef PYEDITOR_INDENTER_H
    setIndenter(new Indenter());
#endif

#ifdef PYEDITOR_AUTOCOMPLETER_H
    setAutoCompleter(new Autocompleter());
#endif

#ifdef PYEDITOR_HIGHLIGHTER_H
    new Highlighter(baseTextDocument());
#endif
}

EditorWidget::~EditorWidget()
{
}

/**
  Comments or uncomments selection using Python commenting syntax
  */
void EditorWidget::unCommentSelection()
{
    Utils::unCommentSelection(this, _commentDefinition);
}

/**
  Handles common IDE fonts&colors settings
  (Tools -> Options -> Text editor -> Fonts and colors)
  */
void EditorWidget::setFontSettings(const TextEditor::FontSettings &fs)
{
    TextEditor::BaseTextEditorWidget::setFontSettings(fs);

#ifdef PYEDITOR_HIGHLIGHTER_H
    Highlighter *highlighter =
            qobject_cast<Highlighter*>(baseTextDocument()->syntaxHighlighter());
    if (highlighter)
    {
        highlighter->setFontSettings(fs);
    }
#endif
}

TextEditor::BaseTextEditor* EditorWidget::createEditor()
{
    return new Editor(this);
}

/**
 * @brief Asks to update code model next time when user willi do nothing
 */
void EditorWidget::queryUpdateCodeModel()
{
    _updateCodeDomTimer.start();
}

/**
 * @brief Updates code model immediatily
 */
void EditorWidget::updateCodeModelNow()
{
    _updateCodeDomTimer.stop();

    const QString contents = toPlainText();
    // NOTE: to latin1 or to utf8?
    const QByteArray code = contents.toLatin1();
    _document->renewCodeDom(code);
    qDebug() << "Python editor document update called!";
}

} // namespace PyEditor
