#include "ModelManager.h"
#include <QtCore/QHash>

namespace PyEditor {

ModelManager::ModelManager()
{
}

void ModelManager::add(const pylang::Document::MutablePtr &pDocument)
{
    _documents.insert(pDocument);
}

void ModelManager::remove(const pylang::Document::MutablePtr &pDocument)
{
    _documents.remove(pDocument);
}

} // namespace PyEditor
