#ifndef PYEDITOR_MODELMANAGER_H
#define PYEDITOR_MODELMANAGER_H

#include <pylang/pythonmodel/Document.h>
#include <QtCore/QSet>

namespace PyEditor {

class ModelManager
{
public:
    ModelManager();

    void add(const pylang::Document::MutablePtr &pDocument);
    void remove(const pylang::Document::MutablePtr &pDocument);

private:
    QSet<pylang::Document::MutablePtr> _documents;
};

} // namespace PyEditor

#endif // PYEDITOR_MODELMANAGER_H
