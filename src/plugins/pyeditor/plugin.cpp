
// Self headers
#include "plugin.h"
#include "constants.h"
#include "pluginfeatures.h"
#include "editorwidget.h"
#include "editorfactory.h"

// QtCreator platform & other plugins
#include <coreplugin/icore.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/mimedatabase.h>
#include <coreplugin/fileiconprovider.h>
#include <coreplugin/id.h>
#include <coreplugin/editormanager/editormanager.h>
#include <extensionsystem/pluginmanager.h>
#include <texteditor/texteditorconstants.h>
#include <texteditor/texteditorsettings.h>

// Qt Library
#include <QtCore/QtPlugin>
#include <QtCore/QCoreApplication>

// Test only
#include <texteditor/plaintexteditor.h>

using namespace PyEditor::Constants;

namespace PyEditor {

Plugin* Plugin::_instance = 0;

/*! Constructs the plugin. Normally plugins don't do anything in
    their constructor except for initializing their member variables. The
    actual work is done later, in the initialize() and extensionsInitialized()
    methods.
*/
Plugin::Plugin()
    :_factory(0)
    ,_actionHandler(0)
{
    _instance = this;
}

/*! Plugins are responsible for deleting objects they created on the heap, and
    to unregister objects from the plugin manager that they registered there.
*/
Plugin::~Plugin()
{
    removeObject(_factory.data());
    _instance = 0;
}

/*! Initializes the plugin. Returns true on success.
    Plugins want to register objects with the plugin manager here.

    Use addAutoReleasedObject() or addObject() to register new objects,
    if you preffered addObject() function, don't forget to call removeObject()
    in Plugin::~Plugin().

    @param errorMessage can be used to pass an error message to the plugin system,
       if there was any.
*/
bool Plugin::initialize(
        const QStringList &arguments, QString *errorMessage)
{
    Q_UNUSED(arguments)

    Core::ICore* pCore = Core::ICore::instance();

    if (! pCore->mimeDatabase()->addMimeTypes(
            QLatin1String(RC_PY_MIME_XML),
            errorMessage))
    {
        return false;
    }

    _factory.reset(new EditorFactory(this));
    addObject(_factory.data());

    // Initialize default editor actions handler (Ctrl+C, Ctrl+V, etc.)
    _actionHandler.reset(new TextEditor::TextEditorActionHandler(
                C_PYEDITOR_ID,
                TextEditor::TextEditorActionHandler::Format
                | TextEditor::TextEditorActionHandler::UnCommentSelection
                | TextEditor::TextEditorActionHandler::UnCollapseAll));
    _actionHandler->initializeActions();

    // Add MIME overlay icons (these icons will be displayed at Project dock panel)
    Core::FileIconProvider *iconProv = Core::FileIconProvider::instance();
    Core::MimeDatabase *mimeDB = Core::ICore::instance()->mimeDatabase();
    iconProv->registerIconOverlayForMimeType(
                  QIcon(QLatin1String(RC_PY_MIME_ICON)),
                  mimeDB->findByType(QLatin1String(C_PY_MIMETYPE))
              );

    // Add Python wizard for "New file/project" action
#ifdef PYEDITOR_FILEWIZARD_H
    addAutoReleasedObject(new FileWizard(Core::ICore::instance()));
#endif
#ifdef PYEDITOR_CLASSWIZARD_H
    addAutoReleasedObject(new ClassWizard(Core::ICore::instance()));
#endif

    return true;
}

/**
 * @brief Plugin::extensionsInitialized
 * Notification that all extensions of that plugin have been initialized.
 */
void Plugin::extensionsInitialized()
{
}

/**
 * @brief Plugin::initializeEditor
 * This hook used to initialize editor with default actions handler
 * @param widget
 */
void Plugin::initializeEditor(EditorWidget *widget)
{
    instance()->_actionHandler->setupActions(widget);
    TextEditor::TextEditorSettings::instance()->initializeEditor(widget);
}

} // namespace PyEditor

Q_EXPORT_PLUGIN(PyEditor::Plugin)

