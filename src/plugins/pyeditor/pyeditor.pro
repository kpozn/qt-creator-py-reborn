TEMPLATE = lib
TARGET = PyEditor

include(../../qtcreatorplugin.pri)

# dependencies
include(../../libs/pylang/pylang.pri)
include(../../plugins/coreplugin/coreplugin.pri)
include(../../plugins/texteditor/texteditor.pri)
include(../../plugins/cpptools/cpptools.pri)
#include(../../plugins/projectexplorer/projectexplorer.pri)

DEFINES += \
    PYEDITOR_LIBRARY \
    QT_CREATOR

OTHER_FILES += \
    pyeditor.mimetypes.xml \
    PyEditor.pluginspec.in

RESOURCES += \
    resource.qrc

HEADERS += \
    wizard/filewizard.h \
    wizard/classwizarddialog.h \
    wizard/classwizard.h \
    wizard/classnamepage.h \
    tools/highlighter.h \
    tools/indenter.h \
    tools/lexical/formattoken.h \
    tools/lexical/scanner.h \
    tools/lexical/sourcecodestream.h \
    editor.h \
    pluginglobal.h \
    plugin.h \
    pluginfeatures.h \
    constants.h \
    editorfactory.h \
    editorwidget.h \
    tools/hoverhandler.h \
    tools/autocompleter.h \
    tools/OutlineTreeView.h \
    tools/OutlineModel.h \
    model/ModelManager.h \
    tools/Icons.h \
    tools/SemanticHighlighter.h

SOURCES += \
    wizard/filewizard.cpp \
    wizard/classwizarddialog.cpp \
    wizard/classwizard.cpp \
    wizard/classnamepage.cpp \
    tools/highlighter.cpp \
    tools/indenter.cpp \
    tools/lexical/scanner.cpp \
    plugin.cpp \
    editor.cpp \
    editorfactory.cpp \
    editorwidget.cpp \
    tools/hoverhandler.cpp \
    tools/autocompleter.cpp \
    tools/OutlineTreeView.cpp \
    tools/OutlineModel.cpp \
    model/ModelManager.cpp \
    tools/Icons.cpp \
    tools/SemanticHighlighter.cpp

FORMS +=
