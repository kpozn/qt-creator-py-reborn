#include "Icons.h"

// Libraries
#include <QtGui/QIcon>

namespace PyEditor {

Icons *Icons::m_instance;

class IconsPrivate
{
public:
    QIcon classIcon;
    QIcon functionIcon;
    QIcon enumIcon;
    QIcon signalIcon;
    QIcon slotIcon;
    QIcon variablelIcon;
};

Icons::Icons()
    : d(new IconsPrivate)
{
    d->classIcon = QIcon(QLatin1String(":/pyeditor/images/class.png"));
    d->enumIcon = QIcon(QLatin1String(":/pyeditor/images/enum.png"));
    d->functionIcon = QIcon(QLatin1String(":/pyeditor/images/func.png"));
    d->signalIcon = QIcon(QLatin1String(":/pyeditor/images/signal.png"));
    d->slotIcon = QIcon(QLatin1String(":/pyeditor/images/slot.png"));
    d->variablelIcon = QIcon(QLatin1String(":/pyeditor/images/var.png"));
}

Icons *Icons::instance()
{
    if (!m_instance)
    {
        m_instance = new Icons();
    }
    return m_instance;
}

const QIcon &Icons::forClass() const
{
    return d->classIcon;
}

const QIcon &Icons::forFunction() const
{
    return d->functionIcon;
}

const QIcon &Icons::forSignal() const
{
    return d->signalIcon;
}

const QIcon &Icons::forSlot() const
{
    return d->slotIcon;
}

} // namespace PyEditor
