#ifndef PYEDITOR_ICONS_H
#define PYEDITOR_ICONS_H

#include <QtGlobal>

QT_BEGIN_NAMESPACE
class QIcon;
QT_END_NAMESPACE

namespace PyEditor {

class IconsPrivate;

class Icons
{
    Icons();
    static Icons *m_instance;

    const QIcon &forClass() const;
    const QIcon &forFunction() const;
    const QIcon &forSignal() const;
    const QIcon &forSlot() const;
    const QIcon &forEnum() const;
    const QIcon &forVariable() const;

public:
    static Icons *instance();
    IconsPrivate *d;
};

} // namespace PyEditor

#endif // PYEDITOR_ICONS_H
