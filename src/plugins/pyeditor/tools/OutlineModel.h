//#ifndef PYEDITOR_OUTLINEMODEL_H
//#define PYEDITOR_OUTLINEMODEL_H

//#include <python/pythonmodel/Document.h>

//#include <QStandardItemModel>

//namespace PyEditor {

//class OutlineModel;
//class EditorWidget;
//class OutlineModelSync;
//class Icons;

//class OutlineItem : public QStandardItem
//{
//public:
//    OutlineItem(OutlineModel &model);
//    QVariant data(int role = Qt::UserRole + 1) const;
//    int type() const;
//    void setItemData(const QMap<int, QVariant> &roles);

//private:
//    OutlineModel &m_outlineModel;
//};

//class OutlineModel : public QStandardItemModel
//{
//    Q_OBJECT
//public:
//    enum CustomRoles {
//        ItemTypeRole = Qt::UserRole + 1,
//        ElementTypeRole,
//        AnnotationRole
//    };

//    enum ItemTypes {
//        ElementType,
//        ElementBindingType, // might contain elements as children
//        NonElementBindingType // can be filtered out
//    };

//    explicit OutlineModel(EditorWidget &parent);

//    // QStandardItemModel
//    QStringList mimeTypes() const;
//    QMimeData *mimeData(const QModelIndexList &indexes) const;
//    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex  &parent);
//    Qt::ItemFlags flags(const QModelIndex &index) const;

//    Python::Document::Ptr document() const;
//    void update();

//    //! Gives more info by model index
//    Python::Ast &ast(const QModelIndex &index) const;
//    Python::SourceLocation sourceLocation(const QModelIndex &index) const;
//    QIcon icon(const QModelIndex &index) const;

//signals:
//    void updated();

//private:
//    QModelIndex enterClassDefinition(Python::ClassDefinitionAst &classDef);
//    void leaveClassDefiniton();

//    QModelIndex enterFunctionDefinition(Python::FunctionDefinitionAst &funcDef);
//    void leaveFunctionDefinition();

//private:
//    OutlineItem *enterAst(QMap<int, QVariant> data, Python::Ast *node, const QIcon &icon);
//    void leaveAst();

////    void reparentNodes(QmlOutlineItem *targetItem, int targetRow, QList<QmlOutlineItem*> itemsToMove);
////    void moveObjectMember(QmlJS::AST::UiObjectMember *toMove, QmlJS::AST::UiObjectMember *newParent,
////                          bool insertionOrderSpecified, QmlJS::AST::UiObjectMember *insertAfter,
////                          Utils::ChangeSet *changeSet, Utils::ChangeSet::Range *addedRange);

//    QStandardItem &parentItem();

////    static QString asString(QmlJS::AST::UiQualifiedId *id);
//    static Python::SourceLocation getLocation(Python::Ast &objMember);
////    QIcon getIcon(QmlJS::AST::UiQualifiedId *objDef);

////    QString getAnnotation(QmlJS::AST::UiObjectInitializer *objInitializer);
////    QString getAnnotation(QmlJS::AST::Statement *statement);
////    QString getAnnotation(QmlJS::AST::ExpressionNode *expression);
////    QHash<QString,QString> getScriptBindings(QmlJS::AST::UiObjectInitializer *objInitializer);


////    QmlJSTools::SemanticInfo m_semanticInfo;
//    QList<int> m_treePos;
//    QStandardItem *m_currentItem;
//    Icons *m_icons;

//    QHash<QString, QIcon> m_typeToIcon;
//    QHash<const OutlineItem *, QIcon> m_itemToIcon;
//    QHash<const OutlineItem *, Python::Ast *> m_itemToNode;
//    EditorWidget *m_textEditor;

//    friend class OutlineModelSync;
//    friend class OutlineItem;
//};

//} // namespace PyEditor

//#endif // PYEDITOR_OUTLINEMODEL_H
