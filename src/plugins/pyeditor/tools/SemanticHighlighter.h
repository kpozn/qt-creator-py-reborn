#ifndef PYEDITOR_SEMANTICHIGHLIGHTER_H
#define PYEDITOR_SEMANTICHIGHLIGHTER_H

#include <pylang/pythonmodel/Document.h>
#include <pylang/ast/BaseVisitor.h>
//#include <pylang/analysis/SemanticInfo.h>

#include <texteditor/semantichighlighter.h>
#include <QtConcurrentRun>

namespace PyEditor {

//class SemanticHighlighter :
//        protected Pylang::BaseVisitor<true>,
//        public QRunnable,
//        public QFutureInterface<TextEditor::SemanticHighlighter::Result>
//{
//public:
//    SemanticHighlighter();
//    virtual ~SemanticHighlighter();

//    typedef TextEditor::SemanticHighlighter::Result Use;
//    typedef Pylang::SemanticInfo::UseKind UseKind;

//protected:

//private:
//};

} // namespace PyEditor

#endif // PYEDITOR_SEMANTICHIGHLIGHTER_H
