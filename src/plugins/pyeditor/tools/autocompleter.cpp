#include "autocompleter.h"
#include <QtGui/QTextCursor>
#include <QtGui/QTextBlock>

/*
 * Use this code for research (if you want to know when particular
 * function will be called, as an example).
 * __PRETTY_FUCTION__ is GCC-only macro, use __func__ in other compilers.
 */
#if 0
#include <QtCore/QDebug>
#define TRACEPOINT (qDebug() << __PRETTY_FUNCTION__);

#if 0
// Copy & paste:
TRACEPOINT;
qDebug() << cursor.block().text();
#endif
#endif

namespace {
QChar safeStringAt(int pos, const QString &string_)
{
    if ((pos >= 0) && (pos < string_.size()))
        return string_.at(pos);
    return '\0';
}
} // anonimous namespace

namespace PyEditor {

Autocompleter::Autocompleter()
{
}

Autocompleter::~Autocompleter()
{
}

QString Autocompleter::autoComplete(QTextCursor &cursor, const QString &text) const
{
    if (text == "(")
    {
        cursor.insertText(")");
        cursor.movePosition(QTextCursor::Left);
    }
    else if (text == "[")
    {
        cursor.insertText("]");
        cursor.movePosition(QTextCursor::Left);
    }
    else if (text == "{")
    {
        cursor.insertText("}");
        cursor.movePosition(QTextCursor::Left);
    }
    else if (text == ")")
    {
        QString text = cursor.block().text();
        int pos = cursor.positionInBlock();
        if ((safeStringAt(pos - 1, text) == '(')
                && (safeStringAt(pos, text) == ')'))
        {
            cursor.deleteChar();
        }
    }
    else if (text == "]")
    {
        QString text = cursor.block().text();
        int pos = cursor.positionInBlock();
        if ((safeStringAt(pos - 1, text) == '[')
                && (safeStringAt(pos, text) == ']'))
        {
            cursor.deleteChar();
        }
    }
    else if (text == "}")
    {
        QString text = cursor.block().text();
        int pos = cursor.positionInBlock();
        if ((safeStringAt(pos - 1, text) == '{')
                && (safeStringAt(pos, text) == '}'))
        {
            cursor.deleteChar();
        }
    }

    return QString();
}

bool Autocompleter::autoBackspace(QTextCursor &cursor)
{
    if (!cursor.hasSelection())
    {
        int pos = cursor.positionInBlock();
        QString text = cursor.block().text();
        QChar ch = safeStringAt(pos - 1, text);
        QChar next = safeStringAt(pos, text);

        if (((ch == '(') && (next == ')'))
            || ((ch == '[') && (next == ']'))
            || ((ch == '{') && (next == '}')))
        {
            cursor.deleteChar();
        }
    }

    return false;
}

int Autocompleter::paragraphSeparatorAboutToBeInserted(QTextCursor &cursor,
                                                       const TextEditor::TabSettings &tabSettings)
{
    Q_UNUSED(cursor)
    Q_UNUSED(tabSettings)
    return 0;
}

bool Autocompleter::isInComment(const QTextCursor &cursor) const
{
    Q_UNUSED(cursor)
    return false;
}

bool Autocompleter::contextAllowsAutoParentheses(const QTextCursor &cursor,
                                                 const QString &textToInsert) const
{
    Q_UNUSED(cursor)
    Q_UNUSED(textToInsert)
    return false;
}

bool Autocompleter::contextAllowsElectricCharacters(const QTextCursor &cursor) const
{
    Q_UNUSED(cursor)
    return false;
}

QString Autocompleter::insertMatchingBrace(
        const QTextCursor &cursor, const QString &text, QChar la, int *skippedChars) const
{
    Q_UNUSED(cursor)
    Q_UNUSED(text)
    Q_UNUSED(la)
    Q_UNUSED(skippedChars)
    return QString();
}

QString Autocompleter::insertParagraphSeparator(const QTextCursor &cursor) const
{
    Q_UNUSED(cursor)
    return QString();
}

} // namespace PyEditor
