#ifndef PYEDITOR_AUTOCOMPLETER_H
#define PYEDITOR_AUTOCOMPLETER_H

#include <texteditor/autocompleter.h>

namespace PyEditor {

class Autocompleter : public TextEditor::AutoCompleter
{
public:
    Autocompleter();
    virtual ~Autocompleter();

    virtual QString autoComplete(
            QTextCursor &cursor,
            const QString &text) const;

    virtual bool autoBackspace(QTextCursor &cursor);

    virtual int paragraphSeparatorAboutToBeInserted(
            QTextCursor &cursor,
            const TextEditor::TabSettings &tabSettings);

    bool contextAllowsAutoParentheses(
            const QTextCursor &cursor,
            const QString &textToInsert) const;

    bool contextAllowsElectricCharacters(const QTextCursor &cursor) const;

    virtual bool isInComment(const QTextCursor &cursor) const;

    QString insertMatchingBrace(
            const QTextCursor &cursor,
            const QString &text, QChar la,
            int *skippedChars) const;

    QString insertParagraphSeparator(const QTextCursor &cursor) const;
};

} // namespace PyEditor

#endif // PYEDITOR_AUTOCOMPLETER_H
