/**
 * @brief The Highlighter class pre-highlights Python source using simple scanner.
 *
 * Highlighter doesn't highlight user types (classes and enumerations), syntax
 * and semantic errors, unnecessary code, etc. It's implements only
 * basic highlight mechanism.
 *
 * Main highlight procedure is highlightBlock().
 */

// Self headers
#include "highlighter.h"
#include "../constants.h"

// QtCreator platform & other plugins
#include <pylang/lexer/HighlightToken.h>
#include <pylang/lexer/HighlightLexer.h>
#include <texteditor/basetextdocumentlayout.h>
#include <texteditor/basetextdocument.h>
#include <texteditor/fontsettings.h>
#include <texteditor/texteditorconstants.h>

using namespace pylang;

namespace PyEditor {

static QVector<TextEditor::TextStyle> highlighterFormatCategories()
{
    /*
        HighlightAs_Number,
        HighlightAs_String,
        HighlightAs_Keyword,
        HighlightAs_Type,
        HighlightAs_Member,
        HighlightAs_Operator,
        HighlightAs_Comment,
        HighlightAs_DoxygenComment,
        HighlightAs_Whitespace,
        HighlightAs_Text,
        HighlightAs_Import,
    */

    static QVector<TextEditor::TextStyle> categories;
    if (categories.isEmpty()) {
        categories << TextEditor::C_NUMBER
                << TextEditor::C_STRING
                << TextEditor::C_KEYWORD
                << TextEditor::C_TYPE
                << TextEditor::C_FIELD
                << TextEditor::C_OPERATOR
                << TextEditor::C_COMMENT
                << TextEditor::C_DOXYGEN_COMMENT
                << TextEditor::C_VISUAL_WHITESPACE
                << TextEditor::C_TEXT
                << TextEditor::C_JS_IMPORT_VAR;
    }

    return categories;
}

static QSet<QString> initStringSet(const char *const words[], size_t size)
{
    size_t amount = size / sizeof(const char *const);
    QSet<QString> ret;
    for (size_t index = 0; index < amount; ++index)
        ret.insert(words[index]);
    return ret;
}

Highlighter::Highlighter(TextEditor::BaseTextDocument *parent) :
    TextEditor::SyntaxHighlighter(parent)
{
    _langKeywords = initStringSet(Constants::LIST_OF_PYTHON_KEYWORDS,
                                  sizeof(Constants::LIST_OF_PYTHON_KEYWORDS));
    _langTypes = initStringSet(Constants::LIST_OF_PYTHON_BUILTINS,
                               sizeof(Constants::LIST_OF_PYTHON_BUILTINS));
}

Highlighter::~Highlighter()
{
}

/**
  QtCreator has own fonts&color settings. Highlighter wants get access to
  this settings before highlightBlock() called first time.
  Settings provided by PyEditor::EditorWidget class.
  */
void Highlighter::setFontSettings(const TextEditor::FontSettings &fs)
{
    _formats = fs.toTextCharFormats(highlighterFormatCategories());
    rehighlight();
}

/**
 * @brief Highlighter::highlightBlock highlights single line of Python code
 * @param text is single line without EOLN symbol. Access to all block data
 * can be obtained through inherited currentBlock() method.
 *
 * This method receives state (int number) from previously highlighted block,
 * scans block using received state and sets initial highlighting for current
 * block. At the end, it saves internal state in current block.
 */
void Highlighter::highlightBlock(const QString &text)
{
    int initialState = previousBlockState();
    if (initialState == -1)
        initialState = 0;

    setCurrentBlockState(highlightLine(text, initialState));
}

/**
 * Returns true if this keyword is acceptable at start of import line
 */
static inline
bool isImportKeyword(const QString &keyword)
{
    return (keyword == QLatin1String("import")
            || keyword == QLatin1String("from"));
}

/**
 * @brief Highlighter::highlight_impl
 * @param text - source code to highlight
 * @param initialState - initial state of scanner, retrieved from previous block
 * @return final state of scanner, should be saved with current block
 */
int Highlighter::highlightLine(const QString &text, int initialState)
{
    HighlightLexer lexer(text, _langKeywords, _langTypes);
    lexer.setState(initialState);

    HighlightToken tk;
    bool hasOnlyWhitespace = true;
    while ((tk = lexer.read()).format() != EndOfHighlight)
    {
        HighlightAs format = tk.format();
        if (format == HighlightAs_Keyword) {
            QString value = lexer.value(tk);
            if (isImportKeyword(value) && hasOnlyWhitespace) {
                setFormat(tk.begin(), tk.length(), _formats[format]);
                highlightImport(lexer);
                return lexer.getState();
            }
        }
        setFormat(tk.begin(), tk.length(), _formats[format]);
        if (format != HighlightAs_Whitespace)
            hasOnlyWhitespace = false;
    }
    return lexer.getState();
}

void Highlighter::highlightImport(HighlightLexer &lexer)
{
    HighlightToken tk;
    while ((tk = lexer.read()).format() != EndOfHighlight) {
        HighlightAs format = tk.format();
        if (tk.format() == HighlightAs_Text)
            format = HighlightAs_Import;
        setFormat(tk.begin(), tk.length(), _formats[format]);
    }
}

} // namespace PyEditor
