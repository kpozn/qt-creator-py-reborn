#ifndef PYEDITOR_HIGHLIGHTER_H
#define PYEDITOR_HIGHLIGHTER_H

#include <QtCore/QMap>

#include <texteditor/syntaxhighlighter.h>
#include <texteditor/fontsettings.h>
#include "../pluginglobal.h"

namespace pylang { class HighlightLexer; }

namespace PyEditor {

class PYEDITOR_EXPORT Highlighter : public TextEditor::SyntaxHighlighter
{
    Q_OBJECT
public:
    explicit Highlighter(TextEditor::BaseTextDocument *parent = 0);
    virtual ~Highlighter();

    void setFontSettings(const TextEditor::FontSettings &fs);

protected:
    virtual void highlightBlock(const QString &text);

private:
    int highlightLine(const QString &text, int initialState);
    void highlightImport(pylang::HighlightLexer &scanner);

    QVector<QTextCharFormat> _formats;
    QSet<QString> _langTypes;
    QSet<QString> _langKeywords;
   // QSet<QString> _langModules;
};

} // namespace PyEditor

#endif // PYEDITOR_HIGHLIGHTER_H
