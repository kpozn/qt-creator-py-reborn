#include "hoverhandler.h"
#include "../editor.h"
#include "../editorwidget.h"

#include <coreplugin/editormanager/ieditor.h>
#include <coreplugin/editormanager/editormanager.h>
#include <texteditor/itexteditor.h>

namespace PyEditor {

HoverHandler::HoverHandler(QObject *parent) :
    TextEditor::BaseHoverHandler(parent)
{
}

HoverHandler::~HoverHandler()
{
}

/**
 * @brief HoverHandler::acceptEditor denies editor if it not handled by PyEditor's subclass
 * of BaseHoverHandler
 * @param editor
 * @return true if given editor is instance of PyEditor::Editor
 */
bool HoverHandler::acceptEditor(Core::IEditor *editor)
{
    if (qobject_cast<Editor *>(editor) != 0)
        return true;
    return false;
}

void HoverHandler::identifyMatch(TextEditor::ITextEditor *editor, int pos)
{
    EditorWidget *widget = qobject_cast<EditorWidget *>(editor->widget());
    if (!widget)
        return;

    if (! widget->extraSelectionTooltip(pos).isEmpty())
    {
        setToolTip(widget->extraSelectionTooltip(pos));
    }
}

void HoverHandler::decorateToolTip()
{
    if (Qt::mightBeRichText(toolTip()))
        setToolTip(Qt::escape(toolTip()));
}

} // namespace PyEditor
