#ifndef PYEDITOR_HOVERHANDLER_H
#define PYEDITOR_HOVERHANDLER_H

#include "../pluginglobal.h"
#include <texteditor/basehoverhandler.h>

namespace Core {
class IEditor;
}

namespace TextEditor {
class ITextEditor;
}

namespace PyEditor {

class PYEDITOR_EXPORT HoverHandler : public TextEditor::BaseHoverHandler
{
    Q_OBJECT
public:
    explicit HoverHandler(QObject *parent = 0);
    virtual ~HoverHandler();
    
signals:
    
public slots:
    
private:
    virtual bool acceptEditor(Core::IEditor *editor);
    virtual void identifyMatch(TextEditor::ITextEditor *editor, int pos);
    virtual void decorateToolTip();
};

} // namespace PyEditor

#endif // PYEDITOR_HOVERHANDLER_H
