#include "indenter.h"

#include <texteditor/tabsettings.h>
#include <pylang/lexer/Lexer.h>

#include <QtCore/QSet>

using namespace pylang;

namespace PyEditor {

// FIXME: Despite the fact that 4 spaces per indent required by PEP8,
// much better is use python-specific IDE settings
static const int TAB_SIZE = 4;

Indenter::Indenter()
{
}

Indenter::~Indenter()
{
}

/**
 * @brief isElectricCharacter Does given character increase indentation level?
 * @param ch Any value
 * @return True if character increases indentation level at next line
 */
bool Indenter::isElectricCharacter(const QChar &ch) const
{
    return (ch == ':');
}

/**
 * @brief Indenter::indentBlock Indents one block (equals to single line)
 * @param block
 * @param typedChar
 * @param tabSettings An IDE tabulation settings
 *
 * Usually this method called once when you begin new line of code by pressing
 * Enter. If Indenter reimplements indent() method, than indentBlock() may be
 * called in other cases.
 */
void Indenter::indentBlock(QTextDocument *doc,
                           const QTextBlock &block,
                           const QChar &typedChar,
                           const TextEditor::TabSettings &settings)
{
    Q_UNUSED(doc)
    Q_UNUSED(typedChar)

    QTextBlock previousBlock = block.previous();
    if (previousBlock.isValid())
    {
        QString previousLine = previousBlock.text();
        int indentation = settings.indentationColumn(previousLine);

        if (isElectricLine(previousLine))
            indentation += TAB_SIZE;
        parsePreviousLine(settings, previousLine, previousBlock, indentation);
        settings.indentLine(block, indentation);
    } else
    {
        // First line in whole document
        settings.indentLine(block, 0);
    }
}

//! Returns true if electric character is last non-space character at given string
bool Indenter::isElectricLine(const QString &line) const
{
    uint index = line.length();
    if (index == 0) {
        return false;
    }
    --index;
    while ((index > 0) && line[index].isSpace()) {
        --index;
    }
    return (isElectricCharacter(line[index]));
}

void Indenter::parsePreviousLine(
        const TextEditor::TabSettings &settings,
        const QString &previousLine,
        const QTextBlock &previousBlock,
        int &indentation) const
{
    (void) previousBlock;
    (void) settings;

    // TODO: replace this dirty code with true AST-based indentation
    std::string source = previousLine.toStdString();
    // TODO: add support for another dialects
    TranslationUnit unit;
    UnitBuilder builder(unit);
    Lexer lexer(builder, source.c_str(), source.size());
    lexer.init();
    Token tk;
    for (;;)
    {
        switch ((tk = lexer.read()).lexem())
        {
        case T_EndOfFile:
        case T_Newline:
            return;
        case T_KwReturn:
        case T_KwYield:
        case T_KwRaise:
        case T_KwBreak:
        case T_KwContinue:
        case T_KwPass:
            indentation = qMax<int>(0, indentation - TAB_SIZE);
            return;
        case T_KwElse:
        case T_KwElif:
        case T_KwExcept:
        case T_KwFinally:
        /** NOTE: backstep indentation turned off becouse it's too annoying
            Maybe full document AST will help */
//                indentation = qMax<int>(0, indentation - TAB_SIZE);
//                settings.reindentLine(previousBlock, -TAB_SIZE);
            return;

        default:
            break;
        }
    }
}

} // namespace PyEditor
