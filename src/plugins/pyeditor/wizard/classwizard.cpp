
// Self headers
#include "classwizard.h"
#include "classwizarddialog.h"
#include "classnamepage.h"
#include "../constants.h"

// QtCreator platform
#include <pylang/wizards/SourceGenerator.h>

namespace PyEditor {

static inline
Core::BaseFileWizardParameters getDefaultParams()
{
    Core::BaseFileWizardParameters p(Core::IWizard::FileWizard);

    p.setId(QLatin1String(Constants::C_PY_CLASS_WIZARD_ID));
    p.setCategory(QLatin1String(Constants::C_PY_WIZARD_CATEGORY));
    p.setDisplayCategory(QLatin1String(Constants::C_PY_DISPLAY_CATEGORY));
    p.setDisplayName(
                QObject::tr(Constants::EN_PY_CLASS_DISPLAY_NAME));
    p.setDescription(
                QObject::tr(Constants::EN_PY_CLASS_DESCRIPTION));

    return p;
}

ClassWizard::ClassWizard(QObject *parent) :
    Core::BaseFileWizard(getDefaultParams(), parent)
{
}

ClassWizard::~ClassWizard()
{
}

QWizard *ClassWizard::createWizardDialog(
        QWidget *parent,
        const Core::WizardDialogParameters &params) const
{
    ClassWizardDialog *wizard = new ClassWizardDialog(parent);
    foreach (QWizardPage *p, params.extensionPages())
        BaseFileWizard::applyExtensionPageShortTitle(wizard, wizard->addPage(p));
    wizard->setPath(params.defaultPath());
    return wizard;
}

Core::GeneratedFiles ClassWizard::generateFiles(const QWizard *w,
                                                QString *errorMessage) const
{
    const ClassWizardDialog *wizard = qobject_cast<const ClassWizardDialog *>(w);
    const ClassWizardParameters params = wizard->parameters();

    const QString fileName = Core::BaseFileWizard::buildFileName(
                params.path, params.fileName, Constants::C_PY_EXTENSION);
    Core::GeneratedFile sourceFile(fileName);

    pylang::SourceGenerator generator;
    generator.setBinding(pylang::SourceGenerator::PySide);
    QString sourceContent = generator.generateClass(
                params.className, params.baseClass, params.classType
                );

    sourceFile.setContents(sourceContent);
    sourceFile.setAttributes(Core::GeneratedFile::OpenEditorAttribute);
    return Core::GeneratedFiles() << sourceFile;
}

} // namespace PyEditor
