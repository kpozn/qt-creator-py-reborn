/**************************************************************************
**
** This file is part of Qt Creator
**
** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**************************************************************************/

#include "applicationwizard.h"
#include "constants.h"
#include "pluginglobal.h"

// QtCreator platform
#include <app/app_version.h>
#include <projectexplorer/customwizard/customwizard.h>
#include <projectexplorer/projectexplorerconstants.h>
#include <qtsupport/qtsupportconstants.h>
#include <pylang/wizards/SourceGenerator.h>

// Qt library
#include <QIcon>
#include <QPainter>
#include <QPixmap>
#include <QDir>
#include <QTextStream>
#include <QCoreApplication>

namespace PythonProjectManager {
namespace Internal {

ApplicationWizardDialog::ApplicationWizardDialog(
        QWidget *parent, const Core::WizardDialogParameters &parameters) :
    ProjectExplorer::BaseProjectWizardDialog(parent, parameters)
{
    setWindowTitle(tr("New PySide Project"));
    setIntroDescription(tr("This wizard generates a Qt Quick UI project."));
}

ApplicationWizard::ApplicationWizard()
    : Core::BaseFileWizard(parameters())
{
}

ApplicationWizard::~ApplicationWizard()
{
}

Core::FeatureSet ApplicationWizard::requiredFeatures() const
{
    return Core::Feature(QtSupport::Constants::FEATURE_QMLPROJECT);
}

Core::BaseFileWizardParameters ApplicationWizard::parameters()
{
    Core::BaseFileWizardParameters parameters(ProjectWizard);
    parameters.setIcon(QIcon(QLatin1String(Constants::RC_WIZARD_ICON)));
    parameters.setDisplayName(tr(Constants::WIZARD_DISPLAY_NAME));
    parameters.setId(Constants::WIZARD_ID);

    parameters.setDescription(tr(
        "Creates a PySide project with a single python file.\n\n"
        "You need not build PySide project.\n\n"
        "Requires installed PyQt4 bindings."));
    parameters.setCategory(QLatin1String(ProjectExplorer::Constants::QT_APPLICATION_WIZARD_CATEGORY));
    parameters.setDisplayCategory(Constants::WIZARD_DISPLAY_CATEGORY);

    return parameters;
}

QWizard *ApplicationWizard::createWizardDialog(
        QWidget *parent,
        const Core::WizardDialogParameters &wizardDialogParameters) const
{
    ApplicationWizardDialog *wizard =
            new ApplicationWizardDialog(parent, wizardDialogParameters);

    wizard->setProjectName(
                ApplicationWizardDialog::uniqueProjectName(
                    wizardDialogParameters.defaultPath()));
    wizard->addExtensionPages(wizardDialogParameters.extensionPages());

    return wizard;
}

Core::GeneratedFiles ApplicationWizard::generateFiles(
        const QWizard *w, QString *errorMessage) const
{
    Q_UNUSED(errorMessage)

    const ApplicationWizardDialog *wizard =
            qobject_cast<const ApplicationWizardDialog *>(w);
    const QString projectName = wizard->projectName();
    const QString projectPath = wizard->path() + QLatin1Char('/') + projectName;

    const QString creatorFileName = Core::BaseFileWizard::buildFileName(
                projectPath, projectName, Constants::PROJECT_SUFFIX);
    const QString mainFileName = Core::BaseFileWizard::buildFileName(
                projectPath, "main", QLatin1String("py"));
    const QString windowFileName = Core::BaseFileWizard::buildFileName(
                projectPath, "mainwindow", QLatin1String("py"));

    pylang::SourceGenerator generator;
    generator.setBinding(pylang::SourceGenerator::PySide);

    Core::GeneratedFile generatedMainFile(mainFileName);
    generatedMainFile.setContents(generator.generateQtMain(projectName));
    generatedMainFile.setAttributes(Core::GeneratedFile::OpenEditorAttribute);

    Core::GeneratedFile generatedWindowFile(windowFileName);
    generatedWindowFile.setContents(
                generator.generateClass("MainWindow",
                                        "QtGui.QMainWindow",
                                        Utils::NewClassWidget::ClassInheritsQWidget));

    QString projectContents;
    {
        QTextStream out(&projectContents);

        //! TODO: pythonize it
        out << "/* File generated by Qt Creator, version " << Core::Constants::IDE_VERSION_LONG << " */" << endl
            << endl
            << "import PythonProject 1.1" << endl
            << endl
            << "Project {" << endl
            << "    mainFile: \"" << QDir(projectPath).relativeFilePath(mainFileName) << '"' << endl
            << endl
            << "    /* Include python, .js, and image files from current directory and subdirectories */" << endl
            << "    PythonFiles {" << endl
            << "        directory: \".\"" << endl
            << "    }" << endl
            << "    JavaScriptFiles {" << endl
            << "        directory: \".\"" << endl
            << "    }" << endl
            << "    ImageFiles {" << endl
            << "        directory: \".\"" << endl
            << "    }" << endl
            << "    /* List of plugin directories passed to QML runtime */" << endl
            << "    // importPaths: [ \"../exampleplugin\" ]" << endl
            << "}" << endl;
    }
    Core::GeneratedFile generatedCreatorFile(creatorFileName);
    generatedCreatorFile.setContents(projectContents);
    generatedCreatorFile.setAttributes(Core::GeneratedFile::OpenProjectAttribute);

    Core::GeneratedFiles files;
    files.append(generatedMainFile);
    files.append(generatedCreatorFile);
    files.append(generatedWindowFile);

    return files;
}

bool ApplicationWizard::postGenerateFiles(const QWizard *, const Core::GeneratedFiles &l, QString *errorMessage)
{
    return ProjectExplorer::CustomProjectWizard::postGenerateOpen(l, errorMessage);
}

} // namespace Internal
} // namespace PythonProjectManager

