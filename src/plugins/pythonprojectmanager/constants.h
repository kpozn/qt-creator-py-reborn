/**************************************************************************
**
** This file is part of Qt Creator
**
** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**************************************************************************/

#ifndef PYTHONPROJECTMANAGER_CONSTANTS_H
#define PYTHONPROJECTMANAGER_CONSTANTS_H

#include <qglobal.h>

namespace PythonProjectManager {
namespace Constants {

const char QML_RC_ID[] =
        "PythonProjectManager.RunConfiguration";
const char QML_VIEWER_QT_KEY[] =
        "PythonProjectManager.RunConfiguration.QtVersion";
const char PYTHON_LAUNCHER_ARGUMENTS_KEY[] =
        "PythonProjectManager.RunConfiguration.PythonLauncherArgs";
const char PYTHON_LAUNCHER_TARGET_ID[] =
        "PythonProjectManager.Target";
const char PYTHON_LAUNCHER_TARGET_DISPLAY_NAME[] = "QML Viewer";
const char PYTHON_LAUNCHER_TARGET_ANNOTATION[] = "QML Viewer target display name";

const char MAINSCRIPT_KEY[] =
        "PythonProjectManager.RunConfiguration.MainScript";
const char USER_ENVIRONMENT_CHANGES_KEY[] =
        "PythonProjectManager.RunConfiguration.UserEnvironmentChanges";

const char PROJECTCONTEXT[] = "PythonProject.ProjectContext";
const char LANG_PYTHON[]       = "Python";
const char PROJECT_MIMETYPE[]    = "application/x-pysideproject";
const char PROJECT_ID[] = "PythonProjectManager.PythonProject";

const char WIZARD_DISPLAY_NAME[] = "PySide app";
const char WIZARD_ID[] = "QB.PySide Application";
const char RC_WIZARD_ICON[] = ":/pysideproject/images/qml_wizard.png";
const char RC_FOLDER_ICON[] = ":/pysideproject/images/qmlfolder.png";
const char RC_PROJECT_ICON[] = ":/pysideproject/images/qmlfolder.png";
const char RC_MIMETYPES[] = ":pysideproject/PythonProject.mimetypes.xml";

const char WIZARD_DISPLAY_CATEGORY[] = QT_TRANSLATE_NOOP(
            "ProjectExplorer",
            "Qt Application");

const char PROJECT_SUFFIX[] = "pysideproject";

const char C_PROJECT_EDITOR_ID[] = "PythonProjectManager.ProjectEditor";
const char C_PROJECT_EDITOR_DISPLAY_NAME[] =
        QT_TRANSLATE_NOOP("OpenWith::Editors", "QMLJS Editor");

/**
 * Enviroment variables and command-line arguments to run python
 */

const char C_PYTHONPATH[] = "PYTHONPATH";

} // namespace Constants
} // namespace PythonProjectManager

#endif // PYTHONPROJECTMANAGER_CONSTANTS_H
