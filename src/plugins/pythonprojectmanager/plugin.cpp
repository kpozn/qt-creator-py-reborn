/**************************************************************************
**
** This file is part of Qt Creator
**
** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**************************************************************************/

#include "plugin.h"
#include "projectmanager.h"
#include "applicationwizard.h"
#include "constants.h"
#include "project.h"
#include "runconfigurationfactory.h"
#include "runcontrol.h"
#include "target.h"
#include "fileformat/pythonprojectfileformat.h"

#include <extensionsystem/pluginmanager.h>

#include <coreplugin/fileiconprovider.h>
#include <coreplugin/icore.h>
#include <coreplugin/mimedatabase.h>

#include <texteditor/texteditoractionhandler.h>

#include <projectexplorer/taskhub.h>
#include <projectexplorer/projectexplorerconstants.h>

#include <qtsupport/qtsupportconstants.h>

#include <QtPlugin>

#include <QApplication>
#include <QMessageBox>
#include <QPushButton>

namespace PythonProjectManager {

Plugin::Plugin()
{
}

Plugin::~Plugin()
{
}

bool Plugin::initialize(const QStringList &, QString *errorMessage)
{
    Core::MimeDatabase *mimeDB = Core::ICore::mimeDatabase();
    const QLatin1String mimetypesXml(Constants::RC_MIMETYPES);
    if (! mimeDB->addMimeTypes(mimetypesXml, errorMessage))
        return false;

    Internal::Manager *manager = new Internal::Manager;
    addAutoReleasedObject(manager);
    addAutoReleasedObject(new Internal::QmlProjectRunConfigurationFactory);
    addAutoReleasedObject(new Internal::QmlProjectRunControlFactory);
    addAutoReleasedObject(new Internal::ApplicationWizard);

    PythonProjectFileFormat::registerDeclarativeTypes();

    Core::FileIconProvider *iconProvider = Core::FileIconProvider::instance();
    iconProvider->registerIconOverlayForSuffix(
                QIcon(Constants::RC_PROJECT_ICON),
                Constants::PROJECT_SUFFIX);
    return true;
}

void Plugin::extensionsInitialized()
{
}

void Plugin::showQmlObserverToolWarning()
{
    QMessageBox dialog(QApplication::activeWindow());
    QPushButton *qtPref = dialog.addButton(tr("Open Qt Versions"),
                                           QMessageBox::ActionRole);
    dialog.addButton(QMessageBox::Cancel);
    dialog.setDefaultButton(qtPref);
    dialog.setWindowTitle(tr("QML Observer Missing"));
    dialog.setText(tr("QML Observer could not be found for this Qt version."));
    dialog.setInformativeText(tr(
                                  "QML Observer is used to offer debugging features for "
                                  "Qt Quick UI projects in the Qt 4.7 series.\n\n"
                                  "To compile QML Observer, go to the Qt Versions page, "
                                  "select the current Qt version, "
                                  "and click Build in the Helpers section."));
    dialog.exec();
    if (dialog.clickedButton() == qtPref) {
        Core::ICore::showOptionsDialog(
                    ProjectExplorer::Constants::PROJECTEXPLORER_SETTINGS_CATEGORY,
                    QtSupport::Constants::QTVERSION_SETTINGS_PAGE_ID);
    }
}

} // namespace PythonProjectManager

Q_EXPORT_PLUGIN(PythonProjectManager::Plugin)
