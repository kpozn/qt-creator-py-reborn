/**************************************************************************
**
** This file is part of Qt Creator
**
** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**************************************************************************/

#include "project.h"
#include "document.h"
#include "constants.h"
#include "fileformat/projectitem.h"
#include "runconfiguration.h"
#include "target.h"
#include "constants.h"
#include "projectnodes.h"
#include "projectmanager.h"

#include <coreplugin/icore.h>
#include <coreplugin/messagemanager.h>
#include <coreplugin/documentmanager.h>
#include <extensionsystem/pluginmanager.h>
#include <qtsupport/qmldumptool.h>
#include <qtsupport/baseqtversion.h>
#include <qtsupport/qtversionmanager.h>
#include <qmljs/qmljsmodelmanagerinterface.h>
#include <utils/fileutils.h>
#include <projectexplorer/toolchainmanager.h>
#include <projectexplorer/profilemanager.h>
#include <utils/filesystemwatcher.h>
#include <projectexplorer/target.h>

#include <QTextStream>
#include <QDeclarativeComponent>
#include <QtDebug>

namespace PE = ProjectExplorer;

namespace PythonProjectManager {

Project::Project(Internal::Manager *manager, const QString &fileName)
    : m_manager(manager),
      m_fileName(fileName),
      m_modelManager(QmlJS::ModelManagerInterface::instance())
{
    setProjectContext(Core::Context(PythonProjectManager::Constants::PROJECTCONTEXT));
    setProjectLanguage(Core::Context(PythonProjectManager::Constants::LANG_PYTHON));

    QFileInfo fileInfo(m_fileName);
    m_projectName = fileInfo.completeBaseName();

    m_file = new Internal::QmlProjectFile(this, fileName);
    m_rootNode.reset(new Internal::QmlProjectNode(this, m_file));

    Core::DocumentManager::addDocument(m_file, true);

    m_manager->registerProject(this);
}

Project::~Project()
{
    m_manager->unregisterProject(this);

    Core::DocumentManager::removeDocument(m_file);

    delete m_projectItem.data();
}

QDir Project::projectDir() const
{
    return QFileInfo(document()->fileName()).dir();
}

QString Project::filesFileName() const
{
    return m_fileName;
}

void Project::parseProject(RefreshOptions options)
{
    Core::MessageManager *messageManager = Core::ICore::messageManager();
    if (options & Files) {
        if (options & ProjectFile)
            delete m_projectItem.data();
        if (!m_projectItem) {
            Utils::FileReader reader;
            if (reader.fetch(m_fileName)) {
                QDeclarativeComponent *component = new QDeclarativeComponent(&m_engine, this);
                component->setData(reader.data(), QUrl::fromLocalFile(m_fileName));
                if (component->isReady()
                    && qobject_cast<PythonProjectItem*>(component->create())) {
                    m_projectItem = qobject_cast<PythonProjectItem*>(component->create());
                    connect(m_projectItem.data(), SIGNAL(qmlFilesChanged(QSet<QString>,QSet<QString>)),
                            this, SLOT(refreshFiles(QSet<QString>,QSet<QString>)));
                } else {
                    messageManager->printToOutputPane(tr("Error while loading project file %1.").arg(m_fileName));
                    messageManager->printToOutputPane(component->errorString(), true);
                }
            } else {
                messageManager->printToOutputPane(tr("Python project: %1").arg(reader.errorString()), true);
            }
        }
        if (m_projectItem) {
            m_projectItem.data()->setSourceDirectory(projectDir().path());
            m_modelManager->updateSourceFiles(m_projectItem.data()->files(), true);

            QString mainFilePath = m_projectItem.data()->mainFile();
            if (!mainFilePath.isEmpty()) {
                mainFilePath = projectDir().absoluteFilePath(mainFilePath);
                if (!QFileInfo(mainFilePath).isReadable()) {
                    messageManager->printToOutputPane(
                                tr("Warning while loading project file %1.").arg(m_fileName));
                    messageManager->printToOutputPane(
                                tr("File '%1' does not exist or is not readable.").arg(mainFilePath), true);
                }
            }
        }
        m_rootNode->refresh();
    }

    if (options & Configuration) {
        // update configuration
    }

    if (options & Files)
        emit fileListChanged();
}

void Project::refresh(RefreshOptions options)
{
    parseProject(options);

    if (options & Files)
        m_rootNode->refresh();

    QmlJS::ModelManagerInterface::ProjectInfo pinfo(this);
    pinfo.sourceFiles = files();
    pinfo.importPaths = importPaths();
    QtSupport::BaseQtVersion *version = 0;
    if (activeTarget()) {
        if (RunConfiguration *rc = qobject_cast<RunConfiguration *>(activeTarget()->activeRunConfiguration()))
            version = rc->qtVersion();
        QList<PE::ToolChain *> tcList;
        if (version && !version->qtAbis().isEmpty())
              tcList = PE::ToolChainManager::instance()->findToolChains(version->qtAbis().at(0));
        if (!tcList.isEmpty())
            QtSupport::QmlDumpTool::pathAndEnvironment(this, version, tcList.first(), false, &pinfo.qmlDumpPath, &pinfo.qmlDumpEnvironment);
    }
    if (version) {
        pinfo.tryQmlDump = true;
        pinfo.qtImportsPath = version->versionInfo().value("QT_INSTALL_IMPORTS");
        pinfo.qtVersionString = version->qtVersionString();
    }
    m_modelManager->updateProjectInfo(pinfo);
}

QStringList Project::convertToAbsoluteFiles(const QStringList &paths) const
{
    const QDir projectDir(QFileInfo(m_fileName).dir());
    QStringList absolutePaths;
    foreach (const QString &file, paths) {
        QFileInfo fileInfo(projectDir, file);
        absolutePaths.append(fileInfo.absoluteFilePath());
    }
    absolutePaths.removeDuplicates();
    return absolutePaths;
}

QStringList Project::files() const
{
    QStringList files;
    if (m_projectItem) {
        files = m_projectItem.data()->files();
    } else {
        files = m_files;
    }
    return files;
}

QString Project::mainFile() const
{
    if (m_projectItem)
        return m_projectItem.data()->mainFile();
    return QString();
}

bool Project::validProjectFile() const
{
    return !m_projectItem.isNull();
}

QStringList Project::importPaths() const
{
    QStringList importPaths;
    if (m_projectItem)
        importPaths = m_projectItem.data()->importPaths();

    // add the default import path for the target Qt version
    if (activeTarget()) {
        const RunConfiguration *runConfig =
                qobject_cast<RunConfiguration*>(activeTarget()->activeRunConfiguration());
        if (runConfig) {
            const QtSupport::BaseQtVersion *qtVersion = runConfig->qtVersion();
            if (qtVersion && qtVersion->isValid()) {
                const QString qtVersionImportPath = qtVersion->versionInfo().value("QT_INSTALL_IMPORTS");
                if (!qtVersionImportPath.isEmpty())
                    importPaths += qtVersionImportPath;
            }
        }
    }

    return importPaths;
}

bool Project::addFiles(const QStringList &filePaths)
{
    QStringList toAdd;
    foreach (const QString &filePath, filePaths) {
        if (!m_projectItem.data()->matchesFile(filePath))
            toAdd << filePaths;
    }
    return toAdd.isEmpty();
}

void Project::renameMainFile(const QString &newFileName)
{
    m_projectItem.data()->setMainFile(newFileName);
    //! TODO: ensure that file name in "*.pysideproject" file changed
}

void Project::refreshProjectFile()
{
    refresh(Project::ProjectFile | Files);
}

void Project::refreshFiles(const QSet<QString> &/*added*/, const QSet<QString> &removed)
{
    refresh(Files);
    if (!removed.isEmpty())
        m_modelManager->removeFiles(removed.toList());
}

QString Project::displayName() const
{
    return m_projectName;
}

Core::Id Project::id() const
{
    return Core::Id(Constants::PROJECT_ID);
}

Core::IDocument *Project::document() const
{
    return m_file;
}

PE::IProjectManager *Project::projectManager() const
{
    return m_manager;
}

QList<PE::BuildConfigWidget*> Project::subConfigWidgets()
{
    return QList<PE::BuildConfigWidget*>();
}

PE::Target *Project::activeTarget() const
{
    return PE::Project::activeTarget();
}

PE::ProjectNode *Project::rootProjectNode() const
{
    return m_rootNode.data();
}

QStringList Project::files(FilesMode) const
{
    return files();
}

bool Project::fromMap(const QVariantMap &map)
{
    if (!PE::Project::fromMap(map))
        return false;

    if (targets().isEmpty()) {
        addTarget(createTarget(PE::ProfileManager::instance()->defaultProfile()));
    }

    refresh(Everything);
    // FIXME workaround to guarantee that run/debug actions are enabled if a valid file exists
    if (activeTarget()) {
        RunConfiguration *runConfig = qobject_cast<RunConfiguration*>(activeTarget()->activeRunConfiguration());
        if (runConfig)
            runConfig->changeCurrentFile(0);
    }

    return true;
}

} // namespace PythonProjectManager

