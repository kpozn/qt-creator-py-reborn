/**************************************************************************
**
** This file is part of Qt Creator
**
** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**************************************************************************/

#ifndef QMLPROJECT_H
#define QMLPROJECT_H

#include "pluginglobal.h"

#include <projectexplorer/project.h>

#include <QDeclarativeEngine>
#include <QScopedPointer>

namespace QmlJS {
class ModelManagerInterface;
}

namespace Utils {
class FileSystemWatcher;
}

namespace PythonProjectManager {

class PythonProjectItem;

namespace Internal {
class Manager;
class QmlProjectFile;
class QmlProjectNode;
} // namespace Internal

class PYTHONPROJECTMANAGER_EXPORT Project : public ProjectExplorer::Project
{
    Q_OBJECT

public:
    Project(Internal::Manager *manager, const QString &filename);
    virtual ~Project();

    QString filesFileName() const;

    QString displayName() const;
    Core::Id id() const;
    Core::IDocument *document() const;
    ProjectExplorer::IProjectManager *projectManager() const;
    ProjectExplorer::Target *activeTarget() const;

    QList<ProjectExplorer::BuildConfigWidget*> subConfigWidgets();

    ProjectExplorer::ProjectNode *rootProjectNode() const;
    QStringList files(FilesMode fileMode) const;

    bool validProjectFile() const;

    enum RefreshOption {
        ProjectFile   = 0x01,
        Files         = 0x02,
        Configuration = 0x04,
        Everything    = ProjectFile | Files | Configuration
    };
    Q_DECLARE_FLAGS(RefreshOptions,RefreshOption)

    void refresh(RefreshOptions options);

    QDir projectDir() const;
    QStringList files() const;
    QString mainFile() const;
    QStringList importPaths() const;

    bool addFiles(const QStringList &filePaths);
    void renameMainFile(const QString &newFileName);

    void refreshProjectFile();

private slots:
    void refreshFiles(const QSet<QString> &added, const QSet<QString> &removed);

protected:
    bool fromMap(const QVariantMap &map);

private:
    // plain format
    void parseProject(RefreshOptions options);
    QStringList convertToAbsoluteFiles(const QStringList &paths) const;

    Internal::Manager *m_manager;
    QString m_fileName;
    QString m_projectName;
    QmlJS::ModelManagerInterface *m_modelManager;

    // plain format
    QStringList m_files;

    // qml based, new format
    QDeclarativeEngine m_engine;
    QWeakPointer<PythonProjectItem> m_projectItem;

    Internal::QmlProjectFile *m_file;
    QScopedPointer<Internal::QmlProjectNode> m_rootNode;
};

} // namespace PythonProjectManager

Q_DECLARE_OPERATORS_FOR_FLAGS(PythonProjectManager::Project::RefreshOptions)

#endif // QMLPROJECT_H
