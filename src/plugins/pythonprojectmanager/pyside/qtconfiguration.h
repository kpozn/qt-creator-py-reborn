#ifndef PYTHONPROJECTMANAGER_QTCONFIGURATION_H
#define PYTHONPROJECTMANAGER_QTCONFIGURATION_H

#include <QString>

namespace PythonProjectManager {

class IResourceCompiler
{
public:
    virtual ~IResourceCompiler() { }
    virtual bool resourceCompilerFound() const = 0;
    virtual void compileResourceFile(const QString &filePath) = 0;
};

class IUICompiler
{
public:
    virtual ~IUICompiler() { }
    virtual bool uiCompilerFound() const = 0;
    virtual void compileUIFile(const QString &filePath) = 0;
};

class ILanguageTool
{
public:
    virtual ~ILanguageTool() { }
    virtual bool languageToolFound() const = 0;
};

//class QtConfiguration :
//        public IResourceCompiler,
//        public IUICompiler,
//        public ILanguageTool
//{
//public:
//    QtConfiguration();
//    ~QtConfiguration();

//    bool resourceCompilerFound() const;
//    void compileResourceFile(const QString &filePath);

//    bool uiCompilerFound() const;
//    void compileUIFile(const QString &filePath);

//    bool languageToolFound() const;

//private:
//    QString m_resourceCompilerString;
//    QString m_uiCompilerString;
//    QString m_languageToolString;
//};

} // namespace PythonProjectManager

#endif // PYTHONPROJECTMANAGER_QTCONFIGURATION_H
