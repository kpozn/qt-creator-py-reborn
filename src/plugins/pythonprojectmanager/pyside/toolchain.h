#ifndef PYTHONPROJECTMANAGER_TOOLCHAIN_H
#define PYTHONPROJECTMANAGER_TOOLCHAIN_H

#include "../pluginglobal.h"
#include <utils/fileutils.h>
#include <utils/environment.h>

/**
 * Based on ProjectExplorer::ToolChain
 *
 * See documentation inside cpp file
 */

namespace PYTHONPROJECTMANAGER_EXPORT PythonProjectManager {

class IOutputParser;
class ToolChainConfigWidget;
class ToolChainFactory;
class ToolChainManager;

namespace Internal { class ToolChainPrivate; }

//class ToolChain
//{
//public:
//    virtual ~ToolChain();

//    QString displayName() const;
//    void setDisplayName(const QString &name);

//    bool isAutoDetected() const;
//    QString id() const;

//    virtual Utils::FileName suggestedDebugger();

//    virtual QString type() const = 0;
//    virtual QString typeDisplayName() const = 0;

//    virtual bool isValid() const = 0;

//    enum Flags {
//        NO_FLAGS = 0,
//        PYTHON_3000 = 0x1,
//        RESTRICTED_PYTHON = 0x2
//    };
//    virtual Flags compilerFlags(const QStringList &flags) const = 0;
//    virtual QList<QString> systemImportPaths() const = 0;
//    virtual void addToEnvironment(Utils::Environment &env) const = 0;
//    virtual QString makeCommand() const = 0;

//    virtual Utils::FileName compilerCommand() const = 0;
//    virtual IOutputParser *outputParser() const = 0;

//    virtual bool operator ==(const ToolChain &) const;

//    virtual ToolChainConfigWidget *configurationWidget() = 0;
//    virtual bool canClone() const;
//    virtual ToolChain *clone() const = 0;

//    // Used by the toolchainmanager to save user-generated tool chains.
//    // Make sure to call this method when deriving!
//    virtual QVariantMap toMap() const;

//protected:
//    ToolChain(const QString &id, bool autoDetect);
//    explicit ToolChain(const ToolChain &);

//    void toolChainUpdated();

//    // Make sure to call this method when deriving!
//    virtual bool fromMap(const QVariantMap &data);

//private:
//    void setAutoDetected(bool);

//    Internal::ToolChainPrivate *const d;

//    friend class ToolChainManager;
//    friend class ToolChainFactory;
//};

} // namespace PythonProjectManager

#endif // PYTHONPROJECTMANAGER_TOOLCHAIN_H
