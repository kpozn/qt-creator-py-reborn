TEMPLATE = lib
TARGET = PythonProjectManager

QT += network
greaterThan(QT_MAJOR_VERSION, 4) {
    QT += quick1
} else {
    QT += declarative
}

include(../../qtcreatorplugin.pri)
include(../../libs/pylang/pylang.pri)
include(pythonprojectmanager_dependencies.pri)
include(fileformat/fileformat.pri)

DEFINES += PYTHONPROJECTMANAGER_LIBRARY

HEADERS += \
    runconfigurationwidget.h \
    runcontrol.h \
    target.h \
    runconfigurationfactory.h \
    runconfiguration.h \
    projectnodes.h \
    project.h \
    constants.h \
    plugin.h \
    pluginglobal.h \
    projectmanager.h \
    applicationwizard.h \
    document.h \
    pyside/qtconfiguration.h \
    pyside/toolchain.h

SOURCES += \
    runcontrol.cpp \
    target.cpp \
    runconfigurationwidget.cpp \
    runconfigurationfactory.cpp \
    runconfiguration.cpp \
    project.cpp \
    applicationwizard.cpp \
    document.cpp \
    plugin.cpp \
    projectmanager.cpp \
    projectnodes.cpp \
    pyside/qtconfiguration.cpp \
    pyside/toolchain.cpp

RESOURCES += pythonproject.qrc

OTHER_FILES += PythonProject.mimetypes.xml \
    PythonProjectManager.pluginspec.in
