/**************************************************************************
**
** This file is part of Qt Creator
**
** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**************************************************************************/

#include "runconfiguration.h"
#include "project.h"
#include "constants.h"
#include "target.h"
#include "runconfigurationwidget.h"
#include <coreplugin/mimedatabase.h>
#include <coreplugin/editormanager/editormanager.h>
#include <coreplugin/editormanager/ieditor.h>
#include <coreplugin/icore.h>
#include <utils/qtcassert.h>
#include <utils/qtcprocess.h>
#include <qtsupport/qtversionmanager.h>
#include <qtsupport/qtoutputformatter.h>
#include <qtsupport/qtsupportconstants.h>
#include <projectexplorer/target.h>

#ifdef Q_OS_WIN
#include <utils/winutils.h>
#endif

using Core::EditorManager;
using Core::ICore;
using Core::IEditor;
using QtSupport::QtVersionManager;

using namespace PythonProjectManager::Internal;

namespace PythonProjectManager {

const char * const M_CURRENT_FILE = "CurrentFile";

/**
 * \class RunConfiguration
 * \brief Prepares command, it's arguments and enviroment variables
 * to run python script.
 */

RunConfiguration::RunConfiguration(ProjectExplorer::Target *parent) :
    ProjectExplorer::RunConfiguration(parent, Core::Id(Constants::QML_RC_ID)),
    m_qtVersionId(-1),
    m_scriptFile(M_CURRENT_FILE),
    m_projectTarget(parent),
    m_isEnabled(false)
{
    ctor();
    updateQtVersions();
}

RunConfiguration::RunConfiguration(ProjectExplorer::Target *parent,
                                   RunConfiguration *source) :
    ProjectExplorer::RunConfiguration(parent, source),
    m_qtVersionId(source->m_qtVersionId),
    m_scriptFile(source->m_scriptFile),
    m_qmlViewerArgs(source->m_qmlViewerArgs),
    m_projectTarget(parent),
    m_isEnabled(source->m_isEnabled),
    m_userEnvironmentChanges(source->m_userEnvironmentChanges)
{
    ctor();
    updateQtVersions();
}

bool RunConfiguration::isEnabled() const
{
    return m_isEnabled;
}

QString RunConfiguration::disabledReason() const
{
    if (!m_isEnabled)
        return tr("No qmlviewer or qmlobserver found.");
    return QString();
}

void RunConfiguration::ctor()
{

    // reset default settings in constructor
    debuggerAspect()->setUseCppDebugger(false);
    debuggerAspect()->setUseQmlDebugger(true);

    EditorManager *em = Core::EditorManager::instance();
    connect(em, SIGNAL(currentEditorChanged(Core::IEditor*)),
            this, SLOT(changeCurrentFile(Core::IEditor*)));

    connect(target(), SIGNAL(profileChanged()),
            this, SLOT(updateEnabled()));

    setDisplayName(tr(Constants::PYTHON_LAUNCHER_TARGET_DISPLAY_NAME,
                      Constants::PYTHON_LAUNCHER_TARGET_ANNOTATION));
}

RunConfiguration::~RunConfiguration()
{
}

QString RunConfiguration::interpreterPath() const
{
    // TODO: add normal python binaries path recognition
    return QLatin1String("/usr/bin/python");
#if 0
    QtSupport::BaseQtVersion *version = qtVersion();
    if (!version)
        return QString();
    else
        return version->qmlviewerCommand();
#endif
}

QString RunConfiguration::observerPath() const
{
    QtSupport::BaseQtVersion *version = qtVersion();
    if (!version)
        return QString();
    else
    {
        if (!version->needsQmlDebuggingLibrary())
            return version->qmlviewerCommand();
        return version->qmlObserverTool();
    }
}

QString RunConfiguration::interpreterArguments() const
{
    // arguments in .user file
#if 0
    QString args = m_qmlViewerArgs;
#else
    QString args;
#endif

    QString s = mainScript();
    if (!s.isEmpty()) {
        s = canonicalCapsPath(s);
        Utils::QtcProcess::addArg(&args, s);
    }
    return args;
}

QString RunConfiguration::workingDirectory() const
{
    QFileInfo projectFile(target()->project()->document()->fileName());
    return canonicalCapsPath(projectFile.absolutePath());
}

int RunConfiguration::qtVersionId() const
{
    return m_qtVersionId;
}

/* QtDeclarative checks explicitly that the capitalization for any URL / path
   is exactly like the capitalization on disk.*/
QString RunConfiguration::canonicalCapsPath(const QString &fileName)
{
    QString canonicalPath = QFileInfo(fileName).canonicalFilePath();

#if defined(Q_OS_WIN)
    canonicalPath = Utils::normalizePathName(canonicalPath);
#endif

    return canonicalPath;
}


QtSupport::BaseQtVersion *RunConfiguration::qtVersion() const
{
    if (m_qtVersionId == -1)
        return 0;

    QtSupport::QtVersionManager *versionManager = QtSupport::QtVersionManager::instance();
    QtSupport::BaseQtVersion *version = versionManager->version(m_qtVersionId);
    QTC_ASSERT(version, return 0);

    return version;
}

QWidget *RunConfiguration::createConfigurationWidget()
{
    QTC_ASSERT(m_configurationWidget.isNull(), return m_configurationWidget.data());
    m_configurationWidget = new QmlProjectRunConfigurationWidget(this);
    return m_configurationWidget.data();
}

Utils::OutputFormatter *RunConfiguration::createOutputFormatter() const
{
    return new QtSupport::QtOutputFormatter(target()->project());
}

RunConfiguration::MainScriptSource RunConfiguration::mainScriptSource() const
{
    Project *project = qobject_cast<Project *>(target()->project());
    if (!project)
        return FileInEditor;
    if (!project->mainFile().isEmpty())
        return FileInProjectFile;
    if (!m_mainScriptFilename.isEmpty())
        return FileInSettings;
    return FileInEditor;
}

/**
  Returns absolute path to main script file.
  */
QString RunConfiguration::mainScript() const
{
    Project *project = qobject_cast<Project *>(target()->project());
    if (!project)
        return m_currentFileFilename;
    if (!project->mainFile().isEmpty()) {
        const QString pathInProject = project->mainFile();
        if (QFileInfo(pathInProject).isAbsolute())
            return pathInProject;
        else
            return project->projectDir().absoluteFilePath(pathInProject);
    }

    if (!m_mainScriptFilename.isEmpty())
        return m_mainScriptFilename;
    return QLatin1String("main.py");
}

void RunConfiguration::setScriptSource(MainScriptSource source,
                                       const QString &settingsPath)
{
    if (source == FileInEditor) {
        m_scriptFile = M_CURRENT_FILE;
        m_mainScriptFilename.clear();
    } else if (source == FileInProjectFile) {
        m_scriptFile.clear();
        m_mainScriptFilename.clear();
    } else { // FileInSettings
        m_scriptFile = settingsPath;
        m_mainScriptFilename
                = target()->project()->projectDirectory() + QLatin1Char('/') + m_scriptFile;
    }
    updateEnabled();
    if (m_configurationWidget)
        m_configurationWidget.data()->updateFileComboBox();
}

Utils::Environment RunConfiguration::environment() const
{
    Utils::Environment env = baseEnvironment();
    env.modify(userEnvironmentChanges());

    // import paths from .pysideproject file
    Project *myProject = qobject_cast<Project*>(target()->project());

    QTC_ASSERT(myProject, return env);

    foreach (const QString &importPath, myProject->importPaths())
    {
        env.appendOrSet(Constants::C_PYTHONPATH, importPath, QLatin1String(":"));
    }

    return env;
}

ProjectExplorer::Abi RunConfiguration::abi() const
{
    ProjectExplorer::Abi hostAbi = ProjectExplorer::Abi::hostAbi();
    return ProjectExplorer::Abi(hostAbi.architecture(), hostAbi.os(), hostAbi.osFlavor(),
                                ProjectExplorer::Abi::RuntimeQmlFormat, hostAbi.wordWidth());
}

QVariantMap RunConfiguration::toMap() const
{
    QVariantMap map(ProjectExplorer::RunConfiguration::toMap());

    map.insert(QLatin1String(Constants::QML_VIEWER_QT_KEY), m_qtVersionId);
    map.insert(QLatin1String(Constants::PYTHON_LAUNCHER_ARGUMENTS_KEY), m_qmlViewerArgs);
    map.insert(QLatin1String(Constants::MAINSCRIPT_KEY),  m_scriptFile);
    map.insert(QLatin1String(Constants::USER_ENVIRONMENT_CHANGES_KEY),
               Utils::EnvironmentItem::toStringList(m_userEnvironmentChanges));
    return map;
}

bool RunConfiguration::fromMap(const QVariantMap &map)
{
    m_qmlViewerArgs = map.value(QLatin1String(Constants::PYTHON_LAUNCHER_ARGUMENTS_KEY)).toString();
    m_scriptFile = map.value(QLatin1String(Constants::MAINSCRIPT_KEY), M_CURRENT_FILE).toString();
    m_userEnvironmentChanges = Utils::EnvironmentItem::fromStringList(
                map.value(QLatin1String(Constants::USER_ENVIRONMENT_CHANGES_KEY)).toStringList());


    if (m_scriptFile == M_CURRENT_FILE) {
        setScriptSource(FileInEditor);
    } else if (m_scriptFile.isEmpty()) {
        setScriptSource(FileInProjectFile);
    } else {
        setScriptSource(FileInSettings, m_scriptFile);
    }

    return ProjectExplorer::RunConfiguration::fromMap(map);
}

void RunConfiguration::changeCurrentFile(Core::IEditor *editor)
{
    if (editor) {
        m_currentFileFilename = editor->document()->fileName();
    }
    updateEnabled();
}

void RunConfiguration::updateEnabled()
{
    bool qmlFileFound = false;
    if (mainScriptSource() == FileInEditor) {
        Core::IEditor *editor = Core::EditorManager::instance()->currentEditor();
        Core::MimeDatabase *db = ICore::mimeDatabase();
        if (editor) {
            m_currentFileFilename = editor->document()->fileName();
            if (db->findByFile(mainScript()).type() == QLatin1String("application/x-qml"))
                qmlFileFound = true;
        }
        if (!editor || (db->findByFile(mainScript()).type() == Constants::PROJECT_MIMETYPE))
        {
            // find a qml file with lowercase filename. This is slow, but only done
            // in initialization/other border cases.
            foreach(const QString &filename, m_projectTarget->project()->files(ProjectExplorer::Project::AllFiles)) {
                const QFileInfo fi(filename);

                if (!filename.isEmpty() && fi.baseName()[0].isLower()
                        && db->findByFile(fi).type() == QLatin1String("application/x-qml"))
                {
                    m_currentFileFilename = filename;
                    qmlFileFound = true;
                    break;
                }

            }
        }
    } else { // use default one
        qmlFileFound = !mainScript().isEmpty();
    }

    bool newValue = (QFileInfo(interpreterPath()).exists()
                     || QFileInfo(observerPath()).exists()) && qmlFileFound;


    // Always emit change signal to force reevaluation of run/debug buttons
    m_isEnabled = newValue;
    emit enabledChanged();
}

void RunConfiguration::updateQtVersions()
{
    QtVersionManager *qtVersions = QtVersionManager::instance();

    //
    // update m_qtVersionId
    //
    if (!qtVersions->isValidId(m_qtVersionId)
            || !isValidVersion(qtVersions->version(m_qtVersionId))) {
        int newVersionId = -1;
        // take first one you find
        foreach (QtSupport::BaseQtVersion *version, qtVersions->validVersions()) {
            if (isValidVersion(version)) {
                newVersionId = version->uniqueId();
                break;
            }
        }
    }

    updateEnabled();
}

bool RunConfiguration::isValidVersion(QtSupport::BaseQtVersion *version)
{
    if (version
            && (version->type() == QtSupport::Constants::DESKTOPQT
                || version->type() == QtSupport::Constants::SIMULATORQT)
            && !version->qmlviewerCommand().isEmpty()) {
        return true;
    }
    return false;
}

Utils::Environment RunConfiguration::baseEnvironment() const
{
    Utils::Environment env = Utils::Environment::systemEnvironment();
    if (qtVersion())
        env = qtVersion()->qmlToolsEnvironment();
    return env;
}

void RunConfiguration::setUserEnvironmentChanges(const QList<Utils::EnvironmentItem> &diff)
{
    if (m_userEnvironmentChanges != diff) {
        m_userEnvironmentChanges = diff;
        if (m_configurationWidget)
            m_configurationWidget.data()->userEnvironmentChangesChanged();
    }
}

QList<Utils::EnvironmentItem> RunConfiguration::userEnvironmentChanges() const
{
    return m_userEnvironmentChanges;
}

} // namespace PythonProjectManager
