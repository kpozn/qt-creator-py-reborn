/**************************************************************************
**
** This file is part of Qt Creator
**
** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**************************************************************************/

#include "runcontrol.h"
#include "runconfiguration.h"
#include "plugin.h"
#include <coreplugin/icore.h>
#include <projectexplorer/projectexplorerconstants.h>
#include <projectexplorer/target.h>
#include <projectexplorer/project.h>
#include <projectexplorer/projectexplorer.h>
#include <utils/qtcassert.h>

#include <debugger/debuggerrunner.h>
#include <debugger/debuggerplugin.h>
#include <debugger/debuggerconstants.h>
#include <debugger/debuggerstartparameters.h>
#include <qtsupport/baseqtversion.h>
#include <qtsupport/qmlobservertool.h>

namespace PE = ProjectExplorer;

namespace PythonProjectManager {

namespace Internal {

ProjectRunControl::ProjectRunControl(RunConfiguration *runConfiguration,
                                           PE::RunMode mode)
    : PE::RunControl(runConfiguration, mode)
{
    Utils::Environment env = runConfiguration->environment();
    m_applicationLauncher.setEnvironment(env);
    m_applicationLauncher.setWorkingDirectory(runConfiguration->workingDirectory());

    if (mode == PE::NormalRunMode) {
        m_executable = runConfiguration->interpreterPath();
    } else {
        m_executable = runConfiguration->observerPath();
    }
    m_commandLineArguments = runConfiguration->interpreterArguments();
    m_mainScriptFile = runConfiguration->mainScript();

    connect(&m_applicationLauncher, SIGNAL(appendMessage(QString,Utils::OutputFormat)),
            this, SLOT(slotAppendMessage(QString,Utils::OutputFormat)));
    connect(&m_applicationLauncher, SIGNAL(processExited(int)),
            this, SLOT(processExited(int)));
    connect(&m_applicationLauncher, SIGNAL(bringToForegroundRequested(qint64)),
            this, SLOT(slotBringApplicationToForeground(qint64)));
}

ProjectRunControl::~ProjectRunControl()
{
    stop();
}

void ProjectRunControl::start()
{
    m_applicationLauncher.start(PE::ApplicationLauncher::Gui, m_executable,
                                m_commandLineArguments);
    setApplicationProcessHandle(PE::ProcessHandle(m_applicationLauncher.applicationPID()));
    emit started();
    QString msg = tr("Starting %1 %2\n")
        .arg(QDir::toNativeSeparators(m_executable), m_commandLineArguments);
    appendMessage(msg, Utils::NormalMessageFormat);
}

PE::RunControl::StopResult ProjectRunControl::stop()
{
    m_applicationLauncher.stop();
    return StoppedSynchronously;
}

bool ProjectRunControl::isRunning() const
{
    return m_applicationLauncher.isRunning();
}

QIcon ProjectRunControl::icon() const
{
    return QIcon(PE::Constants::ICON_RUN_SMALL);
}

void ProjectRunControl::slotBringApplicationToForeground(qint64 pid)
{
    bringApplicationToForeground(pid);
}

void ProjectRunControl::slotAppendMessage(const QString &line, Utils::OutputFormat format)
{
    appendMessage(line, format);
}

void ProjectRunControl::processExited(int exitCode)
{
    QString msg = tr("%1 exited with code %2\n")
        .arg(QDir::toNativeSeparators(m_executable)).arg(exitCode);
    appendMessage(msg, exitCode ? Utils::ErrorMessageFormat : Utils::NormalMessageFormat);
    emit finished();
}

QString ProjectRunControl::mainQmlFile() const
{
    return m_mainScriptFile;
}

QmlProjectRunControlFactory::QmlProjectRunControlFactory(QObject *parent)
    : IRunControlFactory(parent)
{
}

QmlProjectRunControlFactory::~QmlProjectRunControlFactory()
{
}

bool QmlProjectRunControlFactory::canRun(
        PE::RunConfiguration *runConfiguration,
        PE::RunMode mode) const
{
    RunConfiguration *config =
        qobject_cast<RunConfiguration*>(runConfiguration);
    if (!config)
        return false;
    if (mode == PE::NormalRunMode)
        return !config->interpreterPath().isEmpty();
    if (mode != PE::DebugRunMode)
        return false;

    if (!Debugger::DebuggerPlugin::isActiveDebugLanguage(Debugger::QmlLanguage))
        return false;

    if (!config->observerPath().isEmpty())
        return true;
    if (!config->qtVersion())
        return false;
    if (!config->qtVersion()->needsQmlDebuggingLibrary())
        return true;
    if (QtSupport::QmlObserverTool::canBuild(config->qtVersion()))
        return true;
    return false;
}

PE::RunControl *QmlProjectRunControlFactory::create(
        PE::RunConfiguration *runConfiguration,
        PE::RunMode mode)
{
    QTC_ASSERT(canRun(runConfiguration, mode), return 0);
    RunConfiguration *config = qobject_cast<RunConfiguration *>(runConfiguration);

    QList<PE::RunControl *> runcontrols =
            PE::ProjectExplorerPlugin::instance()->runControls();
    foreach (PE::RunControl *rc, runcontrols) {
        if (ProjectRunControl *qrc = qobject_cast<ProjectRunControl *>(rc)) {
            if (qrc->mainQmlFile() == config->mainScript())
                // Asking the user defeats the purpose
                // Making it configureable might be worth it
                qrc->stop();
        }
    }

    PE::RunControl *runControl = 0;
    if (mode == PE::NormalRunMode)
        runControl = new ProjectRunControl(config, mode);
    else if (mode == PE::DebugRunMode)
        runControl = createDebugRunControl(config);
    return runControl;
}

QString QmlProjectRunControlFactory::displayName() const
{
    return tr("Run");
}

PE::RunConfigWidget *QmlProjectRunControlFactory::createConfigurationWidget(
        PE::RunConfiguration *runConfiguration)
{
    Q_UNUSED(runConfiguration)
    return 0;
}

PE::RunControl *QmlProjectRunControlFactory::createDebugRunControl(RunConfiguration *runConfig)
{
    Debugger::DebuggerStartParameters params;
    params.startMode = Debugger::StartInternal;
    params.executable = runConfig->observerPath();
    params.qmlServerAddress = "127.0.0.1";
    params.qmlServerPort = runConfig->debuggerAspect()->qmlDebugServerPort();
    params.processArgs = QString("-qmljsdebugger=port:%1,block").arg(params.qmlServerPort);
    params.processArgs += QLatin1Char(' ') + runConfig->interpreterArguments();
    params.workingDirectory = runConfig->workingDirectory();
    params.environment = runConfig->environment();
    params.displayName = runConfig->displayName();
    params.projectSourceDirectory = runConfig->target()->project()->projectDirectory();
    params.projectSourceFiles = runConfig->target()->project()->files(PE::Project::ExcludeGeneratedFiles);
    if (runConfig->debuggerAspect()->useQmlDebugger())
        params.languages |= Debugger::QmlLanguage;
    if (runConfig->debuggerAspect()->useCppDebugger())
        params.languages |= Debugger::CppLanguage;

    if (!runConfig->qtVersion()->qtAbis().isEmpty())
        params.toolChainAbi = runConfig->qtVersion()->qtAbis().first();

    // Makes sure that all bindings go through the JavaScript engine, so that
    // breakpoints are actually hit!
    const QString optimizerKey = QLatin1String("QML_DISABLE_OPTIMIZER");
    if (!params.environment.hasKey(optimizerKey)) {
        params.environment.set(optimizerKey, QLatin1String("1"));
    }

    if (params.executable.isEmpty()) {
        Plugin::showQmlObserverToolWarning();
        return 0;
    }

    return Debugger::DebuggerPlugin::createDebugger(params, runConfig);
}

} // namespace Internal
} // namespace PythonProjectManager
