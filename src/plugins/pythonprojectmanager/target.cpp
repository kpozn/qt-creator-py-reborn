///**************************************************************************
//**
//** This file is part of Qt Creator
//**
//** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
//**
//** Contact: Nokia Corporation (qt-info@nokia.com)
//**
//**
//** GNU Lesser General Public License Usage
//**
//** This file may be used under the terms of the GNU Lesser General Public
//** License version 2.1 as published by the Free Software Foundation and
//** appearing in the file LICENSE.LGPL included in the packaging of this file.
//** Please review the following information to ensure the GNU Lesser General
//** Public License version 2.1 requirements will be met:
//** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
//**
//** In addition, as a special exception, Nokia gives you certain additional
//** rights. These rights are described in the Nokia Qt LGPL Exception
//** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
//**
//** Other Usage
//**
//** Alternatively, this file may be used in accordance with the terms and
//** conditions contained in a signed written agreement between you and Nokia.
//**
//** If you have questions regarding the use of this file, please contact
//** Nokia at qt-info@nokia.com.
//**
//**************************************************************************/

//#include "target.h"

//#include "project.h"
//#include "constants.h"
//#include "runconfiguration.h"

//#include <QDebug>
//#include <QApplication>
//#include <QStyle>

//namespace PythonProjectManager {
//namespace Internal {

//Target::Target(Project *parent) :
//    ProjectExplorer::Target(parent, QLatin1String(Constants::PYTHON_LAUNCHER_TARGET_ID))
//{
//    setDisplayName(QApplication::translate(Constants::PYTHON_LAUNCHER_TARGET_ID,
//                                           Constants::PYTHON_LAUNCHER_TARGET_DISPLAY_NAME,
//                                           Constants::PYTHON_LAUNCHER_TARGET_ANNOTATION));
//    setIcon(qApp->style()->standardIcon(QStyle::SP_ComputerIcon));
//}

//Target::~Target()
//{
//}

//ProjectExplorer::BuildConfigWidget *Target::createConfigWidget()
//{
//    return 0;
//}

//Project *Target::qmlProject() const
//{
//    return static_cast<Project *>(project());
//}

//ProjectExplorer::IBuildConfigurationFactory *Target::buildConfigurationFactory(void) const
//{
//    return 0;
//}

//bool Target::fromMap(const QVariantMap &map)
//{
//    if (!ProjectExplorer::Target::fromMap(map))
//        return false;

//    if (runConfigurations().isEmpty()) {
//        qWarning() << "Failed to restore run configuration of QML project!";
//        return false;
//    }

//    setDisplayName(QApplication::translate(Constants::PYTHON_LAUNCHER_TARGET_ID,
//                                           Constants::PYTHON_LAUNCHER_TARGET_DISPLAY_NAME,
//                                           Constants::PYTHON_LAUNCHER_TARGET_ANNOTATION));

//    return true;
//}

//TargetFactory::TargetFactory(QObject *parent) :
//    ITargetFactory(parent)
//{
//}

//TargetFactory::~TargetFactory()
//{
//}

//bool TargetFactory::supportsTargetId(const QString &id) const
//{
//    return id == QLatin1String(Constants::PYTHON_LAUNCHER_TARGET_ID);
//}

//QStringList TargetFactory::supportedTargetIds() const
//{
//    return QStringList() << QLatin1String(Constants::PYTHON_LAUNCHER_TARGET_ID);
//}

//QString TargetFactory::displayNameForId(const QString &id) const
//{
//    if (id == QLatin1String(Constants::PYTHON_LAUNCHER_TARGET_ID))
//        return QCoreApplication::translate(Constants::PYTHON_LAUNCHER_TARGET_ID,
//                                           Constants::PYTHON_LAUNCHER_TARGET_DISPLAY_NAME,
//                                           Constants::PYTHON_LAUNCHER_TARGET_ANNOTATION);
//    return QString();
//}

//bool TargetFactory::canCreate(ProjectExplorer::Project *parent, const QString &id) const
//{
//    if (!qobject_cast<Project *>(parent))
//        return false;
//    return id == QLatin1String(Constants::PYTHON_LAUNCHER_TARGET_ID);
//}

//Target *TargetFactory::create(ProjectExplorer::Project *parent, const QString &id)
//{
//    if (!canCreate(parent, id))
//        return 0;
//    Project *qmlproject(static_cast<Project *>(parent));
//    Target *target = new Target(qmlproject);

//    // Add RunConfiguration (QML does not have BuildConfigurations)
//    RunConfiguration *runConf = new RunConfiguration(target);
//    target->addRunConfiguration(runConf);

//    return target;
//}

//bool TargetFactory::canRestore(ProjectExplorer::Project *parent, const QVariantMap &map) const
//{
//    return canCreate(parent, ProjectExplorer::idFromMap(map));
//}

//Target *TargetFactory::restore(ProjectExplorer::Project *parent, const QVariantMap &map)
//{
//    if (!canRestore(parent, map))
//        return 0;
//    Project *qmlproject(static_cast<Project *>(parent));
//    Target *target(new Target(qmlproject));
//    if (target->fromMap(map))
//        return target;
//    delete target;
//    return 0;
//}

//} // namespace Internal
//} // namespace PythonProjectManager
